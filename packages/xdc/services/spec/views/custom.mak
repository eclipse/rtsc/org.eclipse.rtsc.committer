include $(XDCROOT)/packages/xdc/bld/xdc_java.mak

CLASSES  = $(patsubst %.java,%,$(wildcard *.java))
JARFILE  = java/package.jar

PKGNAME     = $(subst /,.,$(PKGDIR))

##JCPATH:=$(PKGROOT)$(PATHCHAR)$(ANTLR)

.libraries: $(JARFILE)
$(JARFILE): $(patsubst %,$(CLASSDIR)/%.class,$(CLASSES))

clean::
	$(RM) $(JARFILE)
