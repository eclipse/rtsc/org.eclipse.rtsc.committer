/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

public class Attr
    implements java.io.Serializable
{
	 
    // Static Serialization UID
    static final long serialVersionUID = 7469859191145684496L;
	
    Atom name;
    Expr val;
    
    final public static String A_Annex = "@Annex";
    final public static String A_CommandOption = "@CommandOption";
    final public static String A_CustomHeader = "@CustomHeader";
    final public static String A_DirectCall = "@DirectCall";
    final public static String A_Encoded = "@Encoded";
    final public static String A_Facet = "@Facet";
    final public static String A_Gated = "@Gated";
    final public static String A_GlobalNames = "@GlobalNames";
    final public static String A_InstanceFinalize = "@InstanceFinalize";
    final public static String A_InstanceInitError = "@InstanceInitError";
    final public static String A_Macro = "@Macro";
    final public static String A_ModuleStartup = "@ModuleStartup";
    final public static String A_NoRuntime = "@NoRuntime";
    final public static String A_Opaque = "@Opaque";
    final public static String A_Prefix = "@Prefix";
    final public static String A_Proxy = "@Proxy";
    final public static String A_System = "@System";
    final public static String A_TargetHeader = "@TargetHeader";
    final public static String A_Template = "@Template";
    final public static String A_XmlDtd = "@XmlDtd";

    Attr( Atom name, Expr val )
    {
        this.name = name;
        this.val = val;
    }
    
    // getName
    public final String getName()
    {
        return this.name.text;
    }
    
    // getVal
    public final Expr getVal()
    {
        return this.val;
    }
    
    // toText
    public String toText()
    {
        return this.name.text + ':' + (this.val == null ? "true" : Expr.toText(this.val));
    }
}
