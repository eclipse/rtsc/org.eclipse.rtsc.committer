include $(XDCROOT)/packages/xdc/bld/xdc_java.mak

# All schema files depend on jar files from xdc.services.intern.cmd,
# xdc.services.spec. xdc.services.global, xdc.services.intern.gen and
# xdc.services.intern.xsr. However, these packages must remove that dependency
# to avoid cycles.
XDCJARS:=

CLASSES  = $(patsubst %.java,%,$(wildcard *.java))
JARFILE  = java/package.jar

ANTLR   = $(XDCROOT)/packages/xdc/shelf/java/antlr.jar
RHINO	= $(XDCROOT)/packages/xdc/shelf/java/js.jar
ECLIPSE = $(XDCROOT)/packages/xdc/shelf/java/ecj.jar

JCPATH  := $(PKGROOT)$(PATHCHAR)$(ANTLR)$(PATHCHAR)$(RHINO)$(PATHCHAR)$(ECLIPSE)
JCOPTS  := -Xlint:unchecked

.libraries: $(JARFILE)
$(JARFILE): $(patsubst %,$(CLASSDIR)/%.class,$(CLASSES))

clean::
	$(RM) $(JARFILE)
