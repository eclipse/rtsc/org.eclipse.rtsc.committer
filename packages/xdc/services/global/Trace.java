/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== Trace.java ========
 *
 *! Revision History
 *! ================
 *! 09-Apr-2009 sasha   created
 */
package xdc.services.global;

import java.util.*;
import org.mozilla.javascript.*;
import xdc.services.intern.xsr.*;

/*
 *  ======== Trace ========
 *  API for using the trace functionality from Java
 *
 *  This class is equivalent to $trace() function in XDCscript. One
 *  significant difference is that the caller of $trace() can be
 *  determined from the name of the capsule to which $trace() is attached,
 *  while here the caller has to pass a reference to itself as a first
 *  parameter of a function call.
 */
public class Trace
{

    /*
     *  ======== print ========
     *  Print a trace statement, if the statement is enabled
     *  
     *  The call to this functioned is forwarded to utils._tracePrint,
     *  where the parameters of the call are compared with the currently
     *  enabled packages, capsules or groups.
     *
     *  @param object	Java object invoking this function
     *
     *  @param msg	trace message
     *
     *  @param level	verbosity level at which this trace statement is
     *			enabled
     *
     *  @groups		array of groups to which this trace statement belongs
     *
     */
    public static void print(Object o, String msg, int level, String groups[]) {

        Scriptable utils = (Scriptable)(Global.get("utils"));
        String cname = o.getClass().getName();
        String pkg = o.getClass().getPackage().getName();
        String path = cname.replace(".", "/") + ".java";

        ArrayList<String> lGroups =
            new ArrayList<String>(Arrays.asList(groups));
        if (!lGroups.contains("all")) {
            lGroups.add("all");
        }

        /* For String arrays, the conversion from Java String to JavaScript
         * String does not happen automatically.
         */
        Scriptable jsGroups = (Scriptable)
            (Context.javaToJS(lGroups.toArray(), Global.getTopScope()));
        
        Global.callFxn("_tracePrint", utils, msg, path, pkg, jsGroups, level);
    }

    /*
     *  ======== print ========
     *  Print a trace statement, if the statement is enabled
     *  
     *  The call to this functioned is eventually forwarded to
     *  utils._tracePrint, where the parameters of the call are compared
     *  with the currently enabled packages, capsules or groups. This
     *  trace statement does not belong to any groups, so it is only
     *  assigned to the group "all".
     *
     *  @param object	Java object invoking this function
     *
     *  @param msg	trace message
     *
     *  @param level	verbosity level at which this trace statement is
     *			enabled
     *
     */
    public static void print(Object o, String msg, int level) {
        String [] all = {"all"};
        print(o, msg, level, all);
    }

    /*
     *  ======== print ========
     *  Print a trace statement, if the statement is enabled
     *  
     *  The call to this functioned is eventually forwarded to
     *  utils._tracePrint, where the parameters of the call are compared
     *  with the currently enabled packages, capsules or groups. This
     *  trace statement does not belong to any groups, so it is only
     *  assigned to the group "all", and it is set to the default level 0.
     *
     *  @param object	Java object invoking this function
     *
     *  @param msg	trace message
     *
     */
    public static void print(Object o, String msg) {
        String [] all = {"all"};
        print(o, msg, 0, all);
    }

}