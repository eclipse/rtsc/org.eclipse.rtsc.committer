/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== Builder.java ========
 *
 *! Revision History
 *! ================
 *! 19-May-2008 dr      added empty rules for .vers files
 *! 04-Jan-2008 sasha	removed .vers files from .interfaces dependencies
 */

package xdc.services.intern.cmd;

import xdc.services.global.*;

import java.io.*;
import java.util.*;

import org.mozilla.javascript.WrappedException;

import xdc.services.intern.gen.*;
import xdc.services.spec.*;
import xdc.services.intern.xsr.Template;

public class Builder
{
    public static final int GREEN_VERS = 160;  // spec
    public static final int RED_VERS   = 170;  // schema
    public static final int BLUE_VERS  = 150;  // body
    public static final int CYAN_VERS  = 150;  // cdoc
    
    public static final int FLAG_WARNINGS = 0x1;
    public static final int FLAG_STRICT = 0x2;
    
    /*
     *  ======== main ========
     *  Process specified package spec files
     *
     *  This command generates meta domain schema, target
     *  domain module and interface headers, target domain package level
     *  headers and sources, and a package-level XML representation of the
     *  package specifications.
     *
     *  Usage:
     *      java xdc.services.intern.cmd.Build
     *                  [-m make_out] [-i include_out] pkgFile [-D dep ...]
     *
     *  Options:
     *      -w      enable warning output
     *
     *      -s      strictly enforce language rules:
     *                  o mustn't remove readonly quality from inherited
     *                    interface
     *                  
     *      -m make_out
     *              generate a GNU make makefile with the specified
     *              name containing rules for updating the generated
     *              files when a spec file changes
     *
     *      -i include_out
     *              generate a file containing all generated files,
     *              spec files in this package, and any source files
     *              referenced by these spec files.
     *
     *      -D dep ...
     *              additional prerequisite files for the generation of the
     *              schema; e.g., Java package jars that implement the
     *              config tool.
     */
    public static void main(String[] args)
    {
        String usage = "xdc.bld.Builder usage: [-m makefile_output] [-i includefile_output] pkgFile [-D dep ...]";

        if (args.length < 1) {
            Err.exit(usage);
        }

        String incFileName = null;
        String makeFileName = null;
        String specFileName = null;
        String[] auxDeps = {};
        int flags = 0;
        
        /* parse options */
        int i;
        for (i = 0; i < args.length; i++) {
            String opt = args[i];
            if (opt.charAt(0) != '-') {
                if (specFileName != null) {
                    Err.exit(usage);
                }
                specFileName = opt;
            }
            else {
                switch (opt.charAt(1)) {
                    case 'm': {
                        makeFileName = args[++i];
                        break;
                    }
                    case 'w': {
                        flags |= FLAG_WARNINGS;
                        break;
                    }
                    case 's': {
                        flags |= FLAG_STRICT;
                        break;
                    }
                    case 'i': {
                        incFileName = args[++i];
                        break;
                    }
                    case 'D': {
                        int len = args.length - (i + 1);
                        if (len <= 0) {
                            Err.exit(usage);
                        }
                        auxDeps = new String[len];
                        System.arraycopy(args, i + 1, auxDeps, 0, len);
                        i = args.length;
                        break;
                    }
                    default: {
                        Err.exit(usage);
                    }
                }
            }
        }
        if (specFileName == null) {  /* err: specFileName not specified */
            Err.exit(usage);    
        }
    
        /* generate schema and all target files */
        Vector<String> gens = new Vector<String>();
        Pkg pkg = gen(specFileName, flags, gens);
    
        /* generate .inc file last to ensure its date is later than
         * any generated file that does not appear in the pattern rule.
         *
         * This allows us to add these "non-conformant" generated files
         * as pre-requisites of this file.  Why?  By adding these
         * prerequisites and adding a special "no rule" rule for the
         * non-conformant files, make will trigger regeneration whenever
         * any of these files is removed or modified.
         */

        if (pkg.isSpecChanged()) {
            /* generate include list for all package releases */
            Vector<String> opts = genIncludeFile(pkg, gens, incFileName);
            /* generate makefile dependencies for schema */
            genMakefile(pkg, gens, opts, auxDeps, makeFileName);
        }
        else {
            /* touch package.xdc.inc */
            new File(incFileName).setLastModified(System.currentTimeMillis());
        }
    }

    /*
     *  ======== gen ========
     *  Generate schema, target module headers, and package level sources
     *
     *  specFileName    the name of the package spec file to process
     *  gens            non-null vector to contain generated file names
     *
     *  Returns a fully built package object
     */
    private static Pkg gen(String specFileName, int flags, Vector<String> gens)
    {
        Clock clock = new Clock();
        clock.reset();
        clock.enable();
        
        /* compile package specs */
        ParserSession parser = new ParserSession();
        if ((flags & FLAG_WARNINGS) != 0) {
            parser.setWarnings(true);
        }
        if ((flags & FLAG_STRICT) != 0) {
            parser.setStrict(true);
        }

        Pkg pkg = null;
        
        try {
            pkg = parser.scan(specFileName);
            Digest digest = new Digest(pkg);
            digest.update();
            gens.add(Digest.FILENAME);
            if (!pkg.isSameContent()) {
                pkg = parser.compile(specFileName, pkg);
            }
        }
        catch (SessionRuntimeException e) {
            Err.exit("compilation failed: " + e.getMessage());
        }
        if (pkg == null) {
            Err.exit("compilation failed.");
        }

        if (pkg.isSameContent()) {
            clock.print("same");
            return pkg;
        }
        
        if (pkg.getBodyChanges() != null) {
            genPkgBodyChanges(pkg, gens);
            clock.print("body changes");
            return pkg;
        }
        
        /* still used to quickly identify packages */
        File sch = new File("package/" + pkg.getName() + ".sch");

        File gvers = new File("package/.vers_g" + GREEN_VERS);
        File rvers = new File("package/.vers_r" + RED_VERS);
        File bvers = new File("package/.vers_b" + BLUE_VERS);
        File cvers = new File("package/.vers_c" + CYAN_VERS);
        
        File xtree = new File("package/." + Glob.xdctree());

        try {
	    /* If there are version files with different version numbers they
	     * have to be removed first, otherwise we may end up in a situation
	     * where two incompatible releases treat the same schema file as
	     * compatible.
	     */
	    File[] list = new java.io.File("package").listFiles();
	    for (File f: list) {
                String fname = f.getName();
	        if (fname.startsWith(".vers_")) {
	            f.delete();
	        }
	    }
            sch.createNewFile();
            gvers.createNewFile();
            rvers.createNewFile();
            bvers.createNewFile();
            cvers.createNewFile();
            xtree.createNewFile();
        }
        catch (Exception e) {
            Err.exit(e);
        }

		if (gens != null) {
	            gens.add("package/" + pkg.getName() + ".sch");
	            gens.add("package/" + gvers.getName());
	            gens.add("package/" + rvers.getName());
	            gens.add("package/" + bvers.getName());
	            gens.add("package/" + cvers.getName());
	            gens.add("package/" + xtree.getName());
		}
        
        clock.print("compile");
		
		/* generate package-level .h and .c files */
	    Out bdyout = getStream("package/package_" + pkg.getName() + ".c", gens);
	    Out defout = getStream("package/package.defs.h", gens);
	    
	    Body bodyGen = new Body();
	    Defs defsGen = new Defs();
	    
	    bodyGen.gen(pkg, bdyout);
	    defsGen.gen(pkg, true, defout);

        /* generate module and interface headers */
        new File("package/internal").mkdirs();
        for (Unit unit : pkg.getUnits()) {
            Out hdrout;
    
            /* generate header for all target modules/interfaces */
            if (!unit.isMeta()) {
                
                /* target contributions to package-level files */
                defsGen.gen(unit, defout);

                hdrout = getStream(null);
                new Header().gen(unit, hdrout);
                String fn = (unit.isProxy() ? "package/" : "")
                    + unit.getName() + ".h";
                closeStream(hdrout, fn, gens);
            }

            /* generate internal header for all non-hostonly modules */
            if (!unit.isMeta() && unit.isMod()) {
                hdrout = getStream(null);
                new Internal().gen(unit, hdrout);
                closeStream(hdrout, "package/internal/"
                    + unit.getName() + ".xdc.h", gens);
            }

            /* generate target headers for designated metaonly units */
            if (unit.isMeta()) {
                String template = unit.attrString(Attr.A_TargetHeader);
                if (template != null) {
                    /* locate the template file */
                    if (Path.search(template) == null) {
                        Err.exit("can't find header template file: '"
                            + template + "' along the path " + Path.curpath());
                    }
                    else { /* gen header */
                        hdrout = getStream(null);
                        Template.exec(template, hdrout, null,
                            new Object[] {unit});
                        closeStream(hdrout, unit.getName() + ".h", gens);
                    }
                }
            }
            genUnitImpl(unit, gens);
        }
        
        defsGen.gen(pkg, false, defout);
    
        closeStream(bdyout);
        closeStream(defout);
            
        clock.print("target");
        
        /* generate meta-domain schema */

        String jsrc = "package/" + pkg.getName().replace('.', '_') + ".java";
	    Out jclsout = getStream(jsrc, gens);
	    new JClass().gen(pkg, jclsout);
	    closeStream(jclsout);
        clock.print("schema gen");
	    JClass.compile(jsrc);
	    gens.add("package/" + pkg.getName().replace('.', '_') + ".class");
        clock.print("schema compile");
        
        Out docout = getStream("package/package.doc.xml", gens);

        new xdc.services.intern.gen.Doc().gen(pkg, docout);
        closeStream(docout);

        clock.print("doc");
        
        // will retain summary/nodoc info from previous Doc.gen 
        if (pkg.getBodyChanges() == null) {
            parser.save();
        }
        gens.add("package/" + pkg.getName() + ".ccs");

        clock.print("ccs");
        
//        System.out.println("total time: " + clock.total());

        return (pkg);
    }

    /*
     *  ======== genIncludeFile ========
     */
    private static Vector<String> genIncludeFile(Pkg pkg, Vector<String> list,
        String outName)
    {
        Vector<String> optFiles = new Vector<String>();
    
        if (outName == null) {
            return (optFiles);
        }
        Out out = getStream(outName);
        
        /* add package specification to sources */
        out.println("package.xdc");
        
        /* look for optional package implementation of IPackage */
        String xs = "package.xs";
        if ((new File(xs)).exists()) {
            optFiles.add(xs);
            out.println(xs);
        }
    
        /* add all unit specs and optional implementation files */
        for (Unit unit : pkg.getUnits()) {
    
            if (unit.isProxy()) {
                continue;
            }
            
            out.println(unit.getName() + ".xdc");
    
            /* look for optional module/interface implementation files */
            String src = unit.getName() + ".xs";
            if ((new File(src)).exists()) {
                optFiles.add(src);
                out.println(src);
            }
        }
        
        /* add misc spec'd sources that are in this package */
        String cwd;
        try {
            cwd = (new File(".")).getCanonicalPath();
        }
        catch (Exception e) {
            cwd = System.getProperty("user.dir");
        }
        List<String> misc = pkg.getMiscSrcs();
        for (String src : misc) {
            if (inPkg(cwd, src)) {
            	/* Fixlist later looks through the list for duplicates and
            	 * the comparison is string-based, so './foo.bar' and 'foo.bar'
            	 * are not detected as the same file.
            	 */
            	if (src.startsWith("./") || src.startsWith(".\\")) {
            	    src = src.substring(2);	
            	}
                out.println(src);
            }
        }
    
        /* add all generated files */
        for (String s : list) {
            out.println(s);
        }

        closeStream(out);

        return (optFiles);
    }

    /*
     *  ======== genMakefile ========
     *  Generate a makefile that keeps the generated files upto date with
     *  respect to the input spec files.
     *
     *  Arguments:
     *      pkg     fully built package object
     *      genList list of files generated during schema generation
     *              (not including the makefile or the include file)
     *      optList list of optional files supplied by the package
     *      outName name of the output makefile to create
     *
     *  The generated makefile must address the following "use cases":
     *      1. After a clean of the package all generated files should be
     *         created (just once) if any of the following goals is
     *         specified:
     *          a. "all", "all,*", or nothing
     *          b. ".interfaces"
     *          c. any one (or more) of the generated files
     *          d. any "release" goal
     *
     *      2. after the ".interfaces" goal is built
     *          a. modification of any included spec file should trigger
     *             regeneration
     *          b. relocation of packages containing package specs
     *             referenced by a package-local spec file should not
     *             trigger regeneration.
     *          c. removal of a meta-domain implementation file directly
     *             included by the generated schema
     *          d. addition of a meta-domain implementation file for a
     *             module/interface specified in the package
     *          e. addition of the meta-domain implementation file
     *             "package.xs"
     *          f. removal of the meta-domain implementation file
     *             "package.xs"
     *          g. removal of a generated file triggers regeneration for
     *             the following goals:
     *              1. "all", "all,*", or nothing
     *              2. "release" or any specific release 
     *              3. the removed file itself
     *              4. ".interfaces"
     *
     *      3. after the all goal is built
     *          a. removal of a generated file will trigger regeneration
     *             if any of the following goals is specified:
     *              1. "all", "all,*", or nothing
     *              2. "release" or any specific release 
     */
    private static void genMakefile(Pkg pkg, Vector<String> genList,
        Vector<String> optList, String[] auxDeps, String outName)
    {
        if (outName == null) {
            return;
        }
        Out out = getStream(outName);
    
        /* output clean rules for all generated files */
        out.println("clean::");
        for (String fileName : genList) {
            out.println("\t$(RM) " + fileName);
        }
        out.print("\n");
    
        /* output dependencies that force .interfaces to be updated */
        out.print(".interfaces: ");
        for (String fileName : genList) {
            // skip generated Mod.c files
            if (fileName.endsWith(".c") && !fileName.startsWith("package/")) {
                continue;
            }
            out.print(fileName + " ");
        }
        out.print("\n");

        /*  Output empty rules for the .vers_* files
         *
         *  We need to output empty rules for the .vers files because
         *  any goal that names a .vers_* file as a prerequisite will fail
         *  with a message from make complaining that "make does not know
         *  how to build .vers_*".  The empty rule tells make that any goal
         *  that names .vers_* as a prequisite must be re-built; during
         *  the rebuild process the prerequisites will be recreated or will
         *  change.  In this case, the prerequisite will change to the .vers_*
         *  files with the new version numbers.
         */
        for (String fileName : genList) {
            if (fileName.startsWith("package/.vers_")) {
                /* force this tool to run to recreate missing .vers files */
                out.print("package/package.xdc.inc: " + fileName + "\n");
                /* output the empty rule  */
                out.print(fileName + ":\n");
            }
        }

        out.print("\n.interfaces: ");
        List<String> locals = pkg.getLocalSpecs();
        for (String fileName : locals) {
            out.print(fileName + " ");
        }
        out.print("\n\n");
    
        /* The generated schema only includes *.xs files if they exist at
         * the time the schema is generated.  If the *.xs file's existance
         * changes, the schema should be rebuilt.
         *
         * There are two cases to handle:
         *  1. a *.xs file is removed after the schema is built
         *  2. a *.xs file is added after the schema is built
         *
         *  Case 1 is handled by creating "no rule" rules for the files that
         *  exist at the time of schema generation.
         *
         *  Case 2 is not handled.  It could be handled by defining PKGCFGS
         *  using the $(wildcard) function; make will create a dependency when
         *  the file is added.  The only hole is if the file modification
         *  time is earlier than the schema file.
         */
        out.print("# schema include file dependencies\n");
        for (String fileName : optList) {
            /* output special "no rule" rules to force schema to be rebuilt
             * in the case that an included .xs file is (re)moved
             */
            out.println(fileName + ":");
            out.println("package/package.xdc.inc: " + fileName);
        }
        out.print("\n");
        
        /* output deps that trigger schema rebuild if a spec changes */
        out.print("# schema update dependencies\n");
        for (String fileName : locals) {
            out.println("package/package.xdc.inc: " + fileName);
        }
    
        List<String> ext = pkg.getOtherSpecs();
        for (String fileName : ext) {

            /* Normally one would simply create dependencies on the included
             * file so that we regen only when referenced modules/interfaces
             * change.
             *
             * Alternatively, we could trigger a rebuild of this package's
             * schema only when the external package's schema is updated.
             * This handles the rare case that a spec file is updated by
             * the external schema file (and any associated header) has
             * not been updated:
             *
             *      String pdir = (new File(fileName)).getParent();
             *      if (pdir != null) {
             *          fileName = pdir + "/package/package.xdc.inc";
             *      }
             */
            
            String pn = fileName.substring(0, fileName.lastIndexOf('/')).replace('/', '.');
            String pdir = (new File(fileName)).getParent();
            if (pdir != null) {
               fileName = pdir + "/package/" + pn + ".sch";
            }
            
            out.println("package/package.xdc.inc: " + fileName);
            /* output special "no rule" rules to force schema to be
             * rebuilt in the case that a schema file *outside* this
             * package is moved; e.g., the package path changed.
             */
            out.println(fileName + ":");
            /* set the vpath for this file so that changes to the absolute
             * location of the spec do not trigger a rebuild.
             */
            out.println("vpath " + fileName + " $(XPKGVPATH)");
        }
        out.print("\n");

        /* emit "external" dependencies that should trigger schema rebuild
         * We protect these dependencies with a check for the clean goal in
         * case we are trying to clean a package from a different host; e.g.,
         * Windows file names (with z:) would prevent *nix hosts from cleaning
         * this package.
         */
        out.print("ifneq (clean,$(MAKECMDGOALS))\n");
        String cwd;
        try {
            cwd = (new File(".")).getCanonicalPath() + File.separator;
        }
        catch (Exception e) {
            cwd = "";
        }
        for (int i = 0; i < auxDeps.length; i++) {
            String fname = mkRelPath(auxDeps[i], cwd);
            if (fname == null) {
                /* output special "no rule" rules to force schema to be
                 * rebuilt in case the file moves and to keep make from
                 * complaining that it does not know how to build the file.
                 */
                fname = _escapeFilename(auxDeps[i]);
                out.println(fname + ":");
            }
            out.println("package/package.xdc.inc: " + fname);
        }
        out.print("endif\n");
        
        /* the following dependencies are required to ensure that the
         * generated files not specified by the pattern rule are remade in
         * the event that they are removed or inadvertently modified
         */
        out.print("# goals for files generated during schema generation but unspecified by schema's pattern rule\n");
        for (String fileName : genList) {
            if (fileName.endsWith(".h") && !fileName.startsWith("package/package")) {
                // skip generated Mod.c files
                out.println("package/package.xdc.inc: " + fileName);
                out.println(fileName + ":");
            }
        }
        out.print("\n");
    
        out.close();
    }

    private static void genPkgBodyChanges( Pkg pkg, Vector<String> gens )
    {
        Set<String> bodyChanges = pkg.getBodyChanges();
        
        for (Unit u : pkg.getUnits()) {
            if (bodyChanges.contains(u.getName())) {
                genUnitImpl(u, gens);
            }
        }
    }
    
    private static void genUnitImpl( Unit unit, Vector<String> gens )
    {
        if (unit.getFxnImpls() != null) {
            Out impout;
            String fn;
            if (!unit.isMeta() && unit.isMod()) {
                impout = getStream(null);
                new ImplC().gen(unit, impout);
                fn = unit.getName() + ".c";
                closeStream(impout, fn, gens);
            }
            impout = getStream(null);
            new ImplS().gen(unit, impout);
            fn = unit.getName() + ".xs";
            closeStream(impout, fn, gens);
        }
    }
    
    /*
     *  ======== mkRelPath ========
     */
    private static String mkRelPath(String path, String cwd)
    {
        try {
            String cpath = (new File(path)).getCanonicalPath();
        
            if (cpath.startsWith(cwd)) {
                String rpath = cpath.substring(cwd.length());
                return (rpath.replace('\\', '/'));
            }
        }
        catch (Exception e) {
            ;
        }

        return (null);
    }

    /*
     *  ======== _escapeFilename ========
     *  Apply all the standard tricks to make sure a filename retains its
     *  meaning in the makefile. In particular treat spaces and backslashes.
     */
    private static String _escapeFilename(String path)
    {
        return path.replace('\\', '/').replace(" ", "\\ ");
    }

    /*
     *  ======== closeStream ========
     */
    private static void closeStream(Out out) { out.close(); }
    
    private static void closeStream(Out out, String fileName, Vector<String> list)
    {
        try {
            out.closeAndCopy(fileName);
        }
        catch (Exception e) {
            Err.exit(e);
        }

        if (list != null) {
            list.add(fileName);
        }
    }
    
    /*
     *  ======== getStream ========
     */
    private static Out getStream( String fileName )
    {
        return getStream(fileName, null);
    }
    
    private static Out getStream(String fileName, Vector<String> list)
    {
        try {
            File file;
            if (fileName == null) {
                file = File.createTempFile(".schema", null, new File("."));
                file.deleteOnExit();
            }
            else {
                file = new File(fileName);
                file.delete();
            }
            
            Out result;
            if (fileName == null) {
                result = new Out(file.getName());
            }
            else {
                result = new Out(new BufferedOutputStream(
                    new FileOutputStream(file)
                ));
                if (list != null) {
                    list.add(fileName);
                }
            }
    
            return (result);
        }
        catch (Exception e) {
            Err.exit(e);
        }
        
        return (null);
    }

    /*
     *  ======== inPkg ========
     *  Return true iff fileName is in the package whose base is specified
     *  by the canonical path pbase.
     *
     *  pbase is the canonical path to the cwd which must contain a
     *         package.xdc file
     *  fileName is the name of the file to be classified
     */
    public static boolean inPkg(String pbase, String fileName)
    {
        /* this is an optimization to avoid searching in common cases */
        if (fileName.startsWith("./") || fileName.startsWith(".\\")
            || (fileName.indexOf('/') == -1 && fileName.indexOf('\\') == -1)) {
            return (true);
        }
    
        /* if it starts with pbase, its in this package or a nested package */
        String cp;
        try {
            cp = (new File(fileName)).getCanonicalPath();
        }
        catch (Exception e) {
            return (false);
        }
        if (!cp.startsWith(pbase)) {
            return (false);
        }
    
        /* find the package spec for the package containing cp */
        String ps = Scan.findPackageSpec(cp);
    
        /* since the package spec name is "package.xdc", it is sufficient to
         * compare lengths of the strings to determine if we are in a nested
         * package or not
         */
        if (ps != null && (ps.length() == (pbase.length() + 11))) {
            return (true);
        }
    
        return (false);
    }
}
