/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

public class Import
    implements java.io.Serializable
{
    // Static Serialization UID
    static final long serialVersionUID = 1L;
    
    Atom uid;
    String alias;
    Unit unit;
    boolean star = false;
    boolean expFlg = false;
    
    Import( Atom uid, String alias )
    {
        this.uid = uid;
        this.alias = alias;
        this.star = false;

        int k = this.uid.text.indexOf(".*");
        
        if (k != -1) {
            this.uid = this.uid.copy(this.uid.text.substring(0, k));
            this.star = true;
        }
        
        if (alias == null) {
            k = this.uid.text.lastIndexOf('.');
            this.alias = (k == -1) ? this.uid.text : this.uid.text.substring(k + 1);
        }
    }
    
    public final String getAlias() { return this.alias; }
    public final Unit getUnit() { return this.unit; }
    public final boolean isExport() { return this.expFlg; }
    
    void resolve( Unit uspec )
    {
        Session ses = uspec.getSession();
        Unit imp = null;

        if (ses.getEnv().resolve(this.uid.text) != null) {
            imp = ses.loadUnit(this.uid.text);
        }
        else {
        	String segs[] = this.uid.text.split("\\.");
        	imp = uspec;
        	for (String s : segs) {
        		String qn = imp.getQualName() + '.' + s;
        		Node n = ses.lookup(qn);
        		if (n instanceof Unit) {
        			imp = (Unit)n;
        		}
        		else if (n instanceof Decl.Imp) {
        			imp = ((Decl.Imp)n).getUnit();
        		}
        		else {
        			imp = null;
        			break;
        		}
        	}
        }
        
        if (imp == null) {
            ses.msg.error(this.uid, "not a unit");
            throw new SessionRuntimeException("parser failed");
        }
        
        this.unit = imp;
        String qn = uspec.getQualName() + '.' + this.alias;
        Atom aid = this.uid.copy(this.alias);
        Node n = ses.lookup(qn);
        if (n != null && !(n instanceof Decl.Imp)) {
        	ses.msg.error(aid, "name already defined in this scope");
        	return;
        }

        if (n instanceof Decl.Imp) {
        	Decl.Imp d = (Decl.Imp)n;
        	if (d.getImport() == null) {
        		d.bindImport(this);
        		this.expFlg = true;
        	}
        	else if (d.getUnit() != imp) {
        		ses.msg.error(aid, "alias already mapped to another unit");
        	}
        	return;
        }

        uspec.uses.add(imp);
        Decl.Imp d = new Decl.Imp(aid, this);
        d.bindParent(uspec);
        ses.enterNode(qn, d);
    }
}
