/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;
import xdc.services.global.*;
import java.util.*;
import java.io.*;

public class Template {

    /*
     *  ======== exec ========
     */
    public static Vector exec( String fname, PrintStream out, Object thisObj, Object[] args )
	    throws JavaScriptException
    {
    	Vector includedFiles = new Vector();
    	String xfname;
	
    	if ((xfname = Path.search(fname)) != null) {
    	    Scriptable tplt = null;
    	    Object [] used = new Object[0];
        
    	    Context cx = Context.getCurrentContext();
    	    try {
    	    	String cmd = "xdc.loadTemplate('" + fname + "')";
    	    	tplt = (Scriptable)cx.evaluateString(Global.getTopScope(), cmd, "$$Template$exec", 0, null);
    	    	Function fxn = (Function)tplt.get("fxn", tplt);
    	    	used = Template.apply(fxn, out, thisObj, args);
    	    }
    	    catch (JavaScriptException e) {
                throw e;
    	    }
    	    catch (Exception e) {
                Err.exit(e);
    	    }
    
    	    /* create vector of included files */
    	    includedFiles.addElement(xfname);
    	    for (int i = 0; i < used.length; i++) {
    	    	includedFiles.addElement(used[i]);
    	    }
        }

        return (includedFiles);
    }
    
    /*
     *  ======== apply ========
     */
    public static Object[] apply( Function fxn, PrintStream out, Object thisObj, Object[] args )
	throws JavaScriptException
    {
        Context cx = Context.getCurrentContext();
        Object[] targs = new Object[1 + args.length];
        System.arraycopy(args, 0, targs, 1, args.length);
        targs[0] = out;

	Scriptable used = null;
        try {
            if (thisObj == null || thisObj instanceof Scriptable) {
                used = (Scriptable)fxn.call(cx, Global.getTopScope(),
                    (Scriptable)thisObj, targs);
            }
            else {
                System.err.println(
                    "Error: can't apply template: thisObj ("
                    + thisObj + ") is not a JavaScript object");
                return (new String[0]);
            }
        }
        catch (JavaScriptException e) {
            throw e;
        }
        catch (Exception e) {
            Err.exit(e);
        }

	return (used.getIds());
    }
    
    /*
     *  ======== test ========
     */
    public static void test()
    {
        try {
            Out out = new Out(new java.io.FileOutputStream("test.out"));
            exec("test.xdt", out, null, new Object[]{"a", "b", "c"});
            out.close();
        }
        catch (Exception e) {
            Err.exit(e);
        }
     }
}
