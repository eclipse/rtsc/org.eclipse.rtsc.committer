/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;
import java.util.*;
import xdc.services.spec.*;

/*
 *  ======== Value ========
 */
public abstract class Value extends XScriptO
    implements AnyType
{
    static final int WHITE = 0;
    static final int GREY = 1;
    static final int BLACK = 2;

    static protected OpTab optab = new OpTab(Value.class, new String[] {
            "seal:$seal",
            "sealed:$sealed",
            "toText:$toText",
            "toXml:$toXml",
            "unseal:$unseal",
    });

    String path;
    Value prnt;
    Value root;
    char mode = 'w';
    int sealColor = WHITE;

    abstract void assignS( Scriptable sobj );
    abstract void assignV( Object val );
    abstract public Object copy();
    abstract void sealAgg( int color );
    abstract void sealProp( String prop, int color );
    abstract boolean sealedProp( String prop );

    protected void init( Value prnt, String name, boolean ronly )
    {
        this.path = name + '/';
        this.prnt = prnt;
        this.root = (prnt == null) ? this : prnt.root;
        this.mode = ronly ? 'r' : (prnt == null) ? 'w' : prnt.mode;
        this.sealColor = (prnt == null) ? WHITE : prnt.sealColor;

        this.setName(name);

        this.bindtab.put("$self", this);
        this.bindtab.put("$$mode", "" + this.mode);
        this.bindtab.put("$$parent", this.prnt);
    }

    public final String getPath() { return this.path; }
    public final Value getPrnt() { return this.prnt; }
    public final Value getRoot() { return this.root; }

    public void seal( Object o )
    {
        xseal(o instanceof String ? (String)o : null, BLACK);
    }

    public boolean sealed( Object o )
    {
        return o instanceof String ? this.sealedProp((String)o) : this.sealColor == BLACK;
    }

    static public Object sealSObj( Object sobj )
    {
        if (sobj instanceof ScriptableObject) {
            ((ScriptableObject)sobj).sealObject();
        }

        return sobj;
    }

    void sealVal( Object o, int color )
    {
        if (o instanceof Value) {
            ((Value)o).xseal(null, color);
        }
    }

    public final String toText()
    {
        return this.toText(false);
    }

    String toText( boolean xml )
    {
        return "";
    }

    public final Scriptable toXml()
    {
        Context cx = Global.getContext();
        Scriptable gscope = Global.getTopScope();

        String cat = ((String)this.bindtab.get("$category")).toLowerCase();
        String res = "";

        res += "<" + cat;
        res += " id='" + this.getName() + "' ";
        res += this.toText(true) + "</" + cat + ">";

        return cx.newObject(gscope, "XML", new Object[] {res});
    }

    public void unseal( Object o )
    {
        xseal(o instanceof String ? (String)o : null, WHITE);
    }

    void xseal( String prop, int color )
    {
        if (prop != null) {
            this.sealProp(prop, color);
            return;
        }

        if (this.sealColor == GREY) {
            return;
        }

        this.sealColor = GREY;
        this.sealAgg(color);
        this.sealColor = color;
    }

    // Value.Arr

    static public class Arr extends Value
    {
        Proto.Arr proto;
        Vector vec = new Vector();
        int curlen = 0;
        String ctype = null;
        String memSect = null;
        int memAlign = 0;
        boolean virgin = true;
        boolean sealedDom = false;

        static OpTab optab = new OpTab(Value.Arr.class, new String[] {
                "add:$add",
                "add:$$add",
                "addrof:$addrof",
                "alloc:$$alloc",
                "copy:$copy",
        });

        public Arr()
        {
            this.bindtab.put("$category", "Vector");
            this.bindtab.putAll(Value.optab);
            this.bindtab.putAll(Value.Arr.optab);
        }

        public void add( Object val )
        {
            int oldlen = this.curlen;

            this.virgin = false;
            this.setLength(oldlen + 1);
            this.setElem(oldlen, val);
        }

        public Object addrof( Object sel )
        {
            if (!(sel instanceof Number)) {
                this.error("$addrof requires an array index");
                return Value.ERROR;
            }

            return new Ptr(this, sel);
        }

        public void alloc( Object memSect, int memAlign )
        {
            this.memSect = (String)memSect;
            this.memAlign = memAlign;
        }

        void assignS( Scriptable sobj )
        {
            if (sobj == Value.DEFAULT) {
                this.setLength(0);
                return;
            }

            int len = ((Number)sobj.get("length", sobj)).intValue();

            this.virgin = false;
            this.setLength(len);

            for (int i = 0; i < len; i++) {
                this.setElem(i, sobj.get(i, sobj));
            }
        }

        void assignV( Object val )
        {

            Value.Arr srcarr = (Value.Arr)val;
            this.assignS(srcarr);

/*
            /// OPTIMIZED VERSION

            Value.Arr srcarr = (Value.Arr)val;
            int len = srcarr.curlen;

            this.virgin = false;
            this.setLength(len);

            if (this.proto.base instanceof Proto.Aggregate) {
                for (int i = 0; i < len; i++) {
                    this.proto.base.assign(this.vec.get(i), srcarr.vec.get(i));
                }
            }
            else {
                for (int i = 0; i < len; i++) {
                    this.vec.set(i, srcarr.vec.get(i));
                }
            }
*/
        }

        public Object copy()
        {
            Value.Arr varr = (Value.Arr)this.proto.create(null, null, false, Value.DEFAULT, false);
            varr.assignV(this);
            return varr;
        }

        void extend( int newlen )
        {
            int oldlen = this.vec.size();
            this.vec.setSize(newlen);

            String name = this.getName();

            for (int i = oldlen; i < newlen; i++) {
                Object val = this.proto.base instanceof Proto.Embedded
                    ? ((Proto.Embedded)this.proto.base).create(this, name + '/' + i, false, Value.DEFAULT, false)
                    : Context.getUndefinedValue();
                this.vec.set(i, val);
            }
        }

        public final String getCtype() { return this.ctype; }
        public final int getMemAlign() { return this.memAlign; }
        public final String getMemSect() { return this.memSect; }
        public final Proto.Arr getProto() { return this.proto; }

        void init( Proto.Arr proto, Value prnt, String name, boolean ronly, Object initval, boolean check )
        {
            super.init(prnt, name, ronly);
            this.proto = proto;
            if (this.proto.dim > 0) {
                this.virgin = false;
            }
            this.assignS((Scriptable)initval);

/*
            /// OPTIMIZED

            Scriptable sobj = (Scriptable)initval;
            int len = sobj instanceof NativeArray ? (int)(((NativeArray)sobj).getLength()) : 0;

            if (check) {
                this.assign(sobj, len);
                return;
            }

            this.setLength(len);
            this.vec.setSize(len);

            for (int i = 0; i < len; i++) {
                Object elmval = sobj.get(i, sobj);
                if (this.proto.base instanceof Proto.Embedded) {
                    elmval = ((Proto.Embedded)this.proto.base).create(name + '/' + i, elmval, check);
                }
                else {
                    elmval = this.proto.base.assign(null, elmval);
                }
                this.vec.set(i, elmval);
            }

            if (this.proto.dim > 0) {
                this.extend(this.proto.dim);
            }

            this.virgin = false;
*/
        }

        public final boolean isVirgin() { return this.virgin; }

        void sealAgg( int color )
        {
            this.sealedDom = (color == BLACK);
            if (this.proto.base instanceof Proto.Sealable) {
                for (Iterator it = this.vec.iterator(); it.hasNext(); ) {
                    sealVal(it.next(), color);
                }
            }
        }

        void sealProp( String prop, int color )
        {
            if (!(prop.equals("length"))) {
                this.error("$seal/$unseal: expected 'length'");
                return;
            }

            this.sealedDom = (color == BLACK);
        }

        boolean sealedProp( String prop )
        {
            return prop.equals("length") && this.sealedDom;
        }

        void setElem( int index, Object val )
        {
            Object res = this.proto.base.assign(this.vec.get(index), val);
            if (res == Value.ERROR) {
                this.error("incompatible assignment to element " + index);
                res = Context.getUndefinedValue();
            }
            this.vec.set(index, res);
        }

        void setLength( int newlen )
        {
            if (this.proto.dim > 0) {
                if (newlen > this.proto.dim) {
                    this.error("source array is too long");
                }
                newlen = this.proto.dim;
            }

            if (this.sealedDom && newlen != this.curlen) {
                this.error("sealed domain: source array has different length");
            }

            this.curlen = newlen;
            this.bindtab.put("length", new Integer(newlen));

            if (!this.virgin) {
                this.extend(newlen);
            }
        }

        public final void setCtype( String ctype ) { this.ctype = ctype; }

        public String toText( boolean xml )
        {
            String res = xml ? (" length='" + Integer.toString(this.curlen) +  "'>") : "[";
            String sep = "";

            for (Iterator it = this.vec.iterator(); it.hasNext(); ) {
                Object val = it.next();

                if (xml) {
                    res += "<elem>";
                }
                else {
                    res += sep;
                }

                if (val instanceof Value && this.proto.base instanceof Proto.Embedded) {
                    res += xml ? ((Value) val).toXml() : ((Value) val).toText();
                }
                else {
                    res += this.proto.base.toText(val);
                }

                if (xml) {
                    res += "</elem>";
                }
                else {
                    sep = ", ";
                }
            }

            return res + (xml ? "" : "]");
        }

        // OVERRIDE Scriptable

        public Object get( int index, Scriptable start )
        {
            if (index < 0 || index >= this.curlen) {
                return super.get(index, start);
            }

            if (this.virgin) {
                this.extend(this.curlen);
                this.virgin = false;
            }

            return this.vec.get(index);
        }

        public Object[] getIds()
        {
            Object[] oa = new Object[this.vec.size()];
            for (int i = 0; i < oa.length; i++) {
                oa[i] = new Integer(i);
            }
            return oa;
        }

        public void put( String name, Scriptable start, Object value )
        {
            if (this.sealedDom) {
                this.error("domain is sealed");
            }

            if (name.equals("$self")) {
                this.proto.assign(this, value);
                return;
            }

            if (!name.equals("length")) {
                super.put(name, start, value);
                return;
            }

            if (!(value instanceof Number)) {
                this.error("illegal length");
            }

            if (this.proto.dim > 0) {
                this.error("fixed-length array");
            }

            int newlen = ((Number)value).intValue();

            if (newlen < 0) {
                this.error("negative length");
            }

            this.setLength(newlen);
        }

        public void put( int index, Scriptable start, Object value )
        {
            if (this.sealed(null)) {
                this.error("sealed array");
            }

            if (index < 0 || index >= this.curlen) {
                super.put(index, start, value);
                return;
            }

            if (this.virgin) {
                this.extend(this.curlen);
                this.virgin = false;
            }

            this.setElem(index, value);
        }
    }

    // Value.Map

    static public class Map extends Value
    {
        Proto.Map proto;
        ArrayList<String> keyvec = new ArrayList();
        HashMap elemmap = new HashMap();
        boolean sealedDom = false;

        static OpTab optab = new OpTab(Value.Map.class, new String[] {
                "copy:$copy",
                "getElem:$get",
                "hasElem:$has",
                "putElem:$put",
                "putHead:$putHead",
                "putTail:$putTail",
        });

        public Map()
        {
            this.bindtab.put("$category", "Map");
            this.bindtab.putAll(Value.optab);
            this.bindtab.putAll(Value.Map.optab);
        }

        void addElem( String name, Object value, int index )
        {
            if (this.sealedDom) {
                this.error("domain is sealed");
                return;
            }

            if (index == -1) {
                this.keyvec.add(name);
            }
            else {
                this.keyvec.add(index, name);
            }

            if (this.proto.base instanceof Proto.Embedded) {
                String ename = this.getName() + "/'" + name + "'";
                this.elemmap.put(name, ((Proto.Embedded)this.proto.base).create(this, ename, false, value, false));
            }
            else {
                this.setElem(name, value);
            }
        }

        void assignS( Scriptable sobj )
        {
            this.keyvec.clear();
            this.elemmap.clear();

            if (sobj == Value.DEFAULT || sobj instanceof Undefined) {
                return;
            }

            int len = ((Number)sobj.get("length", sobj)).intValue();
            for (int i = 0; i < len; i++) {
                Object o = sobj.has(i, sobj) ? sobj.get(i, sobj) : NOT_FOUND;
                if (o == NOT_FOUND || !(o instanceof Scriptable)) {
                    this.error("bad initializer element: [" + i + "]");
                    return;
                }

                Scriptable tup = (Scriptable)o;
                Object key = tup.get(0, tup);
                Object val = tup.get(1, tup);
                if (!(key instanceof String)) {
                    this.error("bad key in initializer element: [" + i + "]");
                    return;
                }

                this.put((String)key, this, val);
            }
        }

        void assignV( Object value )
        {
            Value.Map vmap = (Value.Map)value;

            this.keyvec.clear();
            this.elemmap.clear();

            for (int i = 0; i < vmap.keyvec.size(); i++) {
                Object key = vmap.keyvec.get(i);
                Object val = vmap.elemmap.get(key);
                this.addElem((String)key, val, -1);
            }
        }

        public Object copy()
        {
            Value.Map vmap = (Value.Map)this.proto.create(null, null, false, Value.DEFAULT, false);
            vmap.assignV(this);
            return vmap;
        }

        public Object getElem( String key )
        {
            return this.get(key, this);
        }

        public final Proto.Map getProto() { return this.proto; }

        public boolean hasElem( String key )
        {
            return this.has(key, this);
        }

        void init( Proto.Map proto, Value prnt, String name, boolean ronly, Object initval, boolean check )
        {
            super.init(prnt, name, ronly);
            this.proto = proto;
            if (check) {
                this.proto.assign(this, initval);
            }
            else {
                this.assignS((Scriptable)initval);
            }

            /* OPTIMIZED

           if (check || !(initval instanceof NativeArray)) {
                this.assignS((Scriptable)initval);
                return;
            }

            NativeArray narr = (NativeArray)initval;
            int len = (int)narr.getLength();

            this.keyvec.setSize(len);

            for (int i = 0; i < len; i++) {
                Scriptable tup = (Scriptable)narr.get(i, narr);
                Object key = tup.get(0, tup);
                Object val = tup.get(1, tup);
                this.keyvec.add(key);
                this.elemmap.put(key, val);
            }
*/
        }

        public void putElem( String key, Object val )
        {
            this.put(key, this, val);
        }

        public Object putHead( Object key, Object val )
        {
            if (!(key instanceof String)) {
                this.error("'key' must be a string");
                return this;
            }

            this.keyvec.remove(key);
            this.addElem((String)key, val, 0);
            return this;
        }

        public Object putTail( Object key, Object val )
        {
            if (!(key instanceof String)) {
                this.error("'key' must be a string");
                return this;
            }

            this.keyvec.remove(key);
            this.addElem((String)key, val, -1);
            return this;
        }

        void sealAgg( int color )
        {
            this.sealedDom = (color == BLACK);
            if (this.proto.base instanceof Proto.Sealable) {
                for (Iterator it = this.elemmap.values().iterator(); it.hasNext(); ) {
                    sealVal(it.next(), color);
                }
            }
        }

        void sealProp( String prop, int color )
        {
            if (!(prop.equals("length"))) {
                this.error("$seal/$unseal: expected 'length'");
                return;
            }

            this.sealedDom = (color == BLACK);
        }

        boolean sealedProp( String prop )
        {
            return prop.equals("length") && this.sealedDom;
        }

        public void setElem( String name, Object value )
        {
            if (this.sealed(null)) {
                this.error("sealed map");
            }

            Object newval = this.proto.base.assign(this.elemmap.get(name), value);
            if (newval == Value.ERROR) {
                this.error("incompatible assignment to element ['" + name + "']");
            }

            this.elemmap.put(name, newval);
        }

        public String toText( boolean xml )
        {
            String res = xml ? (" length='" + Integer.toString(this.keyvec.size()) +  "'>") : "[\n";
            String sep = "";

            for (int i = 0; i < this.keyvec.size(); i++) {
                Object key = this.keyvec.get(i);
                Object val = this.elemmap.get(key);

                if (xml) {
                    res += "<entry key='" + key.toString() + "'>";
                }
                else {
                    res += sep + "['" + key.toString() + "', ";
                }

                if (val instanceof Value && this.proto.base instanceof Proto.Embedded) {
                    res += xml ? ((Value) val).toXml() : ((Value) val).toText();
                }
                else {
                    res += this.proto.base.toText(val);
                }

                if (xml) {
                    res += "</entry>";
                }
                else {
                    res += "]";
                    sep = ",\n";
                }
            }

            return res + (xml ? "" : "\n]");
        }

        // OVERRIDE Scriptable

        public void delete( String name )
        {
            if (this.sealedDom) {
                this.error("domain is sealed");
                return;
            }

            if (this.elemmap.containsKey(name)) {
                this.elemmap.remove(name);
                this.keyvec.remove(name);
            }
        }

        public Object get( String name, Scriptable start )
        {
            if (name.equals("length")) {
                return new Integer(this.keyvec.size());
            }
            else if (name.equals("$keys")) {
                return Global.getContext().newArray(Global.getTopScope(), this.getIds());
            }
            else if (name.charAt(0) == '$') {
                return super.get(name, start);
            }
            else {
                Object val = this.elemmap.get(name);
                return (
                    val != null || this.elemmap.containsKey(name)) ? val :
                    this.sealedDom && !this.sealed(null) ? super.get(name, start) : Context.getUndefinedValue();
            }
        }

        public Object get( int index, Scriptable start )
        {
            if (index < 0 || index >= this.keyvec.size()) {
                return super.get(index, start);
            }

            return this.elemmap.get(this.keyvec.get(index));
        }

        public Object[] getIds()
        {
            return this.keyvec.toArray();
        }

        public void put( String name, Scriptable start, Object value )
        {
            if (name.equals("$self")) {
                this.proto.assign(this, value);
                return;
            }

            if (name.equals("length") || name.startsWith("$")) {
                this.error("can't assign " + name);
                return;
            }

            if (this.elemmap.containsKey(name)) {
                this.setElem(name, value);
            }
            else {
                this.addElem(name, value, -1);
            }
        }

        public void put( int index, Scriptable start, Object value )
        {
            if (index < 0 || index >= this.keyvec.size()) {
                super.put(index, start, value);
                return;
            }

            this.setElem((String)this.keyvec.get(index), value);
        }
    }

    // Value.Obj

    static public class Obj extends Value
        implements Cloneable
    {
        static int curid = 0;

        static OpTab optab = new OpTab(Value.Obj.class, new String[] {
                "addrOf:$addrof",
                "bless:$$bless",
                "cast2:$$cast",
                "copy:$copy",
                "init2:$$init",
                "written:$written",
        });

        int id = curid++;
        boolean blessed = false;
        Proto.Obj proto = null;
        Value.Obj orig = this;
        Object[] fields;
        BitSet sealMask;
        BitSet writeMask;

        public Obj( String name ) { this(name, null); }
        // TODO remove eventually

        public Obj( String name, Object pobj )
        {
            this.setName(name);
            this.proto = (Proto.Obj)pobj;

            this.bindtab.put("$orig", this);
            this.bindtab.putAll(Value.optab);
            this.bindtab.putAll(Value.Obj.optab);
        }

        public Object addrOf( Object sel )
        {
            if (!(sel instanceof String)) {
                this.error("$addrof requires a field name");
                return Value.ERROR;
            }

            return new Ptr(this, sel);
        }

        void assignS( Scriptable sobj )
        {
            String id = this.proto.compat(sobj);
            if (id != null) {
                this.error("no property named '" + id + "'");
                return;
            }

            for (Member mbr : this.proto.allFlds()) {
                if (this.blessed && mbr.rflag) {
                    continue;
                }

                Object fldval = sobj.has(mbr.name, sobj) ? sobj.get(mbr.name, sobj) : NOT_FOUND;
                if (fldval == NOT_FOUND) {
                    fldval = mbr.defval;
                }
                this.setFld(mbr, fldval);
            }
        }

        void assignV( Object val )
        {
            Value.Obj vobj = (Value.Obj)val;

            for (Member mbr : this.proto.allFlds()) {
                if (this.blessed && mbr.rflag) {
                    continue;
                }
                if (mbr.proto instanceof Proto.Embedded) {
                    ((Value)this.fields[mbr.idx]).assignV(vobj.fields[mbr.idx]);
                }
                else {
                    this.fields[mbr.idx] = vobj.fields[mbr.idx];
                }
            }
        }

        public void bless()
        {
            if (this.blessed) {
                return;
            }

            this.blessed = true;
            for (int i = 0; i < this.fields.length; i++) {
                Object o = this.fields[i];
                if (o instanceof Value.Obj) {
                    ((Value.Obj)o).bless();
                }
            }
        }

        public Value.Obj cast2( Object pobj )
        {
            return this.castTo((Proto.Obj)pobj);
        }

        Value.Obj castTo( Proto.Obj proto )
        {
            if (this.proto == proto) {
                return this;
            }

            Value.Obj vobj = null;
            try {
                vobj = (Value.Obj)this.clone();
            }
            catch (Exception e) {
                Err.exit(e);
            }
            vobj.proto = proto;
            return vobj;
        }

        protected Object clone()
            throws CloneNotSupportedException
        {
            return super.clone();
        }

        public Object copy()
        {
            String ts = this.proto.tname();
            if (ts.endsWith(".Module") || ts.endsWith(".Instance")) {
                return Context.getUndefinedValue();
            }

            Value.Obj vobj = (Value.Obj)this.proto.create(null, null, false, Value.DEFAULT, false);
            vobj.assignV(this);
            return vobj;
        }

        public final Value.Obj getOrig() { return this.orig; }
        public final Proto.Obj getProto() { return this.proto; }

        public Value.Obj init( Object pobj, Value prnt, String name, boolean ronly, Object initval, boolean check )
        {
            //TODO remove first argument

            super.init(prnt, name, ronly);
            this.proto = (Proto.Obj)pobj;

            if (this.proto == null) {
                return this;
            }

            this.bindtab.put("$$proto", proto);
            String ts = this.proto.tname();
            if (!ts.endsWith(".Module") && !ts.endsWith(".Instance") && !ts.endsWith(".Package")) {
                this.bindtab.put("$category", "Struct");
            }

            this.bindtab.put("$private", Global.getContext().newObject(Global.getTopScope()));

            Scriptable sobj = (Scriptable)initval;
            this.fields = new Object[this.proto.fldlist.size()];
            this.sealMask = new BitSet(this.fields.length);
            this.writeMask = new BitSet(this.fields.length);

            for (Member mbr : this.proto.allFlds()) {
                this.sealMask.set(mbr.idx, ronly | mbr.rflag);
                if (mbr.proto instanceof Proto.Embedded) {
                    this.fields[mbr.idx] = ((Proto.Embedded)mbr.proto).create(
                            this,
                            name + '/' + mbr.name,
                            ronly | mbr.rflag,
                            Value.DEFAULT,
                            false
                    );
                }
            }

            this.assignS(sobj);
            return this;

/*
            if (check) {
                String id = this.proto.compat(sobj);
                if (id != null) {
                    this.error("no property named '" + id + "'");
                    sobj = Value.DEFAULT;
                }
            }

            for (Iterator it = this.proto.getFlds(); it.hasNext(); ) {
                Member mbr = (Member)it.next();
                Object fldval = sobj.get(mbr.name, sobj);
                if (fldval == NOT_FOUND) {
                    fldval = mbr.defval;
                }
                if (mbr.proto instanceof Proto.Aggregate) {
                    String mname = mbr.proto instanceof Proto.Embedded ? name + '/' + mbr.name : null;
                    fldval = ((Proto.Aggregate)mbr.proto).create(mname, (Scriptable)fldval, check);
                }
                if (check && (fldval = mbr.proto.assign(this.fields[mbr.idx], fldval)) == Value.ERROR) {
                    this.error("incompatible initialization of " + mbr.name);
                    fldval = Context.getUndefinedValue();
                }
                this.fields[mbr.idx] = fldval;
            }
*/
        }

        public Value.Obj init2( Object pobj, String name, Object initval, boolean check )
        {
            return this.init(pobj, null, name, false, initval, check);
        }

        void sealAgg( int color )
        {
            this.mode = (color == BLACK) ? 'r' : 'w';
            for (Member mbr : this.proto.allFlds()) {
                this.sealMask.set(mbr.idx, color == BLACK);
                if (mbr.proto instanceof Proto.Sealable) {
                    sealVal(this.fields[mbr.idx], color);
                }
            }
         }

        void sealProp( String prop, int color )
        {
            Member mbr = this.proto.lookupFld(prop);

            if (mbr == null) {
                this.error("$seal/$unseal: no field named '" + prop + "'");
                return;
            }

            this.sealMask.set(mbr.idx, color == BLACK);
        }

        boolean sealedProp( String prop )
        {
            Member mbr = this.proto.lookupFld(prop);
            return mbr != null && this.sealMask.get(mbr.idx);
        }

        void setFld( Member mbr, Object newval )
        {
            if (this.blessed && this.sealMask.get(mbr.idx)) {
                this.error("'" + mbr.name + "' is sealed");
                return;
            }

            Object fldval = mbr.proto.assign(this.fields[mbr.idx], newval);
            if (fldval == Value.ERROR) {
                this.error("incompatible assignment to " + mbr.name + " :"
                    + (newval == null ? "null" : newval.toString()));
                return;
            }

            this.fields[mbr.idx] = fldval;

            if (this.blessed) {
                this.writeMask.set(mbr.idx, true);
            }
        }

        Object superOf()
        {
            if (this.proto.sup == null) {
                return Context.getUndefinedValue();
            }

            Value.Obj vobj = this.castTo(this.proto.sup);
            vobj.orig = vobj;  /// REMOVE???

            return vobj;
        }

        public String toText( boolean xml )
        {
            String res = xml ? ">" : "{";
            String sep = "";

            for (Member mbr : this.proto.allFlds()) {

                if (mbr.name.startsWith("$")) {
                    continue;
                }

                if (xml) {
                    res += "<field name='" + mbr.name + "'>";
                }
                else {
                    res += sep + mbr.name + ": ";
                }

                Object val = this.fields[mbr.idx];
                if (val instanceof Value && mbr.proto instanceof Proto.Embedded) {
                    res += xml ? ((Value) val).toXml() : ((Value) val).toText();
                }
                else {
                    res += mbr.proto.toText(val);
                }

                if (xml) {
                    res += "</field>";
                }
                else {
                    sep = ", ";
                }
            }

            return res + (xml ? "" : "}");
        }

        Object typeOf()
        {
            return this.proto.tname();
        }

        public boolean written( Object sel )
        {
            if (!(sel instanceof String)) {
                this.error("$written requires a field name");
                return false;
            }

            Member mbr = this.proto.lookupFld((String)sel);

            if (mbr == null) {
                this.error("$written: no field named '" + (String)sel + "'");
                return false;
            }

            return this.writeMask.get(mbr.idx);
        }

        // OVERRIDE Scriptable

        public Object get( String name, Scriptable start )
        {
            if (name.equals("$super")) {
                return this.superOf();
            }

            if (name.equals("$type")) {
                return this.typeOf();
            }

            Member mbr = null;

            if (this.proto != null) {
                mbr = this.proto.lookupFld(name);
                if (mbr != null) {
                    if (mbr.getter != null) {
                        Object[] argv = new Object[] {mbr.name};
                        return mbr.getter.call(Context.getCurrentContext(), Global.getTopScope(), this, argv);
                    }
                    else {
                        return this.fields[mbr.idx];
                    }
                }
            }

            if (this.orig.proto != null) {
                mbr = this.orig.proto.lookupMbr(name, this.proto);
                if (mbr != null) {
                    return mbr.defval;
                }
            }

            boolean olderr = this.errflg;
            this.errflg = false;

            Object res = super.get(name, start);
            if (res == NOT_FOUND) {
                res = this.proto.lookupBind(name);
            }

            this.errflg = olderr;

            if (this.errflg && res == NOT_FOUND) {
                this.error("no property named '" + name + "'");
            }

            return res;
        }

        public Object getDefaultValue( java.lang.Class hint )
        {
            String pre = (this.proto != this.orig.proto) ? ("(" + this.proto.name + ")") : "";
            return pre + this.getName();
        }

        public Object[] getIds()
        {
            return this.proto.getMbrIds("$");
        }

        public void put( String name, Scriptable start, Object value )
        {
            if (name.equals("$self") || name.equals("$obj")) {
                new Proto.Str(this.proto).assign(this, value);
//                  this.proto.assign(this, value);
                return;
            }

            Member mbr = this.proto.lookupFld(name);

            if (mbr == null) {
                super.put(name, start, value);
                return;
            }

            if (mbr.setter != null) {
                Object[] argv = new Object[] {mbr.name, value};
                mbr.setter.call(Context.getCurrentContext(), Global.getTopScope(), this, argv);
            }
            else {
                this.setFld(mbr, value);
            }
        }
    }
}
