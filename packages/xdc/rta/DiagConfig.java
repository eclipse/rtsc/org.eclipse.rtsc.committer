/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.rta;

import java.util.BitSet;

/*
 *  ======== DiagConfig ========
 */
public class DiagConfig {       
    public static final int ALWAYS_OFF = 0;
    public static final int ALWAYS_ON = 1;
    public static final int RUNTIME_OFF = 2;
    public static final int RUNTIME_ON = 3;
    
    
    public static final int diags_ENTRY_bit = 0;
    public static final int diags_EXIT_bit = 1;
    public static final int diags_LIFECYCLE_bit = 2;
    
    public static final int diags_INTERNAL_bit = 3;
    public static final int diags_ASSERT_bit = 4;
    
    /* Bits 5-7 unused */
    
    public static final int diags_USER1_bit = 8;
    public static final int diags_USER2_bit = 9;
    public static final int diags_USER3_bit = 10;
    public static final int diags_USER4_bit = 11;
    public static final int diags_USER5_bit = 12;
    public static final int diags_USER6_bit = 13;
    public static final int diags_USER7_bit = 14;
    public static final int diags_USER8_bit = 15;
    
    private int[] diags = new int[16];
    
    public DiagConfig(String diagsMask) {
        
        for (int i = 0; i < 16; i++) {
            this.diags[i] = Integer.parseInt(
                String.valueOf(diagsMask.charAt(i)));
        }
    }
    
    /*
     *  ======== getDiag ========
     *  Returns the value of the specified diag.
     */
    public int getDiag(int bit) {
        return (diags[bit]);
    }
    
    /*
     *  ======== setDiag ========
     *  Sets the specified diag to the specified value.
     */
    public void setDiag(int bit, int value) {
        diags[bit] = value;
    }
    
    /*
     *  ======== updateFromBitSet ========
     *  Update the value of this DiagConfig using the specified bit mask from
     *  the target. The target bit mask only has two values for each diag. 
     *  '0' can mean ALWAYS_OFF, ALWAYS_ON, or RUNTIME_OFF. '1' means 
     *  RUNTIME_ON. This function compares the bit mask to the existing values
     *  and will throw an exception if a diag that was set to 'ALWAYS' is set
     *  to a different value.
     */
    public void updateFromBitSet(BitSet mask) throws Exception {
        for (int i = 3; i < 16; i++) {
            if (mask.get(i)) {
                if ((diags[i] == ALWAYS_OFF) || (diags[i] == ALWAYS_ON)) {
                    throw new Exception(
                        "Diags bit " + i + " set to runtime on which was always off/on.");
                }
                else {
                    diags[i] = RUNTIME_ON;
                }
            }
            else {
                if (diags[i] == RUNTIME_ON) {
                    diags[i] = RUNTIME_OFF;
                }
            }
        }
    }
    
    /* 
     *  ======== printDiags ========
     *  Prints the values of this diags configuration. 
     */
    public void printDiags()
    {
        debugPrint("Printing DiagConfig values: ");
        debugPrint("  diags_ENTRY = " + valToString(diags[diags_ENTRY_bit]));
        debugPrint("  diags_EXIT = " + valToString(diags[diags_EXIT_bit]));
        debugPrint("  diags_LIFECYCLE = " + valToString(diags[diags_LIFECYCLE_bit]));
        debugPrint("  diags_INTERNAL = " + valToString(diags[diags_INTERNAL_bit]));
        debugPrint("  diags_ASSERT = " + valToString(diags[diags_ASSERT_bit]));
        debugPrint("  diags_USER1 = " + valToString(diags[diags_USER1_bit]));
        debugPrint("  diags_USER2 = " + valToString(diags[diags_USER2_bit]));
        debugPrint("  diags_USER3 = " + valToString(diags[diags_USER3_bit]));
        debugPrint("  diags_USER4 = " + valToString(diags[diags_USER4_bit]));
        debugPrint("  diags_USER5 = " + valToString(diags[diags_USER5_bit]));
        debugPrint("  diags_USER6 = " + valToString(diags[diags_USER6_bit]));
        debugPrint("  diags_USER7 = " + valToString(diags[diags_USER7_bit]));
        debugPrint("  diags_USER8 = " + valToString(diags[diags_USER8_bit]));
    }
    
    /*
     *  ======== valToString ========
     *  Converts a diags value to its String form.
     */
    public String valToString(int diagValue) {
        switch (diagValue) {
            case ALWAYS_OFF:
                return ("ALWAYS_OFF");
            case ALWAYS_ON:
                return ("ALWAYS_ON");
            case RUNTIME_OFF:
                return ("RUNTIME_OFF");
            case RUNTIME_ON:
                return ("RUNTIME_ON");
            default:
                return ("Unrecognized diags value");
            }
    }
    
    /*
     *  ======== toBitSet ========
     *  Converts the DiagConfig to a BitSet which can be
     *  passed back to the target.
     */
    public BitSet toBitSet()
    {
        BitSet mask = new BitSet(16);
        for (int i = 3; i < 16; i++) {
            if (diags[i] == RUNTIME_ON) {
                mask.set(i, true);
            }
            else {
                mask.set(i, false);
            }
        }
        return (mask);
    }
    
    /* 
     *  ======== debugPrint ========
     */
    public static String traceEnable = System.getProperty("xdc.rta.traceEnable"); 
    public static void debugPrint(String str) { 
        if ((traceEnable != null) && traceEnable.equals("true")) {
            System.out.println("[RTA] " + str);
        }
    }
}
