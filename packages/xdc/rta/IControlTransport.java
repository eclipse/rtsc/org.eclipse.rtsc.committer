package xdc.rta;

import java.io.IOException;

public interface IControlTransport {
    
    public void initialize() throws Exception;
    public void terminate() throws Exception;
    
    public int read(byte[] buffer, int offset, int length, int timeout) throws java.io.IOException;
    public void write(byte[] buffer, int offset, int length) throws IOException; 
    public int available() throws java.io.IOException;
    
    public long getAddress(String symbol) throws Exception;
}
