/*
 *  ======== TargetType.java ========
 *
 *! Revision History
 *! ================
 *! 29-Jan-2009 cmcc Code reviewed
 */
package xdc.rov;

/*
 *  ======== TargetType ========
 *  This class simply defines an enum to represent target endianess.
 */
public class TargetType {
    public static enum Endianess { LITTLE, BIG }
    
    /*
     *  ======== strToEndianess ========
     *  Helper function for converting the strings "big" and "little" into
     *  the appropriate enum value.
     */
    public static Endianess strToEndianess(String endian) throws Exception {
        if (endian.compareToIgnoreCase("big") == 0) {
            return (Endianess.BIG);
        }
        else if (endian.compareToIgnoreCase("little") == 0) {
            return (Endianess.LITTLE);
        }
        else {
            throw (new Exception("Unrecognized target endianess: " + endian));
        }
    }
    
    /*
     *  ======== isaEndianess ========
     *  Deprecated.
     *  This API is incomplete and specific to BIOS 5. It should be removed, 
     *  but is currently being left in place to preserve compatibility with
     *  older versions of BIOS 5 that reference it.
     */
    public static Endianess isaEndianess(ISymbolTable symTab, int isa) throws Exception {
        switch (isa) {
            case 0x64:
                long bigEndian = symTab.getSymbolValue("GBL_BIGENDIAN");
                
                if (bigEndian == 1) {
                    return (Endianess.BIG);
                }
                else {
                    return (Endianess.LITTLE);
                }
            case 0x55:
                return (Endianess.BIG);
            case 0x28:
                return (Endianess.LITTLE);
            default:
                throw (new Exception("Unrecognized target ISA: 0x" + Integer.toHexString(isa)));
        }
    }
    
    /*
     *  ======== isaBitsPerChar ========
     */
    public static int isaBitsPerChar(int isa) throws Exception {
        switch (isa) {
            case 0x64:
                return (8);
            case 0x55:
            case 0x28:
                return (16);
            default:
                throw (new Exception("Unrecognized target ISA: 0x" + Integer.toHexString(isa)));
        }
    }
}