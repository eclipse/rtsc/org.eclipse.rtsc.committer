/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== xpr.xs ========
 *  Generate HTML pages for the specified packages.
 *
 *  usage: xs -f xpr.xs [-od:<out-dir>] [-fs:<font-size>] [-title <title>] dir ...
 *
 *  If -od: is not specified, output files are placed in the current working
 *  directory.
 *
 *  If -fs: is not specified, the default fontsize is 8.
 *
 *  if -title is not specified, the title is set to "XPR".
 *
 *  This script generates the following files "top-level" index files:
 *      <out-dir>/index.html
 *      <out-dir>/pkgs.html
 *      <out-dir>/units.html
 *
 *  In addition, it generates the following files for each package P:
 *      ; for each executable <exe>
 *      <out-dir>/<P-path>/<exe>.html
 *
 *      ; for every generated cfg script
 *      <out-dir>/<P-path>/<exe_cfg>.xml
 *      <out-dir>/<P-path>/<exe_cfg>.svg
 *
 *      ; top level package "index"
 *      <out-dir>/<P-path>/package.xdc.html
 *      <out-dir>/<P-path>/package/package.rel.svg
 *      <out-dir>/<P-path>/package/package.rel.xml
 *
 *      ; package source files
 *      <out-dir>/<P-path>/package.xs.html
 *      <out-dir>/<P-path>/<src>.html
 *
 *      ; bld related files
 *      <out-dir>/<P-path>/package/package.bld.xml; 
 *      <out-dir>/<P-path>/package.bld.html
 *
 */

var KW_C = "auto|break|case|continue|default|do|else|enum|extern|for|goto|if|register|return|sizeof|static|struct|switch|typedef|union|while|";

var KW_CPP = "catch|class|const|delete|enum|false|friend|inline|namespace|new|operator|private|protected|public|template|this|throw|true|try|using|virtual|volatile|";

var KW_XDC = "config|const|enum|ensure|extern|false|facet|final|inherits|metaonly|import|include|instance|interface|internal|let|length|module|null|override|param|package|readonly|require|requires|return|sizeof|struct|true|typedef|use|union|";

var KW_BLD = "Build|Pkg|$om|";

var KW_CFG = "Program|$PKG|$om|";

var KW_JS = "break|case|catch|continue|default|delete|do|else|exports|false|finally|for|function|if|import|in|new|null|return|switch|this|throw|true|typeof|try|undefined|var|void|while|with|";

var KW_STD = "bool|char|double|float|int|int8|int16|int32|long|short|string|unsigned|uint8|uint16|uint32|void|";

var KW_NULL = "";

var IND = '    ';
var NL = '\n';
var SP = '&nbsp;';

var GREEN = '#009900';
var BLUE = '#3333CC';
var RED = '#FF014A';
var BROWN = '#663300';
var GRAY = 'gray';
var TEAL = 'teal';
var WHITE = 'white';

var COMMENT_COLOR = GREEN;
var KEYWORD_COLOR = BLUE;
var STRING_COLOR = 'maroon';

var DOCTYPE = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n';

var pkgTab = {};                    /* table of pkg objs indexed by name */
var pkgList = [];                   /* sorted list of package names */

/*
 *  ======== fontSize ========
 *  Font size for all generated files (modified via the -fs:<num> option)
 */
var fontSize = 8;

/*
 *  ======== outdir ========
 *  The output directory to contain all generated files (modified via the
 *  -od:<out-dir> option)
 */
var outdir = "." + java.io.File.separator;


/*
 *  ======== bground ========
 */

function bground(color)
{
    return '<font color="gold" style="background-color: ' + color + '">';
}

/*
 *  ======== bldPkg ========
 *  Take the XML description of a package (from package/package.xml) and
 *  return the following package object:
 *      {
 *          name:       // the dot name of the package
 *          obj:        // the xml package object within xobj
 *          modules:    // array of modules in this package
 *          interfaces: // array of interfaces in this package
 *          priv:       // XML object of bld private state
 *          srcs:       // array of sources and cfgs in this package
 *          rpath:      // package directory path starting from repository
 *          cpath:      // complete package directory path
 *      }
 */

function bldPkg(xobj, baseDirName)
{
    var pkg = {};

    pkg.obj = xobj[0];
    pkg.name = String(pkg.obj.@name);
    
    pkg.srcs = [];
    pkg.exes = [];
    pkg.modules = [];
    pkg.interfaces = [];

    var list = pkg.obj.sources.srcFile;
    for (var i in list) {
        var name = String(list[i].@name);
        if (!/package\//.test(name)) {
            pkg.srcs.push(name);
        }
    }
    list = pkg.obj.configurations.srcFile;
    for (var i in list) {
        pkg.srcs.push(String(list[i].@name));
    }

    list = pkg.obj.units.module;
    for (var i in list) {
        pkg.modules.push(String(list[i].@name));
    }

    list = pkg.obj.units["interface"];
    for (var i in list) {
        pkg.interfaces.push(String(list[i].@name));
    }

    list = pkg.obj.executables.executable;
    for (var i in list) {
        pkg.exes.push({
            pname:      String(list[i].@pname),
            xCfgScript: String(list[i].@xCfgScript),
        });
    }
    
    pkg.obj = undefined;    /* allow XML stuff to be garbage collected */
    
    pkg.rpath = pkg.name.replace(/\./g, '/');

    pkg.modules.sort();
    pkg.interfaces.sort();
    pkg.srcs.sort();

    /* add package meta-sources to end of srcs list */
    for (var i in pkg.modules) {
        pkg.srcs.push(pkg.modules[i] + '.xdc');

        tmp = pkg.modules[i] + '.xs';
        if ((new java.io.File(baseDirName + '/' + tmp)).exists()) {
            pkg.srcs.push(tmp);
        }
    }

    for (var i in pkg.interfaces) {
        pkg.srcs.push(pkg.interfaces[i] + '.xdc');

        var tmp = pkg.interfaces[i] + '.xs';
        if ((new java.io.File(baseDirName + '/' + tmp)).exists()) {
            pkg.srcs.push(tmp);
        }
    }

    var tmp = 'package.xs';
    if ((new java.io.File(baseDirName + '/' + tmp)).exists()) {
        pkg.srcs.push(tmp);
    }

    for (var i in pkg.srcs) {
        pkg.srcs[pkg.srcs[i]] = true;
    }

    return pkg;
}


/*
 *  ======== bullet ========
 */

function bullet (color) {
    var s = '<font size="-2" style="background-color: ' + color + '">' + SP + '</font>' + SP;
    return s;
}


/*
 *  ======== dump ========
 */

function dump (obj, indent) {

    if (obj.$mark) {
        return;
    }

    obj.$mark = true;

    if (typeof obj != 'object') {
        print(indent, obj);
        return;
    }
    for (var i in obj) {
        if (i == '$mark') continue;
        print(indent, i, typeof obj[i]);
        dump(obj[i], indent + '    ');
    }
}    


/*
 *  ======== expand ========
 */

function expand(s)
{
    var res = "";
    var mat;

    while (mat = s.match(/(\t+)(.*)/)) {
        res += s.slice(0, mat.index);
        var cnt = (mat[1].length * 8) - (mat.index % 8);
        for (var i = 0; i < cnt; i++) {
            res += ' ';
        }
        s = mat[2];
    }

    return res + s;
}


/*
 *  ======== findPkgs ========
 *  Find (recursively) all packages starting at dn, make a package object
 *  and add it to pkgTab.
 */
function findPkgs(dn)
{
    print("scanning " + dn + "...");
    
    var dir = java.io.File(dn);

    if (!(dir.isDirectory())) {
        return;
    }

    var file = java.io.File(dn + '/package/package.bld.xml');
    if (file.exists()) {
        var xobj = xdc.loadXML(file.getAbsolutePath());
        var pkg = bldPkg(xobj, dn);
        pkg.cpath = dn;
        if (pkgTab[pkg.rpath] == null) {
            pkgTab[pkg.rpath] = pkg;
        }
    }

    var ls = dir.list();

    for (i in ls) {
	if (ls[i] != "package") {
	    findPkgs(dn + '/' + ls[i]);
	}
    }
}

/*
 *  ======== genBody ========
 */

function genBody(cpath, out)
{
    var suf = cpath.substr(cpath.lastIndexOf('.'));
    var cnt = 1;
    var cflg = false;   // in a comment
    var nflg = false;   // in a note
    var oflg = true;    // in template output
    var strend = null;
    var kws;
    var mode;
    var line;
    var mat;

    switch (suf) {
        case '.xdc':
            kws = KW_XDC + KW_STD;
            mode = 'X';
            break;
        case '.cpp':
        case '.hxx':
            kws = KW_CPP + KW_C + KW_STD;
            mode = 'C';
            break;
        case '.h':
        case '.c':
            kws = KW_C + KW_STD;
            mode = 'C';
            break;
        case '.bld':
            kws = KW_BLD + KW_JS;
            mode = 'J';
            break;
        case '.cfg':
            kws = KW_CFG + KW_JS;
            mode = 'J';
            break;
        case '.xs':
        case '.js':
            kws += KW_JS;
            mode = 'J';
            break;
        case '.xdt':
            kws = KW_JS;
            mode = 'T';
            break;
        default:
            kws = KW_NULL;
            mode = 'N';
    }

    var kwtab = kws.split('|');

    out.write('<pre style="font-size: ' + fontSize + 'pt; line-height: ' + (fontSize + 2) + 'pt; text-align: left">\n');

    var inp;
    try {
        inp = new java.io.BufferedReader(new java.io.FileReader(cpath));
    }
    catch (e) {
        out.write('file not found in this package; it\'s either missing or located in another package.\n');
        out.write(NL);
        out.write('</pre>\n');
        return;
    }

    var linec = 1;
    while (line = inp.readLine()) {

//      print("line " + linec);
        line = expand("" + line);

        line = line.replace(/\&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

        if (cflg) {
            out.write('</font>');
        }

        var marg = '     ';
        marg += cnt++;
        
        out.write(marg.substr(-6) + '    ');
        marg = '    ';

        if (mode == 'N') {          /* unrecognized source file type */
            out.write(marg);
            out.write(line);
            out.write(NL);
            continue;
        }

        else if (mode == 'C') {     /* C/C++ source file or header */
            if (nflg) {
                marg = '<font style="background-color: gold"> </font>   ';
            }
            if (mat = line.match(/^(\s*#\w+)(.*)/)) {
                out.write(marg);
                out.write(mat[1].fontcolor(BROWN).bold());
                line = mat[2];
            }
            else if (mat = line.match(/^\s*\/\/\s*#note\s+(\w+)/)) {
                out.write('<a name="' + mat[1] + '">');
                marg = '<font style="background-color: gold"> </font>   ';
                out.write(marg);
                out.write(line.fontcolor(COMMENT_COLOR));
                out.write(NL);
                nflg = true;
                continue;
            }
            else if (mat = line.match(/^\s*\/\/\s*#end/)) {
                out.write(marg);
                out.write(line.fontcolor(COMMENT_COLOR));
                out.write(NL);
                nflg = false;
                continue;
            }
            else {
                out.write(marg);
            }
        }

        else if (mode == 'T') { /* XDC template file */
            if (mat = line.match(/^(%%\{)/)) {
                out.write(marg);
                out.write(mat[1].fontcolor(BROWN).bold());
                out.write(NL);
                oflg = false;
                continue;
            }
            else if (mat = line.match(/^(%%\})/)) {
                out.write(marg);
                out.write(mat[1].fontcolor(BROWN).bold());
                out.write(NL);
                oflg = true;
                continue;
            }
            else if (mat = line.match(/^(\s*%)(.*)/)) {
                out.write(marg);
                out.write(mat[1].fontcolor(BROWN).bold());
                line = mat[2];
            }
            else if (oflg) {
                marg = '<font style="background-color: silver"> </font>   ';
                out.write(marg);
                line = line.replace(/(`[^`]+`)/g, '<font color=black>$1</font>'); 
                out.write(line.fontcolor(GRAY));
                out.write(NL);
                continue;
            }
            else {
                out.write(marg);
            }
        }

        else {
            out.write(marg);
        }

        if (cflg) {
            out.write('<font color=' + COMMENT_COLOR + '>');
        }

        var tcount = 1;

        var pat = /(xdc\.\w+|\$\w+|[\%\-\w]+|\/\/\/|\/\/|\/\*|\*\/|\"|\')(.*)/;
        var curPat = pat;
        while (mat = line.match(curPat)) {
//          print("     ." + tcount++ + ": " + mat[1]);

            out.write(line.slice(0, mat.index));

            if (cflg && mat[1] == '*/') {
                cflg = false;
                curPat = pat;
                out.write(mat[1] + '</font>');
            }

            else if (cflg && mat[1] != '*/') {
                out.write(mat[1]);
            }

            else if (mat[1] == '/*') {
                cflg = true;
                out.write('<font color=' + COMMENT_COLOR + '>' + mat[1]);
            }

            else if (mat[1] == '///') {
                out.write(('///' + mat[2]).replace(/(@\w+)/g, '<b>$1</b>').fontcolor(TEAL));
                line = "";
                break;
            }

            else if (mat[1] == '//') {
                out.write(('//' + mat[2]).fontcolor(COMMENT_COLOR));
                line = "";
                break;
            }

            else if (strend == null && (mat[1] == '"' || mat[1] == "'")) {
                strend = mat[1];
                curPat = /(\"|\')(.*)/;
                out.write('<font color=' + STRING_COLOR + '>' + mat[1]);
            }

            else if (strend != null && strend == mat[1]) {
                strend = null;
                curPat = pat;
                out.write(mat[1] + '</font>');
            }

            else if (strend != null && strend != mat[1]) {
                out.write(mat[1]);
            }

            else if (mode == 'J' && (mat[1] == 'xdc.loadPackage' || mat[1] == 'xdc.useModule')) {
                out.write(mat[1].fontcolor(BROWN).bold());
            }

            else {
                var id = mat[1];
                for (var i = 0; i < kwtab.length; i++) {
                    if (id == kwtab[i]) {
                        id = id.fontcolor(KEYWORD_COLOR).bold();
                        break;
                    }
                }
                out.write(id);
            }

            line = mat[2];
        }

        linec++;
        out.write(line);
        out.write(NL);
    }

    out.write(NL);
    out.write('</pre>\n');
}

/*
 *  ======== copy ========
 */
function copy(from, to)
{
    var inp = new java.io.BufferedReader(new java.io.FileReader(from));
    var out;
    var line;
    try {
        out = new java.io.FileWriter(to);
    }
    catch (e) {
        (new java.io.File(to.substring(0, to.lastIndexOf('/')))).mkdirs();
        out = new java.io.FileWriter(to);
    }

    while (line = inp.readLine()) {
        out.write(line + '\n');
    }
    inp.close();
    out.close();
}

/*
 *  ======== genExe ========
 */
function genExe(pkg, exe)
{
    print(IND + '    processing ' + exe.pname);
    
    var fn = outdir + pkg.rpath + '/' + exe.pname;

    var out;
    try {
        out = new java.io.FileWriter(fn + '.html');
    }
    catch (e) {
        (new java.io.File(fn.substring(0, fn.lastIndexOf('/')))).mkdirs();
        out = new java.io.FileWriter(fn + '.html');
    }
    
    out.write(DOCTYPE + '<html>\n');
    out.write('<head>\n');
    out.write('<title>' + pkg.rpath + '/' + fn + '</title>\n');
    out.write('</head>\n');

    out.write('<body>\n');

    /* generate exe HTML page contents */
    out.write('<h2 align=center>' + bground(BROWN));
    out.write(SP + pkg.rpath + '/' + exe.xCfgScript + SP + '</font></h2>\n');
    out.write('<hr>\n');

    out.write('<h4 align=left>' + 'GENERATED CONFIGURATION SCRIPT' +'</h4>\n');
    genBody(pkg.cpath + '/' + exe.xCfgScript, out);

    var gname = exe.xCfgScript + '.svg';
    var fname = pkg.cpath + '/' + gname;
    if (java.io.File(fname).exists()) {
        copy(fname, outdir + pkg.rpath + '/' + gname);

        out.write('<hr>\n');
        out.write('<h4 align=left>' + 'CONFIGURATION GRAPH' + '</h4>\n');

        /* embed graph in the current output stream */
        embedSVG(gname, out);
    }

    var xname = exe.xCfgScript + '.xml';
    fname = pkg.cpath + '/' + xname;
    if (java.io.File(fname).exists()) {
        copy(fname, outdir + pkg.rpath + '/' + xname);
        
        out.write('<hr>\n');
        out.write('<h4 align=left>' + 'SEE ALSO' + '</h4>\n');
        out.write('<pre style="font-size: 10pt">\n');

        /* see the generated xml file */
        out.write(bullet(GRAY) + bullet(WHITE) + bullet(WHITE)
            + xname.link(xname) + NL);
    }

    /* close out the exe's page */
    out.write('</body>\n');
    out.write('</html>\n');

    out.close();
}

/*
 *  The method suggested by Graphviz FAQ:
 *
 *  <iframe src="/cgi-bin/webdot/webdot/svgembed.dot.dot.svgz"
 *      width="140" height="280"
 *      frameborder="0" marginwidth="0" marginheight="0">
 *      <object data="/cgi-bin/webdot/webdot/svgembed.dot.dot.svgz"
 *          width="140" height="280"
 *          type="image/svg+xml">
 *              <embed src="/cgi-bin/webdot/webdot/svgembed.dot.dot.svgz"
 *                  type="image/svg+xml"
 *                  coding="gzip"
 *                  palette="foreground">
 *              </embed>
 *      </object>
 *  </iframe>
 */
function embedSVG(fname, out)
{
//    out.write('<object width="100%" height="100%" ');
//    out.write('data="' + fname + '" ');
//    out.write('type="image/svg+xml">');
//    out.write('</object>\n');

//    out.write('<embed width="100%" height="100%" ');
//    out.write('src="' + fname + '" ');
//    out.write('type="image/svg+xml" ');
//    out.write('pluginspage="http://www.adobe.com/svg/viewer/install/" > ');
//    out.write('<noembed>You must have an SVG viewer installed to view this image.</noembed>');
//    out.write('</embed>\n');

    /* should size be = '    width="140" height="280"\n'? */
    var size = '    width="100%" height="100%"\n';
    out.write('<iframe src="'+ fname + '"\n');
    out.write(size);
    out.write('    frameborder="0" marginwidth="0" marginheight="0">\n');
    out.write('    <object data="' + fname + '"\n');
    out.write('    ' + size);
    out.write('        type="image/svg+xml">\n');
    out.write('            <embed src="' + fname + '"\n');
    out.write('                type="image/svg+xml"\n');
    out.write('                palette="foreground">\n');
    out.write('            </embed>\n');
    out.write('    </object>\n');
    out.write('</iframe>\n');
}

/*
 *  ======== genGraph ========
 *  Embed the SVG graph specified by name in of the current output stream.
 *
 *  name    - the relative path to the graph src within pkg.rpath
 *            AND within the current output stream (out)
 */
function genGraph(pkg, out)
{
    var inName = pkg.cpath + '/package/package.rel.svg';
    var outName = outdir + pkg.rpath + '/package/package.rel.svg';
    
    print(IND + 'processing ' + inName);

    /* embed graph in the current output stream */
    embedSVG("package/package.rel.svg", out);

    /* copy graph source to out directory */
    copy(inName, outName);

    /* copy a package.rel.xml to out directory */
    var srcName = inName.replace(/\.rel.svg$/, '.ext.xml');
    var srcFile = new java.io.File(srcName);
    if (!srcFile.exists()) {
        /* package.ext.xml does not exist so it must be package.rel.xml */
        srcName = srcName.replace(/ext.xml$/, 'rel.xml');
    }
    copy(srcName, outName.replace(/\.svg$/, '.xml'));
}

/*
 *  ======== genIndex ========
 */

function genIndex(title, topPkgName)
{
    var out = new java.io.FileWriter(outdir + 'index.html');

    if (title == null) {
        title = 'XPR';
    }

    out.write(DOCTYPE + '<html>\n');
    out.write('<head>\n');
    out.write('<title>' + title + '</title>\n');
    out.write('</head>\n');

    out.write('<frameset cols="20%,*">\n');
    out.write('<frameset rows="33%,*">\n');
    out.write('<frame src="pkgs.html">\n');
    out.write('<frame src="units.html">\n');
    out.write('</frameset>\n');
    out.write('<frame name="main" ');
    if (topPkgName != null) {
        out.write('src="' + pkgTab[topPkgName].rpath + '/package.xdc.html"');
    }
    out.write('>\n');
    
    out.write('</frameset>\n');

    out.write('<body>\n');
    out.write('</body>\n');
    out.write('</html>\n');

    out.close();

    out = new java.io.FileWriter(outdir + 'pkgs.html');

    out.write(DOCTYPE + '<html>\n');
    out.write('<head>\n');
    out.write('<style> a:hover{font-weight:bolder} </style>\n');
    out.write('</head>\n');

    out.write('<body>\n');
    out.write('<h4 align=center>' + 'PACKAGES' + '</h4>\n');
    out.write('<pre style="font-size: 10pt; text-align: left">\n');

    for (var i in pkgList) {
        var pkg = pkgTab[pkgList[i]];
        out.write('<a href="' + pkg.rpath
            + '/package.xdc.html" target="main">' + pkg.name + '</a>' + NL);
    }

    out.write('</pre>\n');
    out.write('</body>\n');
    out.write('</html>\n');

    out.close();

    out = new java.io.FileWriter(outdir + 'units.html');

    out.write(DOCTYPE + '<html>\n');
    out.write('<head>\n');
    out.write('<style> a:hover{font-weight:bolder} </style>\n');
    out.write('</head>\n');

    out.write('<body>\n');
    out.write('<h4 align=center>' + 'MODULES & INTERFACES' + '</h4>\n');
    out.write('<pre style="font-size: 10pt; text-align: left">\n');

    var upTab = {};
    var upList = [];

    for (var n in pkgTab) {
        var pkg = pkgTab[n];
        for (var i in pkg.modules) {
            var up = pkg.name + '.' + pkg.modules[i];
            upTab[up] = '<a href="' + pkg.rpath + '/' + pkg.modules[i] + '.xdc.html" target="main">' + up + '</a>' + NL;
            upList.push(up);
        }
        for (var i in pkg.interfaces) {
            var up = pkg.name + '.' + pkg.interfaces[i];
            upTab[up] = '<a href="' + pkg.rpath + '/' + pkg.interfaces[i] + '.xdc.html" target="main">' + up + '</a>' + NL;
            upList.push(up);
        }
    }

    upList.sort();

    for (var i in upList) {
        out.write(upTab[upList[i]]);
    }

    out.write('</pre>\n');
    out.write('</body>\n');
    out.write('</html>\n');

    out.close();
}


/*
 *  ======== genPkg ========
 */

function genPkg(pkg)
{
    print(IND + 'processing ' + pkg.rpath + '/package.xdc');

    java.io.File(outdir + pkg.rpath + "/package").mkdirs();
    var out = new java.io.FileWriter(outdir + pkg.rpath + '/package.xdc.html');

    out.write(DOCTYPE + '<html>\n');
    out.write('<head>\n');

    out.write('<title>' + pkg.rpath + '/package.xdc' + '</title>\n');
    out.write('</head>\n');

    out.write('<body>\n');
    out.write('<h2 align=center>' + bground(GREEN));
    out.write(SP + pkg.rpath + '/package.xdc' + SP + '</font></h2>\n');

    out.write('<hr>\n');
    genBody(pkg.cpath + '/package.xdc', out);

    /* output links to package sources HTML files */
    out.write('<hr>\n');
    out.write('<h4 align=left>' + 'SOURCE FILES' + '</h4>\n');
    out.write('<pre style="font-size: 10pt; line-height: 12pt; text-align: left">\n');

    for (var i = 0; i < pkg.srcs.length; i++) {
        var mat = pkg.srcs[i].match(/(.*)\.(xdc|cpp|c|cfg|xs)$/);
        var fn;
        if (mat && mat[2] == 'xdc') {
            fn = mat[1] + '.xdc';
            var sflg = null;
            if (pkg.srcs[mat[1] + '.c']) {
                sflg = '.c';
            }
            else if (pkg.srcs[mat[1] + '.cpp']) {
                sflg = '.cpp';
            }
            out.write(bullet(GREEN) + bullet(sflg ? BLUE : WHITE) + bullet(WHITE) + fn.link(fn + '.html'));
            genSrc(pkg, fn);
            if (sflg) {
                fn = mat[1] + sflg;
                out.write(', ' + fn.link(fn + '.html') + NL);
                genSrc(pkg, fn);
            }
            else {
                out.write(NL);
            }
        }
        else if (mat && (mat[2] == 'cpp' || mat[2] == 'c')
            && pkg.srcs[mat[1] + '.xdc']) {
            continue;
        }
        else {
            fn = pkg.srcs[i];
            genSrc(pkg, fn);
            var color = (mat && (mat[2] == 'cfg' || mat[2] == 'tcf')) ? RED : GRAY;
            out.write(bullet(color) + bullet(WHITE) + bullet(WHITE) + fn.link(fn + '.html') + NL);
        }
    }

    /* output build script and package graph (if they exist) */
    var bflg = java.io.File(pkg.cpath + '/package.bld').exists();
    var gflg = java.io.File(pkg.cpath + '/package/package.rel.svg').exists();

    out.write(NL);
    out.write(bflg ? bullet(BROWN) : bullet(WHITE));
    out.write(bullet(WHITE));

    var sp = "";

    if (bflg) {
        var see = [];
        var inName = pkg.cpath + '/package/package.bld.xml';
        if (java.io.File(inName).exists()) {
            var outName = outdir + pkg.rpath + '/package/package.bld.xml';
            copy(inName, outName);
            see = [{name: "package.bld.xml", link: 'package/package.bld.xml'}];
        }
        out.write(sp + 'package.bld'.link('package.bld.html'));
        sp = ", ";
        genSrc(pkg, 'package.bld', see);
    }
    out.write(NL);
    out.write('</pre>\n');

    /* output executables, if there are any in this package */
    if (pkg.exes.length > 0) {
        out.write('<hr>\n');
        out.write('<h4 align=left>' + 'EXECUTABLES' + '</h4>\n');
        out.write('<pre style="font-size: 10pt; line-height: 12pt; text-align: left">\n');

        for (var i = 0; i < pkg.exes.length; i++) {
            var fn = pkg.exes[i].pname;
            out.write(bullet(BROWN) + bullet(WHITE) + bullet(WHITE)
                + fn.link(fn + '.html') + NL);
            genExe(pkg, pkg.exes[i]);
        }
    }
    out.write(NL);
    out.write('</pre>\n');

    if (gflg) {
        out.write('<hr>\n');
        out.write('<h4 align=left>' + 'INTER-PACKAGE GRAPH' + '</h4>\n');
        genGraph(pkg, out);
    }
    
    out.write('</body>\n');
    out.write('</html>\n');

    out.close();
}


/*
 *  ======== genSrc ========
 */

function genSrc(pkg, name, see)
{
    print(IND + 'processing ' + pkg.rpath + '/' + name);

    if (see == null) {
        see = [];
    }
    
    var out;
    try {
        out = new java.io.FileWriter(outdir + pkg.rpath + '/' + name + '.html');
    }
    catch (e) {
        var fn = outdir + pkg.rpath + '/' + name;
        (new java.io.File(fn.substring(0, fn.lastIndexOf('/')))).mkdirs();
        out = new java.io.FileWriter(fn + '.html');
    }
    
    var pre = name.substr(0, name.lastIndexOf('.'));
    var suf = name.substr(name.lastIndexOf('.'));

    out.write(DOCTYPE + '<html>\n');
    out.write('<head>\n');
    out.write('<title>' + pkg.rpath + '/' + name + '</title>\n');
    out.write('</head>\n');

    out.write('<body>\n');

    var bc;

    if (suf == '.xdc') {
        bc = GREEN;
    }
    else if (suf == '.cfg') {
        bc = RED;
    }
    else if (suf == '.bld') {
        bc = BROWN;
    }
    else if (suf == '.cpp' && pkg.srcs[pre + '.xdc']) {
        bc = BLUE;
    }
    else {
        bc = GRAY;
    }

    out.write('<h2 align=center>' + bground(bc));
    out.write(SP + pkg.rpath + '/' + name + SP + '</font></h2>\n');

    out.write('<hr>\n');

    genBody(pkg.cpath + '/' + name, out);

    out.write('<hr>\n');
    out.write('<h4 align=left>' + 'SEE ALSO' + '</h4>\n');
    out.write('<pre style="font-size: 10pt">\n');

    /* compute the relative path to the package "base" directory */
    var up = name.replace(/[^\\\/]*[\\\/]/g, "../");
    up = up.substr(0, up.lastIndexOf('/') + 1);

    /* see the package page (which may be up a few from the current page) */
    out.write(bullet(GREEN) + bullet(WHITE) + bullet(WHITE)
        + (pkg.name).link(up + 'package.xdc.html') + NL);

    /* see the specified other files */
    sp = "";
    for (var i = 0; i < see.length; i++) {
        out.write(bullet(GRAY) + bullet(WHITE) + bullet(WHITE)
            + sp + (see[i].name).link(up + see[i].link));
        sp = ", ";
    }
    out.write(NL);
    
    /* see the top-level source or interface page */
    if (suf == '.xdc' && pkg.srcs[pre + '.cpp']) {
        out.write(bullet(BLUE) + bullet(WHITE) + bullet(WHITE) + (pre + '.cpp').link(pre + '.cpp.html') + NL);
    }
    else if (suf == '.cpp' && pkg.srcs[pre + '.xdc']) {
        out.write(bullet(GREEN) + bullet(WHITE) + bullet(WHITE) + (pre + '.xdc').link(pre + '.xdc.html') + NL);
    }

    out.write('</pre>\n');
    
    out.write('</body>\n');
    out.write('</html>\n');

    out.close();
}

/*
 *  ======== main ========
 */

function main(argv)
{
    var mat;
    var title;
    
    for (var i = 0; i < argv.length; i++) {
        if (mat = argv[i].match(/\-fs\:(.*)/)) {
            fontSize = mat[1] - 0;
            continue;
        }
        if (mat = argv[i].match(/\-od\:(.*)/)) {
            outdir = mat[1] + java.io.File.separator;
            java.io.File(outdir).mkdirs();
            continue;
        }
        if (argv[i] == "-title") {
            title = argv[++i];
            continue;
        }
        findPkgs(argv[i]);
    }
    
    /* copy all package objects into the array pkgList and sort by name */
    for (var n in pkgTab) {
        pkgList.push(n);
    }
    pkgList.sort();
    
    /* generate HTML for each package in pkgList */
    for (var i in pkgList) {
        print('package ' + pkgList[i] + '...');
        genPkg(pkgTab[pkgList[i]]);
    }
    
    genIndex(title, pkgList[0]);
}

/* run command */
main(arguments);
