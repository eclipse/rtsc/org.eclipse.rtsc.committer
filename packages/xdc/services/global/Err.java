/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== Err.java ========
 */
package xdc.services.global;

import xdc.services.intern.xsr.*;




/*
 *  ======== Err ========
 */
public class Err
{

    private static boolean allowExit = true;

    public static void setToolMode(boolean allowExit) {
        Err.allowExit = allowExit;
    }

    public static void raiseFatal(int id, String msg, Object args[]) {
	int i;
	String pkgName = "<pkgName>";

    	/* Here we first need to decide what to print, print it here or
    	 * inform some lower level what should be printed.
    	 * Then, we need to throw something that will not cause any further
    	 * printouts.
    	 */

    	String exceptionContent = "";
    	for (i = 0; i < args.length; i++) {
    	    exceptionContent += "@@" + args[i].toString();
    	}

        if (allowExit) {
            System.err.println("Error " + pkgName + " #" + id + ": " + msg);
            System.exit(1);
        }
        else {
            throw new RuntimeException("Error " + pkgName + " #" + id + ": " + msg);
        }
    }

    

    public static void raise(int id, String msg, Object args[]) {
    	
	int i;
	String pkgName = "<pkgName>";

    	/* Here we first need to decide what to print, print it here or
    	 * inform some lower level what should be printed.
    	 * Then, we need to throw something that will not cause any further
    	 * printouts.
    	 */
    	System.err.println("Error " + pkgName + " #" + id + ": " + msg);
    	String exceptionContent = "";
    	for (i = 0; i < args.length; i++) {
    	    exceptionContent += "@@" + args[i].toString();
    	}
    	throw new RuntimeException(exceptionContent);
    	
        /* First thing to do is to figure out who called. We get to it by
         * getting scope and if it's XS Shell we call getFrame.
         */
        //Scriptable scope = Global.getTopScope();
        //System.out.println(scope.getClass().getName());
        //if (scope.getClass().getName().equals("config.Shell")) {
        //    String frame = ((config.Shell)scope).getFrame(0);
        //    System.out.println(frame);
        //}
        
       
    } 

    public static void abort( Exception e )
    {
       if (allowExit) {
           e.printStackTrace();
           System.exit(2);
       }
        else {
            throw new RuntimeException(e);            
        } 
    }

    public static void exit( Exception e )
    {
        if (allowExit) {
            e.printStackTrace();
            System.err.println("exception: " + e.getClass().getName() + ": " + e);
            System.exit(1);
        }
        else {
            throw new RuntimeException(e);            
        }
    }

    public static void exit( String s )
    {
        if (allowExit) {
            System.err.println("error:  " + s);
            System.exit(1);
        }
        else {
            throw new RuntimeException("error:  " + s);            
        }
    }
}
