/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;

/**
 * Instance of this class is attached to a module when addFxn() is called
 * on Proto.Obj.
 *
 * When Rhino calls get(fxnName) on the parent, an instance of this class is
 * returned.
 */
public class Invoke extends XScriptO
    implements Function
{
    String name;
    Function fxn;
    Proto.Obj pobj;
    Proto.Fxn pfxn;

    static OpTab optab = new OpTab(Invoke.class, new String[] {
            "applyFxn:$apply",
        });

    Invoke( String name, Function fxn, Proto.Obj pobj, Proto.Fxn pfxn )
    {
        this.name = name;
        this.fxn = fxn;
        this.pobj = pobj;
        this.pfxn = pfxn;

        this.bindtab.put("$fxn", fxn);
        this.bindtab.putAll(Invoke.optab);
    }

    public Object applyFxn( Scriptable thisObj, Object argsObj )
    {
        Scriptable args = (Scriptable)argsObj;
        int argc = ((Number)args.get("length", args)).intValue();
        Object[] argv = new Object[argc];

        for (int i = 0; i < argv.length; i++) {
            argv[i] = args.get(i, args);
        }

        Object res = null;
        try {
            res = this.call(Context.getCurrentContext(), Global.getTopScope(),
                            thisObj, argv);
        }
        catch (Exception e) {
            Err.exit(e);
        }

        return res;
    }

    // OVERRIDE Scriptable

    public Object getDefaultValue( java.lang.Class hint )
    {
        return this.fxn.getDefaultValue(hint);
    }

    // interface Function

    public Object call( Context cx, Scriptable scope, Scriptable thisObj,
                        Object[] args ) throws JavaScriptException
    {
        if (!(thisObj instanceof Value.Obj)) {
            Err.exit("'this' must be a typed object");
            return null;
        }

///        Value.Obj vobj = ((Value.Obj)thisObj).castTo(this.pobj); /// for interface default fxns ???
        Value.Obj vobj = ((Value.Obj)thisObj).orig;

        if (this.pfxn.minargc == -1) {
            return this.fxn.call(cx, scope, vobj, args);
        }

        if (args.length < this.pfxn.minargc) {
            Err.exit("too few arguments");
            return null;
        }

        Object oa[] = new Object[this.pfxn.args.length
                                 + (this.pfxn.vflag ? 1 : 0)];

        if (!this.pfxn.vflag && args.length > oa.length) {
            Err.exit("too many arguments");
            return null;
        }

        if (this.pfxn.vflag) {
            int k = args.length - oa.length + 1;
            Object va[] = new Object[k > 0 ? k : 0];
            for (int i = 0; i < va.length; i++) {
                va[i] = Context.javaToJS(args[oa.length + i - 1], null);
            }
            oa[oa.length - 1] = cx.newArray(scope, va);
        }

        for (int i = 0; i < this.pfxn.args.length; i++) {
            Member mbr = this.pfxn.args[i];
            if (i < args.length) {
                if ((oa[i] = mbr.proto.assign(null, args[i])) == Value.ERROR) {
                    Err.exit("wrong type of argument: " + mbr.name);
                    return null;
                }
            }
            else {
                oa[i] = mbr.defval;
            }
        }

        Object res = this.fxn.call(cx, scope, vobj, oa);

        if (this.pfxn.ret == null) {
            return Context.getUndefinedValue();
        }
        else if ((res = this.pfxn.ret.assign(null, res)) == Value.ERROR) {
            Err.exit("wrong type of return value: " + this.name);
            return null;
        }
        else {
            return res;
        }
    }

    public Scriptable construct( Context cx, Scriptable scope, Object[] args )
    {
        Err.exit("'new' operator not supported");
        return null;
    }

}
