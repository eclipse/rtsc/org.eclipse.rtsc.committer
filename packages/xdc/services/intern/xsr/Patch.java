/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;

public class Patch extends XScriptO
    implements Function
{
    Function oldFxn;
    Function newFxn;
    
    Patch( Function oldFxn, Function newFxn )
    {
        this.oldFxn = oldFxn;
        this.newFxn = newFxn;
    }
    
    public Object getDefaultValue( java.lang.Class hint )
    {
        return this.newFxn;
    }

    public Object call( Context cx, Scriptable scope, Scriptable thisObj, Object[] args )
    throws JavaScriptException
    {
        XScriptO xobj = (XScriptO)thisObj;
        
        Object savFxn = xobj.bindtab.get("$oldFxn");
        xobj.bindtab.put("$oldFxn", this.oldFxn);
        
        Object res = this.newFxn.call(cx, scope, xobj, args);
        
        xobj.bindtab.put("$oldFxn", savFxn);
        return res;
    }
    
    public Scriptable construct( Context cx, Scriptable scope, Object[] args )
    {
        Err.exit("'new' operator not supported");
        return null;
    }
}
