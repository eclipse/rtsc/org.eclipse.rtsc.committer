/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
//
// ======== xdc.services.spec.Atom ========
//

package xdc.services.spec;

import java.util.*;

/**
 * The class that represents a token in a specification file.
 */

public class Atom extends org.antlr.runtime.CommonToken
    implements java.io.Serializable
{
	// Static Serialization UID
	static final long serialVersionUID = -9098288411027945023L;
	
    String text;
    String file;
    int line;

    Atom( org.antlr.runtime.Token tok, String file, String text )
    {
        this(tok.getType(), text, file, tok.getLine());
    }
    
    Atom( org.antlr.runtime.Token tok, String file )
    {
        this(tok, file, tok.getText());
    }
    
    Atom( String text ) { this(-1, text, "", 0); }
    Atom( int type, String text, String file, int line  )
    {
        super(type);
        this.text = text;
        this.file = file;
        this.line = line;
    }

    Atom copy()
    {
        return new Atom(this.getType(), this.text, this.file, this.line);
    }

    Atom copy( String text )
    {
        return new Atom(this.getType(), text, this.file, this.line);
    }

    public String getFile() { return this.file; }
    public int getLine() { return this.line; }
    public String getText() { return this.text; }
    
    void replaceText( String text ) { this.text = text; }  
}
