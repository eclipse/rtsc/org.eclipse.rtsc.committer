/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

import java.util.*;

/**
 * Abstract superclass for any formally declared object in a .xdc file.
 */ 
abstract public class Node
    implements Comparable<Node>, java.io.Serializable
{ 
	
    // Static Serialization UID
    static final long serialVersionUID = 6316800378495184391L;
	
    Atom name;
    EnumSet<Qual> quals;
    
    List<Attr> attrs = null;
    Map<String,Attr> attrMap = null;
    Node parent = null;

    String summary = null;
    boolean isNodoc = false;

    transient List<DocComment> docs = null;
    transient XDoc xdoc = null;
    transient Session ses = null;
    
    // attrBool
    public boolean attrBool( String id )
    {
        if (attrMap == null) {
            return false;
        }
        
        Attr a = attrMap.get(id);
        return a == null ? false : Expr.toText(a.val).equals("true");
    }
    
    // attrInt
    public int attrInt( String id )
    {
        if (attrMap == null) {
            return 0;
        }
        
        Attr a = attrMap.get(id);
        if (a == null) {
            return 0;
        }
        
        int res = 0;
        try {
            res = Integer.parseInt(Expr.toText(a.val));
        }
        catch (Exception e) {}
        return res;
    }
    
    // attrString
    public String attrString( String id )
    {
        if (attrMap == null) {
            return null;
        }
        
        Attr a = attrMap.get(id);
        if (a == null) {
            return null;
        }

        String es = Expr.toText(a.val);
        return (es.charAt(0) == '"' || es.charAt(0) == '\'') ? es.substring(1, es.length() - 1) : null;
    }
    
    // bindAttrs
    void bindAttrs( ArrayList<Attr> attrs )
    {
        if (this.attrs == null) {
            this.attrs = attrs;
        }
    }

    // bindDocs
    void bindDocs( ArrayList<DocComment> docs )
    {
        if (this.docs == null) {
            this.docs = docs;
        }
    }
    
    // bindParent
    void bindParent( Node parent )
    {
        if (this.parent == null) {
            this.parent = parent;
            this.ses = parent.getSession();
        }
    }
    
    // bindSession
    void bindSession( Session ses )
    {
        if (this.ses == null) {
            this.ses = ses;
        }
    }

    // compareTo
    public int compareTo( Node n )
    {
        return this.getName().compareTo(n.getName());
    }

    // enterAttrs
    final void enterAttrs( Collection<String> reserved )
    {
        if (this.attrs == null) {
            return;
        }
        
        this.attrMap = new HashMap();
        for (Attr a: this.attrs) {
            if (reserved != null && !(reserved.contains(a.name.text))) {
                ses.msg.error(a.name, "unrecognized attribute");
                continue;
            }
            if (this.attrMap.containsKey(a.name.text)) {
                ses.msg.error(a.name, "attribute is already defined in this scope");
                continue;
            }
            this.attrMap.put(a.name.text, a);
        }
    }
    
    // finalCheck
    void finalCheck()
    {
        if (this.name.text.indexOf('.') != -1) {
            String rid;
            if ((rid = Reserved.lookupPath(this.name.text)) != null) {
                ses.msg.error(this.name, "qualifed name cannot contain reserved word '" + rid + "'");
            }
        }
        else if (Reserved.lookupIdent(this.name.text)) {
            ses.msg.error(this.name, "name cannot be a reserved word");
        }
    }
    
    // getAttrMap
    public final Map<String,Attr> getAttrMap()
    {
        return this.attrMap;
    }
    
    // getChildren
    abstract public List<? extends Node>getChildren();
    
    // getDocs
    public final List<DocComment> getDocs()
    {
        /*
         * Make sure the docs have been parsed. This works around a bug in old
         * spec2 where docs for overridden decls were stored in unparsed form
         * when persisted. parseDocs() is idempotent.
         */
        parseDocs();
        return this.docs;
    }
    
    // getName
    public String getName()
    {
        return this.name.text;
    }
    
    public Atom getAtom()
    {
        return this.name;
    }
    
    // getSummary
    public final String getSummary()
    {
        makeXDoc();
        return this.summary;
    }
    
    // getParent
    public Node getParent()
    {
        return this.parent;
    }

    // getPkgName
    public String getPkgName() {
        /*
         * If can't find a parent, return null. This can happen if
         * the Decl is part of a type specification, for example an
         * argument of a function pointer type.
         */
        if (this.parent == null) {
            return null;
        }

        /* return the name of the package the parent is in */
        return this.parent.getPkgName();
    }

    // getSession
    public Session getSession()
    {
        if (this.ses != null) {
            return this.ses;
        }
        else if (this.parent == null) {
            return null;
        }
        else {
            return this.parent.getSession();
        }
    }
    
    // getQualName
    abstract public String getQualName();
    
    // getXmlTag
    abstract public String getXmlTag();
    
    // hasAttr
    public boolean hasAttr( String id )
    {
        return attrMap != null && attrMap.containsKey(id);
    }
    
    // isExternal
    public final boolean isExternal()
    {
        return !this.quals.contains(Qual.INTERNAL);
    }
    
    // isFinal
    public final boolean isFinal()
    {
        return this.quals.contains(Qual.FINAL);
    }
    
    // isInter
    public final boolean isInter()
    {
        return this.quals.contains(Qual.INTERFACE);
    }
    
    // isInternal
    public final boolean isInternal()
    {
        return this.quals.contains(Qual.INTERNAL);
    }
    
    // isInst
    public final boolean isInst()
    {
        return this.quals.contains(Qual.INSTANCE);
    }
    
    // isMeta
    public final boolean isMeta()
    {
        return this.quals.contains(Qual.METAONLY);
    }
    
    // isMod
    public final boolean isMod()
    {
        return this.quals.contains(Qual.MODULE);
    }
    
    // isNodoc
    public final boolean isNodoc()
    {
        makeXDoc();
        return this.isNodoc;
    }
    
    // isOver
    public final boolean isOver()
    {
        return this.quals.contains(Qual.OVERRIDE);
    }
    
    // isReadonly
    public final boolean isReadonly()
    {
        return this.quals.contains(Qual.READONLY);
    }
    
    // isStatic
    public final boolean isStatic()
    {
        return !this.quals.contains(Qual.INSTANCE);
    }
    
    // isSys
    public final boolean isSys()
    {
        return this.quals.contains(Qual.SYSTEM) || this.attrBool(Attr.A_System);
    }
    
    // makeXDoc
    public final XDoc makeXDoc()
    {
        if (this.xdoc == null) {
            parseXDoc();
            this.summary = this.xdoc.getSummary();
            this.isNodoc = this.xdoc.isNodoc();
        }

        return this.xdoc;
    }

    // parseXDoc
    void parseXDoc() {
        this.xdoc = new XDoc(getDocs(), null, summary, isNodoc);
    }
    
    // parseDocs
    void parseDocs()
    {
        /* check whether the docs have already been parsed */
        if (this.docs != null && !DocComment.isParsed(this.docs)) {
            /* and parse them, if not */
            this.docs = DocComment.parse(this.docs);
        }
    }
}
