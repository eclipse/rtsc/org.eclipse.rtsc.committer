/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

test();

function test()
{
    var ofdPath = "C:\\CCStudio_v3.3\\C6000\\cgtools\\bin\\ofd6x.exe";
    //var ofdPath = "/db/toolsrc/library/tools/vendors/ti/c6x/6.0.7/Linux/bin/ofd6x";
    
    //var dumpFile = "Z:\\cmcc\\avala-g10x\\src\\ti\\sysbios\\knl\\tests\\whole_program\\TaskTest1.x64_dump.xml";
    //var dumpFile = "Z:\\cmcc\\avala-g10x\\src\\ti\\sysbios\\ipc\\tests\\whole_program\\SemaphoreTest3.x64_dump.xml";
    var dumpFile = "Z:\\cmcc\\avala-g10x\\src\\ti\\sysbios\\knl\\tests\\whole_program\\TaskTest3.x64_dump.xml";
    //var dumpFile = "/db/ztree/cmcc/avala-g10x/src/ti/sysbios/knl/tests/whole_program/TaskTest1.x64_dump.xml";
    
    //var executable = "Z:\\cmcc\\avala-g10x\\src\\ti\\sysbios\\knl\\tests\\whole_program\\TaskTest1.x64";    
    //var executable = "Z:\\cmcc\\avala-g10x\\src\\ti\\sysbios\\ipc\\tests\\whole_program\\SemaphoreTest3.x64";    
    var executable = "Z:\\cmcc\\avala-g10x\\src\\ti\\sysbios\\knl\\tests\\whole_program\\TaskTest3.x64";    
    //var executable = "/db/ztree/cmcc/avala-g10x/src/ti/sysbios/knl/tests/whole_program/TaskTest1.x64"; 

    //var factory = xdc.jre.javax.xml.parsers.DocumentBuilderFactory.newInstance();

    print('Creating IMemoryReader Instance');
    var MemoryImage = xdc.useModule('xdc.rov.CoreDumpImage');
    var memInst = MemoryImage.create(dumpFile);

    print('Creating ISymbolReader Instance');
    var SymbolTable = xdc.useModule('xdc.rov.SymbolsViaOfd');
    var symInst = SymbolTable.create(executable, ofdPath);

    print('Calling Model Start');
    var Model = xdc.useModule('xdc.rov.Model');
    Model.start(executable, symInst, memInst);
    print('Done with Model Start');    


    //var isr = new xdc.jre.java.io.InputStreamReader(xdc.jre.java.lang.System["in"]);
    //var stdin = new xdc.jre.java.io.BufferedReader(isr);
    
    while (true) {
        
        var input = "m ti.sysbios.knl.Task Detailed";
        //var input = "lm";
        
        /* Clear Cache */
        if (input == "cc") {
            Program.resetMods();
        }
        /* List modules */
        else if (input == "lm") {
            print("Listing modules...");
            var moduleList = listModules();
            print(moduleList.toString());
            //print(moduleList.toString());
        }
        /* 
         * Retrieve Module (m is 109 in Java) 
         * 
         * Module requests have the form:
         *   m <module name> <tab name>
         *   m ti.sysbios.knl.Task Detailed
         *
         * The tab name comes last so that it may contain spaces.
         */
        else if (input.charAt(0) == 'm') {
            print("RovServer received request: " + input);
            var modName = input.substring(2);
            
            /* Find the index of the space after the module name */
            var index = input.indexOf(' ', 3);
     
            var modName = input.substring(2, index);
            var tabName = input.substring(index + 1, input.length);
            
            print("Retrieving:    tab " + tabName + "     for module " + modName);
            var view = new XML();
            
            var viewType = Program.getViewType(modName, tabName);
            
            switch (viewType) {
                case "module":
                    view = retrieveModuleView(modName, tabName);
                    break;
                case "instance":
                    view = retrieveInstanceView(modName, tabName);
                    break;
                case "raw":
                    view = retrieveRawView(modName);
                    break;
                default:
                    throw (new Error("Undefined view type!"));
                    break;
            }
            print(view.toString());
        }
        /* Unrecognized command */
        else if (input == "q") {
            break;
        }
        else {
            print("Unrecognized command");
        }
        
        break;
    }
}

/* 
 *  ======== listModules ======== 
 *  Lists the packages and modules in the system using an XML
 *  format.
 */
function listModules()
{
    var root = new XML('<ModuleList></ModuleList>');
    var elem;
    
    /* For each module in the system... */
    for (var i = 0; i < Program.moduleNames.length; i++) {
        /* Always start back at the root for each new module */
        elem = root;
                
        /* Break the module name into packages */
        var names = Program.moduleNames[i].split(".");
        
        for (var j = 0; j < names.length; j++) {
            
            /* The pkg and module names should include the parents */
            var pkgName = getPackageName(names, j);
            
            /* Create the package if it doesn't exist already */
            if (elem[pkgName].toString() == "") {
                elem[pkgName] = new XML('<' + pkgName + '/>');
                
                /* Set the package's type, package or module. */
                if (j == (names.length - 1)) {
                    elem[pkgName].@type = "module";
                    addViewList(elem[pkgName], pkgName);
                }
                else {
                    elem[pkgName].@type = "package";
                }
            }
            
            /* Move the elem down to the next package */
            elem = elem[pkgName];
        }
    }
    
    return(root);
}

/*
 *  ======== getPackageName ========
 *  Takes the array of names created by splitting a module's name
 *  by the periods '.' and finds the package name referenced at
 *  the given index.
 *  For example, for the module xdc.runtime.HeapStd, index 1 will return 
 *  "xdc.runtime".
 */
function getPackageName(names, index)
{
    var pkgName = "";
    for (var i = 0; i <= index; i++) {
        if (i == 0) {
            pkgName += names[i];
        }
        else {
            pkgName += "." + names[i];
        }
    }
    return(pkgName);
}

/*
 *  ======== addViewList ========
 *  Adds the list of supported views to the XML element.
 */
function addViewList(elem, modName)
{
    var tabs = Program.getSupportedTabs(modName);
    
    if (tabs.length != 0) {
        elem.view = new XML("<view/>");
     
        for (var i = 0; i < tabs.length; i++) {
            elem.view[i] = tabs[i];
        }
    }
}

/*
 *  ======== retrieveModuleView ========
 *
 */
function retrieveModuleView(modName, tabName)
{
    var mod;
    
    try {
        mod = Program.scanModuleView(modName, tabName);
    } catch (e) {
        if (e.message == "abort") {
            return(new XML("<abort/>"));
        }
    }
        
    /*
     * The final result has the form:
     * <Module> <-- tab name
     *   <ti.sysbios.knl.Task> <-- mod name
     *     <ti.sysbios.knl.Task.Module_View> <-- mod view
     *       <schedulerLocked>
     */
    var viewXML = viewToXML(mod.$viewMap[tabName]);
    
    var modRoot = new XML('<' + modName + '/>');
    modRoot[viewXML.name()] = viewXML;
    
    var tabRoot = new XML('<' + tabName + '/>');
    tabRoot[modName] = modRoot;
    
    return (tabRoot);
   
}

/*
 *  ======== retrieveInstanceView ========
 *
 */
function retrieveInstanceView(modName, tabName)
{
    var mod;
    
    try {
        mod = Program.scanInstanceView(modName, tabName);
    } catch (e) {
        if (e.message == "abort") {
            return(new XML("<abort/>"));
        }
        else {
            throw (e);
        }
    }
        
    var root = new XML('<' + modName + '/>');
    
    // Get the instance views, add them to the XML output
    for (var i in mod.$instances) {
        var view = mod.$instances[i].$viewMap[tabName];
        
        var viewXml = viewToXML(view);
        
        /* The first time around... */
        if (i == 0) {
            root[viewXml.name()] = viewXml;
            //elem = new XML('<' + viewXml.name() + '/>');
            //viewXml[viewXml.name()] = elem;
        }
        
        root[viewXml.name()][i] = viewXml;
        
    }
    
    /* Add the module root underneath the tab name root */
    var tabRoot = new XML('<' + tabName + '/>');
    tabRoot[modName] = root;
    
    return (tabRoot);
    
    // Get the embedded object views, add them to the XML output
    /*
    for each (var obj in mod.$objects) {
        if (obj.$view) {
            print(obj.$view.$xml().toString());
        }
    }
    */
}

/*
 *  ======== retrieveRawView ========
 *
 */
function retrieveRawView(modName)
{
    var mod = Program.scanRawView(modName);
    
    var rawView = new java.lang.StringBuilder();
    
    rawView.append('<Raw>\n');
    rawView.append('<' + modName + '>\n');
    
    for (var i in mod.$instances) {
        var obj = mod.$instances[i].$object;
        
        var instView = rawToXML(obj);
        
        rawView.append(instView);        
    }
    
    rawView.append('</' + modName + '>\n');
    rawView.append('</Raw>\n');
    
    return(String(rawView));
}

/*
 *  ======== viewToXML ========
 *
 */
function viewToXML(view) {
    var viewXml = new XML('<' + view.$type + '/>');    
    
    for (var p in view) {
        /* If the status is undefined, don't include this field. */
        if (view.$status[p] === undefined) {
            continue;
        }
        /* If the status is null, don't do anything special. */
        else if (view.$status[p] == null) {
            if ((view[p]) && (view[p].$category == 'Vector')) {
                var array = new XML('<' + p + '/>');
                for (var i = 0; i < view[p].length; i++) {
                    var index = p + "-" + i;
                    array[index] = view[p][i];
                }
 
                viewXml[p] = array;
            }
            else {
                viewXml[p] = view[p];
            }
        }
        /* Otherwise, the status contains an error message. */
        else {
            viewXml[p] = view[p];
            viewXml[p].@error = view.$status[p];
        }
    }
    
    return(viewXml);
}

/* 
function rawToXML(view) {
    var viewXml = new XML('<' + view.$type + '/>');    
    
    for (var p in view) {
        if ((view[p]) && (view[p].$category == 'Vector')) {
            var array = new XML('<' + p + '/>');
            for (var i = 0; i < view[p].length; i++) {
                var index = p + "-" + i;
                array[index] = view[p][i];
            }
 
            viewXml[p] = array;
        }
        else {
            viewXml[p] = view[p];
        }
    }
    
    return(viewXml);
}
*/



function rawToXML(obj)
{
    var r = new java.lang.StringBuilder();

    r.append('<' + obj.$type + '>');
    xmlS(r, obj);
    r.append('</' + obj.$type + '>');

    return String(r);
}

function xmlE( r, f, e, k )
{
    if (e && typeof e == 'object' && '$category' in e) {
        switch (e.$category) {
        case 'Struct':
            r.append('<' + f + key(k) + '>\n');
            xmlS(r, e);
            r.append('</' + f + '>\n');
            break;
        case 'Vector':
            r.append('<' + f + '-length>' + e.length + '</' + f + '-length>\n');
            xmlV(r, f, e);
            break;
        case 'Map':
            r.append('<' + f + '-length>' + e.length + '</' + f + '-length>\n');
            xmlM(r, f, e);
            break;
        case 'Addr':
        case 'EnumVal':
            r.append('<' + f + key(k) + '>' + e.$name + '</' + f + '>\n');
            break;
        }
    }
    else {
        r.append('<' + f + key(k) + '>' + e + '</' + f + '>\n');
    }
}

function xmlS( r, s )
{
    for (var f in s ) {
        xmlE(r, f, s[f]);
    }
}

function xmlV( r, f, v )
{
    for (var i = 0; i < v.length; i++) {
        xmlE(r, f, v[i]);
    }
}

function xmlM( r, f, m )
{
    var keys = m.$keys;

    for (var i = 0; i < keys.length; i++) {
        var k = keys[i];
        xmlE(r, f, m[k], k);
    }
}

function key( k )
{
    return k ? (' key="' + k + '"') : '';
}


