/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.gen;

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import xdc.services.global.*;
import xdc.services.intern.xsr.*;
import xdc.services.spec.*;

public class JClass
{
    static final String K_MOD = "Module";
    static final String K_INS = "Instance";
    static final String K_OBJ = "Object";
    static final String K_PRM = "Params";

    private String clsname;
    private String curpkg;
    private String curpre;
    private String curbase;

    private List<Decl.Config> createList = new ArrayList();
    private Glob glob = new Glob();
    
    // compile
    public static void compile( String jfile )
    {
        String cfile = jfile.substring(0, jfile.lastIndexOf(".java")) + ".class";
        new java.io.File(cfile).delete();
        
        String cp = System.getProperty("java.class.path");
        String sep = System.getProperty("path.separator"); 

        cp += sep + System.getProperty("xdc.root")
            + "/packages/xdc/services/intern/xsr/java/package.jar";
        cp += sep + System.getProperty("xdc.root")
            + "/packages/xdc/services/spec/java/package.jar";
        cp += sep + System.getProperty("xdc.root")
            + "/packages/xdc/shelf/java/js.jar";

        /*
         * The non-static compile(String[]) method is more work to call than
         * static compile(String), but avoids argument parsing problems caused
         * by spaces in the class path and file name.
         */
        org.eclipse.jdt.internal.compiler.batch.Main compiler =
            new org.eclipse.jdt.internal.compiler.batch.Main(
                new PrintWriter(System.out), new PrintWriter(System.err),
                false);
        String args[] = new String[] {"-1.5", "-nowarn", "-cp", cp, jfile};
        if (!compiler.compile(args)) {
            xdc.services.global.Err.exit("internal java compilation error");
        }
    }
    
    // gen
    public void gen( Pkg pkg, Out out )
    {
        curpkg = pkg.getName();
        curpre = curpkg.replace('.', '$');
        curbase = pkg.getBaseDir();
        clsname = curpkg.replace('.', '_');
        
        glob.mode = Glob.JCLMODE;
        
        glob.out = out;
        
        glob.genWarning();
        
        glob.out.printf("import java.util.*;\n");
        glob.out.printf("import org.mozilla.javascript.*;\n");
        glob.out.printf("import xdc.services.intern.xsr.*;\n");
        glob.out.printf("import xdc.services.spec.*;\n");
        glob.out.printf("\n");
        
        glob.out.printf("public class %1\n", clsname);
        glob.out.printf("{%+\n");

        glob.out.printf("%tstatic final String VERS = \"%1\\n\";\n", Glob.vers());
        glob.out.printf("\n");
        glob.out.printf("%tstatic final Proto.Elm $$T_Bool = Proto.Elm.newBool();\n");
        glob.out.printf("%tstatic final Proto.Elm $$T_Num = Proto.Elm.newNum();\n");
        glob.out.printf("%tstatic final Proto.Elm $$T_Str = Proto.Elm.newStr();\n");
        glob.out.printf("%tstatic final Proto.Elm $$T_Obj = Proto.Elm.newObj();\n");
        glob.out.printf("\n");
        glob.out.printf("%tstatic final Proto.Fxn $$T_Met = new Proto.Fxn(null, null, 0, -1, false);\n");
        glob.out.printf("%tstatic final Proto.Map $$T_Map = new Proto.Map($$T_Obj);\n");
        glob.out.printf("%tstatic final Proto.Arr $$T_Vec = new Proto.Arr($$T_Obj);\n");
        glob.out.printf("\n");
        glob.out.printf("%tstatic final XScriptO $$DEFAULT = Value.DEFAULT;\n");
        glob.out.printf("%tstatic final Object $$UNDEF = Undefined.instance;\n");
        glob.out.printf("\n");
        glob.out.printf("%tstatic final Proto.Obj $$Package = (Proto.Obj)Global.get(\"$$Package\");\n");
        glob.out.printf("%tstatic final Proto.Obj $$Module = (Proto.Obj)Global.get(\"$$Module\");\n");
        glob.out.printf("%tstatic final Proto.Obj $$Instance = (Proto.Obj)Global.get(\"$$Instance\");\n");
        glob.out.printf("%tstatic final Proto.Obj $$Params = (Proto.Obj)Global.get(\"$$Params\");\n");
        glob.out.printf("\n");
        glob.out.printf("%tstatic final Object $$objFldGet = Global.get(\"$$objFldGet\");\n");
        glob.out.printf("%tstatic final Object $$objFldSet = Global.get(\"$$objFldSet\");\n");
        glob.out.printf("%tstatic final Object $$proxyGet = Global.get(\"$$proxyGet\");\n");
        glob.out.printf("%tstatic final Object $$proxySet = Global.get(\"$$proxySet\");\n");
        glob.out.printf("%tstatic final Object $$delegGet = Global.get(\"$$delegGet\");\n");
        glob.out.printf("%tstatic final Object $$delegSet = Global.get(\"$$delegSet\");\n");
        glob.out.printf("\n");
        glob.out.printf("%tScriptable xdcO;\n");
        glob.out.printf("%tSession ses;\n");
        glob.out.printf("%tValue.Obj om;\n");
        glob.out.printf("\n");
        glob.out.printf("%tboolean isROV;\n");
        glob.out.printf("%tboolean isCFG;\n");
        glob.out.printf("\n");
        glob.out.printf("%tProto.Obj pkgP;\n");
        glob.out.printf("%tValue.Obj pkgV;\n");
        glob.out.printf("\n");
        glob.out.printf("%tArrayList<Object> imports = new ArrayList<Object>();\n");
        glob.out.printf("%tArrayList<Object> loggables = new ArrayList<Object>();\n");
        glob.out.printf("%tArrayList<Object> mcfgs = new ArrayList<Object>();\n");
        glob.out.printf("%tArrayList<Object> proxies = new ArrayList<Object>();\n");
        glob.out.printf("%tArrayList<Object> sizes = new ArrayList<Object>();\n");
        glob.out.printf("%tArrayList<Object> tdefs = new ArrayList<Object>();\n");
        glob.out.printf("\n");
        
        glob.out.printf("%tvoid $$IMPORTS()\n");
        glob.out.printf("%t{%+\n");
        genIncs(pkg);
        glob.out.printf("%-%t}\n\n");

        glob.out.printf("%tvoid $$OBJECTS()\n");
        glob.out.printf("%t{%+\n");
        genPkgObjects(pkg);
        glob.out.printf("%-%t}\n\n");
        for (Unit u : pkg.getUnits()) {
            glob.out.printf("%tvoid %1$$OBJECTS()\n", u.getName());
            glob.out.printf("%t{%+\n");
            genUnitObjects(u);
            glob.out.printf("%-%t}\n\n");
        }

        for (Unit u : pkg.getUnits()) {
            glob.out.printf("%tvoid %1$$CONSTS()\n", u.getName());
            glob.out.printf("%t{%+\n");
            genUnitConsts(u);
            glob.out.printf("%-%t}\n\n");
        }

        for (Unit u : pkg.getUnits()) {
            glob.out.printf("%tvoid %1$$CREATES()\n", u.getName());
            glob.out.printf("%t{%+\n");
            genUnitCreates(u);
            glob.out.printf("%-%t}\n\n");
        }

        for (Unit u : pkg.getUnits()) {
            glob.out.printf("%tvoid %1$$FUNCTIONS()\n", u.getName());
            glob.out.printf("%t{%+\n");
            genUnitFxns(u);
            glob.out.printf("%-%t}\n\n");
        }

        for (Unit u : pkg.getUnits()) {
            glob.out.printf("%tvoid %1$$SIZES()\n", u.getName());
            glob.out.printf("%t{%+\n");
            genUnitSizes(u);
            glob.out.printf("%-%t}\n\n");
        }

        for (Unit u : pkg.getUnits()) {
            glob.out.printf("%tvoid %1$$TYPES()\n", u.getName());
            glob.out.printf("%t{%+\n");
            genUnitTypes(u);
            glob.out.printf("%-%t}\n\n");
        }
        
        for (Unit u : pkg.getUnits()) {
            glob.out.printf("%tvoid %1$$ROV()\n", u.getName());
            glob.out.printf("%t{%+\n");
            genRovUnit(u);
            glob.out.printf("%-%t}\n\n");
        }
        
        glob.out.printf("%tvoid $$SINGLETONS()\n");
        glob.out.printf("%t{%+\n");
        genPkgValues(pkg);
        glob.out.printf("%-%t}\n\n");
        for (Unit u : pkg.getUnits()) {
            glob.out.printf("%tvoid %1$$SINGLETONS()\n", u.getName());
            glob.out.printf("%t{%+\n");
            genUnitValues(u);
            glob.out.printf("%-%t}\n\n");
        }
        
        glob.out.printf("%tvoid $$INITIALIZATION()\n");
        glob.out.printf("%t{%+\n");
        genPkgInit(pkg);
        glob.out.printf("%-%t}\n\n");

        glob.out.printf("%tpublic void exec( Scriptable xdcO, Session ses )\n");
        glob.out.printf("%t{%+\n");

        glob.out.printf("%tthis.xdcO = xdcO;\n");
        glob.out.printf("%tthis.ses = ses;\n");
        glob.out.printf("%tom = (Value.Obj)xdcO.get(\"om\", null);\n");
        glob.out.printf("\n");
        
        glob.out.printf("%tObject o = om.geto(\"$name\");\n");
        glob.out.printf("%tString s = o instanceof String ? (String)o : null;\n");
        glob.out.printf("%tisCFG = s != null && s.equals(\"cfg\");\n");
        glob.out.printf("%tisROV = s != null && s.equals(\"rov\");\n");
        glob.out.printf("\n");

        glob.out.printf("%t$$IMPORTS();\n");
        glob.out.printf("%t$$OBJECTS();\n");
        for (Unit u : pkg.getUnits()) glob.out.printf("%t%1$$OBJECTS();\n", u.getName());
        for (Unit u : pkg.getUnits()) glob.out.printf("%t%1$$CONSTS();\n", u.getName());
        for (Unit u : pkg.getUnits()) glob.out.printf("%t%1$$CREATES();\n", u.getName());
        for (Unit u : pkg.getUnits()) glob.out.printf("%t%1$$FUNCTIONS();\n", u.getName());
        for (Unit u : pkg.getUnits()) glob.out.printf("%t%1$$SIZES();\n", u.getName());
        for (Unit u : pkg.getUnits()) glob.out.printf("%t%1$$TYPES();\n", u.getName());
        genGuardBeg("ROV", true);
        for (Unit u : pkg.getUnits()) glob.out.printf("%t%1$$ROV();\n", u.getName());
        genGuardEnd("ROV", true);
        glob.out.printf("%t$$SINGLETONS();\n");
        for (Unit u : pkg.getUnits()) glob.out.printf("%t%1$$SINGLETONS();\n", u.getName());
        glob.out.printf("%t$$INITIALIZATION();\n");
        
        glob.out.printf("%-%t}\n");
        glob.out.printf("%-%t}\n");
    }
    
    // genArr
    void genArr( String code, Type.Array tarr )
    {
        glob.out.printf("new Proto.Arr(");
        genProto(code.substring(1), tarr.getBase());
        glob.out.printf(", %1", code.startsWith("V") ? "true" : "false");
        if (tarr.getDim() != null) {
            glob.out.printf(", xdc.services.intern.xsr.Enum.intValue(");
            glob.genExpr(tarr.getDim());
            glob.out.printf(")");
        }
        glob.out.printf(")");
    }
    
    // genAttrs
    void genAttrs( Node n )
    {
        String var = n instanceof Pkg ? "pkgV" : "vo";
        glob.out.printf("%tValue.Map atmap = (Value.Map)%1.getv(\"$attr\");\n", var);

        if (n.getAttrMap() != null) {
            for (Attr a : n.getAttrMap().values()) {
                glob.out.printf("%tatmap.setElem(\"%2\", ", a.toText());
                if (a.getVal() == null) {
                    glob.out.printf("true");
                }
                else {
                    glob.genExpr(a.getVal());
                }
                glob.out.printf(");\n");
            }
        }

        glob.out.printf("%tatmap.seal(\"length\");\n", var);
    }

    // genBanner
    void genBanner( Node n )
    {
        String xs = (n instanceof Unit || n instanceof Pkg) ? "" : (n.getParent().getName() + '.');
        glob.out.printf("%t// %1 %2%3\n", n.getXmlTag(), xs, n.getName()); 
    }

    // genCreate
    void genCreate( Unit unit, boolean iflg )
    {
        genGuardBeg("CFG", !unit.isMeta());
        
        String qn = unit.getQualName();
        String sp;
        String NL = "\\n\");\n";
        
        if (iflg && !unit.isMeta()) {
            glob.out.printf("%tsb = new StringBuilder();\n");
            glob.out.printf("%tsb.append(\"%1$%2$$__initObject = function( inst ) {%3%+", curpre, unit.getName(), NL);
            /* If __mod.$used is false and we made it to instance create(), it
             * means no one called xdc.useModule() for __mod, and
             * modules$static$init wasn't called.
             */
            glob.out.printf("%tsb.append(\"if (!this.$used) {%1%+", NL);
            glob.out.printf("%tsb.append(\"throw new Error(\\\"Function %1.create() called before xdc.useModule('%1')\\\");%2", qn, NL);
            glob.out.printf("%-%tsb.append(\"}%1", NL);
            if (!unit.isProxy()) {
                String ns = "inst.instance.name";
                glob.out.printf("%tsb.append(\"var name = xdc.module('xdc.runtime.Text').defineRopeCord(%1);%2", ns, NL);
                glob.out.printf("%tsb.append(\"inst.$object.$$bind('__name', name);%1", NL);
                glob.out.printf("%tsb.append(\"this.instance$static$init.$fxn.apply(inst, [inst.$object, ");
                if (unit.getCreator() != null) {
                    for (Decl.Arg a : unit.getCreator().getArgs()) {
                        glob.out.printf("inst.$args.%1, ", a.getName());
                    }
                }
                glob.out.printf("inst, inst.$module]);%1", NL);
                /* seal all static instances */
                glob.out.printf("%tsb.append(\"inst.$seal();%1", NL);
            }
            glob.out.printf("%-%tsb.append(\"};%1", NL);
            glob.out.printf("%tGlobal.eval(sb.toString());\n");
        }
    
        String fn = iflg ? "create" : "construct";

        glob.out.printf("%tfxn = (Proto.Fxn)om.bind(\"%1$$%2\", new Proto.Fxn(", qn, fn);
        glob.out.printf("om.findStrict(\"%1.Module\", \"%2\"), ", qn, curpkg);
        
        if (iflg) {
            glob.out.printf("om.findStrict(\"%1.Instance\", \"%2\"), ", qn, curpkg);
        }
        else {
            glob.out.printf("null, ");
        }

        int argc = iflg ? 1 : 2;

        if (unit.getCreator() == null) {
            glob.out.printf("%1, 0, false));\n", argc);
        }
        else {
            argc += unit.getCreator().getArgs().size();
            glob.out.printf("%1, %2, false));\n", argc, unit.getCreator().getMinArgc());
        }
        
        if (!iflg) {
            glob.out.printf("%t%tfxn.addArg(0, \"__obj\", "
                + "(Proto)om.findStrict(\"%1$$Object\", \"%2\"), null);\n",
                qn, curpkg);
        }
        
        if (unit.getCreator() != null) {
            genFxnArgs(unit.getCreator(), iflg ? 0 : 1);
        }
        
        glob.out.printf("%t%tfxn.addArg(%1, \"__params\", "
            + "(Proto)om.findStrict(\"%2.Params\", \"%3\"), Global.newObject());\n",
            argc - 1, qn, curpkg);
        

        glob.out.printf("%tsb = new StringBuilder();\n");
        glob.out.printf("%tsb.append(\"%1$%2$$%3 = function(", curpre, unit.getName(), fn);
        
        sp = " ";
        if (!iflg) {
            glob.out.printf(" __obj");
            sp = ", ";
        }
        glob.genCreArgNames(unit, sp);
        glob.out.printf("__params ) {%1%+", NL);

        glob.out.printf("%tsb.append(\"var __mod = xdc.om['%1'];%2", qn, NL);

        if (iflg) {
            glob.out.printf("%tsb.append(\"var __inst = xdc.om['%1.Instance'].$$make();%2", qn, NL);
            glob.out.printf("%tsb.append(\"__inst.$$bind('$package', xdc.om['%1']);%2", curpkg, NL);
            glob.out.printf("%tsb.append(\"__inst.$$bind('$index', __mod.$instances.length);%1", NL);
            glob.out.printf("%tsb.append(\"__inst.$$bind('$category', 'Instance');%1", NL);
        }
        else {
            glob.out.printf("%tsb.append(\"var __inst = __obj;%1", NL);
        }

        glob.out.printf("%tsb.append(\"__inst.$$bind('$args', {");
        if (unit.getCreator() != null) {
            sp = "";
            for (Decl.Arg a : unit.getCreator().getArgs()) {
                glob.out.printf("%1%2:%2", sp, a.getName());
                sp = ", ";
            }
        }
        glob.out.printf("});%1", NL);

        glob.out.printf("%tsb.append(\"__inst.$$bind('$module', __mod);%1", NL);
//        glob.out.printf("%t__inst.$$bind('$private', {});\n");
        String ns = iflg ? "#I" : "#O";
        String ts = iflg ? "$instances" : "$objects";
        glob.out.printf("%tsb.append(\"__mod.%1.$add(__inst);%2", ts, NL);
        
        if (!unit.isMeta()) {
            glob.out.printf("%tsb.append(\"__inst.$$bind('$object', ");
            if (iflg) {
                glob.out.printf("new xdc.om['%1'].Instance_State);%2", qn, NL);
            }
            else {
                glob.out.printf("xdc.om['%1'].Instance_State.$$make(__inst.$$parent, __inst.$name));%2", qn, NL);
            }
        }
        
        if (unit.isProxy()) {
            glob.out.printf("%tsb.append(\"if (!__mod.delegate$) {%+%1", NL);
            glob.out.printf("%tsb.append(\"throw new Error(\\\"Unbound proxy module: %1\\\");%2", qn, NL);
            glob.out.printf("%-%tsb.append(\"}%1", NL);
            glob.out.printf("%tsb.append(\"var __dmod = __mod.delegate$.$orig;%1", NL);
            glob.out.printf("%tsb.append(\"var __dinst = __dmod.create(");
            glob.genCreArgNames(unit, "");
            glob.out.printf("__params);%1", NL);
            glob.out.printf("%tsb.append(\"__inst.$$bind('delegate$', __dinst);%1", NL);
        }
        else {
            for (Decl d : unit.getDecls()) {
                if (d instanceof Decl.Config && d.isInst() && ((Decl.Config) d).getInit() == null) {
                    glob.out.printf("%tsb.append(\"__inst.%1 = __mod.PARAMS.%1;%2", d.getName(), NL);
                }
            }
            glob.out.printf("%tsb.append(\"for (__p in __params) __inst[__p] = __params[__p];%1", NL);
        }
        
        if (iflg) {
            glob.out.printf("%tsb.append(\"var save = xdc.om.$curpkg;%1", NL);
            glob.out.printf("%tsb.append(\"xdc.om.$$bind('$curpkg', __mod.$package.$name);%1", NL);
            glob.out.printf("%tsb.append(\"__mod.instance$meta$init.$fxn.apply(__inst, [", qn);
            if (unit.getCreator() != null) {
                sp = "";
                for (Decl.Arg a : unit.getCreator().getArgs()) {
                    glob.out.printf("%1%2", sp, a.getName());
                    sp = ", ";
                }
            }
            glob.out.printf("]);%1", NL);
            glob.out.printf("%tsb.append(\"xdc.om.$$bind('$curpkg', save);%1", NL);
        }
        
        glob.out.printf("%tsb.append(\"__inst.$$bless();%1", NL);
        if (!unit.isMeta()) {
            glob.out.printf("%tsb.append(\"if (xdc.om.$$phase >= 5) xdc.om['%1'].__initObject(__inst);%2", qn, NL);
            glob.out.printf("%tsb.append(\"__inst.$$bind('$$phase', xdc.om.$$phase);%1", NL);
        }
        glob.out.printf("%tsb.append(\"return %1;%2", iflg ? "__inst" : "null", NL);
        glob.out.printf("%-%tsb.append(\"}%1", NL);
        glob.out.printf("%tGlobal.eval(sb.toString());\n");
        
        genGuardEnd("CFG", !unit.isMeta());
    }

    // genCst
    void genCst( Decl.Const cst, String qn )
    {
        if (cst.getInit() != null) {
            glob.out.printf("%tom.bind(\"%1.%2\", ", qn, cst.getName());
            glob.genExpr(cst.getInit());
            glob.out.printf(");\n");
        }
    }
    
    void genCst( Decl.Const cst )
    {
        glob.out.printf("%t%tpo.addFld(\"%1\", ", cst.getName());
        genProto(cst.getTypeCode(), cst.getType());
        glob.out.printf(", ");
        genDefault(cst.getTypeCode(), cst.getInit());
        glob.out.printf(", \"rh\");\n");
    }

    // genDefault
    void genDefault( String code, Expr init )
    {
        if (init == null) {
            char c0 = code.charAt(0);
            String is = c0 == 'A' || c0 == 'M' || c0 == 'O' || c0 == 'S' || c0 == 'V' ? "$$DEFAULT" : "$$UNDEF";
            glob.out.printf("%1", is);
        }
        else if (init instanceof Expr.Const && ((Expr.Const) init).getVal().equals("undefined")) {
            glob.out.printf("$$UNDEF");
        }
        else {
            glob.genExpr(init);
        }
    }
    
    // genElm
    void genElm( String code, Type type )
    {
        if (code.equals("n")) {
            glob.out.printf("Proto.Elm.newCNum(\"(%1)\")", type.tsig());
        }
        else {
            String ts = 
                code.startsWith("s") ? "$$T_Str" :
                code.startsWith("b") ? "$$T_Bool" :
                code.startsWith("v") ? "null" : "$$T_Obj";
            glob.out.printf("%1", ts);
        }
    }
    
    // genEnmVals
    void genEnmVals( Decl.Enum enm, String qn )
    {
        String etype = qn + '.' + enm.getName();
        String prev = null;
        int curval = 0;
        for (Decl.EnumVal evl : enm.getVals()) {
            glob.out.printf("%tom.bind(\"%1.%2\", "
                + "xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict(\"%3\", \"%4\"), \"%1.%2\", ",
                qn, evl.getName(), etype, curpkg);
            if (evl.getInit() != null) {
                glob.out.printf("xdc.services.intern.xsr.Enum.intValue(");
                glob.genExpr(evl.getInit());
                glob.out.printf(")+0");
                prev = "om.findStrict(\"" + qn + '.' + evl.getName()
                    + "\", \"" + curpkg + "\")";
                curval = 1;
            }
            else {
                if (prev != null) {
                    glob.out.printf("xdc.services.intern.xsr.Enum.intValue(");
                    glob.out.printf("%1)+", prev);
                }
                glob.out.printf("%1", curval++);
            }
            glob.out.printf("));\n");
        }
    }

    // genExt
    void genExt( Decl.Extern ext, String qn )
    {
        String sig;
        boolean fxnT;
        
        if (ext.getTypeCode().startsWith("F")) {
            sig = ext.getType().ptrsig();
            fxnT = true;
        }
        else {
            sig = ext.getType().tsig() + '*';
            fxnT = false;
        }

        glob.out.printf("%tom.bind(\"%1\", new Extern(\"%2\", \"%3\", %4, false, \"%1\"));\n",
            qn + '.' + ext.getName(), ext.getValue(), sig, new Boolean(fxnT));
    }

    // genFxnArgs
    void genFxnArgs( Decl.Fxn fxn, int i0 )
    {
        int argc = 0;
        for (Decl.Arg arg : fxn.getArgs()) {
        	if (fxn.isStruct() && arg.getName().equals("__this")) {
        		continue;
        	}
            glob.out.printf("%t%tfxn.addArg(%1, \"%2\", ", i0 + argc++, arg.getName());
            genProto(arg.getTypeCode(), arg.getType());
            glob.out.printf(", ");
            genDefault(arg.getTypeCode(), arg.getInit());
            glob.out.printf(");\n");
        }
    }
    
    // genFxnExt
    void genFxnExt( Decl.Fxn fxn, String qn )
    {
        String suf = fxn.isExternal() ? "__E" : "__I";
        
        if (fxn.isStatic() && !fxn.isSys() && !fxn.attrBool(Attr.A_Macro)) {
            glob.out.printf("%tom.bind(\"%1\", new Extern(\"%2%3\", \"%4\", true, false, \"%1\"));\n",
                    qn + '.' + fxn.getName(), glob.mkCname(qn) + fxn.getName(), suf, fxn.getTypeSig());
        }
    }
    
    // genGuardBeg
    void genGuardBeg( String guard, boolean cond )
    {
        if (cond) {
            glob.out.printf("%tif (is%1) {\n%+", guard);
        }
    }
    
    // genGuardEnd
    void genGuardEnd( String guard, boolean cond )
    {
        if (cond) {
            glob.out.printf("%-%t}//is%1\n", guard);
        }
    }
    
    // genIncs
    void genIncs( Pkg pkg )
    {
        Set<String> nS = new HashSet();
        
        for (Atom a : pkg.getRequires()) {
            String pn = a.getText();
            if (pn.startsWith("*")) {
                continue;
            }
            int k = pn.indexOf('{');
            if (k != -1) {
                pn = pn.substring(0, k);
            }
            if (pkg.getName().equals(pn)) {
                continue;
            }
            if (nS.add(pn)) {
                glob.out.printf("%tGlobal.callFxn(\"loadPackage\", xdcO, \"%1\");\n", pn);
            }
        }
        
        if (!pkg.getName().equals("xdc")) {
            if (nS.add("xdc")) {
                glob.out.printf("%tGlobal.callFxn(\"loadPackage\", xdcO, \"xdc\");\n");
            }
            if (!pkg.getName().equals("xdc.corevers") && nS.add("xdc.corevers")) {
                glob.out.printf("%tGlobal.callFxn(\"loadPackage\", xdcO, \"xdc.corevers\");\n");
            }
        }
        
        for (Unit unit : pkg.getUnits()) {
            for (Unit u2 : unit.getUses()) {
                String qn = u2.getQualName();
                int idx = qn.lastIndexOf('.');
                if (idx < 0) {
                    continue;
                }
                qn = qn.substring(0, idx);
                if (pkg.getName().equals(qn)) {
                    continue;
                }
                if (nS.add(qn)) {
                    glob.out.printf("%tGlobal.callFxn(\"loadPackage\", xdcO, \"%1\");\n", qn);
                }
            }
        }
    }
    
    // genInitFxn
    void genInitFxn( String fn, String qn, boolean capflg )
    {
        if (capflg) {
            glob.out.printf("%t%tfxn = Global.get(cap, \"%1\");\n", fn);
            glob.out.printf("%t%tif (fxn != null) om.bind(\"%1$$%2\", true);\n", qn, fn);
            glob.out.printf("%t%tif (fxn != null) po.addFxn(\"%1\", $$T_Met, fxn);\n", fn);
        }
    }
    
    // genMap
    void genMap( String code, Type.Array tarr )
    {
        glob.out.printf("new Proto.Map(");
        genProto(code.substring(1), tarr.getBase());
        glob.out.printf(")");
    }

    // genMbrs
    void genMbrs( Unit unit, String ks )
    {
        boolean mflg = (ks == K_MOD);
        boolean iflg = (ks == K_INS);
        boolean oflg = (ks == K_OBJ);
        boolean pflg = (ks == K_PRM);

        String qn = unit.getQualName();

        String cn = unit.getName() + ".xs";
        boolean capflg = new File(curbase + '/' + cn).exists();

        if (mflg && capflg) {
            glob.out.printf("%tcap = (Scriptable)Global.callFxn(\"loadCapsule\", xdcO, \"%1.xs\");\n",
                    unit.getQualName().replace('.', '/'));
            glob.out.printf("%tom.bind(\"%1$$capsule\", cap);\n", qn);
        }
        
        String sep = pflg || oflg ? "$$" : ".";
        String ss;
        
        if (oflg) {
            ss = "om.findStrict(\"" + qn + ".Instance\", \"" + curpkg + "\")"; 
        }
        else if (unit.getSuper() == null) {
            ss = "$$" + (qn.equals("xdc.IPackage") ? "Package" : ks); 
        }
        else if (!mflg && (unit.getSuper().isMod() || unit.getSuper().isStatic())) {
            ss = "$$" + ks;
        }
        else if (unit.isHeir()) {
            ss = "om.findStrict(\"" + unit.getSuper().getQualName() + sep + ks
                + "\", \"" + curpkg + "\")";
        }
        else if (!unit.needsRuntime()) {
            ss = "$$" + ks;
        }
        else {
            ss = "om.findStrict(\"xdc.runtime.IModule" + sep + ks + "\", \""
                + curpkg + "\")";
        }

        glob.out.printf("%tpo = (Proto.Obj)om.findStrict(\"%1%3%2\", \"%4\");\n",
            qn, ks, sep, curpkg);
        glob.out.printf("%tpo.init(\"%1.%2\", %3);\n", qn, ks, ss);

        if (mflg && unit.isProxy()) {
            glob.out.printf("%t%tpo.addFld(\"delegate$\", (Proto)om.findStrict(\"%1.Module\", \"%2\"), null, \"wh\");\n",
                    unit.getSuper().getQualName(), curpkg);
            if (unit.isInst()) {
                glob.out.printf("%t%tpo.addFld(\"abstractInstances$\", $$T_Bool, false, \"wh\");\n");
             }
        }
        
        if (!oflg) {

            glob.out.printf("%t%tpo.addFld(\"$hostonly\", $$T_Num, %1, \"r\");\n", unit.isMeta() ? "1" : "0");

            for (Decl d : unit.getDecls()) {
                if (d instanceof Decl.Const) {
                    genCst((Decl.Const)d);
                }
            }
            
            genGuardBeg("CFG", !unit.isMeta());
            
            for (Decl d : unit.getDecls()) {
                
                if (!unit.isForwarding() && d.getParent() != unit) {
                    continue;
                }

                if (mflg && d instanceof Decl.Proxy) {
                    Decl.Proxy prx = (Decl.Proxy)d;
                    String fs = unit.isProxy() ? "$$proxy" : "$$deleg";
                    glob.out.printf("%t%tpo.addFldV(\"%1\", (Proto)om.findStrict(\"%2.Module\", \"%4\"), null, \"wh\", %3Get, %3Set);\n",
                        prx.getName(), prx.getInherits().getQualName(), fs, curpkg);
                }

                if (!(d instanceof Decl.Config) || d.isStatic() != mflg) {
                    continue;
                }

                if (unit.isForwarding() && d.getParent().getQualName().equals("xdc.runtime.IModule")) {
                    continue;
                }
                
                boolean virt = false;
                if ((mflg || iflg) && unit.isProxy() && d.getParent() != unit) {
                    virt = true;
                }
                else if (mflg && unit.delegatesTo() != null && d.getParent() != unit) {
                    virt = true;
                }
                
                Decl.Config cfg = (Decl.Config)d;
                glob.out.printf("%t%tpo.%1(\"%2\", ", virt ? "addFldV" : "addFld", cfg.getName());
                genProto(cfg.getTypeCode(), cfg.getType());
                glob.out.printf(", ");
                
                if (cfg.getInit() instanceof Expr.Creat) {
                    genDefault(cfg.getTypeCode(), null);
                    createList.add(cfg);
                }
                else {
                    genDefault(cfg.getTypeCode(), cfg.getInit());
                }
                
                String flags = cfg.isReadonly() || cfg.isFinal() ? "r" : "w";  /// TODO: seal if final
                if (cfg.isMeta()) {
                    flags += 'h';
                }
                glob.out.printf(", \"%1\"%2);\n", flags, virt ? ", $$proxyGet, $$proxySet" : "");
            }

            String iis = "xdc.runtime.IInstance";
            if (!mflg && !unit.isMeta() && unit.isMod() && !unit.getQualName().equals(iis)) {
                glob.out.printf("%t%tpo.addFld(\"instance\", (Proto)om.findStrict(\"%1.Params\", \"%2\"), $$UNDEF, \"w\");\n",
                    iis, curpkg);
            }
            
            genGuardEnd("CFG", !unit.isMeta());
        }

        if (unit.isInst() && unit.isMod() && mflg) {
            genGuardBeg("CFG", !unit.isMeta());
            glob.out.printf("%t%tpo.addFxn(\"create\", (Proto.Fxn)om.findStrict(\"%1$$create\", \"%4\"), Global.get(\"%2$%3$$create\"));\n",
                    qn, curpre, unit.getName(), curpkg);
            if (!unit.isProxy()) {
                glob.out.printf("%t%tpo.addFxn(\"construct\", (Proto.Fxn)om.findStrict(\"%1$$construct\", \"%4\"), Global.get(\"%2$%3$$construct\"));\n",
                        qn, curpre, unit.getName(), curpkg);
            }
            genGuardEnd("CFG", !unit.isMeta());
        }

        if (pflg || (!capflg && unit.isInter())) {
            return;
        }

        if (mflg) {
            genInitFxn("module$use", qn, capflg);
            genInitFxn("module$meta$init", qn, capflg);
            if (unit.isInst()) {
                genInitFxn("instance$meta$init", qn, capflg);
            }
            if (!unit.isMeta()) {
                genInitFxn("module$static$init", qn, capflg);
            }
            genInitFxn("module$validate", qn, capflg);
            if (!unit.isMeta() && unit.isInst()) {
                genInitFxn("instance$static$init", qn, capflg);
            }
        }
        
        for (Decl d : unit.getDecls()) {
            if (!(d instanceof Decl.Fxn) || !d.isMeta() || d.isStatic() != mflg || d.overrides() != null) {
                continue;
            }
            if (((Decl.Fxn)d).isStruct()) {
            	continue;
            }
            String fn = d.getParent().getQualName() + "$$" + d.getName();
            if (unit.isInter() || (capflg && d.getParent() != unit)) {
                /// supports "default" implementations in base interfaces -- REMOVE
                glob.out.printf("%t%tfxn = Global.get(cap, \"%1\");\n", d.getName());
                glob.out.printf(
                    "%t%tif (fxn != null) po.addFxn(\"%1\", (Proto.Fxn)om.findStrict(\"%2\", \"%3\"), fxn);\n",
                    d.getName(), fn, curpkg);
            }
            else {
                glob.out.printf(
                    "%t%tpo.addFxn(\"%1\", (Proto.Fxn)om.findStrict(\"%2\", \"%4\"), %3);\n",
                    d.getName(), fn, capflg ? ("Global.get(cap, \"" + d.getName() + "\")") : "$$UNDEF",
                    curpkg);
            }
        }
    }
    
    // genPkgBuild
    void genPkgBuild()
    {
        String NL = "\\n\");\n";
        
        glob.out.printf("%tStringBuilder sb = new StringBuilder();\n");
        glob.out.printf("%tsb.append(\"var pkg = xdc.om['%1'];%2", curpkg, NL);
        glob.out.printf("%tsb.append(\"if (pkg.$vers.length >= 3) {%1%+", NL);
        glob.out.printf("%tsb.append(\"pkg.$vers.push(Packages.xdc.services.global.Vers.getDate(xdc.csd() + '/..'));%1", NL);
        glob.out.printf("%-%tsb.append(\"}%1", NL);

        /* the libraries are read from package/package.bld.xml, which is always
         * present.
         */
        try {
            DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            org.w3c.dom.Document doc = builder.parse("file:" + curbase
                + "/package/package.bld.xml");
            org.w3c.dom.NodeList libList = doc.getElementsByTagName("library");
            org.w3c.dom.NodeList trgList = doc.getElementsByTagName("target");

            glob.out.printf("%tsb.append(\"pkg.build.libraries = [%1%+", NL);
            for (int i = 0; i < libList.getLength(); i++) {
                org.w3c.dom.Element lib = (org.w3c.dom.Element)libList.item(i);
                glob.out.printf("%tsb.append(\"'%1',%2",
                    lib.getAttribute("pname"), NL);
            }
            glob.out.printf("%-%tsb.append(\"];%1", NL);

            glob.out.printf("%tsb.append(\"pkg.build.libDesc = [%1%+", NL);
            for (int i = 0; i < libList.getLength(); i++) {
                org.w3c.dom.Element lib = (org.w3c.dom.Element)libList.item(i);
                glob.out.printf("%tsb.append(\"['%1', {target: '%2'}],%3", lib.getAttribute("pname"), lib.getAttribute("target"), NL);
            }
            glob.out.printf("%-%tsb.append(\"];%1", NL);
            
            glob.out.printf("%tsb.append(\"if('suffix' in xdc.om['xdc.IPackage$$LibDesc']) {%1%+", NL);
            for (int i = 0; i < libList.getLength(); i++) {
                org.w3c.dom.Element lib = (org.w3c.dom.Element)libList.item(i);
                glob.out.printf("%tsb.append(\"pkg.build.libDesc['%1'].suffix = ", lib.getAttribute("pname"));
                String tn = lib.getAttribute("target");
                String suf = "";
                for (int j = 0; j < trgList.getLength(); j++) {
                    org.w3c.dom.Element trg = (org.w3c.dom.Element)trgList.item(j);
                    if (trg.getAttribute("name").equals(tn)) {
                        suf = trg.getAttribute("suffix");
                        break;
                    }
                }
                glob.out.printf("'%1';%2", suf, NL);
            }
            glob.out.printf("%-%tsb.append(\"}%1", NL);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        glob.out.printf("%tGlobal.eval(sb.toString());\n");
    }

    // genPkgInit
    void genPkgInit( Pkg pkg )
    {
        glob.out.printf("%tValue.Obj vo;\n");
        glob.out.printf("\n");
        
        genGuardBeg("CFG", true);
        String s = "xdc.runtime.IInstance";
        boolean first = true;
        for (Unit unit : pkg.getUnits()) {
            if (unit.isMod() && unit.isInst() && !unit.isMeta() && !unit.getQualName().equals(s)) {
                if (first) {
                    glob.out.printf(
                        "%tObject srcP = ((XScriptO)om.findStrict(\"%1\", \"%2\")).findStrict(\"PARAMS\", \"%2\");\n", s, curpkg);
                    glob.out.printf("%tScriptable dstP;\n");
                    glob.out.printf("\n");
                    first = false;
                }
                glob.out.printf("%tdstP = (Scriptable)((XScriptO)om.findStrict(\"%1\", \"%2\")).findStrict(\"PARAMS\", \"%2\");\n",
                    unit.getQualName(), curpkg);
                glob.out.printf("%tGlobal.put(dstP, \"instance\", srcP);\n");
            }
        } 
        genGuardEnd("CFG", true);

        for (Unit unit : pkg.getUnits()) {
            if (unit.isMod()) {
                glob.out.printf("%tGlobal.callFxn(\"module$meta$init\", (Scriptable)om.findStrict(\"%1\", \"%2\"));\n",
                    unit.getQualName(), curpkg); 
            }
        } 

        for (Decl.Config cfg : createList) {
            genGuardBeg("CFG", !cfg.getParent().isMeta());
            glob.out.printf("%tvo = (Value.Obj)om.findStrict(\"%1\", \"%2\");\n",
                cfg.getParent().getQualName(), curpkg);
            glob.out.printf("%tGlobal.put(vo, \"%1\", ", cfg.getName());
            glob.genExpr(cfg.getInit());
            glob.out.printf(");\n");
            if (cfg.isReadonly()) {
                glob.out.printf("%t((Value.Obj)Global.get(vo, \"%2\")).seal(null);\n", cfg.getParent().getQualName(), cfg.getName());
            }
            genGuardEnd("CFG", !cfg.getParent().isMeta());
        }

        glob.out.printf("%tGlobal.callFxn(\"init\", pkgV);\n");
        for (Unit unit : pkg.getUnits()) {
            glob.out.printf("%t((Value.Obj)om.getv(\"%1\")).bless();\n", unit.getQualName()); 
        } 
        glob.out.printf(
            "%t((Value.Arr)om.findStrict(\"$packages\", \"%1\")).add(pkgV);\n",
            curpkg);
    }

    // genPkgObjects
    void genPkgObjects( Pkg pkg )
    {
        glob.out.printf("%tpkgP = (Proto.Obj)om.bind(\"%1.Package\", new Proto.Obj());\n", curpkg);
        glob.out.printf("%tpkgV = (Value.Obj)om.bind(\"%1\", new Value.Obj(\"%1\", pkgP));\n", curpkg);
    }

    // genPkgValues
    void genPkgValues( Pkg pkg )
    {
        String qn = "xdc.IPackage";
        Unit ipu = pkg.getSession().findUnit(qn);
        
        glob.out.printf("%tpkgP.init(\"%1.Package\", (Proto.Obj)om.findStrict(\"xdc.IPackage.Module\", \"%1\"));\n", curpkg);

        if (new File(curbase + "/package.xs").exists()) {
            glob.out.printf("%tScriptable cap = (Scriptable)Global.callFxn(\"loadCapsule\", xdcO, \"%1/package.xs\");\n",
                    pkg.getQualName().replace('.', '/'));
            glob.out.printf("%tom.bind(\"%1$$capsule\", cap);\n", qn);
            glob.out.printf("%tObject fxn;\n");
            for (Decl d : ipu.getDecls()) {
                if (!(d instanceof Decl.Fxn) || !d.isMeta() || d.overrides() != null) {
                    continue;
                }
                if (((Decl.Fxn)d).isStruct()) {
                	continue;
                }
                String fn = qn + "$$" + d.getName();
                glob.out.printf("%t%tfxn = Global.get(cap, \"%1\");\n", d.getName());
                glob.out.printf("%t%tif (fxn != null) pkgP.addFxn(\"%1\", (Proto.Fxn)om.findStrict(\"%2\", \"%3\"), fxn);\n",
                    d.getName(), fn, curpkg);
            }
            glob.out.printf("%tpkgP.bind(\"$capsule\", cap);\n");
        }
        else {
            glob.out.printf("%tpkgP.bind(\"$capsule\", $$UNDEF);\n");
        }

        glob.out.printf("%tpkgV.init2(pkgP, \"%1\", Value.DEFAULT, false);\n", curpkg);

        glob.out.printf("%tpkgV.bind(\"$name\", \"%1\");\n", curpkg);
        glob.out.printf("%tpkgV.bind(\"$category\", \"Package\");\n");
        glob.out.printf("%tpkgV.bind(\"$$qn\", \"%1.\");\n", curpkg);
        
//        glob.out.printf("%tpkgV.bind(\"$spec\", ses.findPkg(\"%1\"));\n", curpkg);
        glob.out.printf("%tpkgV.bind(\"$vers\", Global.newArray(%1));\n",
            pkg.getKey() != null ? ("\"" + pkg.getKey() + "\"") : "");

        genAttrs(pkg);

        glob.out.printf("%timports.clear();\n");
        for (Atom a : pkg.getRequires()) {
            String is = a.getText();
            int k = is.indexOf('{');
            glob.out.printf("%timports.add(Global.newArray(\"%1\", Global.newArray(%2)));\n",
                k == -1 ? is : is.substring(0, k),
                k == -1 ? "" : is.substring(k + 1).replace('.', ',')
            );
        }
        glob.out.printf("%tpkgV.bind(\"$imports\", imports);\n");
        
        genPkgBuild();
    }

    // genProto
    void genProto( String code, Type type )
    {
        xdc.services.spec.Ref etr = glob.encodedTypeRef(type);
        if (etr != null) {
            glob.out.printf("(Proto)om.findStrict(\"%1$$%2\", \"%3\")",
                etr.getScope(), etr.getId(), curpkg);
            return;
        }
        
        type = glob.rawType(type);
        
        switch (code.charAt(0)) {
            case 'A':
            case 'V':
                genArr(code, ((Type.Array)type));
                break;
            case 'M':
                genMap(code, ((Type.Array)type));
                break;
            case 'P':
                genPtr(code, type);
                break;
            case 'O':
            case 'S':
            case 'o':
            case 'e':
                genTRef(type);
                break;    
            default:
                genElm(code, type);
                break;
        }
    }
    
    // genPtr
    void genPtr( String code, Type type )
    {
        String sig = type.tsig();
        glob.out.printf("new Proto.Adr(\"%1\", \"%2\")", sig, code);
    }
    
    // genRovArrFld
    void genRovArrFld( Decl.Field fld, Ref r, char fc )
    {
        Type.Array ta = (Type.Array)glob.rawType(fld.getType());
        if (ta.getDim() != null) {
            return;
        }
        
        String stype;
        
        if (fc == 'S' || fc == 'O') {
            stype = r.getScope() + '.' + r.getId();
        }
        else if (fc == 'o') {
            stype = "xdc.rov.support.ScalarStructs.S_Ptr";
        }
        else {
            Ref br = glob.rawType(ta.getBase()).tspec().getRef();
            stype =  "xdc.rov.support.ScalarStructs.S_" + br.getId();
        }
        
        glob.out.printf("%tpo.bind(\"%1$fetchDesc\", Global.newObject(\"type\", \"%2\", \"isScalar\", %3));\n", 
                        fld.getName(), stype, Character.isLowerCase(fc));
    }
    
    // genRovStr
    void genRovStr( Decl.Struct str, String qn )
    {
        if (str.isMeta() || str.getFields() == null) {
            return;
        }
        
        /* State structure */
        glob.out.printf("%tvo.bind(\"%1$fetchDesc\", Global.newObject(\"type\", \"%2.%1\", \"isScalar\", false));\n", str.getName(), qn);

        glob.out.printf("%tpo = (Proto.Obj)om.findStrict(\"%1$$%2\", \"%3\");\n",
            qn, str.getName(), curpkg);
        for (Decl.Field fld : str.getFields()) {
            String tc = fld.getTypeCode();
            //System.out.println(fld.getName() + ' ' + tc);
            
            /* Strings */
            if (tc.startsWith("s")) {
                /* Don't need a $fetchDesc for strings */ 
                continue;
            }
            
            /* Structures */
            Ref r = glob.rawType(fld.getType()).tspec().getRef();
            if (tc.startsWith("PS") || tc.startsWith("PO")) {
                glob.out.printf("%tpo.bind(\"%1$fetchDesc\", Global.newObject(\"type\", \"%2.%3\", \"isScalar\", false));\n", 
                                fld.getName(), r.getScope(), r.getId());
            }
            
            /* Arrays */
            else if (tc.length() == 2 && tc.charAt(0) == 'A') {
                genRovArrFld(fld, r, tc.charAt(1));
            }
            
        }
    }

    // genRovUnit
    void genRovUnit( Unit unit )
    {       
        if (unit.isMeta()) {
            return;
        }
        
        String qn = unit.getQualName();

        glob.out.printf("%tProto.Obj po;\n");
        glob.out.printf("%tValue.Obj vo;\n");
        glob.out.printf("\n");
        
        glob.out.printf("%tvo = (Value.Obj)om.findStrict(\"%1\", \"%2\");\n",
            qn, curpkg);

        if (unit.isInst() && unit.isHeir()) {
            glob.out.printf("%tpo = (Proto.Obj)om.findStrict(\"%1$$Instance_State\", \"%2\");\n",
                qn, curpkg);
            glob.out.printf("%tpo.addFld(\"__fxns\", new Proto.Adr(\"xdc_Ptr\", \"Pv\"), $$UNDEF, \"w\");\n");
        }
        
        for (Decl d : unit.getDecls()) {
            if (d instanceof Decl.Struct && d.getParent() == unit) {
                genRovStr((Decl.Struct) d, qn);
            }
        }
    }

    // genStrType
    void genStrType( Decl.Struct str, String qn, Unit unit )
    {
        boolean first = true;
        
        genBanner(str);
        
        if (str.isUnion()) {
            glob.out.printf("%tps = (Proto.Str)om.findStrict(\"%1.%2\", \"%3\");",
                qn, str.getName(), curpkg);
            glob.out.printf("%tps.bind(\"$$union\", 1);\n");
        }

        glob.out.printf("%tpo = (Proto.Obj)om.findStrict(\"%1$$%2\", \"%3\");\n",
            qn, str.getName(), curpkg);
        glob.out.printf("%tpo.init(\"%1.%2\", null);\n", qn, str.getName());
        glob.out.printf("%t%tpo.addFld(\"$hostonly\", $$T_Num, %1, \"r\");\n", str.isMeta() ? "1" : "0");

        if (str.getName().equals("Instance_State")) {
            if (unit.delegatesTo() != null) {
                glob.out.printf("%t%tpo.addFld(\"$delegate\", (Proto.Obj)om.findStrict(\"%1.Object\", \"%2\"), $$DEFAULT, \"w\")\n",
                    unit.delegatesTo().getQualName(), curpkg);
            }
        }

        boolean isState = str.getName().equals("Instance_State") || str.getName().equals("Module_State");
        
        for (Decl.Field fld : str.getFields()) {
            String fn = fld.getName();
            if (isState && Decl.objKind(fld) != Decl.Signature.ObjKind.NONE) {
                fn = "Object_field_" + fn;
                glob.out.printf("%t%tpo.addFldV(\"%1\", ", fld.getName());
                genProto(fld.getTypeCode(), fld.getType());
                glob.out.printf(", ");
                genDefault(fld.getTypeCode(), null);
                glob.out.printf(", \"wh\", $$objFldGet, $$objFldSet);\n");
            }
            glob.out.printf("%t%tpo.addFld(\"%1\", ", fn);
            genProto(fld.getTypeCode(), fld.getType());
            glob.out.printf(", ");
            genDefault(fld.getTypeCode(), null);
            glob.out.printf(", \"%1\");\n", str.isUnion() && !first ? "wh" : "w");
            first = false;
        }

        if (str.attrBool(Attr.A_XmlDtd)) {
            glob.out.printf("%t%tpo.addFxn(\"$xml\", $$T_Met, Global.get(\"$$xml\"));\n");
        }

        boolean capflg = new File(curbase + '/' + unit.getName() + ".xs").exists();

        for (Decl d : unit.getDecls()) {

        	if (!(d instanceof Decl.Fxn) || !d.isMeta() || d.overrides() != null) {
                continue;
            }

        	Decl.Fxn fxn = (Decl.Fxn)d;

        	if (fxn.isStruct() && fxn.getStructName().equals(str.getName())) {
                String fn = d.getParent().getQualName() + "$$" + d.getName();
                String dn = d.getName().substring(str.getName().length() + 1);
                if (unit.isInter() || (capflg && d.getParent() != unit)) {
                    /// supports "default" implementations in base interfaces -- REMOVE
                    glob.out.printf("%t%tfxn = Global.get(cap, \"%1\");\n", d.getName());
                    glob.out.printf(
                        "%t%tif (fxn != null) po.addFxn(\"%1\", (Proto.Fxn)om.findStrict(\"%2\", \"%3\"), fxn);\n",
                        dn, fn, curpkg);
                }
                else {
                    glob.out.printf(
                        "%t%tpo.addFxn(\"%1\", (Proto.Fxn)om.findStrict(\"%2\", \"%4\"), %3);\n",
                        dn, fn, capflg ? ("Global.get(cap, \"" + d.getName() + "\")") : "$$UNDEF",
                        curpkg);
                }
        	}
        }
    }
    
    // genTRef
    void genTRef( Type type )
    {
        xdc.services.spec.Ref tr = type.tspec().getRef();
        glob.out.printf("(Proto)om.findStrict(\"%1.%2\", \"%3\")",
            tr.getScope(), tr.getId(), curpkg);
    }

    // genTypedef
    void genTypedef( Decl.Typedef typ, String qn )
    {
        genBanner(typ);
        
        if (typ.attrBool(Attr.A_Encoded)) {
            glob.out.printf("%tpt = (Proto.Typedef)om.findStrict(\"%1$$%2\", \"%3\");\n",
                qn, typ.getName(), curpkg);
            glob.out.printf("%tpt.init(\"%1.%2\", ", qn, typ.getName());
            genProto(typ.getTypeCode(), glob.rawType(typ.getType()));
            glob.out.printf(", Global.get(cap, \"%1$encode\"));\n", typ.getName());
            glob.out.printf("%tom.bind(\"%1.%2\", pt);\n", qn, typ.getName());
        }
        else {
            glob.out.printf("%tom.bind(\"%1.%2\", ", qn, typ.getName());
            genProto(typ.getTypeCode(), typ.getType());
            glob.out.printf(");\n");
        }
    }

    // genUnitConsts
    void genUnitConsts( Unit unit )
    {
        genBanner(unit);

        String qn = unit.getQualName();
        
        for (Decl d : unit.getDecls()) {
            if (d.getParent() != unit) {
                continue;
            }
            else if (d instanceof Decl.Const) {
                genCst((Decl.Const)d, qn);
            }
            else if (d instanceof Decl.Enum) {
                genEnmVals((Decl.Enum)d, qn);
            }
            else if (d instanceof Decl.Extern) {
                genExt((Decl.Extern)d, qn);
            }
        }
        
        if (unit.isInter()) {
            return;
        }
        
        for (Decl.Fxn f : unit.getFxns()) {
             if (!f.isMeta()) {
                genFxnExt(f, qn);
            }
        }
        
        for (Decl.Fxn f : unit.getInternFxns()) {
            if (!f.isMeta()) {
                genFxnExt(f, qn);
            }
        }
    }

    // genUnitCreates
    void genUnitCreates( Unit unit )
    {
        glob.out.printf("%tProto.Fxn fxn;\n");
        glob.out.printf("%tStringBuilder sb;\n");
        glob.out.printf("\n");
        
        if (unit.isInst() && unit.isMod()) {
            genCreate(unit, true);
            if (!unit.isProxy()) {
                genCreate(unit, false);
            }
        }
    }

    // genUnitFxns
    void genUnitFxns( Unit unit )
    {
        String qn = unit.getQualName();
        
        glob.out.printf("%tProto.Fxn fxn;\n");
        glob.out.printf("\n");

        for (Decl d : unit.getDecls()) {
            if (!d.isMeta() || d.getParent() != unit || !(d instanceof Decl.Fxn)) {
                continue;
            }
            Decl.Fxn fxn = (Decl.Fxn)d;
            String tn = fxn.isStruct() ? fxn.getStructName() : fxn.isStatic() ? "Module" : "Instance";
            genBanner(fxn);
            glob.out.printf("%tfxn = (Proto.Fxn)om.bind(\"%1$$%2\", new Proto.Fxn(", qn, fxn.getName());
            glob.out.printf("om.findStrict(\"%1%4%2\", \"%3\"), ", qn, tn, curpkg, fxn.isStruct() ? "$$" : ".");
            if (fxn.getType() == null) {
                glob.out.printf("null, 0, -1, false));\n");
                continue;
            }
            genProto(fxn.getTypeCode(), fxn.getType());
            if (fxn.isStruct()) {
                glob.out.printf(", %1, %2, %3));\n", fxn.getArgs().size() - 1, fxn.getMinArgc() -1, fxn.isVarg());
            }
            else {
                glob.out.printf(", %1, %2, %3));\n", fxn.getArgs().size(), fxn.getMinArgc(), fxn.isVarg());
            }
            genFxnArgs(fxn, 0);
        }
    }

    // genUnitLogging
    void genUnitLogging( Unit unit )
    {
        glob.out.printf("%tloggables.clear();\n");
        
        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isLoggable()) {
                String enFmt = "";
                String sep = "";
                int argc = Glob.LOGARGS;
                if (fxn.isInst()) {
                    enFmt += "%p";
                    sep = ", ";
                    --argc;
                }
                for (Decl.Arg arg : fxn.getArgs()) {
                    if (--argc == 0) {
                        break;
                    }
                    String fs = glob.fmtSpec(arg);
                    if (fs.length() == 0) {
                        break;
                    }
                    enFmt += (sep + fs);
                    sep = ", ";
                }
                String exFmt = glob.fmtSpec(fxn);
                glob.out.printf("%tloggables.add(Global.newObject(\"name\", \"%1\", \"entry\", \"%2\", \"exit\", \"%3\"));\n",
                        fxn.getName(), enFmt.replace("\"", "\\\""), exFmt.replace("\"", "\\\""));
            }
        }

        glob.out.printf("%tvo.bind(\"$$loggables\", loggables.toArray());\n");
    }
    
    // genUnitObjects
    void genUnitObjects( Unit unit )
    {
        String qn = unit.getQualName();

        glob.out.printf("%tProto.Obj po, spo;\n");
        glob.out.printf("%tValue.Obj vo;\n");
        glob.out.printf("\n");
        
        glob.out.printf("%tpo = (Proto.Obj)om.bind(\"%1.Module\", new Proto.Obj());\n", qn);
        glob.out.printf("%tvo = (Value.Obj)om.bind(\"%1\", new Value.Obj(\"%1\", po));\n", qn);
        glob.out.printf("%tpkgV.bind(\"%1\", vo);\n", unit.getName());
        glob.out.printf("%t// decls \n");
        String hs = unit.isMeta() ? "true" : "false";
        for (Decl d : unit.getDecls()) {
            boolean alias = d.getParent() != unit;
            if (d instanceof Decl.Struct && ((Decl.Struct) d).getFields() != null) {
                if (!alias) {
                    glob.out.printf("%tspo = (Proto.Obj)om.bind(\"%1$$%2\", new Proto.Obj());\n", qn, d.getName());
                    glob.out.printf("%tom.bind(\"%1.%2\", new Proto.Str(spo, %3));\n", qn, d.getName(), hs);
                }
            }
            else if (d instanceof Decl.Enum) {
                if (!alias) {
                    glob.out.printf("%tom.bind(\"%1.%2\", new Proto.Enm(\"%1.%2\"));\n", qn, d.getName());
                }
            }
            else if (d instanceof Decl.Typedef && ((Decl.Typedef) d).attrBool(Attr.A_Encoded)) {
                if (alias) {
                    glob.out.printf("%tom.bind(\"%1.%2\", om.findStrict(\"%3.%2\", \"%4\");\n",
                        qn, d.getName(), d.getParent().getQualName(), curpkg);
                    continue;
                }
                glob.out.printf("%tom.bind(\"%1$$%2\", new Proto.%3());\n",
                        qn, d.getName(), glob.isAgg((Decl.Typedef)d) ? "Tag" : "Tel");
            }
            else {
                alias = false;
            }

            if (alias) {
                glob.out.printf("%tom.bind(\"%1.%2\", om.findStrict(\"%3.%2\", \"%4\"));\n",
                    qn, d.getName(), d.getParent().getQualName(), curpkg);
            }
        }
        
        if (unit.isInst()) {
            glob.out.printf("%t// insts \n");
            glob.out.printf("%tObject insP = om.bind(\"%1.Instance\", new Proto.Obj());\n", qn);
            glob.out.printf("%tpo = (Proto.Obj)om.bind(\"%1$$Object\", new Proto.Obj());\n", qn);
            glob.out.printf("%tObject objP = om.bind(\"%1.Object\", new Proto.Str(po, %2));\n", qn, hs);
            glob.out.printf("%tpo = (Proto.Obj)om.bind(\"%1$$Params\", new Proto.Obj());\n", qn);
            glob.out.printf("%tom.bind(\"%1.Params\", new Proto.Str(po, %2));\n", qn, hs);
            String ios = "Instance_State";
            if (!unit.hasInstObj()) {
                glob.out.printf("%tpo = (Proto.Obj)om.bind(\"%1$$%2\", new Proto.Obj());\n", qn, ios);
                glob.out.printf("%tom.bind(\"%1.%2\", new Proto.Str(po, %3));\n", qn, ios, hs);
            }
            if (!unit.isMeta()) {
                glob.out.printf("%tom.bind(\"%1.Handle\", insP);\n", qn);
                genGuardBeg("ROV", true);
                glob.out.printf("%tom.bind(\"%1.Object\", om.findStrict(\"%1.%2\", \"%3\"));\n",
                    qn, ios, curpkg);
                genGuardEnd("ROV", true);
            }
        }
    }

    // genUnitSizes
    void genUnitSizes( Unit unit )
    {
        if (unit.isMeta()) {
            return;
        }
        
        glob.out.printf("%tProto.Str so;\n");
        glob.out.printf("%tObject fxn;\n");
        glob.out.printf("\n");
        
        for (Decl d : unit.getDecls()) {
            if (d.isMeta() || d.getParent() != unit) {
                continue;
            }
            if (d instanceof Decl.Struct && ((Decl.Struct) d).getFields() != null) {
                Decl.Struct str = (Decl.Struct)d;
                glob.out.printf("%tso = (Proto.Str)om.findStrict(\"%1\", \"%2\");\n",
                    str.getQualName(), curpkg);
                glob.out.printf("%tsizes.clear();\n");
                int k = 0;
                String pre = "";

                if (str.getName().equals("Instance_State")) {
                    if (unit.isHeir()) {
                        glob.out.printf("%tsizes.add(Global.newArray(\"__fxns\", \"UPtr\"));\n");
                    }
                }
                
                for (String ts : str.getSizes()) {
                    if (ts.startsWith("A")) {
                        pre += ts + ';';
                    }
                    else {
                        glob.out.printf("%tsizes.add(Global.newArray(\"%1\", \"%2\"));\n", str.getFields().get(k++).getName(), pre + ts);
                        pre = "";
                    }
                }
                glob.out.printf("%tso.bind(\"$$sizes\", Global.newArray(sizes.toArray()));\n");
                
                glob.out.printf("%tfxn = Global.eval(\"function() { return $$sizeof(xdc.om['%1']); }\");\n", str.getQualName());
                glob.out.printf("%tso.bind(\"$sizeof\", fxn);\n");
                glob.out.printf("%tfxn = Global.eval(\"function() { return $$alignof(xdc.om['%1']); }\");\n", str.getQualName());
                glob.out.printf("%tso.bind(\"$alignof\", fxn);\n");
                glob.out.printf("%tfxn = Global.eval(\"function(fld) { return $$offsetof(xdc.om['%1'], fld); }\");\n", str.getQualName());
                glob.out.printf("%tso.bind(\"$offsetof\", fxn);\n");
            }
        }
    }

    // genUnitTypes
    void genUnitTypes( Unit unit )
    {
        glob.out.printf("%tScriptable cap;\n");
        glob.out.printf("%tProto.Obj po;\n");
        glob.out.printf("%tProto.Str ps;\n");
        glob.out.printf("%tProto.Typedef pt;\n");
        glob.out.printf("%tObject fxn;\n");
        glob.out.printf("\n");
        
        String qn = unit.getQualName();
        
        genMbrs(unit, K_MOD);
        if (unit.isInst()) {
            genMbrs(unit, K_INS);
            genMbrs(unit, K_PRM);
            if (unit.isMod()) {
                genMbrs(unit, K_OBJ);   // inherits from Instance
            }
        }

        for (Decl d : unit.getDecls()) {
            if (d.getParent() != unit) {
                continue;
            }
            else if (d instanceof Decl.Struct && ((Decl.Struct) d).getFields() != null) {
                genStrType((Decl.Struct) d, qn, unit);
            }
            else if (d instanceof Decl.Typedef) {
                genTypedef((Decl.Typedef) d, qn);
            }
        }
        
        if (unit.isInst() && unit.isMod() && !unit.hasInstObj()) {
            glob.out.printf("%tpo = (Proto.Obj)om.findStrict(\"%1$$%2\", \"%3\");\n",
                qn, "Instance_State", curpkg);
            glob.out.printf("%tpo.init(\"%1.%2\", null);\n", qn, "Instance_State");
            glob.out.printf("%t%tpo.addFld(\"$hostonly\", $$T_Num, 0, \"r\");\n");
            if (unit.delegatesTo() != null) {
                glob.out.printf("%t%tpo.addFld(\"$delegate\", (Proto)om.findStrict(\"%1.Object\", \"%2\"), $$DEFAULT, \"w\")\n",
                    unit.delegatesTo().getQualName(), curpkg);
            }
        }
    }

    // genUnitValues
    void genUnitValues( Unit unit )
    {
        String qn = unit.getQualName();
        
        glob.out.printf("%tProto.Obj po;\n");
        glob.out.printf("%tValue.Obj vo;\n");
        glob.out.printf("\n");

        glob.out.printf("%tvo = (Value.Obj)om.findStrict(\"%1\", \"%2\");\n",
            qn, curpkg);
        glob.out.printf("%tpo = (Proto.Obj)om.findStrict(\"%1.Module\", \"%2\");\n",
            qn, curpkg);
        glob.out.printf("%tvo.init2(po, \"%1\", $$DEFAULT, false);\n", qn);
        glob.out.printf("%tvo.bind(\"Module\", po);\n");
        glob.out.printf("%tvo.bind(\"$category\", \"%1\");\n", unit.isMod() ? "Module" : "Interface");

//        glob.out.printf("%tvo.bind(\"$spec\", ses.findUnit(\"%1\"));\n", unit.getQualName());
        
        if (new File(curbase + '/' + unit.getName() + ".xs").exists()) {
            glob.out.printf("%tvo.bind(\"$capsule\", om.findStrict(\"%1$$capsule\", \"%2\"));\n",
                qn, curpkg);
        }
        else {
            glob.out.printf("%tvo.bind(\"$capsule\", $$UNDEF);\n", qn);
        }

        if (unit.isInst()) {
            glob.out.printf("%tvo.bind(\"Instance\", om.findStrict(\"%1.Instance\", \"%2\"));\n",
                qn, curpkg);
            glob.out.printf("%tvo.bind(\"Params\", om.findStrict(\"%1.Params\", \"%2\"));\n",
                qn, curpkg);
            glob.out.printf("%tvo.bind(\"PARAMS\", ((Proto.Str)om.findStrict(\"%1.Params\", \"%2\")).newInstance());\n",
                qn, curpkg);
            if (!unit.isMeta()) {
                glob.out.printf("%tvo.bind(\"Handle\", om.findStrict(\"%1.Handle\", \"%2\"));\n",
                    qn, curpkg);
            }
        }

        glob.out.printf("%tvo.bind(\"$package\", om.findStrict(\"%1\", \"%1\"));\n",
            curpkg);
        glob.out.printf("%ttdefs.clear();\n");
        glob.out.printf("%tproxies.clear();\n");
        if (unit.isProxy()) {
            glob.out.printf("%tproxies.add(\"delegate$\");\n");
        }
        if (unit.isMod() && !unit.isProxy()) {
            glob.out.printf("%tmcfgs.clear();\n");
        }
        
        for (Decl d : unit.getDecls()) {
            if (d instanceof Decl.Config && d.isStatic() && !d.isMeta() && d.overrides() == null) {
                if (unit.isMod() && !unit.isProxy()) {
                    glob.out.printf("%tmcfgs.add(\"%1\");\n", d.getName());
                }
            }
            if (d instanceof Decl.AuxDef && !(d instanceof Decl.Const)) {
                Unit bu = (Unit)d.getParent();
                if (d instanceof Decl.Struct && ((Decl.Struct) d).getFields() == null) {
                    continue;
                }
                if (d instanceof Decl.Imp) {
                    glob.out.printf("%tvo.bind(\"%1\", om.find(\"%2\"));\n",
                    		d.getName(), ((Decl.Imp)d).getImport().getUnit().getQualName());
                    continue;
                }
                glob.out.printf("%tvo.bind(\"%1\", om.findStrict(\"%2.%1\", \"%3\"));\n",
                    d.getName(), bu.getQualName(), curpkg);
                // TODO: used to be Decl.IsType; problem with typedef <=> struct aliasing
                if (d instanceof Decl.Struct) {
                    glob.out.printf("%ttdefs.add(om.findStrict(\"%2.%1\", \"%3\"));\n",
                        d.getName(), bu.getQualName(), curpkg);
                }
            }
            else if (d instanceof Decl.Proxy && unit.isMod() && !unit.isProxy()) {
                glob.out.printf("%tvo.bind(\"%1$proxy\", om.findStrict(\"%2_%1\", \"%3\"));\n",
                    d.getName(), qn, curpkg);
                glob.out.printf("%tproxies.add(\"%1\");\n", d.getName());
            }
        }

        glob.out.printf("%tvo.bind(\"$$tdefs\", Global.newArray(tdefs.toArray()));\n");
        glob.out.printf("%tvo.bind(\"$$proxies\", Global.newArray(proxies.toArray()));\n");
        if (unit.isMod() && !unit.isProxy()) {
            glob.out.printf("%tvo.bind(\"$$mcfgs\", Global.newArray(mcfgs.toArray()));\n");
        }
        
        if (unit.isMod()) {
            glob.out.printf("%t((Value.Arr)pkgV.getv(\"$modules\")).add(vo);\n");
            glob.out.printf("%t((Value.Arr)om.findStrict(\"$modules\", \"%1\")).add(vo);\n", curpkg);
            glob.out.printf("%tvo.bind(\"$$instflag\", %1);\n", unit.isInst() ? "1" : "0");
            glob.out.printf("%tvo.bind(\"$$iobjflag\", %1);\n", unit.hasInstObj() ? "1" : "0");
            glob.out.printf("%tvo.bind(\"$$sizeflag\", %1);\n", unit.isSized() ? "1" : "0");
            glob.out.printf("%tvo.bind(\"$$dlgflag\", %1);\n", unit.delegatesTo() != null ? "1" : "0");
            glob.out.printf("%tvo.bind(\"$$iflag\", %1);\n", unit.isHeir() ? "1" : "0");
            glob.out.printf("%tvo.bind(\"$$romcfgs\", \"|\");\n");
            
            genGuardBeg("CFG", !unit.isMeta());
            glob.out.printf("%tProto.Str ps = (Proto.Str)vo.find(\"Module_State\");\n");
            glob.out.printf("%tif (ps != null) vo.bind(\"$object\", ps.newInstance());\n");

            if (unit.hasInstObj()) {
                glob.out.printf("%tvo.bind(\"$$meta_iobj\", om.has(\"%1$$instance$static$init\", null) ? 1 : 0);\n", qn);
            }
            else {
                glob.out.printf("%tvo.bind(\"$$meta_iobj\", 1);\n");
            }
            if (unit.isInst() && !unit.isMeta()) {
                glob.out.printf("%tvo.bind(\"%3\", Global.get(\"%1$%2$$%3\"));\n", curpre, unit.getName(), "__initObject");
            }
            genGuardEnd("CFG", !unit.isMeta());

            if (unit.delegatesTo() != null) {
                glob.out.printf("%tvo.bind(\"delegate$\", om.findStrict(\"%1\", \"%2\"));\n",
                    unit.delegatesTo().getQualName(), curpkg);
            }
            for (Decl.Fxn fxn : unit.getFxns()) {
                if (fxn.isStatic() && !fxn.isSys() && !fxn.isMeta() && !fxn.attrBool(Attr.A_Macro)) {
                    glob.out.printf("%tvo.bind(\"%2\", om.findStrict(\"%1.%2\", \"%3\"));\n",
                        qn, fxn.getName(), curpkg);
                }
            }
            for (Decl.Fxn fxn : unit.getInternFxns()) {
                if (!fxn.isMeta() && !fxn.attrBool(Attr.A_Macro)) {
                    glob.out.printf("%tvo.bind(\"%2\", om.findStrict(\"%1.%2\", \"%3\"));\n",
                        qn, fxn.getName(), curpkg);
                }
            }
/*            
            for (Import imp : unit.getImports()) {
            	if (imp.isExport()) {
            		glob.out.printf("%tvo.bind(\"%1\", om.find(\"%2\"));\n", imp.getAlias(), imp.getUnit().getQualName());
            	}
            }
*/            
            String sep;

            glob.out.printf("%tvo.bind(\"$$fxntab\", Global.newArray(");
            sep = "";
            for (Decl.Fxn fxn : unit.getFxns()) {
                if (unit.isProxy() && fxn.getParent() != unit) {
                    glob.out.printf("%3\"%1DELEGATE__%2\"", glob.mkCname(qn), fxn.getName(), sep);
                    sep = ", ";
                }
                else if (!fxn.isMeta()){
                    glob.out.printf("%4\"%1%2%3__E\"", glob.mkCname(qn), fxn.getName(), fxn.isVarg() ? "_va" : "", sep);
                    sep = ", ";
                }
            }
            glob.out.printf("));\n");

            glob.out.printf("%tvo.bind(\"$$logEvtCfgs\", Global.newArray(");
            sep = "";
            for (Decl.Config cfg : unit.getConfigs()) {
                if (cfg.isStatic() && !cfg.isMeta() && cfg.getTypeSig().equals("xdc_runtime_Log_Event")) {
                    glob.out.printf("%2\"%1\"", cfg.getName(), sep);
                    sep = ", ";
                }
            }
            glob.out.printf("));\n");
            
            glob.out.printf("%tvo.bind(\"$$errorDescCfgs\", Global.newArray(");
            sep = "";
            for (Decl.Config cfg : unit.getConfigs()) {
                if (cfg.isStatic() && !cfg.isMeta() && cfg.getTypeSig().equals("xdc_runtime_Error_Id")) {
                    glob.out.printf("%2\"%1\"", cfg.getName(), sep);
                    sep = ", ";
                }
            }
            glob.out.printf("));\n");

            glob.out.printf("%tvo.bind(\"$$assertDescCfgs\", Global.newArray(");
            sep = "";
            for (Decl.Config cfg : unit.getConfigs()) {
                if (cfg.isStatic() && !cfg.isMeta() && cfg.getTypeSig().equals("xdc_runtime_Assert_Id")) {
                    glob.out.printf("%2\"%1\"", cfg.getName(), sep);
                    sep = ", ";
                }
            }
            glob.out.printf("));\n");

            genAttrs(unit);
        }
        else {
            glob.out.printf("%t((Value.Arr)pkgV.getv(\"$interfaces\")).add(vo);\n");
        }

        if (unit.isMod() && unit.isInst()) {
            glob.out.printf("%tvo.bind(\"Object\", om.findStrict(\"%1.Object\", \"%2\"));\n",
                qn, curpkg);
            if (!unit.hasInstObj()) {
                glob.out.printf("%tvo.bind(\"Instance_State\", om.findStrict(\"%1.Instance_State\", \"%2\"));\n",
                    qn, curpkg);
            }
// TODO seal          glob.out.printf("%tvo.PARAMS.$seal();\n");
        }
        
        if (unit.isMod() && !unit.isMeta()) {
            glob.out.printf("%tvo.bind(\"MODULE_STARTUP$\", %1);\n",
                    unit.attrBool(Attr.A_ModuleStartup) ? "1" : "0");
            glob.out.printf("%tvo.bind(\"PROXY$\", %1);\n", unit.isProxy() ? "1" : "0");
            genUnitLogging(unit);
         }
        
        if (unit.attrString(Attr.A_Template) != null) {
            glob.out.printf("%tvo.bind(\"TEMPLATE$\", \"%1\");\n", unit.attrString(Attr.A_Template));
        }
            
        glob.out.printf("%tpkgV.bind(\"%1\", vo);\n", unit.getName());
        glob.out.printf("%t((Value.Arr)pkgV.getv(\"$unitNames\")).add(\"%1\");\n", unit.getName());

        if (unit.isInter()) {
            glob.out.printf("%tvo.seal(null);\n");
        }
    }
}
