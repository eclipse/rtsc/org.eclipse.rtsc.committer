/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;

public class Extern extends XScriptO
    implements AnyType
{
    String name;
    String sig;
    boolean fxnT;
    boolean raw;

    public Extern( String name, String sig, boolean fxnT )
    {
        this(name, sig, fxnT, false, null);
    }
    
    public Extern( String name, String sig, boolean fxnT, boolean raw ) 
    {
        this(name, sig, fxnT, raw, null);
    }

    public Extern( String name, String sig, boolean fxnT, boolean raw, String specName )
    {
        this.name = name;
        this.sig = sig;
        this.fxnT = fxnT;
        this.raw = raw;
        
        this.setName(name);
        
        this.bindtab.put("$sig", sig);
        this.bindtab.put("$raw", new Integer(raw ? 1 : 0));
        this.bindtab.put("$fxnT", new Integer(fxnT ? 1 : 0));
        this.bindtab.put("$category", "Extern");
        this.bindtab.put("$specName", specName);
    }
    
    public final String getName() { return this.name; }
    public final String getSig() { return this.sig; }

    public final boolean isFxnT() { return this.fxnT; }
    public final boolean isRaw() { return this.raw; }
    
    // OVERRIDE Scriptable

    public Object getDefaultValue( java.lang.Class hint )
    {
        if (Character.isDigit(this.name.charAt(0))) {
            return Long.decode(this.name);
        }
        else {
            return "&" + this.name;
        }
    }
}
