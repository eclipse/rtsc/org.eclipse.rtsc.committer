include $(XDCROOT)/packages/xdc/bld/xdc_java.mak

CLASSES = $(patsubst %.java,%,$(wildcard *.java))
JARFILE = java/package.jar

RHINO	= $(XDCROOT)/packages/xdc/shelf/java/js.jar
ANTLR	= $(XDCROOT)/packages/xdc/shelf/java/antlr.jar
ECLIPSE = $(XDCROOT)/packages/xdc/shelf/java/ecj.jar

JCPATH := $(PKGROOT)$(PATHCHAR)$(RHINO)$(PATHCHAR)$(ANTLR)$(PATHCHAR)$(ECLIPSE)
JCOPTS := -Xlint:deprecation

.libraries: $(JARFILE)
$(JARFILE): $(patsubst %,$(CLASSDIR)/%.class,$(CLASSES))

clean::
	$(RM) $(JARFILE)

