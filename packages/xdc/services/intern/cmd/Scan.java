/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.cmd;

import xdc.services.global.*;
import java.io.*;
import java.util.*;
import xdc.services.spec.*;

/*
 *  ======== Scan ========
 */
public class Scan
{
    public Scan() {
        this(Path.getGlobal());
    }
    public Scan(Env env) {
        this.env = env;
    }

    /*
     *  ======== read ========
     */
    public String read(String fileName)
    {
        return this.read(fileName, true);
    }
     
    public String read(String fileName, boolean validate)
    {
        Pkg pkg = new ParserSession(env).scan(fileName);

        this.name = pkg.getName();

	this.key = pkg.getKey();
	if (this.key == null) {
	   this.key = "";
	   this.version = "";
        }
        else {
            this.version = pkg.getReleaseVersion();
        }

        this.bundle = pkg.getProviderId();
        
        this.interfaces = new String [pkg.getInterNames().size()];
        for (int i = 0; i < interfaces.length; i++) {
            this.interfaces[i] = pkg.getInterNames().get(i).getText();
        }

        this.modules = new String [pkg.getModNames().size()];
        for (int i = 0; i < modules.length; i++) {
            this.modules[i] = pkg.getModNames().get(i).getText();
        }
        
        this.imports = new String [pkg.getRequires().size()];
        for (int i = 0; i < imports.length; i++) {
            this.imports[i] = pkg.getRequires().get(i).getText();
        }

        if (pkg.getAttrMap() != null) {
            this.attributes = new String [pkg.getAttrMap().size()];
            int i = 0;
            for (Attr a : pkg.getAttrMap().values()) {
                this.imports[i] = a.toText();
            }
        }
        else {
            this.attributes = new String [0];
        }

        return (this.name);
    }

    /*
     *  ======== readName ========
     */
    public String readName(String fileName)
    {
        Pkg pkg = new ParserSession(env).scan(fileName);

        return (pkg.getName());
    }
    
    /*
     *  ======== readp ========
     *  Given a 'dot' package name, find the package along the package
     *  path, read the specification, and return the package's full path.
     */
    public String readp(String packageName)
    {
	String specFile = packageName.replace('.', '/') + "/package.xdc";
	
	if ((specFile = env.search(specFile)) != null) {
	    read(specFile);
	    /* specFile may now be a Windows file name (with '\\'s) */
	    return (specFile.substring(0,
		specFile.lastIndexOf(File.separatorChar)));
	}

	return (null);
    }

    /*
     *  ======== findPackageSpec ========
     *  Find the package.xdc file of the package that contains the
     *  specified file.  Note: the file does not have to exist.
     */
    static public String findPackageSpec(String fileName)
    {
	String name = (new File(fileName)).getAbsolutePath();
	int end = name.length();
	int i;

	for (end = name.length(); end > 0; end = i - 1) {
	    if ((i = name.lastIndexOf('/', end)) >= 0
		|| (i = name.lastIndexOf('\\', end)) >= 0) {

		String ps = name.substring(0, i + 1) + "package.xdc";
		if ((new File(ps)).exists()) {
		    return (ps);
		}
	    }
	}

        return (null);
    }

    /*
     *  ======== getKey ========
     */
    public String getKey()
    {
        return (this.key);
    }

    /*
     *  ======== getPackageDirs ========
     *  Find all directories, starting at root, that contains a package.xdc
     *  specification file.
     */
    static public String [] getPackageDirs(String root)
    {
	Vector pkgList = getPackageDirs(new File(root));
	
	/* return an array of strings from the pkgList vector */
	String [] result = new String [pkgList.size()];
	pkgList.toArray(result);
	return (result);
    }

    static private Vector getPackageDirs(File root)
    {
	final Vector pkgList = new Vector();
	final Vector dirList = new Vector();
	
	/* create an object that implements java.io.FilenameFilter */
	FilenameFilter filter = new FilenameFilter() {
	    public boolean accept(File dir, String name) {
		if (name.equals("package.xdc")) {
		    pkgList.add(dir.toString());
		}
		else {
		    File tmp = new File(dir + "/" + name);
		    if (tmp.isDirectory()) {
			dirList.add(tmp);
		    }
		}
		return (false);
	    }
	};
    
	/* scan the root directory */
	root.list(filter);
	
	/* scan all sub-directories (recursively) */
        for (Enumeration e = dirList.elements(); e.hasMoreElements(); ) {
            File dir = (File)e.nextElement();
	    pkgList.addAll(getPackageDirs(dir));
        }

	/* return final accumulation */
	return (pkgList);
    }
    
    /*
     *  ======== getProviderId ========
     */
    public String getProviderId()
    {
        return (this.bundle);
    }

    /*
     *  ======== getReleaseVersion ========
     */
    public String getReleaseVersion()
    {
        return (this.version);
    }

    /*
     *  ======== getModules ========
     */
    public String [] getModules()
    {
        return (this.modules);
    }

    /*
     *  ======== getInterfaces ========
     */
    public String [] getInterfaces()
    {
        return (this.interfaces);
    }

    /*
     *  ======== getImports ========
     */
    public String [] getImports()
    {
        return (this.imports);
    }

    /*
     *  ======== getAttributes ========
     */
    public String [] getAttributes()
    {
        return (this.attributes);
    }

    private String [] interfaces = null;
    private String [] modules    = null;
    private String    name       = null;
    private String    version    = "";
    private String    bundle     = "";
    private String    key        = "";
    private String [] imports    = null;
    private String [] attributes = null;
    private Env       env;
}
