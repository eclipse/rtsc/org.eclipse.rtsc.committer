/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import java.io.*;
import org.mozilla.javascript.*;

public class Compiler
{
    static public void compile( String srcfile, String objfile )
    {
        Context cx = Global.getContext();
        try {
            Script script = cx.compileReader(new FileReader(srcfile), srcfile, 1, null);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(objfile));
            oos.writeObject(script);
            oos.close();
        }
        catch (Exception e) {
            Err.exit(e);
        }
    }
    
    static public void exec( String objfile )
    {
        Context cx = Global.getContext();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(objfile));
            Script script = (Script)ois.readObject();
            script.exec(cx, Global.getTopScope());
            ois.close();
        }
        catch (Exception e) {
            Err.exit(e);
        }
        
    }
}
