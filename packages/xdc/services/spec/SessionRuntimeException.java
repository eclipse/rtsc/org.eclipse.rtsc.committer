/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/**
 * 
 */
package xdc.services.spec;

import org.mozilla.javascript.EvaluatorException;

public class SessionRuntimeException extends EvaluatorException {
    static final long serialVersionUID = -7134897190745766939L;
    final String SESSION_ERROR = "Session Error: ";
    
    public SessionRuntimeException(String message) {
    	super(message);
    }
}
