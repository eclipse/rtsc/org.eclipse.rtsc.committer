/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== Host.java ========
 *
 *! Revision History
 *! ================
 *! 19-Feb-2008 sasha   replaced getCanonicalPath() with getAbsolutePath()
 */

/*
 *  ======== xdc.services.global.Host ========
 */
package xdc.services.global;

import java.io.*;
import java.util.Vector;
import java.util.Map;
import java.util.Enumeration;
import java.util.Iterator;

public class Host {

    /*
     *  ======== copyFile ========
     */
    public static void copyFile(File source, File dest) throws IOException
    {
        FileInputStream fi = new FileInputStream(source);
        java.nio.channels.FileChannel fic = fi.getChannel();
        java.nio.MappedByteBuffer mbuf =
            fic.map(java.nio.channels.FileChannel.MapMode.READ_ONLY, 0, 
                    source.length());
        fic.close();
        fi.close();

        FileOutputStream fo = new FileOutputStream(dest);
        java.nio.channels.FileChannel foc = fo.getChannel();
        foc.write(mbuf);
        foc.close();
        fo.close();
    }

    /*
     *  ======== deepCopyFiles ========
     */
    public static void deepCopyFiles(File srcdir, File dstdir)
        throws IOException
    {
        dstdir.mkdirs();

        for (File f : srcdir.listFiles()) {
            if (!f.isDirectory()) {
                copyFile(f, new File(dstdir, f.getName()));
            }
            else {
                deepCopyFiles(f, new File(dstdir, f.getName()));
            }
        }
    }
    
/*
    static public void copyFile( File src, File dst )
    {
        InputStream in = null;
        OutputStream out = null;
        
        try {
            in = new FileInputStream(src);
            out = new FileOutputStream(dst);
              
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            
            in.close();
            out.close();
        }
        catch (Exception e) {
            Err.exit(e);
        }
    }
*/    
    
    /*
     *  ======== ExecAttrs ========
     */
    static public class ExecAttrs {
        public String [] envs = null;
        public boolean useEnv = false;
        public String filter = null;
        public String cwd = ".";
        public String outName = null;
        public boolean merge = true;
    };

    /*
     *  ======== ExecResult ========
     */
    static public class ExecResult {
        public String output = null;
        public String errors = null;
        public int    status = 0;
    };

    /*
     *  ======== getPathComponents ========
     *  Creates a vector of strings out of one string specifying the path,
     *  where the components of the path are separated by File.pathSeparator.
     *  If the path references environment variables, they get expanded.
     *
     *  @param pathString       the value of the environment variable PATH
     *
     *  @return                 vector of strings, each containing one
     *                          component of PATH
     */
    static private Vector<String> getPathComponents(String pathString) 
    {
        Vector<String> pathVector = new Vector<String>(10);
        String [] pathComponents = pathString.split(File.pathSeparator);
        for (int i = 0; i < pathComponents.length; i++) {
            if (!pathComponents[i].trim().equals("")
                && !pathComponents[i].startsWith("$")) {

                File pc = new File(pathComponents[i]);
                pathVector.addElement(pc.getAbsolutePath());
            }
            else if (pathComponents[i].startsWith("$")) {
                String env = pathComponents[i].substring(1);
                if (env.startsWith("(") || env.startsWith("{")) {
                    env = env.substring(1, env.length() - 1);
                }
                String prop = System.getenv(env);
                if (prop != null) {
                    pathVector.addAll(getPathComponents(prop));
                }
            }
        }
        return (pathVector);    
    }

    /*
     *  ======== exec ========
     *  Run the specified command cmd in the environment specified by attrs.
     *
     *  If the attrs.envs doesn't contain HOME or TOOLS, they are read from
     *  the environment and passed to the new process.
     */
    static public synchronized ExecResult exec(String cmd, ExecAttrs attrs)
        throws java.io.IOException
    {
        /* ProcessBuilder requires that the executable and arguments are in
         * separate strings.
         */
        String [] splitCmd = cmd.split("\\s+");
        return exec(splitCmd, attrs);
    }

    /*
     *  ======== exec ========
     *  Run the specified command cmd in the environment specified by attrs.
     *
     *  If the attrs.envs doesn't contain HOME or TOOLS, they are read from
     *  the environment and passed to the new process.
     *
     *  This version receives the arguments in separate array elements,
     *  bypassing the problem of parsing delimiters and quotes.
     */
    static public synchronized ExecResult exec(String[] splitCmd,
        ExecAttrs attrs) throws java.io.IOException
    {
        if (attrs == null) {
            attrs = new ExecAttrs();
        }

        /* start command with the right environment */
        if (attrs.envs == null) {
            String path = "PATH=.";
            String root = System.getProperty("config.rootDir");
            if (root != null) {
                path = path + File.pathSeparator + root;
            }
            String oldPath = System.getenv("PATH");
            if (oldPath != null) {
                path = path + File.pathSeparator + oldPath;
            }
            attrs.envs = new String [1];
            attrs.envs[0] = path;
        }

        ExecResult result = new ExecResult();

        File file = new File(splitCmd[0]);

        /* If an additional path is specified in attrs.envs, and the executable
         * is not an absolute path, we need to search the additional path for
         * the executable.
         */
        Vector pathVector = new Vector();
        if (!file.isAbsolute()) {
            for (int i = 0; i < attrs.envs.length; i++) {
                if (attrs.envs[i].toUpperCase().startsWith("PATH=")) {
                    pathVector = getPathComponents(attrs.envs[i].substring(5));
                }
            }
        }

        /* pathVector is used only for looking for the executable. We don't
         * pass the expanded path to the underlying process, but we do pass 
         * attrs.envs[] array and let the underlying process do the expansion
         * if needed.
         */
        if (!pathVector.isEmpty()) {
            boolean found = false;
            for (Enumeration e = pathVector.elements(); e.hasMoreElements(); ) {
                String path = (String)e.nextElement();
                file = new File(path + File.separator + splitCmd[0]);
                if (file.exists()) {
                    splitCmd[0] = file.getAbsolutePath();
                    found = true;
                    break;
                }
            }
            if (!found) {
                /* If we didn't find anything along the path, return -1 */
                result.status = -1;
                result.output = "Cannot find " + splitCmd[0]
                    + " along the path " + pathVector.toString();
                return (result);
            }
        } 

        Process proc;
        ProcessBuilder pb = new ProcessBuilder(splitCmd);
        File cwd = new File(attrs.cwd == null ? "." : attrs.cwd);
        pb.directory(cwd);
        Map<String, String> env = pb.environment();

        if (!attrs.useEnv) {
            env.clear();
        }
        
        /* The environment variables passed through attrs.envs are written over
         * the inherited once. In the case where the environment is not
         * inherited from the caller, we need to check if attrs.envs defines
         * $HOME and $SystemRoot. If any of them is not defined, we try to get
         * them from the environment and add them to attrs.envs.
         */
        boolean homeFound = false;
        boolean systemRootFound = false;
        for (int i = 0; i < attrs.envs.length; i++) {
            String [] envVar = attrs.envs[i].split("=", 2);
            if (envVar[0].equals("HOME")) {
                homeFound = true;
            }
            else if (envVar[0].equals("SYSTEMROOT")) {
                systemRootFound = true;
            }
            
            env.put(envVar[0], envVar[1]);
        }

        if (homeFound == false && !attrs.useEnv) {
            String home = System.getenv("HOME");
            if (home != null) {
                env.put("HOME", home);
            }
        }
        if (systemRootFound == false && !attrs.useEnv) {
            String root = System.getenv("SYSTEMROOT");
            if (root != null) {
                env.put("SYSTEMROOT", root);
            }
        }

        try {
            proc = pb.start();
        }
        catch (Exception e) {
            result.status = -1;
            result.output = "" + e;
            return (result);
        }

        /* get the command stdout and strerr streams */
        BufferedReader stdout = new BufferedReader(
            new InputStreamReader(proc.getInputStream()));
        BufferedReader stderr = new BufferedReader(
            new InputStreamReader(proc.getErrorStream()));
    
        /* open an output stream */
        Writer out;
        if (attrs.outName != null) {
            out = new FileWriter(attrs.outName, true);
        }
        else {
            out = new StringWriter();
        }

        /* open an output errors stream */
        Writer err = attrs.merge ? out : new StringWriter();
    
        /* create a background thread for stderr */
        FilteredStreamReader stderrReader =
            new FilteredStreamReader(stderr, null, err);

        /* accumulate the command's stdout and start stderr thread */
        stderrReader.acopy();
        FilteredStreamReader.scopy(stdout, attrs.filter, out);
        stderrReader.acopyWait();
        
        /* return command output and exit status to optional output argument */
        try {
            result.status = proc.waitFor();
        }
        catch (InterruptedException e) {
            result.status = -1;
        }
        
        /* close output streams */
        out.close();
        err.close();

        /* return command's output to caller (if necessary) */
        result.output = attrs.outName != null ? null : out.toString();

        /* return command's errors to caller (if necessary) */
        result.errors = attrs.merge ? null : err.toString();

        return (result);
    }

    /*
     *  ======== lsdir ========
     *  Return an array of all files and directories starting from the
     *  directory root.
     */
    static public String [] lsdir(String root)
    {
        /* get a complete list of files and directories starting from root */
        Vector<String> dlist = new Vector<String>();
        Vector<String> flist = new Vector<String>();
        lsr(root, dlist, flist);

        /* combine files and directories into one list
         *
         * We use a loop to ensure the items in the resulting array may
         * be deleted in the order that they appear in the array
         */
        for (int i = dlist.size() - 1; i >= 0; i--) {
            flist.add(dlist.get(i));
        }
        
        /* create a string array from the vector */
        String [] result = new String [flist.size()];
        flist.copyInto(result);

        return (result);
    }

    /*
     *  ======== tmpdir ========
     *  Create a temporary directory
     */
    static private int tid = 0;
    static public String tmpdir()
    {
        return (tmpdir(".tmpdir"));
    }
    static public synchronized String tmpdir(String prefix)
    {
        String tmp;
        for (int i = 1; i < 1000; i++) {
            tmp = prefix + (tid + i);
            File file = new File(tmp);
            if (!file.exists()) {
                file.mkdirs();
                tid += i;
                return (tmp);
            }
        }

        return (null);
    }

    /*
     *  ======== rmdir ========
     *  Recursively remove the directory root.
     */
    static public void rmdir(File root) { rmdir(root.getAbsolutePath()); }
    
    static public void rmdir(String root)
    {
        if (root != null && root.length() > 0) {
            String [] list = lsdir(root);
            for (int i = 0; i < list.length; i++) {
                (new File(list[i])).delete();
            }
            (new File(root)).delete();
        }
    }

    /*
     *  ======== lsr ========
     *  Accumulate all files and directories starting from the specified
     *  directory root.
     */
    static private void lsr(String root,
        Vector<String> dlist, Vector<String> flist)
    {
        File file = new java.io.File(root);
        String [] ls = file.list();
        if (ls != null) {
            /* for everything in root .. */
            for (int i = 0; i < ls.length; i++) {
                /* create complete name */
                String name = root + java.io.File.separator + ls[i];

                /* if it's a directory recurse ... */
                if ((new File(name)).isDirectory()) {
                    dlist.add(name);
                    lsr(name, dlist, flist);
                }
                else {  /* otherwise add name to list */
                    flist.add(name);
                }
            }
        }
    }

    /*
     *  ======== FilteredStreamReader ========
     *  This class exists to read a stderr stream and write the result to
     *  an output stream using a separate (from main) thread.
     *
     *  This class provides two methods for transfering lines from a
     *  BufferedReader to a Writer: a static method scopy, and an instance
     *  method acopy.
     *
     *  The instance method is used to perform the copy "in the background"
     *  while the caller continues to execute.  The caller may wait for the
     *  copy to complete via acopyWait().
     *
     *  The static method, scopy(), may be used to write to the same output
     *  stream that was passed to acopy(); access to the output stream is
     *  serialized.  No serialization of the input stream (BufferedReader)
     *  is provided.
     */
    static class FilteredStreamReader extends Thread {

        /**
         * Create a new asynchronous stream reader.
         */
        public FilteredStreamReader(BufferedReader in, String filter,
            Writer out)
        {
            this.in = in;
            this.filter = filter;
            this.out = out;
        }

        /*
         *  ======== scopy ========
         */
        static void scopy(BufferedReader in, String filter, Writer out)
            throws IOException
        {
            /* copy lines from in to out */
            for (;;) {
                String nextLine = in.readLine();    /* get next line */
                if (nextLine == null) {
                    break;  /* end of input stream */
                }

                if (filter == null || nextLine.matches(filter)) {
                    synchronized (FilteredStreamReader.class) {
                        out.write(nextLine + "\n");
                        out.flush();
                    }
                }
            }
        }

        /*
         *  ======== acopy ========
         */
        public void acopy()
        {
            /* tell run() to start the copy */
            start();        
        }

        /*
         *  ======== acopyWait ========
         */
        public void acopyWait()
        {
            try {
                /* wait for run() to complete the copy */
                join();
            }
            catch (InterruptedException e) {
                return;
            }
        }
        
        /*
         *  ======== Runnable.run ========
         */
        public void run()
        {
            /* copy everything from this stream to out */
            try {
                scopy(in, filter, out);
            }
            catch (IOException e) {};
        }

        private BufferedReader in = null;
        private String filter;
        private Writer out;
    }

    /*
     *  ======== main ========
     *  Simple unit test of this module
     */
    static public void main(String [] args)
    {
        if (args.length < 1) {
            args = new String [1];
            args[0] = "ls f00";
        }

        for (int i = 0; i < args.length; i++) {
            System.out.println("cmd = " + args[i]);

            try {
                ExecResult result = exec(args[i], null);
                System.out.println("    exit status = " + result.status);
                System.out.println("    output = '" + result.output + "'");
            }
            catch (IOException e) {}
        }
    }
}
