/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

import xdc.services.global.CacheElem;
import xdc.services.global.Clock;
import xdc.services.global.Env;
import xdc.services.global.Path;
import org.mozilla.javascript.*;
import java.io.*;
import java.util.*;

public class BrowserSession
    extends Session
{
    private boolean rscan = false;
    private String[] dirs;
    private ParserSession parser;
    private boolean loadDocs = true;
    private Clock clock = new Clock();

    /** Canonical path to qualified name */
    private HashMap<String,String> cp2qn = new HashMap<String,String>();
    /** Qualified name to canonical path */
    private HashMap<String,String> qn2cp = new HashMap<String,String>();

    /** Regex to match trace enable mask */
    private static final String traceRegex = "xdc[/\\\\]services[/\\\\]spec";

    /**
     * Create a session to browse the current package path.
     */
    public BrowserSession() {
        this(Path.getGlobal());
    }

    /**
     * Create a new session with a given environment
     */
    public BrowserSession(Env env) {
        this(env, new Cache());
    }

    public BrowserSession(Env env, Cache cache) {
        super(env, cache);
        this.dirs = env.getPath();
        this.parser = new ParserSession(env, cache);
        this.parser.setVerbose(false);
        this.parser.msg = this.msg;
    }
    
    /**
     * Create a new session with a given package path
     */
    public BrowserSession( String[] dirs )
    {
        this(dirs, Path.getGlobal());
    }

    /**
     * Create a new session with a given package path and environment
     */
    public BrowserSession( String[] dirs, Env env )
    {
        this(dirs, env, new Cache());
    }

    /**
     * Create a new session with a given package path, environment,
     * and cache.
     */
    BrowserSession( String[] dirs, Env env, Cache cache )
    {
        super(env, cache);
        env.setPath(dirs);
        /* expand all "^" tokens in dirs */
        this.dirs = env.getPath();
        this.parser = new ParserSession(env, cache);
        this.parser.setVerbose(false);
    }

    // curPkg
    Pkg curPkg()
    {
        return null;
    }

    // findPkg
    public synchronized Pkg findPkg( String qn )
    {
        Pkg pkg = super.findPkg(qn);
        if (pkg != null) {
            return pkg;
        }

//        clock.enable();
        clock.reset();
        
        String cpath = this.qn2cp.get(qn);
        if (cpath != null) {
            pkg = this.loadPkg(cpath, qn);
        }
        else {
            String path = "/" + qn.replace('.', '/');
            for (int i = 0; i < this.dirs.length; i++) {
                if ((pkg = this.loadPkg(dirs[i] + path, qn)) != null) {
                    break;
                }
            }
        }
        
        clock.print("findPkg " + qn);
        
        return pkg;
    }
    
    // isCurrentPkg
    public boolean isCurrentPkg( Pkg pkg )
    {
        return this.isCurrentPkg(pkg.getBaseDir(), pkg.getName());
    }
    
    // isCurrentPkg
    public boolean isCurrentPkg( String cpath, String qn )
    {
        return new File(cpath + "/package/.vers_g" + this.getVers()).exists();
    }

    // loadPkg
    public synchronized Pkg loadPkg( String dir, String qn )
    {
        String cpath;
        try {
            cpath = new File(dir).getCanonicalPath();
        }
        catch (IOException e) {
            msg.error("Error reading directory: " + dir);
            throw new RuntimeException(e);
        }

        Pkg pkg = super.findPkg(qn);
        if (pkg != null) {
            return (pkg);
        }

        if (!new File(cpath + "/package/" + qn + ".sch").exists()) {
            return (null);
        }

        File file = null;
        
        if (new File(cpath + "/package/" + ".vers_g" + this.getVers()).exists()) {
            file = new File(cpath + "/package/" + qn + ".ccs");
        }
        
        if (file == null || !file.exists()) {
            CacheElem cacheElem = new CacheElem(qn, cpath, "spec", this.getVers());
//            System.out.println("  Re-building " + cpath + "/package.xdc");
            if (cacheElem.isTemp()) {
                pkg = this.parser.compile(cpath + "/package.xdc");
            }
            else {
                File tmpElemDir = cacheElem.create();
                if (tmpElemDir != null) {
                    pkg = this.parser.compile(cpath + "/package.xdc");
                    parser.save(tmpElemDir);
                    cacheElem.commit();
                }
                File curElemDir = cacheElem.access();
                file = new File(curElemDir, qn + ".ccs");
                cacheElem.release();
            }
//            System.out.println("  Re-build done (" + cpath + ")");
        }
        
        if (pkg == null && file != null && file.exists()) {
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            ObjectInputStream ois = null;
            try {
                /* read the package spec -- may throw an exception */
//                System.out.println("    reading pkg from " + file);
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                ois = new Persist.Input(qn, bis, this);
                pkg = (Pkg)ois.readObject();
            }
            catch (SessionRuntimeException e) {
                throw (e);
            }
            catch (Exception e) {
                System.out.println("EXCEPTION");
                e.printStackTrace();
                throw new WrappedException(e);
            }
            finally {
                /* clean up file objects */
                try {
                    if (fis != null) {
                        fis.close();
                    }
                    if (bis != null) {
                        bis.close();
                    }
                    if (ois != null) {
                        ois.close();
                    }
                }
                catch (IOException e) {
                }
            }
        }

        if (pkg != null) {
            pkg.bindSession(this);
            this.cp2qn.put(cpath, qn);
            this.enterPackage(qn, pkg);
            pkg.setBaseDir(cpath);
        }
        
        return pkg;
    }

    /**
     * Check whether the named directory contains a built package.
     * @param dir the pathname to the directory to check.
     * @return the fully qualified package name, or else null if the
     *    directory is not recognizably a built package.
     */
    public String isPkgDir(String dir) {
        /* check that the directory exists */
        String cpath;
        try {
            cpath = new File(dir).getCanonicalPath();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

        /* check that the package.xdc file exists */
        File pkgXdc = new File(cpath + "/package.xdc");
        if (!pkgXdc.exists()) {
            return null;
        }

        /* check that the package/ subdirectory exists */
        File pdir = new File(cpath + "/package/");
        if (!pdir.exists()) {
            return null;
        }

        /*
         * Search for the schema file, which will tell us the name of the
         * package, if it exists.
         */
        String pkgName = null;
        for (String fileName : pdir.list()) {
            if (fileName.endsWith(".sch")) {
                /* found the schema file */
                pkgName = fileName.substring(0, fileName.length()-4);
                break;
            }
        }

        /* return with the package name, or null if not found */
        return pkgName;
    }
    
    /**
     * Load the named unit's spec object by searching the package path.
     * @param name Package-qualified name of the module or interface.
     * @return unit, or null if not found.
     */
    public synchronized Unit loadUnit( String name )
    {
        /* find the dot separating the unit name from the package name */
        int dotIdx = name.lastIndexOf('.');
        if (dotIdx < 0) {
            /* no dot in the name, can't be a unit name */
            return null;
        }

        /* separate the unit name from the package name */
        String pkgName = name.substring(0, dotIdx);
        String unitName = name.substring(dotIdx+1);
        
        /* load the package */
        Pkg pkg = findPkg(pkgName);
        if (pkg == null) {
            msg.warn("Warning: BrowserSession loadUnit could not " +
                "find package " + pkgName + " on the package path: ");
            for (int i = 0; i < this.dirs.length; i++) {
                msg.warn("    " + dirs[i]);
            }
            
            return null;
        }
        
        /* search the package for the unit */
        for (Unit unit : pkg.getChildren()) {
            if (unit.getName().equals(unitName)) {
                /* found it */
                return unit;
            }
        }

        /* never found it */
        return null;
    }
    
    // scan
    private void scan( File dir, String qname )
    {
        String[] fnames = dir.list();
        
        if (fnames == null) {
            return;
        }

        for (int i = 0; i < fnames.length; i++) {
            String fn = fnames[i];
            File f = new File(dir, fn);
            if (f.isDirectory()) {
                scan(f, qname == null ? fn : qname + '.' + fn);
            }
        }

        String cpath;
        try {
            cpath = dir.getCanonicalPath();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
    	}
        
        if (qname == null) {
            return;
        }
        
        if (this.qn2cp.containsKey(qname) && this.qn2cp.get(qname).equals(cpath)) {
            return;
        }

        File file = new File(cpath + "/package/" + qname + ".sch");
        if (file.exists()) {
            qn2cp.put(qname, cpath);
            cp2qn.put(cpath, qname);
        }
    }
    
    /**
     * Return a Map of package names indexed by the canonical path to
     * the package's base.
     */
    public synchronized Map<String,String> scanDirectories()
    {
        if (!this.rscan) {
            for (int i = 0; i < this.dirs.length; i++) {
                this.scan(new File(dirs[i]), null);
            }
            this.rscan = true;
        }

        return this.cp2qn;
    }
    
    /**
     * Return a Map all package base directories indexed by the package's
     * name.
     */
    public synchronized Map<String,String> scanRepositories()
    {
    	scanDirectories();
        return this.qn2cp;
    }

    /**
     * Set whether this session should preserve docs in Nodes.
     *
     * If false, spec objects loaded by this session will not
     * contain documentation. This is a Java heap memory
     * optimization.
     *
     * Default is to load documentation.
     */
    public void setLoadDocs(boolean loadDocs) {
        this.loadDocs = loadDocs;
    }

    /**
     * Return whether documentation is available in this session.
     *
     * Default is to load documentation.
     */
    public boolean getLoadDocs() {
        return loadDocs;
    }
}
