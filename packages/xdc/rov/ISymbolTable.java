package xdc.rov;

public interface ISymbolTable {
    /*!
     *  ======== getSymbolValue ========
     *  Returns the value of the symbol.
     *
     *  Returns -1 if the symbol does not exist.
     */
    long getSymbolValue(String name);

    /*!
     *  ======== lookupDataSymbol ========
     *  Find all symbol names for a data address.
     *
     *  Requirements:
     *  - If no symbols are found at this address, the appropriate return
     *    value is a zero-length array, not null.
     *  - This API should not throw any exceptions. If the underlying
     *    mechanism could throw an exception, this API should catch it and
     *    return a zero-length array.
     */
    String[] lookupDataSymbol(long addr);
    
    /*!
     *  ======== lookupFuncName ========
     *  Find all symbol names for a function address.
     *
     *  Requirements:
     *  - If no symbols are found at this address, the appropriate return
     *    value is a zero-length array, not null.
     *  - This API should not throw any exceptions. If the underlying
     *    mechanism could throw an exception, this API should catch it and
     *    return a zero-length array.
     */
    String[] lookupFuncName(long addr);

    /*!
     *  ======== getISA ========
     *  Returns the ISA of the current target as an integer.
     *
     *  Examples: 0x64, 0x28
     */
     int getISA();
}