include $(XDCROOT)/packages/xdc/bld/xdc_java.mak

# All schema files depend on jar files from xdc.services.intern.cmd,
# xdc.services.spec. xdc.services.global, xdc.services.intern.gen and
# xdc.services.intern.xsr. However, these packages must remove that dependency
# to avoid cycles.
XDCJARS:=

CLASSES  = $(patsubst %.java,%,$(wildcard *.java))
JARFILE  = java/package.jar

DOCDIR      = doc-files
JAVADOCDIR  = $(DOCDIR)/javadoc
PKGNAME     = $(subst /,.,$(PKGDIR))

ANTLR_TOOL   = $(XDCROOT)/packages/xdc/shelf/local/java/antlr_tool.jar
ANTLR   = $(XDCROOT)/packages/xdc/shelf/java/antlr.jar
RHINO	= $(XDCROOT)/packages/xdc/shelf/java/js.jar
ECLIPSE = $(XDCROOT)/packages/xdc/shelf/java/ecj.jar

JCPATH:=$(PKGROOT)$(PATHCHAR)$(ANTLR)$(PATHCHAR)$(RHINO)$(PATHCHAR)$(ECLIPSE)

.interfaces: GrammarParser.java

%Lexer.java %.tokens %Parser.java: %.g $(ANTLR)
	$(JAVA) $(JOPTS) -cp "$(ANTLR)$(PATHCHAR)$(ANTLR_TOOL)" org.antlr.Tool -o . $*.g

.libraries: $(JARFILE)
$(JARFILE): $(patsubst %,$(CLASSDIR)/%.class,$(CLASSES))

# build and release javadoc
$(DOCDIR): $(JAVADOCDIR)
$(JAVADOCDIR): .javadoc

.javadoc: .libraries
##	$(JDK)/bin/javadoc -quiet -d $(JAVADOCDIR) -sourcepath $(PKGROOT) -classpath $(JCPATH) *.java
	$(RM) $@
	$(TOUCH) $@

clean::
	$(RM) $(JARFILE)
	$(RMDIR) .javadoc $(JAVADOCDIR)
	$(RM) Grammar.tokens GrammarLexer.java GrammarParser.java
