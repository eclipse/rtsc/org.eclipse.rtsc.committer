/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== Dos.cpp ========
 */
#include <afx.h>

#include <jni.h>
#include <malloc.h>
#include <stdio.h>

#include "StringUtils.h"
#include "Dos.h"
#include "DosDll.h"

/*
 *  ======== toShortPath ========
 *  Convert a single Windows path from long to short names.
 */
CString toShortPath(const CString& path)
{
    /* find the last space character in the pathname */
    int spaceIndex = path.ReverseFind(' ');

    /* if no space in the pathname, do nothing */
    if (spaceIndex < 0) {
        return (path);
    }

    /* find the next directory separator after the space */
    int fwdIndex = path.Find('/', spaceIndex);
    int backIndex = path.Find('\\', spaceIndex);
    int slashIndex;
    if (fwdIndex < 0 && backIndex >= 0) {
        /* no foward slash found, so take the backslash index */
        slashIndex = backIndex;
    }
    else if (backIndex < 0 && fwdIndex >= 0) {
        /* no backslash found, so take the forward slash index */
        slashIndex = fwdIndex;
    }
    else if (fwdIndex < 0 && backIndex < 0) {
        /* no forward slash or backslash, so take the rest of the string */
        slashIndex = path.GetLength();
    }
    else {
        /* both found, so take whichever one comes first in the string */
        slashIndex = fwdIndex < backIndex? fwdIndex : backIndex;
    }

    /* separate path into a head containing spaces and a tail without */
    CString head = path.Left(slashIndex);
    CString tail = path.Right(path.GetLength() - slashIndex);

    /* make a temp buffer */
    TCHAR *buffer = (TCHAR*)malloc(MAX_PATH + 1);
    if (buffer == NULL) {
        return (path);
    }

    /* get the short path of the head part of the path */
    DWORD ret = GetShortPathName((LPCTSTR)head, buffer, MAX_PATH);

    /* if no short path, clean up and return the original path */
    if (ret == 0) {
        free(buffer);
        return (path);
    }

    /* convert the short path to CString and clean up memory */
    CString shortHead(buffer);
    free(buffer);

    /* join the tail back onto the shortened head */
    CString result(shortHead + tail);

    /* remove whitespace from before and after */
    result.TrimLeft();
    result.TrimRight();

    /* and return */
    return (result);
}

/*
 *  ======== getDOSPath ========
 *  Convert a semicolon-separated list of directories from long to short
 *  Windows names.
 */
CString xdc_services_io_Dos_getDOSPath(LPCTSTR pathCStr)
{
    CString path(pathCStr);

    /* split the string into an array of directory names */
    CStringArray longPaths;
    StringUtils::split(path, ";", longPaths);

    /* convert each directory name from long to short */
    CStringArray shortPaths;
    for (int i = 0; i < longPaths.GetSize(); i++) {
        /* remove whitespace from before and after */
        longPaths[i].TrimLeft();
        longPaths[i].TrimRight();

        /* convert to a short path */
        CString shortPath = toShortPath(longPaths[i]);

        /* only add the path if it is non-empty */
        if (shortPath.GetLength() > 0) {
            shortPaths.Add(shortPath);
        }
    }

    /* reassemble the string using a semicolon separator */
    CString shortPath;
    StringUtils::join(shortPaths, ";", shortPath);

    return (shortPath);
}

/*
 *  ======== Java_getDOSPath ========
 *  
 * Class:     Dos
 * Method:    getDOSPath
 */ 
JNIEXPORT jstring JNICALL Java_xdc_services_io_Dos_getDOSPath(JNIEnv *env,
                        jclass clazz, jstring path)
{
    /* convert from Java string to C string */
    const TCHAR *cpath = env->GetStringUTFChars(path, NULL);

    /* convert path from long to short names */
    CString result = xdc_services_io_Dos_getDOSPath(cpath);

    /* convert C string to to Java string */
    return (env->NewStringUTF(result));
}
