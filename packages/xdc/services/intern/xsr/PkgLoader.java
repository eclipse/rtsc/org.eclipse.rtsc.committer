/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import java.lang.reflect.*;
import java.io.*;
import java.net.*;
import java.util.*;
import org.mozilla.javascript.*;
import org.mozilla.javascript.optimizer.*;
import org.mozilla.javascript.tools.*;
import xdc.services.spec.*;
import xdc.services.global.*;
import xdc.services.intern.gen.*;

public class PkgLoader
{
    static PkgClassLoader clsLoader = new PkgClassLoader();
    
    Scriptable xdc;
    Scriptable gbl;
    Value.Obj om;
    BrowserSession ses;
    HashSet<String> pkgset;

    Pkg curpkg;
    
/*
    public PkgLoader( Scriptable xdc, String[] dirs, Env env )
    {
        this.xdc = xdc;
        this.om = (Value.Obj)xdc.get("om", null);
        this.gbl = (Scriptable)xdc.get("global", null);
        this.ses = new BrowserSession(dirs, env);
        this.pkgset = new HashSet();
    }
*/    
    public PkgLoader( Scriptable xdc, BrowserSession ses )
    {
        this.xdc = xdc;
        this.om = (Value.Obj)xdc.get("om", null);
        this.gbl = (Scriptable)xdc.get("global", null);
        this.ses = ses;
        this.pkgset = new HashSet();
    }
/*    
    // gen
    public void gen( String pn )
    {
        if (!this.pkgset.add(pn)) {
            return;
        }
        
        Pkg pkg = this.ses.findPkg(pn);
        Context cx = Global.getContext();
        
        System.out.println("compiling " + pn);

        String cmd = "d:/jdk1.5.0_05/bin/javac.exe ";
        cmd += "-cp /work/iliad/packages/xdc/shelf/java/js.jar;d:/work/iliad/packages/xdc/services/intern/xsr/java/package.jar ";
        cmd += pkg.getBaseDir() + "/package/" + pn.replace('.', '_') + ".java";
        
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            int stat = proc.waitFor();
            if (stat != 0) {
                System.out.println("*** compile failed ***");
            }
        }
        catch (Exception e) {
            Err.exit(e);
        }
        
//        genIncs(pkg);
    }
    
    // genIncs
    void genIncs( Pkg pkg )
    {
        Set<String> nS = new HashSet();
        
        for (Atom a : pkg.getRequires()) {
            String pn = a.getText();
            if (pn.startsWith("*")) {
                continue;
            }
            int k = pn.indexOf('{');
            if (k != -1) {
                pn = pn.substring(0, k);
            }
            if (pkg.getName().equals(pn)) {
                continue;
            }
            if (nS.add(pn)) {
                this.gen(pn);
            }
        }
        
        if (!pkg.getName().equals("xdc")) {
            if (nS.add("xdc")) {
                this.gen("xdc");
            }
            if (!pkg.getName().equals("xdc.corevers") && nS.add("xdc.corevers")) {
                this.gen("xdc.corevers");
            }
        }
        
        for (Unit unit : pkg.getUnits()) {
            for (Unit u2 : unit.getUses()) {
                String qn = u2.getQualName();
                int idx = qn.lastIndexOf('.');
                if (idx < 0) {
                    continue;
                }
                qn = qn.substring(0, idx);
                if (pkg.getName().equals(qn)) {
                    continue;
                }
                if (nS.add(qn)) {
                    this.gen(qn);
                }
            }
        }
    }

    // exec
    public void exec( String pn ) { this.exec(pn, this.ses.findPkg(pn).getBaseDir()); }
*/
    // exec
    public void exec( String pkgName, File clsFile ) throws 
        java.lang.reflect.InvocationTargetException {
        Class cls = PkgLoader.clsLoader.findPkgClass(pkgName, clsFile);
        try {
            Method met = cls.getMethod("exec", Scriptable.class, Session.class);
            Object inst = cls.newInstance();
            met.invoke(inst, this.xdc, this.ses);
        }
        catch (NoSuchMethodException e) {
            Err.exit(e);
        }
        catch (InstantiationException e) {
            Err.exit(e);
        }
        catch (IllegalAccessException e) {
            Err.exit(e);
        }
    }
    
    // ClsLoader
    static class PkgClassLoader extends URLClassLoader {
        
        PkgClassLoader()
        {
            super(new URL[0], PkgLoader.class.getClassLoader());
        }
        
        Class findPkgClass( String pkgName, File clsFile ) 
        {
            Class cls = null;
            String clsname = pkgName.replace('.', '_');
            byte[] bytes = null;

            try {
                FileInputStream fin = new FileInputStream(clsFile);
                bytes = new byte[fin.available()];
                fin.read(bytes);
                fin.close();
                cls = super.defineClass(clsname, bytes, 0, bytes.length);
            }
            catch (Exception e) {
                Err.exit(e);
            }

            return cls;
        }
    }
}
