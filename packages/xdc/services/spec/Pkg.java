/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
//
// ======== xdc.services.spec.Pkg ========
//

package xdc.services.spec;

import java.util.*;
import java.io.File;
import xdc.services.global.Vers;

public class Pkg
    extends Node
{
	
    // Static Serialization UID
    static final long serialVersionUID = 2236555847538118936L;
	
    List<Atom>   interNames;
    List<String> localSpecs;
    List<String> otherSpecs;
    List<String> miscSrcs;
    List<Atom>   modNames;
    List<Atom>   requires;
    List<Unit>   units;
    String       key;
    String       vers;
    String       bundle;
    
    volatile String baseDir;
    volatile Set<Atom> metaSet;
    volatile Map<Atom,List<String>> genMap;
    volatile boolean specChanged = false;
    volatile Set<String> bodyChanges = null;
    
    Pkg(Atom name, String key, List<Atom> modNames, List<Atom> interNames, List<Atom> requires)
    {
        this.name = name;
        this.key = key;
        this.vers = null;
        this.bundle = null;
        this.modNames = modNames;
        this.interNames = interNames;
        this.requires = requires;

        this.quals = EnumSet.noneOf(Qual.class);

        this.localSpecs = new ArrayList();
        this.miscSrcs = new ArrayList();
        this.otherSpecs = new ArrayList();
        this.units = new ArrayList();
    }

    // bindGenMap
    final void bindGenMap( Map<Atom, List<String>> genMap ) { this.genMap = genMap; }
    
    // bindMetaSet
    final void bindMetaSet( Set<Atom> metaSet ) { this.metaSet = metaSet; }
    
    // finalCheck
    void finalCheck()
    {
        super.finalCheck();

        for (Unit u : this.units) {
            u.finalCheck();
        }
    }

    // getBaseDir
    public final String getBaseDir()
    {
        return (this.baseDir);
    }

    public final String getRepository() {
    	int index = this.baseDir.lastIndexOf(this.name.text.replace('.', File.separatorChar));
    	return  this.baseDir.substring(0, index);
    }
    
    // getBodyChanges
    public final Set<String> getBodyChanges()
    {
        return bodyChanges;
    }
    
    // getChildren
    public final List<Unit> getChildren()
    {
        return (this.units);
    }

    // getMetaSet
    public final Map<Atom, List<String>> getGenMap()
    {
    	return this.genMap;
    }
    
    // getInterNames
    public final List<Atom> getInterNames()
    {
        return (this.interNames);
    }

    // getMetaSet
    public final Set<Atom> getMetaSet()
    {
    	return this.metaSet;
    }
    
    // getModNames
    public final List<Atom> getModNames()
    {
        return (this.modNames);
    }

    // getRequires
    public final List<Atom> getRequires()
    {
        return (this.requires);
    }

    // getQualName
    public final String getQualName()
    {
        return (this.name.text);
    }

    // getPkgName
    public final String getPkgName()
    {
        return (this.name.text);
    }

    // getLocalSpecs
    public final List<String> getLocalSpecs()
    {
        return (this.localSpecs);
    }

    // getMiscSrcs
    public final List<String> getMiscSrcs()
    {
        return (this.miscSrcs);
    }

    // getOtherSpecs
    public final List<String> getOtherSpecs()
    {
        return (this.otherSpecs);
    }

    // getUnits
    public final List<Unit> getUnits()
    {
        return (this.units);
    }

    // getKey
    public final String getKey()
    {
        return (this.key);
    }

    // getVersion (deprecated, use getKey()); we need to keep this method
    // to maintain compatibility with old schema files that reference the
    // compatibility key with this method.
    public final String getVersion()
    {
        return (getKey());
    }

    // getReleaseVersion
    public final String getReleaseVersion()
    {
        if (this.vers == null) {
            if (this.key != null && this.key.split(",").length >= 3) {
                this.vers = this.key + ", " + Vers.getDate(baseDir);
            }
            else {
                this.vers = this.key;
            }
        }
        return (this.vers);
    }

    // getProviderId
    public final String getProviderId()
    {
        if (this.bundle == null && this.baseDir != null) {
            this.bundle = Vers.getProviderId(this.baseDir);
        }

        return (this.bundle);
    }

    // getXmlTag
    public final String getXmlTag()
    {
        return ("package");
    }
    
    // isSameContent
    public final boolean isSameContent()
    {
        return !specChanged && bodyChanges == null;
    }
    
    // isSpecChanged
    public final boolean isSpecChanged()
    {
        return specChanged;
    }

    // parseDocs
    void parseDocs()
    {
        super.parseDocs();

        for (Unit u : this.units) {
            u.parseDocs();
        }
    }
    
    // setBaseDir
    void setBaseDir(String baseDir)
    {
        this.baseDir = baseDir;
        this.vers = null;
        this.bundle = null;
    }
    
    // setBodyChanges
    public void setBodyChanges( Set<String> set )
    {
        bodyChanges = set;
    }
    
    // setSpecChanged
    public void setSpecChanged()
    {
        specChanged = true;
    }
}
