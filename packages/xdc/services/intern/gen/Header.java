/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== Header.java ========
 *
 *! Revision History
 *! ================
 *! 11-Sep-2008 sasha   added Mod_Module_heap macro
 *! 13-Feb-2008 sasha   changed the names of the members of Types_Label
 */

package xdc.services.intern.gen;

import xdc.services.global.*;
import java.util.*;
import xdc.services.spec.*;
import xdc.services.intern.cmd.Builder;

public class Header
{
    private Glob glob = new Glob();
    private Cpp cppGen;

    // gen
    public void gen( Unit unit, Out out )
    {
        glob.setNames(unit);
        glob.mode = Glob.CDLMODE;
        glob.out = out;
        
        this.cppGen = new Cpp(glob);

        glob.genWarning();
        skip();

        ArrayList<String> secL = new ArrayList();
        secL.add("");
        secL.add("PROLOGUE");
        secL.add("INCLUDES");
        secL.add("");
        if (unit.needsRuntime()) {
            if (unit.hasCreateArgs()) {
                secL.add("CREATE ARGS");
            }
            if (unit.isMod() && !unit.isProxy()) {
                secL.add("INTERNAL DEFINITIONS");
            }
            if (unit.isMod()) {
                secL.add("MODULE-WIDE CONFIGS");
            }
            if (unit.isInst()) {
                secL.add("PER-INSTANCE TYPES");
            }
            if (unit.isInter() || unit.isHeir()) {
                secL.add("VIRTUAL FUNCTIONS");
            }
            if (unit.isMod()) {
                secL.add("FUNCTION DECLARATIONS");
            }
            else {
                secL.add("FUNCTION STUBS");
            }
            if (unit.isInter() || unit.isInst()) {
                secL.add("FUNCTION SELECTORS");
            }
            if (unit.isHeir()) {
                secL.add("CONVERTORS");
            }
            if (unit.isMod()) {
                secL.add("SYSTEM FUNCTIONS");
            }
            if (cppGen.isEnabled()) {
                secL.add("C++ CLIENT WRAPPER [experimental]");
            }
        }

        secL.add("");
        secL.add("EPILOGUE");
        if (unit.isMod() && !unit.isProxy()) {
            secL.add("STATE STRUCTURES");
        }
        secL.add("PREFIX ALIASES");
        if (unit.isMod() && !unit.isProxy()) {
            if (cppGen.isEnabled()) {
                secL.add("C++ SUPPLIER WRAPPER [experimental]");
            }
        }

        String[] secA = new String[]{};
        glob.genSections(secL.toArray(secA));
        skip();

        glob.genTitle("PROLOGUE");
        genPrologue(unit);

        glob.genTitle("INCLUDES");
        genHdrs(unit);

        glob.genTitle("AUXILIARY DEFINITIONS");
        genDecls(unit, true);

        if (unit.needsRuntime()) {

            if (unit.hasCreateArgs()) {
                glob.genTitle("CREATE ARGS");
                genCreateArgs(unit);
            }

            if (unit.isMod() && !unit.isProxy()) {
                glob.genTitle("INTERNAL DEFINITIONS");
                genDecls(unit, false);
            }

            if (unit.isMod()) {
                glob.genTitle("MODULE-WIDE CONFIGS");
                genModCfgs(unit);
            }

            if (unit.isInst()) {
                glob.genTitle("PER-INSTANCE TYPES");
                genInsTypes(unit);
            }

            if (unit.isInter() || unit.isHeir()) {
                glob.genTitle("VIRTUAL FUNCTIONS");
                genFxnTabV(unit);
            }

            if (unit.isMod()) {
                glob.genTitle("FUNCTION DECLARATIONS");
                genFxnDcls(unit);
            }
            else {
                glob.genTitle("FUNCTION STUBS");
                genFxnStubs(unit);
            }

            if (unit.isInter() || unit.isInst()) {
                glob.genTitle("FUNCTION SELECTORS");
                genFxnSelectors(unit);
            }

            if (unit.isHeir()) {
                glob.genTitle("CONVERTORS");
                genConvs(unit);
            }

            if (unit.isMod()) {
                glob.genTitle("SYSTEM FUNCTIONS");
                genSysFxns(unit);
            }
            
            if (cppGen.isEnabled()) {
                glob.genTitle("C++ CLIENT WRAPPER [experimental]");
            }
            cppGen.genPublics(unit);
        }

        glob.genTitle("EPILOGUE");
        genEpilogue(unit);

        if (unit.isMod() && !unit.isProxy()) {
            glob.genTitle("STATE STRUCTURES");
            genState(unit);
        }
        
        glob.genTitle("PREFIX ALIASES");
        genAlias(unit);

        if (unit.isMod() && !unit.isProxy()) {
            if (cppGen.isEnabled()) {
                glob.genTitle("C++ SUPPLIER WRAPPER [experimental]");
            }
            cppGen.genInternals(unit);
        }
    }

    // genAlias
    private void genAlias( Unit unit )
    {
        glob.out.printf("#if %1(__nested__) && %1(%2_nolocalnames)\n\n",
                "!defined", glob.cname);

        Alias alias = new Alias(glob);

        glob.genTitleD("module prefix");
        alias.gen(unit, unit.getName() + '_');
        String prefix = unit.attrString(Attr.A_Prefix);
        if (prefix != null && prefix.length() > 0) {
            glob.genTitleD("alternate prefix (@Prefix)");
            alias.gen(unit, prefix);
        }
        if (unit.attrBool(Attr.A_GlobalNames)) {
            glob.genTitleD("empty prefix (@GlobalNames)");
            alias.gen(unit, "", true);
        }

        // pickup proxy aliases
        for (Unit up : unit.getProxies()) {
            glob.genTitleD("proxies");
            glob.out.printf("#include <%1/package/%2.h>\n", glob.mkFname(unit.getPkgName()), up.getName());
        }

        cppGen.genClientAlias(unit);
        
        glob.out.printf("\n#endif\n");
    }

    // genArrTypes
    private void genArrTypes( String scope, Type type, String code, boolean fldflg )
    {
        int dims = glob.dims(code);
        Integer iobj = new Integer(dims);
        String cn = glob.cname + scope;

        genArrTypes(dims - 1, cn, type);

        if (fldflg) {
            glob.out.printf("%ttypedef __ARRAY%1_%2 __TA_%2;\n", iobj, cn);
            return;
        }

        glob.out.printf("%ttypedef const __ARRAY%1_%2 __TC_%2;\n", iobj, cn);

        Type.Array tarr = (Type.Array)glob.rawType(type);

        if (tarr.getDim() != null) {
            glob.out.printf("%ttypedef const __T%1_%2* __TF_%2;\n", iobj, cn);
        }
        else {
            glob.out.printf("%ttypedef __ARRAY%1_%2 __TF_%2;\n", iobj, cn);
        }
    }

    // genArrTypes
    private void genArrTypes( int dim, String cn, Type type )
    {
        Integer iobj = new Integer(dim + 1);
        String tn = "__T" + iobj + "_" + cn;

        Type traw = glob.rawType(type);
        Type.Array tarr = (dim > 0) ? (Type.Array)traw : null;

        if (tarr != null) {
            genArrTypes(dim - 1, cn, tarr.getBase());
        }

        glob.out.printf("%ttypedef ");
        if (dim == 0) {
            if (type.tcode().endsWith("O")) {
                String tcn = glob.mkCname(type.tspec().getRef().getScope());
                glob.out.printf("%1Instance_State %2", tcn, tn);
            }
            else {
                glob.genType(traw.term(), Glob.Type$TYP, tn);
            }
        }
        else {
            glob.out.printf("__ARRAY%1_%2 %3", new Integer(dim), cn, tn);
        }
        glob.out.printf(";\n");

        if (tarr != null && tarr.isVec() && tarr.getBase() instanceof Type.Array) {
            glob.out.printf("%ttypedef struct { int length; __T%1_%2* elem; } __ARRAY%1_%2;\n", iobj, cn);
        }
        else {
            glob.out.printf("%ttypedef ");
            glob.genArrType(type, "__ARRAY" + iobj + "_" + cn);
            glob.out.printf(";\n");
        }
    }

    // genConvs
    private void genConvs( Unit unit )
    {
        int k = 0;
        for (Unit iu : unit.getInherits()) {
            if (iu.getQualName().equals("xdc.runtime.IModule")) {
                continue;
            }
            String iin = glob.mkCname(iu.getQualName());
            iin = iin.substring(0, iin.length() - 1);
            String ks = (++k == 1) ? "" : ("" + k);

            glob.genTitleD("Module_upCast" + ks);
            glob.out.printf("static inline %2_Module %1Module_upCast%3(", glob.cname, iin, ks);
            if (unit.isInter()) {
                glob.out.printf(" %1Module m )\n{\n%+%treturn(%2_Module)m;\n}\n%-", glob.cname, iin);
            }
            else {
                glob.out.printf(" void )\n{\n%+");
                String fs = !unit.isProxy() ? ("&" + glob.cname + "Module__FXNS__C") : (glob.cname + "Proxy__delegate__S()");
                glob.out.printf("%treturn (%1_Module)%2;\n}\n%-", iin, fs);
            }
            glob.genTitleD("Module_to_" + iin);
            glob.out.printf("#define %1Module_to_%2 %1Module_upCast%3\n", glob.cname, iin, ks);
            if (unit.isInter()) {
                glob.genTitleD("Module_downCast" + ks);
                glob.out.printf("static inline %1Module %1Module_downCast%2(",
                    glob.cname, ks);
                glob.out.printf(" %1_Module m ", iin);
                glob.out.printf(")\n{\n%+");
                glob.out.printf("%txdc_runtime_Types_Base* b; for (b = m->__base; b; b = b->base) {\n%+");
                glob.out.printf("%tif (b == &%1Interface__BASE__C) return (%1Module)m;\n%-", glob.cname);
                glob.out.printf("%t} return 0;\n}\n%-");
                glob.genTitleD("Module_from_" + iin);
                glob.out.printf("#define %1Module_from_%2 %1Module_downCast%3\n", glob.cname, iin, ks);
            }

            if (unit.isStatic() || iu.isStatic()) {
                continue;
            }


            glob.genTitleD("Handle_upCast" + ks);
            glob.out.printf("static inline %2_Handle %1Handle_upCast%3(",
                glob.cname, iin, ks);
            glob.out.printf(" %1Handle i ", glob.cname);
            glob.out.printf(")\n{\n%+");
            glob.out.printf("%treturn (%1_Handle)i;\n}\n%-", iin);
            glob.genTitleD("Handle_to_" + iin);
            glob.out.printf("#define %1Handle_to_%2 %1Handle_upCast%3\n", glob.cname, iin, ks);

            glob.genTitleD("Handle_downCast" + ks);
            glob.out.printf("static inline %1Handle %1Handle_downCast%2(", glob.cname, ks);
            glob.out.printf(" %1_Handle i ", iin);
            glob.out.printf(")\n{\n%+");
            glob.out.printf("%t%1_Handle i2 = (%1_Handle)i;\n", iin);
            if (unit.isMod()) {
                if (unit.isProxy()) {
                    glob.out.printf("if (%1Proxy__abstract__S()) return (%1Handle)i;\n", glob.cname);
                }
                String fs = !unit.isProxy() ? ("&" + glob.cname + "Module__FXNS__C") : (glob.cname + "Proxy__delegate__S()");
                glob.out.printf("%treturn (void*)i2->__fxns == (void*)%2 ? (%1Handle)i : 0;\n}\n%-",
                    glob.cname, fs);
            }
            else {
                glob.out.printf("%txdc_runtime_Types_Base* b; for (b = i2->__fxns->__base; b; b = b->base) {\n%+");
                glob.out.printf("%tif (b == &%1Interface__BASE__C) return (%1Handle)i;\n%-", glob.cname);
                glob.out.printf("%t} return 0;\n}\n%-");
            }
            glob.genTitleD("Handle_from_" + iin);
            glob.out.printf("#define %1Handle_from_%2 %1Handle_downCast%3\n", glob.cname, iin, ks);
        }
    }

    // genCreateArgs
    private void genCreateArgs( Unit unit )
    {
        String ts = "Args__create";
        glob.genTitleD(ts);

        for (Decl.Arg arg : unit.getCreator().getArgs()) {
            if (glob.isArr(arg.getTypeCode())) {
                genArrTypes(ts + "__" + arg.getName(), arg.getType(), arg.getTypeCode(), true);
            }
        }

        glob.out.printf("%ttypedef struct %1%2 {\n%+", glob.cname, ts);
        for (Decl.Arg arg : unit.getCreator().getArgs()) {
            if (glob.isArr(arg.getTypeCode())) {
                glob.out.printf("%t__TA_%1 %2;\n",
                    glob.cname + ts + "__" + arg.getName(), arg.getName());
            }
            else {
                glob.out.printf("%t");
                glob.genType(arg.getType(), Glob.Type$TYP, arg.getName());
                glob.out.print(";\n");
            }
        }
        glob.out.printf("%-%t} %1%2;\n", glob.cname, ts);
    }

    // genDecls
    private void genDecls( Unit unit, boolean eflag )
    {
        for (Decl d : unit.getDecls()) {
            if (d.isMeta() || d.isExternal() != eflag) {
                continue;
            }
            else if (d.getParent() != unit && d instanceof Decl.AuxDef && !(d instanceof Decl.OverridableDef)) {
                String dcn = d.getQualName().replace('.', '_');
                glob.genTitleD(d.getName());
                if (d instanceof Decl.IsType) {
                    glob.out.printf("%ttypedef %3 %1%2;\n", glob.cname, d.getName(), dcn);
                }
                else {
                    glob.out.printf("%t#define %1%2 %3\n", glob.cname, d.getName(), dcn);
                }
                continue;
            }
            else if (d instanceof Decl.EnumVal) {
                continue;
            }
            else if (d instanceof Decl.Struct) {
                genStruct((Decl.Struct)d, unit);
            }
            else if (d instanceof Decl.AuxDef) {
                glob.genTitleD(d.getName());
                glob.genDecl(d);
            }
        }

        if (eflag) {
            return;
        }

        if (unit.isStatic()) {
            return;
        }

        //TODO: optimize for pure proxies


        if (unit.delegatesTo() != null && unit.delegatesTo().isInst()) {
            String qs = unit.isSized() ? "static inline" : "__extern";
            String dcn = glob.mkCname(unit.delegatesTo().getQualName());
            if (unit.isSized()) {
                glob.out.printf("xdc__CODESECT(%1, \"%1\")\n", glob.cname + "Object_delegate");
            }
            glob.out.printf("%1 %2Handle %3Object_delegate( %3Object* obj )",
                    qs, dcn, glob.cname);
            if (unit.isSized()) {
                glob.out.printf(" { return ((%1Handle)(&obj->__deleg)); }\n", dcn);
            }
            else {
                glob.out.printf(";\n");
            }
        }
    }

    // genEpilogue
    private void genEpilogue( Unit unit )
    {
        if (unit.isMod() && unit.hasCustHdr()) {
            glob.out.printf("#include <%1__epilogue.h>\n\n", glob.mkFname(unit.getQualName()));
        }

        glob.out.printf("#ifdef %1_top__\n#undef __nested__\n#endif\n\n", glob.iname);

        glob.out.printf("#endif /* %1_include */\n\n", glob.iname);
    }

    // genFxnDcl
    private void genFxnDcl( Unit unit, Decl.Fxn fxn, String fn, boolean extFlag )
    {
        genFxnDcl(unit, fxn, fn, extFlag, null);
    }

    // genFxnDcl
    private void genFxnDcl( Unit unit, Decl.Fxn fxn, String fn, boolean extFlag, String deleg )
    {
        glob.genType(fxn.getType(), Glob.Type$TYP, fn);
        glob.out.print("(");
        String sp = " ";
        if (fxn.isInst()) {
            glob.out.printf(" %1%2 __inst", glob.cname, fn.endsWith("__F") ? "Object*" : "Handle");
            sp = ", ";
        }
        glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, sp);
        if (fxn.isVarg()) {
            glob.out.printf(", %1", extFlag ? "..." : "va_list _va");
        }

        if (deleg == null) {
            glob.out.printf(" );\n");
            return;
        }

        glob.out.printf(" ) {\n%+%t");
        glob.out.printf("%1%2%3(", !glob.isVoid(fxn) ? "return " : "", deleg, fxn.getName());
        sp = "";
        if (fxn.isInst()) {
            glob.out.printf("%1Object_delegate(__inst)", glob.cname);
            sp = ", ";
        }
        glob.genArgNames(fxn.getArgs(), sp);
        glob.out.printf("%1); }\n%-", fxn.isVarg() ? ", _va" : "");
    }

    // genFxnDcls
    private void genFxnDcls( Unit unit )
    {
        boolean iFinalize = unit.attrBool(Attr.A_InstanceFinalize);
        boolean iInitErr = unit.attrBool(Attr.A_InstanceInitError);
        boolean mStart = unit.attrBool(Attr.A_ModuleStartup);

        glob.genTitleD("Module_startup");
        if (unit.isProxy()) {
            glob.out.printf("#define %1Module_startup( state ) -1\n", glob.cname);
        }
        else if (mStart) {
            glob.out.printf("#define %1%2 %1%2__E\n", glob.cname, "Module_startup");
            for (String suf : new String[]{"__E", "__F", "__R"}) {
                glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", glob.cname + "Module_startup", suf);
                glob.out.printf("%t__extern xdc_Int %1Module_startup%2( xdc_Int state );\n",
                        glob.cname, suf);
            }
        }
        else {
            glob.out.printf("#define %1Module_startup( state ) -1\n", glob.cname);
        }

        if (unit.isInst()) {

            // TODO remove unnecessary decls if !unit.iobjflag

            if (!unit.isProxy()) {

                for (String suf : new String[]{"__F", "__R"}) {
                    glob.genTitleD("Instance_init" + suf);
                    glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", glob.cname + "Instance_init", suf);
                    glob.out.printf("%t__extern %2 %1Instance_init%3(",
                            glob.cname, iInitErr ? "int" : "void", suf);
                    glob.out.printf(" %1Object*", glob.cname);
                    glob.genCreArgDecls(unit, Glob.Type$LCL, ", ");
                    glob.out.printf("const %1Params*%2 );\n", glob.cname, glob.errBlk(unit));
                    if (iFinalize) {
                        glob.genTitleD("Instance_finalize" + suf);
                        glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", glob.cname + "Instance_finalize", suf);
                        glob.out.printf("%t__extern void %1Instance_finalize%3( %1Object* %2);\n",
                                glob.cname, iInitErr ? ", int " : "", suf);
                    }
                }
            }
        }

        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isMeta()) {
                continue;
            }

            String fn = glob.cname + fxn.getName();
            String suf;

            if (fxn.isSys()) {
                glob.genTitleD(fxn.getName() + "__S");
                glob.out.printf("%txdc__CODESECT(%1__S, \"%1\")\n", fn);
                glob.out.printf("%t__extern ");
                genFxnDcl(unit, fxn, fn + "__S", true);
            }
            else if (unit.isProxy()) {
                suf = "__E";
                glob.genTitleD(fxn.getName() + suf);
                glob.out.printf("#define %1%2 %1%2%3\n", glob.cname, fxn.getName(), suf);
                glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", fn, suf);
                glob.out.printf("%t__extern ");
                genFxnDcl(unit, fxn, fn + suf, true);
                if (fxn.isVarg()) {
                    suf = "_va__E";
                    glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", fn, suf);
                    glob.out.printf("%t__extern ");
                    genFxnDcl(unit, fxn, fn + suf, false);
                }
                suf = fxn.isVarg() ? "_va__R" : "__R";
                glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", fn, suf);
                glob.out.printf("%t__extern ");
                genFxnDcl(unit, fxn, fn + suf, true);
            }
            else if (!fxn.attrBool(Attr.A_Macro)) {
                suf = "__E";
                glob.genTitleD(fxn.getName() + suf);
                glob.out.printf("#define %1%2 %1%2%3\n", glob.cname, fxn.getName(), suf);
                glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", fn, suf);
                glob.out.printf("%t__extern ");
                genFxnDcl(unit, fxn, fn + suf, true);
                if (fxn.isVarg()) {
                    suf = "_va__E";
                    glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", fn, suf);
                    glob.out.printf("%t__extern ");
                    genFxnDcl(unit, fxn, fn + suf, false);
                }
                suf = fxn.isVarg() ? "_va__F" : "__F";
                glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", fn, suf);
                glob.out.printf("%t__extern ");
                genFxnDcl(unit, fxn, fn + suf, false);
                suf = fxn.isVarg() ? "_va__R" : "__R";
                glob.out.printf("%t__extern ");
                genFxnDcl(unit, fxn, fn + suf, false);
            }
        }

        for (Decl.Fxn fxn : unit.getInternFxns()) {
            if (fxn.isMeta()) {
                continue;
            }
            String fn = glob.cname + fxn.getName();
            String suf = "__I";
            glob.genTitleD(fxn.getName() + suf);
            glob.out.printf("#define %1%2 %1%2%3\n", glob.cname, fxn.getName(), suf);
            glob.out.printf("%txdc__CODESECT(%1%2, \"%1\")\n", fn, suf);
            glob.out.printf("%t__extern ");
            genFxnDcl(unit, fxn, fn + suf, true);
        }
    }

    // genFxnSelectors
    private void genFxnSelectors( Unit unit )
    {
        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isMeta() || fxn.isSys() || fxn.hasAttr(Attr.A_Macro)) {
                continue;
            }
            if (unit.isMod() && fxn.isStatic()) {
                continue;
            }

            glob.genTitleD(fxn.getName() + "_{FxnT,fxnP}");
            glob.out.printf("typedef ");
            String tn = glob.cname + fxn.getName() + "_FxnT";
            glob.genType(fxn.getType(), Glob.Type$TYP, "(*" + tn + ")(");
            String sp = "";
            if (fxn.isInst()) {
                glob.out.print("void*");
                sp = ", ";
            }
            glob.genArgDecls(fxn.getArgs(), Glob.Type$ABS, sp);
            if (fxn.isVarg()) {
                glob.out.printf(", va_list");
            }
            glob.out.print(");\n");

            glob.out.printf("static inline %1 %2%3_fxnP( ", tn, glob.cname, fxn.getName());
            if (unit.isMod()) {
                glob.out.printf("void )\n{\n%+%treturn (%1)%2%3; \n}\n%-", tn, glob.cname, fxn.getName());
            }
            else {
                glob.out.printf("%1%2 __inst )\n{\n%+",
                        glob.cname, fxn.isStatic() ? "Module" : "Handle");
                if (fxn.isStatic()) {
                    glob.out.printf("%treturn (%1)__inst->%2;\n}\n%-", tn, fxn.getName());
                }
                else {
                    glob.out.printf("%treturn (%1)__inst->__fxns->%2;\n}\n%-", tn, fxn.getName());
                }
            }
        }
    }

    // genFxnStubs
    private void genFxnStubs( Unit unit )
    {
        String ts = "xdc_runtime_Types_";

        if (unit.isInst() && unit.getCreator() != null) {
            glob.genTitleD("create");
            glob.out.printf("static inline %1Handle %1create( %1Module __mod", glob.cname);
            glob.genArgDecls(unit.getCreator().getArgs(), Glob.Type$LCL, ", ");
            glob.out.printf(", const %1Params* __prms, %2 )\n{\n%+", glob.cname, Glob.ERRARG);
            if (unit.hasCreateArgs()) {
                glob.out.printf("%t%1Args__create __args;\n", glob.cname);
                for (Decl.Arg arg : unit.getCreator().getArgs()) {
                    glob.out.printf("%t__args.%1 = %1;\n", arg.getName());
                }
            }
            glob.out.printf("%treturn (%1Handle) __mod->__sysp->__create(0, 0, ", glob.cname);
            glob.out.printf("%1, ", unit.hasCreateArgs() ? "&__args" : "0");
            glob.out.printf("(const xdc_UChar*)__prms, sizeof (%1Params), __eb);\n}\n%-", glob.cname);
        }

        if (unit.isInst()) {
            glob.genTitleD("delete");
            glob.out.printf("static inline void %1delete( %1Handle* instp )\n{\n%+", glob.cname);
            glob.out.printf("%t(*instp)->__fxns->__sysp->__delete(instp);\n}\n%-", glob.cname);
            glob.genTitleD("Handle_to_Module");
            glob.out.printf("static inline %1Module %1Handle_to_Module( %1Handle inst )\n{\n%+",
                    glob.cname);
                glob.out.printf("%treturn inst->__fxns;\n}\n%-", glob.cname);
            glob.genTitleD("Handle_label");
            glob.out.printf("static inline %2* %1Handle_label( %1Handle inst, %2* lab )\n{\n%+",
                    glob.cname, ts + "Label");
            glob.out.printf("%treturn inst->__fxns->__sysp->__label(inst, lab);\n}\n%-", glob.cname);
        }

        glob.genTitleD("Module_id");
        glob.out.printf("static inline %2ModuleId %1Module_id( %1Module mod )\n{\n%+",
                glob.cname, ts);
        glob.out.printf("%treturn mod->__sysp->__mid;\n}\n%-", glob.cname);

        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isMeta() || fxn.isSys() || fxn.overrides() != null || fxn.hasAttr(Attr.A_Macro)) {
                continue;
            }

            glob.genTitleD(fxn.getName());
            glob.out.printf("static inline ");
            glob.genType(fxn.getType(), Glob.Type$CDL);
            glob.out.printf("( %1%2 __inst", glob.cname, fxn.isStatic() ? "Module" : "Handle");
            glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, ", ");
            if (fxn.isVarg()) {
                glob.out.printf(", va_list _va");
            }
            glob.out.printf(" )\n{\n%+%t");
            if (!glob.isVoid(fxn)) {
                glob.out.printf("return ");
            }
            if (fxn.isStatic()) {
                glob.out.printf("__inst->%1(", fxn.getName());
            }
            else {
                glob.out.printf("__inst->__fxns->%2(", glob.cname, fxn.getName());
            }
            String sp = "";
            if (fxn.isInst()) {
                glob.out.printf("(void*)__inst");
                sp = ", ";
            }
            glob.genArgNames(fxn.getArgs(), sp);
            if (fxn.isVarg()) {
                glob.out.printf(", _va");
            }
            glob.out.printf(");\n}\n%-");
        }
    }

    // genFxnTabV
    private void genFxnTabV( Unit unit )
    {
        glob.genTitleD("Fxns__");
        glob.out.printf("%tstruct %1Fxns__ {\n%+", glob.cname);
        glob.out.printf("%txdc_runtime_Types_Base* __base;\n");
        glob.out.printf("%tconst xdc_runtime_Types_SysFxns2* __sysp;\n");

        for (Decl.Fxn fxn : unit.getAllFxns()) {
            if (fxn.isMeta() || fxn.isSys() || fxn.overrides() != null) {
                continue;
            }
            if (unit.isMod() && fxn.getParent().isMod()) {
                continue;
            }
            if (fxn.attrBool(Attr.A_Macro)) {
                continue;
            }
            glob.out.tab();
            glob.genType(fxn.getType(), Glob.Type$PTR);
            glob.out.print("(");
            String sp = "";
            if (fxn.isInst()) {
                glob.out.printf("%1", unit.isMod() ? glob.cname + "Handle" : "void*");
                sp = ", ";
            }
            glob.genArgDecls(fxn.getArgs(), Glob.Type$ABS, sp);
            if (fxn.isVarg()) {
                glob.out.printf(", va_list");
            }
            glob.out.print(");\n");
        }

        glob.out.printf("%txdc_runtime_Types_SysFxns2 __sfxns;\n");
        glob.out.printf("%-%t};\n");

        if (unit.isMod()) {
            glob.genTitleD("Module__FXNS__C");
            glob.out.printf("%t__extern const %1Fxns__ %1Module__FXNS__C;\n", glob.cname);
        }
        else {
            String bs = "xdc_runtime_Types_Base";
            glob.genTitleD("Interface__BASE__C");
            glob.out.printf("%t__extern const %2 %1Interface__BASE__C;\n", glob.cname, bs);
        }
    }

    // genHdrs
    private void genHdrs( Unit unit )
    {
        // TODO remove exclusions of xdc.runtime

        glob.out.printf("#include <xdc/std.h>\n");
        skip();

        if (unit.needsRuntime()) {
            glob.out.printf("#include <xdc/runtime/xdc.h>\n");
            if (!unit.getQualName().equals("xdc.runtime.Types")) {
                glob.out.printf("#include <xdc/runtime/Types.h>\n");
            }
            if (unit.isInst() && !unit.getQualName().equals("xdc.runtime.IInstance")) {
                glob.out.printf("#include <xdc/runtime/IInstance.h>\n");
            }
        }

        if (unit.isMod() && unit.hasCustHdr()) {
            glob.out.printf("#include <%1__prologue.h>\n", glob.mkFname(unit.getQualName()));
        }

        glob.out.printf("#include <%1package/package.defs.h>\n", glob.mkFname(unit.getPkgName() + '.'));
        skip();

        if (unit.getQualName().equals("xdc.runtime.Types")) {
            return;
        }

        boolean nl = false;

        for (Unit ud : unit.getUses()) {
            if (ud == unit || ud.isMeta() || ud.isProxy()) {
                continue;
            }
            glob.out.printf("#include <%1.h>\n", glob.mkFname(ud.getQualName()));
            nl = true;
        }

        for (Unit up : unit.getProxies()) {
            glob.out.printf("#include <%1/package/%2.h>\n", glob.mkFname(unit.getPkgName()), up.getName());
            nl = true;
        }

        if (nl) {
            skip();
        }
    }

    // genInsTypes
    private void genInsTypes( Unit unit )
    {
        String cn = glob.mode == Glob.SYNMODE ? glob.sname : glob.cname;
        boolean isIInst = unit.getQualName().equals("xdc.runtime.IInstance");

        glob.genTitleD("Params");

        for (Decl d : unit.getDecls()) {
            if (!(d instanceof Decl.Config) || d.isStatic() || d.isMeta() || d.overrides() != null) {
                continue;
            }
            Decl.Config cfg = (Decl.Config)d;
            if (glob.isArr(cfg.getTypeCode())) {
                genArrTypes(cfg.getName(), cfg.getType(), cfg.getTypeCode(), true);
            }
        }

        glob.out.printf("%tstruct %1Params {\n%+", cn);
        glob.out.printf("%tsize_t __size;\n");
        if (!isIInst) {
            glob.out.printf("%tconst void* __self;\n");
            glob.out.printf("%tvoid* __fxns;\n");
        }
        if (!isIInst) {
            glob.out.printf("%txdc_runtime_IInstance_Params* instance;\n");
        }
        for (Decl d : unit.getDecls()) {
            if (!(d instanceof Decl.Config) || d.isStatic() || d.isMeta() || d.overrides() != null) {
                continue;
            }
            Decl.Config cfg = (Decl.Config)d;
            if (cfg.isStatic() || cfg.isMeta()) {
                continue;
            }
            if (glob.isArr(cfg.getTypeCode())) {
                glob.out.printf("%t__TA_%1 %2;\n", glob.cname + cfg.getName(), cfg.getName());
            }
            else {
                glob.out.tab();
                glob.genType(cfg.getType(), Glob.Type$TYP, cfg.getName());
                glob.out.printf(";\n");
            }
        }
        if (unit.isMod() && !isIInst) {
            glob.out.printf("%txdc_runtime_IInstance_Params __iprms;\n");
        }
        glob.out.printf("%-%t};\n");

        if (unit.isMod()) {
            glob.genTitleD("Struct");
            glob.out.printf("struct %1Struct {\n%+", glob.cname);
            glob.genStateFlds(unit, true, true, false);
            glob.out.printf("%txdc_runtime_Types_CordAddr __name;\n");
            glob.out.printf("%-};\n");
        }
    }

    // genModCfgs
    private void genModCfgs( Unit unit )
    {
        if (unit.isMod()) {
            for (Decl.Config cfg : unit.getConfigs()) {

            	if (!cfg.isStatic() || cfg.isMeta()) {
                    continue;
                }
                glob.genTitleD(cfg.getName());
                if (!cfg.isSys()) {
                    glob.out.printf("%t#define %1%2 (%1%2__C)\n", glob.cname, cfg.getName());
                }
                if (glob.isArr(cfg.getTypeCode())) {
                    genArrTypes(cfg.getName(), cfg.getType(), cfg.getTypeCode(), true);
                }
                String cfgn = glob.cname + cfg.getName();
                String cfgt = "CT__" + cfgn;
                glob.out.printf("%ttypedef ");
                if (glob.isArr(cfg.getTypeCode())) {
                    glob.out.printf("__TA_%1 %2", cfgn, cfgt);
                }
                else if (cfg.getTypeCode().equals("s")) {
                    glob.out.printf("xdc_StringC %1", cfgt);
                }
                else {
                    glob.genType(cfg.getType(), Glob.Type$TYP, cfgt);
                }
                glob.out.printf(";\n");
                glob.out.printf("%t__extern __FAR__ const CT__%1%2 %1%2__C;\n", glob.cname, cfg.getName());
            }
        }
    }

    // genPrologue
    private void genPrologue( Unit unit )
    {
        glob.out.printf("#ifndef %1_include\n#define %1_include\n", glob.iname);
        skip();

        glob.out.printf("#ifndef __nested__\n#define __nested__\n#define %1_top__\n#endif\n", glob.iname);
        skip();

        glob.genDefines();

        glob.out.printf("#define %1__VERS %2\n", glob.cname, Builder.BLUE_VERS);
        skip();
    }

    // genState
    private void genState( Unit unit )
    {
        glob.out.printf("#if %2(__config__) || (!%2(__nested__) && %2(%1_internalaccess))\n", glob.cname, "defined");
        skip();
        glob.out.printf("#ifndef %1_include_state\n#define %1_include_state\n", glob.iname);
        skip();

        if (unit.getSession().findDecl(unit, "Module_State") != null) {
            glob.genTitleD("Module_State");
            glob.out.printf("struct %1Module_State {\n%+", glob.cname);
            glob.genStateFlds(unit, false, false, true);
            glob.out.printf("%-};\n");
            glob.genTitleD("Module__state__V");
            glob.out.printf("extern struct %1Module_State__ %1Module__state__V;\n", glob.cname);
        }

        if (unit.isInst()) {
            glob.genTitleD("Object");
            glob.out.printf("struct %1Object {\n%+", glob.cname);
            glob.genStateFlds(unit, true, false, true);
            glob.out.printf("%-};\n");
        }

        for (Decl.Field fld : glob.offsetFlds(unit)) {
            String sn = fld.getParent().getName();
            boolean mflg = sn.charAt(0) == 'M';
            boolean aflg = (Decl.objKind(fld) == Decl.Signature.ObjKind.ARRAY);
            Ref r = glob.rawType(fld.getType()).tspec().getRef();
            String on = glob.cname + sn + '_' + fld.getName();
            String ot = glob.mkCname(r.getScope()) + (aflg ? "Instance_State*" : "Handle");
            String a0 = mflg ? "" : (glob.cname + "Object* obj");
            String base = mflg ? ("&" + glob.cname + "Module__state__V") : "obj";
            glob.genTitleD(sn + '_' + fld.getName());
            glob.out.printf("__extern __FAR__ const xdc_SizeT %1__O;\n", on);
            glob.out.printf("static inline %1 %2(%3)\n{\n%+", ot, on, a0);
            glob.out.printf("%treturn (%1)(((char*)%2) + %3__O);\n}\n%-", ot, base, on);
        }

        cppGen.genState(unit);

        skip();
        glob.out.printf("#endif /* %1_include_state */\n", glob.iname);
        skip();
        glob.out.printf("#endif\n");
    }

    // genStruct
    private void genStruct( Decl.Struct str, Unit unit )
    {
        boolean isState =  str.getName().equals("Instance_State") || str.getName().equals("Module_State");
        boolean first = true;

        if (str.getFields() != null) {
            for (Decl.Field fld : str.getFields()) {
                if (isState && Decl.objKind(fld) == Decl.Signature.ObjKind.ARRAY) {
                    continue;
                }
                else if (glob.isArr(fld.getTypeCode())) {
                    if (first) {
                        glob.genTitleD(str.getName());
                        first = false;
                    }
                    genArrTypes(str.getName() + "__" + fld.getName(), fld.getType(), fld.getTypeCode(), true);
                }
            }
        }

        if (isState) {
            return;
        }

        if (first) {
            glob.genTitleD(str.getName());
        }

        String skw = str.isUnion() ? "union" : "struct";
        String sid = glob.cname + str.getName();
        if (str.getFields() == null) {
            glob.out.printf("%t%1 %2;\n", skw, sid);
            return;
        }

        if (str.overrides() != null) {
    		sid = str.overrides().getQualName().replace('.', '_');
        }
        
        glob.out.printf("%t%1 %2 {\n%+", skw, sid);
        for (Decl.Field fld : str.getFields()) {
            String fn = fld.getName();
            if (glob.isArr(fld.getTypeCode())) {
                glob.out.printf("%t__TA_%1 %2;\n",
                        glob.cname + str.getName() + "__" + fld.getName(), fn);
            }
            else {
                glob.out.tab();
                glob.genType(fld.getType(), Glob.Type$TYP, fn);
                glob.out.print(";\n");
            }
        }
        glob.out.printf("%-%t};\n");
    }

    // genSysFxns
    private void genSysFxns( Unit unit )
    {
        glob.genTitleD("Module_startupDone");
        glob.out.printf("#define %1%2%3() %1%2_%3__S()\n", glob.cname,
            "Module_", "startupDone");
        glob.genTitleD("Object_heap");
        glob.out.printf("#define %1%2%3() %1%2_%3__C\n", glob.cname, "Object_",
            "heap");
        glob.genTitleD("Module_heap");
        glob.out.printf("#define %1%2%3() %1%4_%3__C\n", glob.cname, "Module_",
            "heap", "Object_");

        glob.genTitleD("Module_id");
        glob.out.printf("static inline CT__%1%2 %1Module_id( void ) \n{\n%+",
            glob.cname, "Module__id");
        glob.out.printf("%treturn %1%2__C;\n", glob.cname, "Module__id");
        glob.out.printf("%-}\n");

        if (unit.isProxy()) {
            glob.genTitleD("Proxy_abstract");
            glob.out.printf("#define %1%2%3() %1%2_%3__S()\n", glob.cname,
                "Proxy_", "abstract");
            String icn = glob.mkCname(unit.getSuper().getQualName());
            glob.genTitleD("Proxy_delegate");
            glob.out.printf("#define %1%2%3() ((%4Module)%1%2_%3__S())\n",
                glob.cname, "Proxy_", "delegate", icn);
        }
        else {
            glob.genTitleD("Module_hasMask");
            glob.out.printf("static inline xdc_Bool %1Module_hasMask( void ) \n{\n%+",
                glob.cname);
            glob.out.printf("%treturn %1%2 != NULL;\n", glob.cname,
                "Module__diagsMask__C");
            glob.out.printf("%-}\n");
            glob.genTitleD("Module_getMask");
            glob.out.printf("static inline xdc_Bits16 %1Module_getMask( void ) \n{\n%+", glob.cname);
            glob.out.printf("%treturn %1%2 != NULL ? *%1%2 : 0;\n", glob.cname,
                "Module__diagsMask__C");
            glob.out.printf("%-}\n");
            glob.genTitleD("Module_setMask");
            glob.out.printf("static inline xdc_Void %1Module_setMask( xdc_Bits16 mask ) \n{\n%+", glob.cname);
            glob.out.printf("%tif (%1%2 != NULL) *%1%2 = mask;\n", glob.cname,
                "Module__diagsMask__C");
            glob.out.printf("%-}\n");
        }

        if (!unit.isInst()) {
            return;
        }

        boolean iInitErr = unit.attrBool(Attr.A_InstanceInitError);

        glob.genTitleD("Params_init");
        glob.out.printf("static inline void %1Params_init( %1Params* prms ) \n{\n%+", glob.cname);
        glob.out.printf("%tif (prms) {\n%+");
        glob.out.printf("%t%1%2__init__S(prms, 0, %3(%1%2), %3(xdc_runtime_IInstance_%2));\n",
            glob.cname, "Params", "sizeof");
        glob.out.printf("%-%t}\n");
        glob.out.printf("%-%t}\n");

        glob.genTitleD("Params_copy");
        glob.out.printf("static inline void %1Params_copy( %1Params* dst, const %1Params* src ) \n{\n%+", glob.cname);
        glob.out.printf("%tif (dst) {\n%+");
        glob.out.printf("%t%1%2__init__S(dst, (xdc_Ptr)src, %3(%1%2), %3(xdc_runtime_IInstance_%2));\n",
            glob.cname, "Params", "sizeof");
        glob.out.printf("%-%t}\n");
        glob.out.printf("%-%t}\n");

        if (!unit.isProxy()) {
            glob.genTitleD("Object_count");
            glob.out.printf("#define %1%2%3() %1%2_%3__C\n", glob.cname,
                "Object_", "count");
            glob.genTitleD("Object_sizeof");
            glob.out.printf("#define %1%2%3() %1%2_%3__C\n", glob.cname,
                "Object_", "sizeof");
            glob.genTitleD("Object_get");
            glob.out.printf("static inline %1Handle %1Object_get( %1Instance_State* oarr, int i ) \n{\n%+",
                    glob.cname);
            glob.out.printf("%treturn (%1Handle)%1Object__get__S(oarr, i);\n}\n%-", glob.cname);
            glob.genTitleD("Object_first");
            glob.out.printf("static inline %1Handle %1Object_first( void )\n{\n%+", glob.cname);
            glob.out.printf("%treturn (%1Handle)%1Object__first__S();\n}\n%-", glob.cname);
            glob.genTitleD("Object_next");
            glob.out.printf("static inline %1Handle %1Object_next( %1Object* obj )\n{\n%+",
                    glob.cname);
            glob.out.printf("%treturn (%1Handle)%1Object__next__S(obj);\n}\n%-", glob.cname);
            String ls = "xdc_runtime_Types_Label";
            glob.genTitleD("Handle_label");
            glob.out.printf("static inline %1* %2Handle_label( %2Handle inst, %1* lab )\n{\n%+",
                    ls, glob.cname);
            glob.out.printf("%treturn %1Handle__label__S(inst, lab);\n}\n%-", glob.cname);
            glob.genTitleD("Handle_name");
            glob.out.printf("static inline String %1Handle_name( %1Handle inst )\n{\n%+", glob.cname);
            glob.out.printf("%t%1 lab;\n", ls);
            glob.out.printf("%treturn %1Handle__label__S(inst, &lab)->iname;\n", glob.cname);
            glob.out.printf("%-}\n");
        }

        // TODO: combine create/construct

        glob.genTitleD("create");
        glob.out.printf("static inline %1Handle %1create(", glob.cname);
        glob.genCreArgDecls(unit, Glob.Type$LCL, " ");
        glob.out.printf("const %1Params* __prms, %2 )\n{\n%+", glob.cname, Glob.ERRARG);
        if (unit.hasCreateArgs()) {
            glob.out.printf("%t%1Args__create __args;\n", glob.cname);
            for (Decl.Arg arg : unit.getCreator().getArgs()) {
                glob.out.printf("%t__args.%1 = %1;\n", arg.getName());
            }
        }
        glob.out.printf("%treturn (%1Handle)%1Object__create__S(0, 0, ", glob.cname);
        glob.out.printf("%1, ", unit.hasCreateArgs() ? "&__args" : "0");
        glob.out.printf("(const xdc_UChar*)__prms, sizeof(%1Params), __eb);\n}\n%-", glob.cname);

        if (!unit.isProxy()) {
            glob.genTitleD("construct");
            glob.out.printf("static inline void %1construct(", glob.cname);
            glob.out.printf(" %1Struct* __obj", glob.cname);
            glob.genCreArgDecls(unit, Glob.Type$LCL, ", ");
            glob.out.printf("const %1Params* __prms%2 )\n{\n%+", glob.cname, glob.errArg(unit));
            if (unit.hasCreateArgs()) {
                glob.out.printf("%t%1Args__create __args;\n", glob.cname);
                for (Decl.Arg arg : unit.getCreator().getArgs()) {
                    glob.out.printf("%t__args.%1 = %1;\n", arg.getName());
                }
            }
            glob.out.printf("%t%1Object__create__S(__obj, sizeof (%1Struct), ", glob.cname);
            glob.out.printf("%1, ", unit.hasCreateArgs() ? "&__args" : "0");
            glob.out.printf("(const xdc_UChar*)__prms, sizeof(%1Params), %2);\n}\n%-",
                    glob.cname, iInitErr ? "__eb" : "NULL");
        }

        glob.genTitleD("delete");
        glob.out.printf("static inline void %1delete( %1Handle* instp )\n{\n%+", glob.cname);
        glob.out.printf("%t%1Object__delete__S(instp);\n}\n%-", glob.cname);

        if (!unit.isProxy()) {
            glob.genTitleD("destruct");
            glob.out.printf("static inline void %1destruct( %1Struct* obj )\n{\n%+", glob.cname);
            glob.out.printf("%t%1Object__destruct__S(obj);\n}\n%-", glob.cname);
        }

        if (!unit.isProxy()) {
            glob.genTitleD("handle");
            glob.out.printf("static inline %1Handle %1handle( %1Struct* str )\n{\n%+", glob.cname);
            glob.out.printf("%treturn (%1Handle)str;\n}\n%-", glob.cname);
            glob.genTitleD("struct");
            glob.out.printf("static inline %1Struct* %1struct( %1Handle inst )\n{\n%+", glob.cname);
            glob.out.printf("%treturn (%1Struct*)inst;\n}\n%-", glob.cname);
        }
    }

    // skip
    private void skip()
    {
        glob.out.print("\n");
    }
}
