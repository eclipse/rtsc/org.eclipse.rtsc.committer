/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== _xmlRawDump ========
 */
function _xmlRawDump(pkg, fileName)
{
    debug("generating XML ...");

    var bldPkg = pkg.$package;
    
    function mkXMLObject(obj, typeName, out, indent) {
	debug("generating XML object " + typeName + ": " + obj + " ...");

	typeName = (typeName.replace(/\//g, '_')).replace(/^\./, "");
	
	if (typeof obj == 'object') {
	    if (obj.$private == null) {
		obj.$private = {};
	    }
	    if (obj.$private.$color == "black") {
		return;
	    }
	    obj.$private.$color = "black";
	}
	
	out.write(indent + '<' + typeName);
	mkXMLAttrs(obj, out, indent + '    ');

	for (var i in obj) {
	    if (i.match(/^\d+/) != null) {
		mkXMLObject(obj[i], 'item_' + i, out, indent + '    ');
	    }
	    else if (i[0] == '$'
		|| obj[i] == null
		|| typeof obj[i] == 'string'
		|| typeof obj[i] == 'number'
		|| typeof obj[i] == 'boolean') {
		continue;
	    }
	    else if (typeof obj[i] == 'function') {
		/* don't do "getter" processing since some functions
		 * may not be executable in the build model
		 */
		continue;
	    }
	    else if (typeof obj[i] == 'object') {
		mkXMLObject(obj[i], i, out, indent + '    ');
	    }
	}

	/* if it's one of our objects, dump its $private state */
	if (obj instanceof bldPkg.PackageContents.Module
	    || obj instanceof bldPkg.Executable.Module
	    || obj instanceof bldPkg.Object.Module
	    || obj instanceof bldPkg.Release.Module
	    || obj instanceof bldPkg.Test.Module
	    || obj instanceof bldPkg.Library.Module) {
	    mkXMLObject(obj.$private, "_private", out, indent + '    ');
	}

	out.write(indent + '</' + typeName + '>\n');

	if (typeof obj == 'object') {
	    obj.$private.$color = "white";
	}
    }	

    function mkXMLAttrs(obj, out, indent) {
	debug("generating XML attrs for: " + obj + " ...");
	indent = '\n' + indent;

	if (typeof obj == 'number' || typeof obj == 'boolean') {
	    out.write(indent + 'value ="' + obj + '"');
	}
	else if (typeof obj == 'string') {
	    out.write(indent + 'value ="' + obj.replace(/\"/g, '&quot;') + '"');
	}
	else {
	    var attrs = [];
	    for (var i in obj) {
		if (i.match(/^\d+|^\$/) != null || obj[i] == null) {
		    continue;
		}
		else if (typeof obj[i] == 'string'
			 || typeof obj[i] == 'boolean'
			 || typeof obj[i] == 'number') {
		    var val = obj[i];
		    if (typeof val == 'string') {
			val = val.replace(/\"/g, '&quot;');
			val = val.replace(/\&/g, '&amp;');
		    }
		    attrs[attrs.length] =
			i.replace(/[\/:]/g, '_').replace(/^\./, '_')
			+ '="' + val + '"';
		    if (i == "name" && attrs.length > 1) {
			var tmp = attrs[attrs.length - 1];
			attrs[attrs.length - 1] = attrs[0];
			attrs[0] = tmp;
		    }
		}
	    }
	    for (var i = 0; i < attrs.length; i++) {
		out.write(indent + attrs[i]);
	    }
	}
	out.write('>\n');
    }

    var file = new java.io.File(fileName);
    file["delete"]();
    var out = new java.io.BufferedWriter(new java.io.FileWriter(file));

    out.write('<?xml version="1.0" encoding="UTF-8"?>\n');
    mkXMLObject(pkg, 'package', out, '');

    out.close();
}
