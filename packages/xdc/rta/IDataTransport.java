package xdc.rta;

import java.io.IOException;

public interface IDataTransport {
    
    public void initialize() throws Exception; 
        
    public void terminate() throws Exception;
    
    /**
     * Reads an array of 8-bit bytes.
     * 
     * @param   b byte array to contain data    
     * @param   off index into array where data should start    
     * @param   len maximum number of bytes to read 
     * @return  bytesRead number of bytes actually read
     * @throws  IOException
     */
    public int read(byte b[], int off, int len) throws IOException;
    
    /**
     * Reads an array of 8-bit bytes with timeout.
     * 
     * @param   buffer   array to contain data  
     * @param   offset   index into array where data should start   
     * @param   count    number of array elements to read   
     * @param   timeout  in milliseconds, zero to wait forever, negative to return whatever is avaiable 
     * @return  the total number of array elements read into the buffer
     * @throws  IOException if an I/O error occurs
     */
    public int read(byte buffer[], int offset, int count, int timeout) throws IOException;
        
    /**
     * Returns the number of bytes that can be read without blocking.
     * 
     * @return  bytesRead number of bytes in host buffer
     * @throws  IOException
     */
    public int available() throws IOException;

}
