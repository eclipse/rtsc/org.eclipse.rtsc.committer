/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 * ======== xdc.services.spec.ParserSession ========
 */

package xdc.services.spec;

import org.antlr.runtime.*;
import xdc.services.global.*;
import java.io.*;
import java.util.*;

public class ParserSession
    extends Session
{
    enum Kind {ANY, MOD, INT};
    
    private boolean curAddFiles = true;
    private Pkg curPkg = null;
    private boolean verbose = true;
    private boolean strict = false;
    private Clock clock = new Clock();
    private int unitCnt;

    /**
     * No arg constructor
     */
    public ParserSession() {
        this(Path.getGlobal());
    }

    /**
     * Create a parser session with a given environment
     */
    public ParserSession(Env env) {
        this(env, new Cache());
    }

    public ParserSession(Env env, Cache cache) {
        super(env, cache);
    }
    
    // buildUnits
    void buildUnits( List<Atom> unitAts, Kind kind )
    {
        for (Atom a: unitAts) {
            String name = a.getText();
            if (this.verbose) {
                System.out.println("    translating " + name);
            }
            Unit unit = this.pass1(this.curPkg.getName() + '.' + name, kind);
            unit.bindParent(curPkg);
            curPkg.units.add(unit);
        }
    }

    // quickIntf
    /**
     * Make sure a unit is available in the cache.
     * This still has problems. The parent of the unit is set wrongly.
     * It should be set to the package that contains the named unit,
     * but instead it's set to the package that triggered the parse.
     * But the correct package isn't in the cache. When the full
     * package is later loaded, it will overwrite the quickly loaded
     * unit, creating multiple copies of the quickly loaded unit.
     * Conversely, if quickIntf loads the full package, it will be
     * very slow as xdc.runtime contains many units. Probably the 
     * right answer is to lazily load the units of the package so
     * that the references are correct. This will require pass1()
     * recognize when the unit in the cache is incomplete.
     */
    private void quickIntf( String intfName )
    {
        /* done if the interface is already there */
        if (this.findUnit(intfName) != null) {
             return;
        }
        /* do minimal work to read the unit correctly */
        Unit intf = this.pass1(intfName, Kind.INT);
        this.pass2(intf);
        intf.parseDocs();
    }
    
    // compile
    public Pkg compile( String pkgFile )
    {
        return compile(pkgFile, null);
    }
    
    public Pkg compile( String pkgFile, Pkg pkg )
    {
        unitCnt = 0;
        clock.enable();
        clock.reset();
        
        /* discard old errors */
        msg.clearErrors();
        
        this.curPkg = (pkg == null) ? this.scan(pkgFile) : pkg;
        this.curPkg.bindSession(this);
        this.enterNode(this.curPkg.getQualName(), this.curPkg);

        /* find the directory the package is in */
        String pkgDir = new File(pkgFile).getParent();
        if (pkgDir == null) {
            pkgDir = ".";
        }
        pkgDir = pkgDir.replace('\\','/');

        /* save curent package repository */
        String savPkgRoot = getEnv().getCurPkgRoot();
        
        /* set "^" in XDCPATH to the repository the package is in */
        getEnv().setCurPkgBase(pkgDir, this.curPkg.getName());
        
        buildUnits(this.curPkg.modNames, Kind.MOD);
        buildUnits(this.curPkg.interNames, Kind.INT);

        this.curAddFiles = false;
        this.quickIntf("xdc.runtime.IModule");
        curAddFiles = true;
        this.quickIntf("xdc.IPackage");

        for (Unit u: curPkg.units) {
            u.pkgName = this.curPkg.getName();
            this.pass2(u);
        }

        this.curPkg.finalCheck();
        this.curPkg.parseDocs();
        
        /* restore saved curent package repository */
        getEnv().setCurPkgRoot(savPkgRoot);

        tsort(this.curPkg.units);

        if (msg.hasErrors()) {
            msg.clearErrors();
            SessionRuntimeException se =
                new SessionRuntimeException("parser failed");
            throw (se);
        }
        
        clock.print("" + unitCnt + " units");
        
        return (this.curPkg);
    }
    
    // curPkg
    Pkg curPkg()
    {
        return this.curPkg;
    }
    
    // loadUnit
    Unit loadUnit( String name )
    {
    	return (this.pass2(this.pass1(name, Kind.ANY)));
    }
    
    // pass1
    Unit pass1( String name, Kind kind )
    {
        int k = name.lastIndexOf('.');
        String qn = (k != -1) ? name : (this.curPkg.getName() + '.' + name);
        
        Unit unit = this.findUnit(qn);
        if (unit != null) {
            return (unit);
        }
        
//      System.out.println("pass1: " + name);

        String fname = qn.replace('.', '/') + ".xdc";

        Reader in = null;
        String cpath = getEnv().resolve(qn);
        
        if (cpath != null) {
            try {
                in = new BufferedReader(new FileReader(cpath));
            }
            catch (FileNotFoundException fnf) {
                ;
            }
        }

        if (in == null) {
            SessionRuntimeException se =
                new SessionRuntimeException("can't find specification file: "
                    + fname);
            throw (se);
        }
        try {
            GrammarLexer lexer = new GrammarLexer(new ANTLRReaderStream(in));
            GrammarParser parser = new GrammarParser(lexer, fname, this);
            String pn = k != -1 ? name.substring(0, k) : this.curPkg.getName();
            boolean local = pn.equals(this.curPkg.getName());
            unit = parser.unitSpec(pn, local);
            ++unitCnt;
        }
        catch (Exception e) {
            SessionRuntimeException se = new SessionRuntimeException(
                e.toString());
            throw (se);
        }
        finally {
            try {
                in.close();
            }
            catch (java.io.IOException e) {
                ;
            }
        }

        if (msg.hasErrors()) {
            msg.clearErrors();
            SessionRuntimeException se =
                new SessionRuntimeException("parser failed");
            throw (se);
        }
        
        if (!unit.getName().equals(k != -1 ? name.substring(k + 1) : name)) {
            SessionRuntimeException se =
                new SessionRuntimeException("unit '" + unit.getName()
                    + "' must be named " + name);
            throw (se);
        }
        
        if (kind == Kind.MOD && unit.isInter()) {
            SessionRuntimeException se =
                new SessionRuntimeException(name + ": unit must be a module");
            throw (se);
        }
        
        if (kind == Kind.INT && unit.isMod()) {
            SessionRuntimeException se =
                new SessionRuntimeException(name
                    + ": unit must be an interface");
            throw (se);
        }
        
        this.enterNode(qn, unit);
        
        if (this.curAddFiles) {
            if (k != -1 && !unit.pkgName.equals(this.curPkg.getName())) {
                String fn = qn.replace('.', '/') + ".xdc";
                this.curPkg.otherSpecs.add(fn);
            }
            else {
                this.curPkg.localSpecs.add(unit.getName() + ".xdc");
            }
        }
        
        unit.bindParent(this.curPkg);
        unit.pass1Check();
        
        return (unit);
    }
    
    // pass2
    private Unit pass2( Unit unit )
    {
//     System.out.println("pass2: " + unit.getQualName());

        unit.pass2Check(strict);

        if (msg.hasErrors()) {
            msg.clearErrors();
            SessionRuntimeException se =
                new SessionRuntimeException("parser failed (pass 2)");
            throw (se);
        }
        
        return (unit);
    }
    
    // save
    public void save()
    {
        this.save(new File("./package"));
    }
    
    // save
    public void save( File pdir )
    {
        clock.enable();
        clock.reset();
        
        ObjectOutputStream oos = null;
        
        try {
            File file = new File(pdir, this.curPkg.getName() + ".ccs");
            file.delete();
            oos = new Persist.Output(
                new BufferedOutputStream(new FileOutputStream(file)),
                this.curPkg.getName()
            );
            oos.writeObject(this.curPkg);
            oos.writeChars(xdc.services.intern.gen.Glob.vers() + '\n');
        }
        catch (Exception e) {
            e.printStackTrace();
            SessionRuntimeException se =
                new SessionRuntimeException(e.getMessage());
            throw (se);
        }
        finally {
            if (oos != null) {
                try {
                    oos.close();
                }
                catch (java.io.IOException e) {
                    ;
                }
            }
        }
        
        clock.print("save");
    }
        
    // scan
    public Pkg scan( String pkgFile )
    {
        /* TODO: validate requires */
        
    	Env env = this.getEnv();
    	Pkg pkg = env.getCurPkg();
    	
/*		TODO optimize re-scanning
    	if (pkg != null) {
    		return pkg;
    	}
*/
    	File infile = new File(pkgFile);
        Reader in = null;
        String cpath = null;

        try {
            in = new BufferedReader(new FileReader(infile));
            cpath = infile.getCanonicalPath();
        }
        catch (Exception e) {
            SessionRuntimeException se =
                new SessionRuntimeException("can't find " + pkgFile);
            throw (se);
        }

        pkg = null;
        try {
            GrammarParser parser = new GrammarParser(new GrammarLexer(new ANTLRReaderStream(in)), pkgFile, this);
            pkg = parser.pkgSpec(pkgFile);
        }
        catch (Exception e) {
            e.printStackTrace();
            SessionRuntimeException se =
                new SessionRuntimeException(e.getMessage());
            throw (se);
        }
        finally {
            try {
                in.close();
            }
            catch (java.io.IOException e) {
                ;
            }
        }

        if (msg.hasErrors()) {
            msg.clearErrors();
            SessionRuntimeException se =
                new SessionRuntimeException("parser failed");
            throw (se);
        }

        pkg.setBaseDir(cpath.substring(0, cpath.lastIndexOf(File.separatorChar)));
        env.setCurPkg(pkg);
        
        return pkg;
    }

    // setVerbose
    public void setVerbose( boolean verbose )
    {
        this.verbose = verbose;
    }

    // setStrict
    public boolean setStrict(boolean strict)
    {
        boolean ret = this.strict;
        this.strict = strict;
        return (ret);
    }
    
    // tsort
    private void tsort( List<Unit> uL )
    {
        Unit[] uA = new Unit[uL.size()];
        uA = uL.toArray(uA);
        uL.clear();
        
        for (Unit u : uA) {
            tsort1(uL, u);
        }
        
        for (Unit u : uA) {
            uL.addAll(u.prxs);
        }
    }
    
    // tsort1
    private void tsort1( List<Unit> uL, Unit u )
    {
        if (u.getSuper() != null) {
            tsort1(uL, u.getSuper());
        }
        
        if (u.pkgName.equals(this.curPkg.getName()) && !uL.contains(u)) {
            uL.add(u);
        }
    }
}
