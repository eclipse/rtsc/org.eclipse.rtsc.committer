/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
//
// ======== xdc.services.spec.Expr ========
//

package xdc.services.spec;

import java.util.*;

public class Expr
    implements java.io.Serializable
{
    // Static Serialization UID
    static final long serialVersionUID = -6252677966391348100L;

    // Aggregate
    public interface Aggregate
    {
        boolean isEmpty();
    }

    // Path
    public interface Path
    {
    	void buildChain( List<Expr.Path> chain );
    	Cat getCat();
    	void setCat( Cat cat );
    }
    
    // eText
    String eText( boolean norm )
    {
        return "";
    }
    
    // getAtom
    public Atom getAtom()
    {
    	return null;
    }
    
    // getTerm
    public Expr.Term getTerm()
    {
        return null;
    }
    
    // resolve
    void resolve( Unit uspec )
    {
    }
    
    // toText
    static final public String toText( Expr e )
    {
        return e == null ? "" : e.eText(true);
    }

    // toXml
    static final public String toXml( Expr e )
    {
        return e == null ? "" : e.eText(false);
    }

    // Array
    static public class Array
        extends Expr implements Aggregate
    {
    	static final long serialVersionUID = -4769465389054526623L;
    	
        List<Expr> elems;

        Array( List<Expr> elems )
        {
            this.elems = elems;
        }

        // eText
        String eText( boolean norm )
        {
            if (this.elems.size() == 0) {
                return "[ ]";
            }
            String res = norm ? "[" : "[@+";
            String sep = "";
            for (Expr e : elems) {
                res += sep + e.eText(norm);
                sep = norm ? ", " : ",@=";
            }
            return res + (norm ? "]" : "@-]");
        }
        
        // getElems
        public final List<Expr> getElems()
        {
            return this.elems;
        }
        
        // isEmpty
        public boolean isEmpty()
        {
            return this.elems.size() == 0;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            for (Expr e : this.elems) {
                e.resolve(uspec);
            }
        }
    }

    // Addr
    static public class Addr extends Expr
    {
		private static final long serialVersionUID = -8300368019783181588L;

		Expr body;
    	Atom tok;
    	
    	Addr( Expr body, Atom tok )
    	{
    		this.body = body;
    		this.tok = tok;
    	}
    	
    	public final Atom getAtom() { return this.body.getAtom(); }
    	public final Expr getBody() { return this.body; }
    	public final Atom getTok() { return this.tok; }
    	
    	void resolve( Unit uspec )
    	{
			Impl.resolveAddr(this);
    	}
    }
    
    // Binary
    static public class Binary
        extends Expr
    {
    	static final long serialVersionUID = -8527302494988163510L;
    	
        Atom op;
        Expr left;
        Expr right;

        Binary( Atom op, Expr left, Expr right )
        {
            this.op = op;
            this.left = left;
            this.right = right;
        }

        // eText
        String eText( boolean norm )
        {
            return this.left.eText(norm) + ' ' + this.op.text + ' '
                    + this.right.eText(norm);
        }
        
        // getAtom
        public Atom getAtom()
        {
        	return this.left.getAtom();
        }
        
        // getLeft
        public final Expr getLeft()
        {
            return this.left;
        }
        
        // getOp
        public final String getOp()
        {
            return this.op.text;
        }
        
        // getRight
        public final Expr getRight()
        {
            return this.right;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.left.resolve(uspec);
            this.right.resolve(uspec);
        }
    }

    // Call
    static public class Call extends Expr implements Path
    {
		private static final long serialVersionUID = 5661709430396877414L;
		
		private Expr fxn;
		private List<Expr> args;
		private Atom tok;
		private Cat cat;

		Call( Expr fxn, List<Expr> args, Atom tok )
    	{
    		this.fxn = fxn;
    		this.args = args;
    		this.tok = tok;
    	}

		public Atom getAtom() { return this.fxn.getAtom(); }
		public final Expr getFxn() { return this.fxn; }
		public final List<Expr> getArgs() { return this.args; }
		public final Atom getTok() { return this.tok; }
		
		public final Cat getCat() { return this.cat; }
		public final void setCat( Cat cat ) { this.cat = cat; }
		
    	public void buildChain( List<Expr.Path> chain )
    	{
    		((Expr.Path)this.fxn).buildChain(chain);
    		chain.add(this);
    	}
		
		void resolve( Unit uspec )
		{
			Impl.resolveCall(this);
		}
    }
    
    // Cast
    static public class Cast extends Expr
    {
    	Type type;
    	Expr body;
    	
    	Cast( Type type, Expr body )
    	{
    		this.type = type;
    		this.body = body;
    	}
    	
    	public Atom getAtom() { return this.body.getAtom(); }
    	public final Type getType() { return this.type; }
    	public final Expr getBody() { return this.body; }
    	
    	void resolve( Unit uspec )
    	{
    		this.type.resolve(uspec);
    		this.body.resolve(uspec);
    	}
    }
    
    // Cond
    static public class Cond
        extends Expr
    {
    	static final long serialVersionUID = -3016213789698060325L;
    	
        Expr cond;
        Expr left;
        Expr right;

        Cond( Expr cond, Expr left, Expr right )
        {
            this.cond = cond;
            this.left = left;
            this.right = right;
        }
        
        // eText
        String eText( boolean norm )
        {
            return this.cond.eText(norm) + " ? " + this.left.eText(norm)
                    + " : " + this.right.eText(norm);
        }
        
        // getAtom
        public Atom getAtom() { return this.cond.getAtom(); }
        
        // getCond
        public final Expr getCond()
        {
            return this.cond;
        }
        
        // getLeft
        public final Expr getLeft()
        {
            return this.left;
        }
        
        // getRight
        public final Expr getRight()
        {
            return this.right;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.cond.resolve(uspec);
            this.left.resolve(uspec);
            this.right.resolve(uspec);
        }
    }

    // Const
    static public class Const
        extends Expr
    {
    	static final long serialVersionUID = 3804334373597760702L;
    	
        Atom val;

        Const( Atom val )
        {
            this.val = val;
        }
        
        // eText
        String eText( boolean norm )
        {
            if (norm) {
                return this.val.text;
            }
            else if (Character.isLetter(this.val.text.charAt(0))) {
                return "$K" + this.val.text;
            }
            else {
                return this.val.text.replaceAll("@", "@@");
            }
        }
        
        // getAtom
        public Atom getAtom() { return this.val; }
        
        // getVal
        public final String getVal()
        {
            return this.val.text;
        }
    }

    // Creat
    static public class Creat
        extends Expr
    {
    	static final long serialVersionUID = 5455736916581289951L;
    	
        Expr.Term mod;
        List<Expr> args;

        Creat( Expr.Term mod, List<Expr> args )
        {
            this.mod = mod;
            this.args = args;
        }
        
        // eText
        String eText( boolean norm )
        {
            return this.mod.eText(norm);
        }
        
        // getAtom
        public Atom getAtom() { return this.mod.getAtom(); }
        
        // getArgs
        public final List<Expr> getArgs()
        {
            return this.args;
        }
        
        // getMod
        public final Expr.Term getMod()
        {
            return this.mod;
        }
        
        // getTerm
        public Expr.Term getTerm()
        {
            return this.mod;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.mod.resolve(uspec);
            for (Expr e : this.args) {
                e.resolve(uspec);
            }
        }
    }

    // Error
    static class Error
        extends Expr
    {
    	static final long serialVersionUID = -7456683367635014801L;
    	
        Error()
        {
        }
    }

    // Frag
    static public class Frag extends Expr
    {
		private static final long serialVersionUID = -1146194553318420954L;

		String text;
		List<Expr> args;
    	
    	Frag( String text )
    	{
    		this(text, null);
    	}
    	
    	Frag( String text, List<Expr> args )
    	{
    		this.text = text;
    		this.args = args;
    	}
    	
    	public final List<Expr> getArgs() { return this.args; }
    	public final String getText() { return this.text; }
    }
    
    // Hash
    static public class Hash
        extends Expr implements Aggregate
    {
        static final long serialVersionUID = 5610650247782362148L;

        List<Atom> ids;
        List<Expr> vals;

        Hash( List<Atom> ids, List<Expr> vals )
        {
            this.ids = ids;
            this.vals = vals;
        }
        
        // eText
        String eText( boolean norm ) {
            if (this.ids.size() == 0) {
                return "{ }";
            }
            String res = norm ? "{" : "{@+";
            String sep = "";
            for (int i = 0; i < ids.size(); i++) {
                res += sep + ids.get(i).text + ": " + vals.get(i).eText(norm);
                sep = norm ? ", " : ",@=";
            }
            return res + (norm ? "}" : "@-}");
        }
        
        // getIds
        public final List<Atom> getIds()
        {
            return this.ids;
        }
        
        // getVals
        public final List<Expr> getVals()
        {
            return this.vals;
        }
        
        // isEmpty
        public boolean isEmpty()
        {
            return this.vals.size() == 0;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            for (Expr e : this.vals) {
                e.resolve(uspec);
            }
        }
    }

    // Index
    static public class Index extends Expr implements Path
    {
		private static final long serialVersionUID = 7979132454183195334L;

		private Expr arr;
		private Expr idx;
		private Atom tok;
		private Cat cat;

		Index( Expr arr, Expr idx, Atom tok )
    	{
    		this.arr = arr;
    		this.idx = idx;
    		this.tok = tok;
    	}

        public Atom getAtom() { return this.arr.getAtom(); }
        
		public final Expr getArr() { return this.arr; }
		public final Expr getIdx() { return this.idx; }
		public final Atom getTok() { return this.tok; }
		
		public final Cat getCat() { return this.cat; }
		public final void setCat( Cat cat ) { this.cat = cat; }
		
    	public void buildChain( List<Expr.Path> chain )
    	{
    		((Expr.Path)this.arr).buildChain(chain);
    		chain.add(this);
    	}
		
		void resolve( Unit uspec )
		{
			Impl.resolveIndex(this);
		}
    }
    
    // Paren
    static public class Paren
        extends Expr
    {
    	static final long serialVersionUID = -8628875245839027888L;
    	
        Expr body;

        Paren( Expr body )
        {
            this.body = body;
        }
        
        // eText
        String eText( boolean norm )
        {
            return "(" + (this.body == null ? "" : this.body.eText(norm)) + ")";
        }
                
        // getAtom
        public Atom getAtom() { return this.body.getAtom(); }

        // getBody
        public final Expr getBody()
        {
            return this.body;
        }
        
        // getTerm
        public Expr.Term getTerm()
        {
            return body.getTerm();
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.body.resolve(uspec);
        }
    }

   // Select
    static public class Select extends Expr implements Path
    {
    	private static final long serialVersionUID = 2512874495262822292L;
    	
		private Expr obj;
		private Atom sel;
		private Cat cat;
		private Unit unit = null;

		Select( Expr obj, Atom sel )
    	{
    		this.obj = obj;
    		this.sel = sel;
    	}
		
		public Atom getAtom() { return this.obj != null ? this.obj.getAtom() : this.sel; }
		
		public final Expr getObj() { return this.obj; }
		public final Atom getSel() { return this.sel; }
		
		public final Cat getCat() { return this.cat; }
		public final void setCat( Cat cat ) { this.cat = cat; }

		public final Unit getUnit() { return this.unit; }
		public final void setUnit( Unit unit ) { this.unit = unit; }

    	public void buildChain( List<Expr.Path> chain )
    	{
    		if (this.obj != null) {
        		((Expr.Path)this.obj).buildChain(chain);
    		}
    		chain.add(this);
    	}
		
		void resolve( Unit uspec )
		{
			Impl.resolveSelect(this);
		}
    }
    
    // Size
    static public class Size
        extends Expr
    {
		private static final long serialVersionUID = 7852364199455111319L;

		Atom kw;
		Type.Spec tspec;
		String fldName = null;

        Size( Atom kw, Type.Spec tspec )
        {
        	this.kw = kw;
        	this.tspec = tspec;
        }
        
        // eText
        String eText( boolean norm )
        {
            return "sizeof 0";
        }
                
        // getAtom
        public Atom getAtom() { return this.kw; }
        
        // getField
        public final String getFieldName() { return this.fldName; }
        
        // getKeyword
        public final Atom getKeyword() { return this.kw; }
        
        // getRef
        public final Type.Spec getTspec() { return this.tspec; }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.tspec.resolve(uspec, true);
            if (kw.getText().charAt(0) != 'o') {
            	return;
            }
            
            Ref r = this.tspec.getRef();
            Session ses = uspec.getSession();
            
            Node str = r.getNode();
            
            if (!(str instanceof Decl.Struct)) {
            	ses.msg.error(this.kw, "operand must name a struct type");
            }
           
            String qn = r.getScope() + '.' + r.getId();
            Node n = ses.lookup(qn);
            if (!(n instanceof Decl.Field)) {
            	ses.msg.error(this.kw, "operand must name a struct field");
            }
            else {
            	int k = r.getId().indexOf('.');
            	this.fldName = r.getId().substring(k + 1);
            	r.id.replaceText(r.getId().substring(0, k));
            }
        }
    }

     // Term
    static public class Term
        extends Expr
    {
    	static final long serialVersionUID = 484911847601585871L;
    	
        Ref eref;
        Expr val;

        Term( Ref eref )
        {
            this.eref = eref;
        }
        
        // eText
        String eText( boolean norm )
        {
            if (this.eref.isGlobal()) {
                return this.eref.id.text;
            }
            else if (norm) {
                return Expr.toText(this.val);
            }
            else {
                return "$!" + this.eref.scope.text + ":" + this.eref.id.text + "!";
            }
        }
        
        // getAtom
        public Atom getAtom() { return this.eref.id; }
        
        // getRef
        public final Ref getRef()
        {
            return this.eref;
        }

        // getTerm
        public Expr.Term getTerm()
        {
            return this;
        }
        
        // getVal
        public final Expr getVal()
        {
            return this.val;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            if (this.eref.isGlobal() || this.eref.node != null) {
                return;
            }
            
            this.eref.resolve(uspec, true);
            
            uspec.addUse(this.eref);

            /* get session for error reporting */
            Session ses = uspec.getSession();

            if (this.eref.id.text.equals("module")
                || this.eref.id.text.equals("create")) {
                this.val = null;
            }
            else if (this.eref.node instanceof Decl.Const) {
                this.val = ((Decl.Const)this.eref.node).init;
            }
            else if (this.eref.node instanceof Decl.EnumVal || this.eref.node instanceof Decl.Extern) {
                this.val = null;
            }
            else if (this.eref.node instanceof Decl.Fxn) {
                Decl.Fxn fxn = (Decl.Fxn)this.eref.node;
                if (!(fxn.getType() instanceof Type.Creator) && fxn.isMeta()) {
                    ses.msg.error(this.eref.toAtom(), "metaonly functions not allowed");
                }
                this.val = null;
            }
            else if (this.eref.node instanceof Decl.Config) {
                if ((this.val = ((Decl.Config)this.eref.node).init) == null) {
                    ses.msg.error(this.eref.toAtom(), "uninitialized config parameter");
                }
            }
            else {
                ses.msg.error(this.eref.toAtom(), "not a valid initializer");
            }
        }
    }

    // Unary
    static public class Unary
        extends Expr
    {
    	static final long serialVersionUID = -3418172231763962792L;
    	
        Atom op;
        Expr right;
        boolean isPost;

        Unary( Atom op, Expr right ) { this(op, right, false); }

        Unary( Atom op, Expr right, boolean isPost )
        {
            this.op = op;
            this.right = right;
            this.isPost = isPost;
        }
        
        // eText
        String eText( boolean norm )
        {
            return this.op.text + this.right.eText(norm);
        }
        
        // getAtom
        public Atom getAtom() { return this.op; }
        
        // getOp
        public final String getOp()
        {
            return this.op.text;
        }
        
        // getRight
        public final Expr getRight()
        {
            return this.right;
        }
        
        // isPost
        public final boolean isPost()
        {
        	return this.isPost;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.right.resolve(uspec);
        }
    }
    
	// Var
	static public class Var extends Expr
	{
		private static final long serialVersionUID = 7909109925698487558L;

		private String keyword;
		private Decl.Arg decl;
		
		Var( String keyword, Decl.Arg decl )
        {
			this.keyword = keyword;
			this.decl = decl;
        }
		
		public final String getKeyword() { return this.keyword; }
		public final Decl.Arg getDecl() { return this.decl; }
		
		void resolve( Unit uspec )
		{
			Impl.resolveVar(this);
		}
	}
}
