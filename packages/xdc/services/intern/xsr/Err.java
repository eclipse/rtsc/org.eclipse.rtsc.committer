/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;

public class Err
{
     public static void exit( Exception e )
    {
        if (e instanceof JavaScriptException) {
            //System.err.println("Err$ JSE");
            throw (JavaScriptException)e;
        }
        
        if (e instanceof EcmaError) {
            //System.err.println("Err$ Ecma");
            throw (EcmaError)e;
        }
        else if (e instanceof EvaluatorException) {
            //System.err.println("Err$ EE");
            throw (EvaluatorException)e;
        }
        else {
            //System.err.println("Err$ Exception");
            e.printStackTrace();
            throw Context.reportRuntimeError("XDC runtime exception: "
                + e.toString());
        }
    }
    
    public static void exit( String msg )
    {
        //System.err.println("Err$ message");
        throw new EvaluatorException("XDC runtime error: " + msg);
   }
}

/*
 *> In rhino, I would like throw from a host object my own scriptable types as
 *> catchable exceptions of "my type" in javascript.  I seems that 
 *> 
 *> extending EvaluatorException 
 *> 
 *> and using
 *> 
 *> Context.throwAsScriptRuntimeEx(ex);
 *> 
 *> is the path to follow but it does not allow me to propagate my type.  The
 *> exception is caught as either "internalError" in the case of an evaluator
 *> exception and "JavaException" otherwise. Do I need to extend ScriptRuntime
 *> and override getCatchObject(Context cx, Scriptable scope, Throwable t) to
 *> handle and convert my exceptions (didn't want to go there)?
 *> 
 *> So if have 
 *> 
 *> class MyExceptionType extends ScriptableObject
 *> 
 *> and in host object I throw
 *> 
 *> Context.throwAsScriptRuntimeEx(myScriptable);  // illegal
 *> 
 *> 
 *> then i would
 *> 
 *> try {
 *>  host.causeExceptions();
 *> } catch(exception) {
 *>  if(excetion instanceof MyExceptionType) {
 *>   // got it
 *>  }
 *> }
 *
 * To get the desired functionality, use
 *
 * throw new JavaScriptException(myScriptable, "native-method", 1)
 *
 * which will throw your scriptable in the same way as the throw statement 
 * in JS does.
 *
 * If you can not add JavaScriptException to the method declaration, then 
 * you can replace that line by
 * Context.throwAsScriptRuntimeEx(new JavaScriptException(myScriptable, 
 * "native-method", 1))
 *
 * That wraps JavaScriptException into RuntimeException and Rhino should 
 * unwrap it properly and feed to catch in scripts.
 *
 */
