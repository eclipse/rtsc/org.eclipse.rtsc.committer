/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import java.util.*;
import java.lang.ref.*;
import org.mozilla.javascript.*;

public class XScriptO
    implements Comparable, Scriptable
{
    static OpTab optab = new OpTab(XScriptO.class, new String[] {
            "bind:$$bind",
            "bindings:$$bindings",
        });
        
    static XScriptO curOM;
    static WeakHashMap curObjTab = new WeakHashMap();

    static public final XScriptO DEFAULT = new XScriptO("$$DEFAULT");
    static public final XScriptO ERROR = new XScriptO("$$ERROR");
    
    private Scriptable parent;
    private Scriptable prototype;
    
    protected HashMap bindtab = new HashMap();
    protected boolean errflg = true;
    
    XScriptO()
    {
        this(null);
    }
    
    XScriptO( String name )
    {
        this.setName(name);

        this.bindtab.put("$category", "Other");
        this.bindtab.putAll(XScriptO.optab);
    }
    
    public Object bind( String name, Object val )
    {
        this.bindtab.put(name, val);
        return val;
    }
    
    public Object bindings()
    {
        return this.bindtab.keySet().toArray();
    }
    
    public static void dump( Object obj )
    {
        XScriptO xo = (XScriptO)obj;
    }

    public void error( String msg )
    {
        Err.exit(this.getDefaultValue(null) + ": " + msg);
    }
    
    static void errorThrow( String msg )
        throws EvaluatorException
    {
        throw new EvaluatorException(msg);
    }

    /*
     *  ======== find ========
     *  Return 'null' if the object is not found
     */
    public final Object find( String name )
    {
        return this.bindtab.get(name);
    }

    /*
     *  ======== findStrict ========
     *  Fail if the object is not found
     */
    public final Object findStrict (String name, String loadingPackage)
    {
        Object o = find(name);
        if (o == null) {
            errorThrow("The object '" + name
                + "' is not found in the object model, while loading the "
                + "package '" + loadingPackage + "'.");
        }
        return (o);
    }
    
    public int geti( String name )
    {
        Object res = this.get(name, this);
        return toInt(res);
    }

    public int geti( int index )
    {
        Object res = this.get(index, this);
        return toInt(res);
    }

    public Object geto( String name )
    {
        return this.get(name, this);
    }

    public Object geto( int index )
    {
        return this.get(index, this);
    }

    public String gets( String name )
    {
        Object res = this.get(name, this);
        return (String)res;
    }

    public String gets( int index )
    {
        Object res = this.get(index, this);
        return (String)res;
    }

    public Value getv( String name )
    {
        this.errflg = false;
        Object res = this.get(name, this);
        this.errflg = true;
        return res == NOT_FOUND ? null : (Value)res;
    }

    public Value getv( int index )
    {
        Object res = this.get(index, this);
        return (Value)res;
    }
    
    public String getName()
    {
        return this.bindtab.get("$name").toString();
    }
    
    public static Object lookup( String name )
    {
        if (curObjTab.containsKey(name)) {
            Object res = curObjTab.get(name);
            return res instanceof WeakReference ? ((WeakReference)res).get() : res;
        }
        else if (curOM != null) {
            return curOM.bindtab.get(name);
        }
        else {
            return null;
        }
    }
    
    public Object invoke( String fname )
    {
        return this.invoke(fname, new Object[] {});
    }
    
    public Object invoke( String fname, Object[] args )
    {
        Object ofxn = this.get(fname, this);

        if (!(ofxn instanceof Function)) {
            return Context.getUndefinedValue();
        }
        
        Object res = null;
        try {
            res = ((Function)ofxn).call(Context.getCurrentContext(), Global.getTopScope(), this, args);
        }
        catch (Exception e) {
            Err.exit(e);
        }
        return res;
    }
    
    public void setName( String name )
    {
        if (name != null) {
            this.bindtab.put("$name", name);
            curObjTab.put(name, new WeakReference(this));
/*            
            if (curOM != null) {
                curOM.bind(name, this);
            }
*/            
        }
    }
    
    public static void setOM( Object omObj )
    {
        XScriptO.curOM = (XScriptO)omObj;
    }
    
    int toInt( Object iobj )
    {
        if (iobj instanceof Number) {
            return ((Number)iobj).intValue();
        }
        else if (iobj instanceof Boolean) {
            return ((Boolean)iobj).booleanValue() ? 1 : 0;
        }
        else {
            return ((Number)((Scriptable)iobj).getDefaultValue(Number.class)).intValue();
        }
    }
    
    // OVERRIDE Object
    
    public String toString()
    {
        return super.toString() + "::" + (String)this.getDefaultValue(String.class);
    }
    
    // Comparable
    
    public int compareTo( Object o )
    {
        String n1 = this.getName();
        String n2 = ((XScriptO)o).getName();
        return n1.compareTo(n2);
    }
    
    // Scriptable

    public void delete( String name )   { }

    public void delete( int index ) { }

    public Object get( String name, Scriptable start )
    {
        if (this.bindtab.containsKey(name)) {
            return bindtab.get(name);
        }

        if (this.errflg) {
            this.error("no property named '" + name + "'");
        }

        return NOT_FOUND;
    }

    public Object get( int index, Scriptable start )
    {
        if (this.errflg) {
            this.error("index out of range (" + index + ")");
        }
        return NOT_FOUND;
    }
    
    public String getClassName()
    {
        return "$$XScriptO";
    }

    public Object getDefaultValue( java.lang.Class hint )
    {
        Object res = this.bindtab.get("$name");
        return res == null ? "<unknown>" : res;
    }

    public Object[] getIds()
    {
        return new Object[0];
    }

    public boolean has( String name, Scriptable start )
    {
        this.errflg = false;
        boolean res = this.get(name, start) != NOT_FOUND;
        this.errflg = true;
        return res;
    }

    public boolean has( int index, Scriptable start )
    {
        this.errflg = false;
        boolean res = this.get(index, start) != NOT_FOUND;
        this.errflg = true;
        return res;
    }

    public boolean hasInstance( Scriptable value )
    {
        return false;
    }

    public void put( String name, Scriptable start, Object value )
    {
        if (this.has(name, start)) {
            this.error("cannot assign to element '" + name + "'");
        }
        else {
            this.error("no element named '" + name + "'");
        }
    }

    public void put( int index, Scriptable start, Object value )
    {
        if (this.has(index, start)) {
            this.error("cannot assign to element indexed  [" + index + "]");
        }
        else {
            this.error("no element indexed  [" + index + "]");
        }
    }

    public Scriptable getPrototype()
    {
        return prototype;
    }

    public void setPrototype( Scriptable prototype )
    {
        this.prototype = prototype;
    }

    public Scriptable getParentScope()
    {
        return parent;
    }

    public void setParentScope( Scriptable parent )
    {
        this.parent = parent;
    }
}
