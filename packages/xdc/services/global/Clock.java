package xdc.services.global;

public class Clock {
    
    static private boolean master = false;
    
    private long t0 = 0;
    private long prev = 0;
    private boolean enabled = false;
    
    public void enable()
    {
        enabled = true;
    }
    
    public void print( String msg )
    {
        if (master && enabled) {
            long cur = System.currentTimeMillis();
            System.out.printf("%s [%d]\n", msg, cur - prev);
            prev = cur;
        }
    }
    
    public void reset()
    {
        prev = t0 = System.currentTimeMillis();
    }
    
    public long total()
    {
        return System.currentTimeMillis() - t0;
    }
}
