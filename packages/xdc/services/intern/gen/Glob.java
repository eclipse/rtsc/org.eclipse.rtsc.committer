/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.gen;

import xdc.services.global.*;

import java.io.*;
import java.util.*;
import java.net.*;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import xdc.services.spec.*;

public class Glob
{
    // CONSTANTS

    static final int Decl$Met$SPI = 0x0;
    static final int Decl$Met$API = 0x1;
    static final int Decl$Met$OBJ = 0x2;
    static final int Decl$Met$SEC = 0x0;
    static final int Decl$Met$INL = 0x4;
    static final int Decl$Met$PTR = 0x8;
    static final int Decl$Met$DEF = 0x10;
    static final int Decl$Met$EXT = 0x0;

    static final int Decl$Uni$ISEC = 0;
    static final int Decl$Uni$IFAC = 1;
    static final int Decl$Uni$IMOD = 2;

    static final int Type$ABS = 0;
    static final int Type$SCO = 1;
    static final int Type$LCL = 2;
    static final int Type$PTR = 3;
    static final int Type$SCT = 4;
    static final int Type$LCT = 5;
    static final int Type$TYP = 6;
    static final int Type$CDL = 7;
    static final int Type$LPC = 8;
    static final int Type$SPC = 9;
    static final int Type$SPR = 10;
    static final int Type$INT = 11;

    static final int LOGARGS = 8;

    static final int CDLMODE = 0;
    static final int CPFMODE = 1;
    static final int CPPMODE = 2;
    static final int SCHMODE = 3;
    static final int SYNMODE = 4;
    static final int XMLMODE = 5;
    static final int JCLMODE = 5;

    static final String ERRBLK = "xdc_runtime_Error_Block*";
    static final String ERRARG = ERRBLK + " __eb";

    static final String[] GATEFXNS = {
        "create",
        "delete",
        "enter",
        "leave",
        "query",
    };

    // FIELDS

    Vector<String> nsvec;

    String dpath = "";

    Unit curUnit;

    String cname;
    String iname;
    String qname;
    String sname;
    String uname;

    boolean sch2 = false;

    int mode = -1;

    Out out;

    private boolean spflg;
    private int idflg;
    private boolean encflg;

    private boolean curSkip = false;

    static private String xdctree = null;

    static
    {
        File xdcroot = new File(System.getProperty("xdc.root"));

        Glob.scanVers(new File(xdcroot, "../ident.c"));

        if (Glob.xdctree == null) {
            Glob.scanVers(new File(xdcroot, "etc/TOOLS"));
        }
    }

    // scanVers
    private static void scanVers( File versFile )
    {
        if (!versFile.exists()) {
            return;
        }

        try {
            String what = Vers.getWhatString(versFile);
            int k0 = what.lastIndexOf("xdc-");
            int n = 4;
            if (k0 == -1) {
                k0 = what.lastIndexOf("xdccore-");
                n = 8;
            }
            if (k0 != -1) {
                for (int k1 = k0 + n; k1 < what.length(); k1++) {
                    char c = what.charAt(k1);
                    if (c != '.' && !Character.isLetterOrDigit(c)) {
                        Glob.xdctree = what.substring(k0, k1);
                        return;
                    }
                }
            }
        }
        catch (Exception e) {
            Err.exit(e);
        }
    }

    // genAny
    public void genAny( Object o, Class cls, Object ths )
    {
        Method m = null;

        try {
            String cn = o.getClass().getName();
            cn = cn.substring(cn.lastIndexOf('.') + 1);
            m = cls.getDeclaredMethod("gen" + cn, o.getClass());
            m.invoke(ths, o);
        }
        catch (InvocationTargetException ite ) {
            System.err.println(ite.getTargetException());
            Err.abort(ite);
        }
        catch (Exception e) {
            Err.abort(e);
        }
    }

    // genAny
    private void genAny( Object o ) { this.genAny(o, Glob.class, this); }

    // genArgNames
    void genArgNames( List<Decl.Arg> args, String sp )
    {
        for (Decl.Arg arg : args) {
            this.out.printf("%1%2", sp, arg.getName());
            sp = ", ";
        }
    }

    // genArgDecls
    void genArgDecls( List<Decl.Arg> args, int tid, String sp )
    {
        if (mode == CDLMODE && args.size() == 0 && sp.indexOf(',') == -1) {
            this.out.print(sp + "void");
            return;
        }

        for (Decl.Arg arg : args) {
            this.out.print(sp);
            if (mode == SYNMODE && tid == Type$LCL) {
                this.genType(arg.getType(), Type$TYP, arg.getName());
            }
            else {
                this.genType(arg.getType(), tid);
            }
            sp = ", ";
        }
    }

    // genArrType
    void genArrType( Type type, String id )
    {
        genArrType(type, id, true);
    }

    // genArrType
    void genArrType( Type type, String id, boolean top )
    {
        if (!(type instanceof Type.Array)) {
            if (type.tcode().endsWith("O")) {
                String tcn = mkCname(type.tspec().getRef().getScope());
                out.printf("%1Instance_State %2", tcn, id);
            }
            else {
                this.genType(type, Type$TYP, id);
            }
            return;
        }

        Type.Array tarr = (Type.Array)type;
        Type rb = tarr.getBase().root();
        boolean par = rb instanceof Type.Array
            && ((Type.Array)rb).getDim() != null;

        if (tarr.getDim() != null) {
            genArrType(tarr.getBase(), id, false);
            if (top) {
                genArrTypeTail(tarr);
            }
        }
        else if (tarr.isVec()) {
            this.out.printf("struct { int length; ");
            genArrType(tarr.getBase(), par ? "(*elem)" : "*elem", true);
            this.out.printf("; } %1", id);
        }
        else {
            genArrType(tarr.getBase(), "", false);
            this.out.printf(par ? "(*%1)" : "*%1", id);
            if (top) {
                genArrTypeTail(tarr);
            }
        }
    }

    // genArrTypeTail
    void genArrTypeTail( Type type )
    {
        if (!(type instanceof Type.Array)) {
            return;
        }

        Type.Array tarr = (Type.Array)type;

        if (tarr.getDim() != null) {
            this.out.printf("[");
            this.genExpr(tarr.getDim());
            this.out.printf("]");
            genArrTypeTail(tarr.getBase());
        }
        else if (tarr.isVec()) {
            ;
        }
        else {
            genArrTypeTail(tarr.getBase());
        }
    }

    // genBanner
    void genBanner( String title )
    {
        this.out.printf("%t/* %1 */\n", title);
    }

    // genBase
    void genBase( Unit iu )
    {
        if (iu == null) {
            this.out.printf("0");
        }
        else if (mode == CDLMODE) {
            String qn = this.mkCname(iu.getQualName());
            this.out.printf("(void*)&%1Interface__BASE__C", qn);
        }
    }

    // genClink
    void genClink( String ns )
    {
        this.out.printf("#ifdef __ti__\n%+");
        this.out.printf("%t#pragma DATA_SECTION(%1, \".const:%1\");\n", ns);
        this.out.printf("%tasm(\"\t.sect \\\".const:%1\\\"\");\n", ns);
        this.out.printf("%tasm(\"\t .clink\");\n");

        // TODO weird alchemy bug!!!
        this.out.printf("%tasm(\"\t.sect \\\"[0].const:%1\\\"\");\n", ns);
        this.out.printf("%tasm(\"\t .clink\");\n");
        this.out.printf("%tasm(\"\t.sect \\\"[1].const:%1\\\"\");\n", ns);
        this.out.printf("%tasm(\"\t .clink\");\n");
        this.out.printf("%-#endif\n");
    }

    // genClinkMacro
    void genClinkMacro()
    {
        /* #defines are created in the following way, so they will be
         * applied in the right order. 'xdc__cdecl_constant' must be applied
         * first, so it replaces #var with the actual variable name, and 
         * concatenates it with '.const'. Only then, _Pragma can take that
         * literal, which will not require any further expansion, and create
         * #pragma.
         */
        this.out.printf("#define PRAGMA(x) _Pragma(#x)\n");
        this.out.printf("#define xdc__clink_constant(var) \\\n");
        this.out.printf("%tPRAGMA(DATA_SECTION(var, \".const:\" #var))  \\\n");
        this.out.printf("%tasm(\"   .sect \\\".const:\" #var \"\\\"\\n\"  \\\n");
        this.out.printf("%t%t  \"   .clink\\n\"  \\\n");

        // TODO weird alchemy bug!!!
        this.out.printf("%t%t  \"   .sect \\\"[0].const:\" #var \"\\\"\\n\"  \\\n");
        this.out.printf("%t%t  \"   .clink\\n\"  \\\n");
        this.out.printf("%t%t  \"   .sect \\\"[1].const:\" #var \"\\\"\\n\"  \\\n");
        this.out.printf("%t%t  \"   .clink\")\n\n");
    }

    // genCreArgsDecls
    void genCreArgDecls( Unit unit, int tid, String sp )
    {
        if (unit.getCreator() == null) {
            this.out.print(sp);
            return;
        }

        if (unit.getCreator().getArgs().size() > 0) {
            this.genArgDecls(unit.getCreator().getArgs(), tid, sp);
            this.out.print(", ");
        }
        else {
            this.out.print(sp);
        }
    }

    // genCreArgsNames
    void genCreArgNames( Unit unit, String sp )
    {
        if (unit.getCreator() == null) {
            this.out.print(sp);
            return;
        }

        this.genArgNames(unit.getCreator().getArgs(), sp);
        this.out.print(unit.getCreator().getArgs().size() > 0 ? ", " : sp);
    }

    // genDecl
    void genDecl( Decl d )
    {
        this.genAny(d);
    }

    // genDecl$Arg
    void genDecl$Arg( Decl.Arg d )
    {
        if (mode == XMLMODE) {
            this.xmlAttr("type", this.xmlType(d.getType()));
            this.xmlAttr("code", d.getTypeCode());
            this.xmlAttr("init", this.xmlExpr(d.getInit()));
            return;
        }
    }

    // genDecl$Config
    void genDecl$Config( Decl.Config d )
    {
        if (mode == XMLMODE) {
            this.xmlAttr("type", this.xmlType(d.getType()));
            this.xmlAttr("code", d.getTypeCode());
            this.xmlAttr("init", this.xmlExpr(d.getInit()));
            if (d.isInst()) {
                this.xmlAttr("instance", true);
            }
            return;
        }
    }

    // genDecl$Const
    void genDecl$Const( Decl.Const d )
    {
        switch (mode) {
        case CDLMODE:
            if (d.getInit() != null) {
                this.out.printf("%t#define %1%2 (", this.cname, d.getName());
                this.genExpr(d.getInit());
                this.out.printf(")\n");
/*
                this.out.printf("%t#define %1%2 ((", this.cname, d.getName());
                this.genType(d.getType(), Type$ABS);
                this.out.printf(")(");
                this.genExpr(d.getInit());
                this.out.printf("))\n");
*/
            }
            break;
        case CPPMODE:
            this.out.printf("%tstatic const ");
            this.genType(d.getType(), Glob.Type$TYP, d.getName());
            this.out.printf(" = %1%2;\n", this.cname, d.getName());
            break;
        case SYNMODE:
            this.out.printf("%tstatic const ");
            this.genType(d.getType(), Type$TYP, this.sname + d.getName());
            this.out.print(";\n");
            break;
        case XMLMODE:
            this.xmlAttr("type", this.xmlType(d.getType()));
            this.xmlAttr("code", d.getTypeCode());
            if (d.getInit() != null) {
                this.xmlAttr("init", this.xmlExpr(d.getInit()));
            }
            break;
        }
    }

    // genDecl$Enum
    void genDecl$Enum( Decl.Enum d )
    {
        if (mode == XMLMODE) {
            return;
        }

        if (mode == CPPMODE) {
            this.out.printf("typedef %1%2 %2;\n", this.cname, d.getName());
            for (Decl.EnumVal e : d.getVals()) {
                this.out.printf("%tstatic const %1 %2 = %3%2;\n", d.getName(), e.getName(), this.cname);
            }
            return;
        }

        Type.Spec rep = d.getRep();
        String pre = mode == CDLMODE ? this.cname : mode == SYNMODE ? this.sname : "";
        String en = mode == CDLMODE && rep != null ? "" : (pre + d.getName());
        String sep = "";

        this.out.printf("%tenum %1 {%+", en);
        for (Decl.EnumVal e : d.getVals()) {
            this.out.printf("%1\n%t", sep);
            this.genDecl(e);
            sep = ",";
        }
        this.out.printf("\n%-%t};\n");

        if (mode == CDLMODE) {
            this.out.printf("%ttypedef ");
            if (rep != null) {
                this.genType$Spec(rep);
            }
            else {
                this.out.printf("enum %1%2", this.cname, d.getName());
            }
            this.out.printf(" %1%2;\n", this.cname, d.getName());
        }
    }

    // genDecl$EnumVal
    void genDecl$EnumVal( Decl.EnumVal d )
    {
        if (mode == XMLMODE) {
            return;
        }

        String pre = mode == CDLMODE ? this.cname : mode == SYNMODE ? this.sname : "";
        this.out.printf(pre + d.getName());

        if (d.getInit() != null) {
            this.out.print(" = ");
            this.genExpr(d.getInit());
        }
    }

    // genDecl$Extern
    void genDecl$Extern( Decl.Extern d )
    {
        switch (mode) {
        case CDLMODE:
            this.out.printf("%t#define %1%2 %3\n", this.cname, d.getName(), d.getValue());
            this.out.printf("%t__extern ");
            this.genType(d.getType(), Type$TYP, d.getValue());
            this.out.printf(";\n");
            break;
        case CPPMODE:
            this.out.printf("%tstatic ");
//          this.genType(d.getType(), Type$TYP, "(*const " + d.getName() + ")");
            this.genType(d.getType(), Type$TYP, "(& " + d.getName() + ")");
            this.out.printf(" = (");
            this.genType(d.getType(), Type$TYP, "(&)");
            this.out.printf(")%1;\n", d.getValue());
//          this.genType(d.getType(), Type$TYP, "(*)");
//          this.out.printf(")&%1;\n", d.getValue());
            break;
        case SYNMODE:
            this.out.printf("%tstatic ");
            this.genType(d.getType(), Type$TYP, "(*const " + this.sname + d.getName() + ")");
            this.out.printf(" = &%1;\n", d.getValue());
            break;
        case XMLMODE:
            this.xmlAttr("type", this.xmlType(d.getType()));
            this.xmlAttr("code", d.getTypeCode());
            break;
        }
    }

    // genDecl$Field
    void genDecl$Field( Decl.Field d )
    {
        if (mode == XMLMODE) {
            this.xmlAttr("type", this.xmlType(d.getType()));
            this.xmlAttr("code", d.getTypeCode());
            return;
        }

        this.out.tab();
        this.genType(d.getType(), Type$TYP, d.getName());
    }

    // genDecl$Fxn
    void genDecl$Fxn( Decl.Fxn d )
    {
        if (mode == XMLMODE) {
            if (d.getType() != null) {
                this.xmlAttr("type", this.xmlType(d.getType()));
                this.xmlAttr("code", d.getTypeCode());
            }
            if (d.isVarg()) {
                this.xmlAttr("vargs", true);
            }
            if (d.isInst()) {
                this.xmlAttr("instance", true);
            }
            return;
        }

        if (mode == SYNMODE) {
            this.out.tab();
            this.genType(d.getType(), Type$TYP, this.sname + d.getName());
            this.out.print("(");
            String sp = " ";
            if (!this.curUnit.isMod() && d.isStatic()) {
                this.out.printf(" %1Module", this.sname);
                sp = ", ";
            }
            else if (d.isInst()) {
                this.out.printf(" %1Handle", this.sname);
                sp = ", ";
            }
            this.genArgDecls(d.getArgs(), Type$LCL, sp);
            if (d.isVarg()) {
                this.out.printf(", ...");
            }
            this.out.print(" );\n");
            return;
        }
    }

    // genDecl$Imp
    void genDecl$Imp( Decl.Imp d )
    {
    }
    
    // genDecl$Proxy
    void genDecl$Proxy( Decl.Proxy d )
    {
        if (mode == XMLMODE) {
            this.xmlAttr("extends", d.getInherits().getQualName());
        }
    }

    // genDecl$Struct
    void genDecl$Struct( Decl.Struct d )
    {
        if (mode == XMLMODE) {
            return;
        }

        if (mode == CPPMODE) {
            this.out.printf("typedef %1%2 %2;\n", this.cname, d.getName());
//            this.out.printf("struct %2 : public %1%2 { };\n", this.cname, d.getName());
            return;
        }

        String skw = d.isUnion() ? "union" : "struct";
        String sid = (mode == CDLMODE ? this.cname : mode == SYNMODE ? this.sname : "") + d.getName();
        if (mode == CDLMODE) {
            this.out.printf("typedef %1 %2 %2; ", skw, sid);
        }
        if (d.getFields() == null) {
            this.out.printf("%t%1 %2;\n", skw, sid);
            return;
        }
        this.out.printf("%t%1 %2 {\n%+", skw, sid);
        for (Decl.Field fld : d.getFields()) {
            this.genDecl(fld);
            this.out.print(";\n");
        }
        this.out.printf("%-%t};\n");
    }

    // genDecl$Typedef
    void genDecl$Typedef( Decl.Typedef d )
    {
        if (mode == XMLMODE) {
            this.xmlAttr("type", this.xmlType(d.getType()));
            this.xmlAttr("code", d.getTypeCode());
            return;
        }

        if (d.attrBool(Attr.A_Encoded)) {
            return;
        }

        this.out.printf("%ttypedef ");
        if (mode == SYNMODE) {
            this.genType(d.getType(), Type$TYP, this.sname + d.getName());
        }
        else {
            this.genType(d.getType(), mode == CDLMODE ? Type$CDL : Type$LCL);
        }
        this.out.print(";\n");
    }

    // genDefines
    void genDefines()
    {
        this.out.printf("#ifdef __cplusplus\n");
        this.out.printf("#define __extern extern \"C\"\n");
        this.out.printf("#else\n");
        this.out.printf("#define __extern extern\n");
        this.out.printf("#endif\n\n");
    }

    // genExpr
    void genExpr( Expr e )
    {
        genExpr(e, "", false);
    }

    // genExpr
    void genExpr( Expr e, boolean encflg )
    {
        genExpr(e, "", encflg);
    }

    // genExpr
    void genExpr( Expr e, String dpath )
    {
        genExpr(e, dpath, false);
    }

    // genExpr
    void genExpr( Expr e, String dpath, boolean encflg )
    {
        this.dpath = dpath;
        this.encflg = encflg;

        if (e == null) {
            this.out.print('0');
            return;
        }
        this.genAny(e);

        this.dpath = "";
        this.encflg = false;
    }

    // genExpr$Array
    void genExpr$Array( Expr.Array e )
    {
        String sep = "";
        this.out.print(mode == JCLMODE ? "Global.newArray(new Object[]{" :"$$SO([");
        for (Expr elem : e.getElems()) {
            this.out.print(sep);
            this.genAny(elem);
            sep = ", ";
        }
        this.out.print(mode == JCLMODE ? "})": "])");
    }

    // genExpr$Binary
    void genExpr$Binary( Expr.Binary e )
    {
        boolean jm = mode == JCLMODE;

        if (jm) {
            this.out.print("Global.eval(\"");
            mode = SCHMODE;
        }

        this.genAny(e.getLeft());
        this.out.printf(" %1 ", e.getOp());
        this.genAny(e.getRight());

        if (jm) {
            this.out.print("\")");
            mode = JCLMODE;
        }
    }

    // genExpr$Cond
    void genExpr$Cond( Expr.Cond e )
    {
        boolean jm = mode == JCLMODE;

        if (jm) {
            this.out.print("Global.eval(\"");
            mode = SCHMODE;
        }

        this.genAny(e.getCond());
        this.out.print(" ? ");
        this.genAny(e.getLeft());
        this.out.print(" : ");
        this.genAny(e.getRight());

        if (jm) {
            this.out.print("\")");
            mode = JCLMODE;
        }
    }

    // getExpr$Const
    void genExpr$Const( Expr.Const e )
    {
        String cs = e.getVal();

        switch (mode) {

        case SCHMODE:
        case JCLMODE:
            char ch = cs.charAt(0);
            if (Character.isDigit(ch)) {
                for (int i = 0; i < 2; i++) {
                    int k = cs.length() - 1;
                    char c = Character.toUpperCase(cs.charAt(k));
                    if (c != 'L' && c != 'U') {
                        break;
                    }
                    cs = cs.substring(0, k);
                }
                if (mode == JCLMODE && !cs.contains(".")) {
                    cs += 'L';
                }
            }
            else if (this.encflg && (ch == '"' || ch == '\'')) {
                cs = this.encode(cs.substring(1, cs.length() - 1));
                cs = "\\\"\\\"+java.net.URLDecoder.decode(\\" + ch + cs + "\\" + ch + ", 'UTF-8')";
            }
            else if (ch == '\'') {
                cs = cs.substring(1, cs.length() - 1);
                cs = cs.replace("'", "\"");
                cs = cs.replace("\"", "\\\"");
                cs = '"' + cs + '"';
            }
            break;

        case CDLMODE:
            if (cs.length() > 3 && cs.startsWith("'\"")) {
                cs = cs.substring(1, cs.length() - 1);
            }
            break;

        }

        this.out.print(cs);
    }

    // genExpr$Creat
    void genExpr$Creat( Expr.Creat e )
    {
        Ref r = e.getMod().getRef();
        String sep = "";

        if (mode == JCLMODE) {
            this.out.printf("Global.callFxn(\"create\", (Scriptable)om.find(\"%1\")", r.getScope());
            sep = ", ";
        }
        else {
            this.out.printf("xdc.om['%1'].create(", r.getScope());
        }

        for (Expr arg : e.getArgs()) {
            this.out.printf(sep);
            this.genExpr(arg);
            sep = ", ";
        }
        this.out.printf(")");
    }

    // genExpr$Hash
    void genExpr$Hash( Expr.Hash e )
    {
        String sep = "";

        if (mode == JCLMODE) {
            this.out.print("Global.newObject(");
            for (int i = 0; i < e.getIds().size(); i++) {
                this.out.printf("%1\"%2\"", sep, e.getIds().get(i).getText());
                this.out.print(", ");
                this.genAny(e.getVals().get(i));
                sep = ", ";
            }
            this.out.print(")");
        }
        else {
            this.out.print("$$SO({");
            for (int i = 0; i < e.getIds().size(); i++) {
                this.out.printf("%1%2", sep, e.getIds().get(i).getText());
                this.out.print(": ");
                this.genAny(e.getVals().get(i));
                sep = ", ";
            }
            this.out.print("})");
        }
    }

    // genExpr$Paren
    void genExpr$Paren( Expr.Paren e )
    {
        this.out.print('(');
        if (e.getBody() != null) {
            this.genAny(e.getBody());
        }
        this.out.print(')');
    }
/*
    // genExpr$Size
    void genExpr$Size( Expr.Size e )
    {
        if (mode == SCHMODE) {
            this.genRef(e.getRef());
            this.out.printf(".$sizeof()");
        }
        else {
            this.out.print("sizeof(");
            this.genRef(e.getRef());
            this.out.print(')');
        }
    }
*/
    // genExpr$Term
    void genExpr$Term( Expr.Term e )
    {
        if (e.getVal() != null) {
            genExpr(e.getVal());
        }
        else {
            genRef(e.getRef());
        }

/*
        Decl d = e.getDecl();

        if (d instanceof Decl.Cst) {
            genExpr(((Decl.Cst)d).init);
        }
        else {
            this.genName(e.name);
        }
*/
    }

    // genExpr$Unary
    void genExpr$Unary( Expr.Unary e )
    {
        boolean jm = mode == JCLMODE;

        if (jm) {
            this.out.print("Global.eval(\"");
            mode = SCHMODE;
        }

        this.out.print(e.getOp());
        this.genAny(e.getRight());

        if (jm) {
            this.out.print("\")");
            mode = JCLMODE;
        }
    }

    // genInclude
    public void genInclude( String guard, String file )
    {
        genInclude(guard, file, this.out);
    }

    // genInclude
    public void genInclude( String guard, String file, Out out )
    {
        out.printf("#ifndef %1include\n", guard);
        out.printf("#ifndef __nested__\n");
        out.printf("#define __nested__\n");
        out.printf("#include <%1>\n", file);
        out.printf("#undef __nested__\n");
        out.printf("#else\n");
        out.printf("#include <%1>\n", file);
        out.printf("#endif\n");
        out.printf("#endif\n\n");
    }

    // genModuleDefines
    void genModuleDefines()
    {
        this.genTitleD("per-module runtime symbols");

        this.out.printf("#undef Module__MID\n");
        this.out.printf("#define Module__MID %1Module__id__C\n", this.cname);

        this.out.printf("#undef Module__DGSINCL\n");
        this.out.printf("#define Module__DGSINCL %1Module__diagsIncluded__C\n", this.cname);

        this.out.printf("#undef Module__DGSENAB\n");
        this.out.printf("#define Module__DGSENAB %1Module__diagsEnabled__C\n", this.cname);

        this.out.printf("#undef Module__DGSMASK\n");
        this.out.printf("#define Module__DGSMASK %1Module__diagsMask__C\n", this.cname);

        this.out.printf("#undef Module__LOGDEF\n");
        this.out.printf("#define Module__LOGDEF %1Module__loggerDefined__C\n", this.cname);

        this.out.printf("#undef Module__LOGOBJ\n");
        this.out.printf("#define Module__LOGOBJ %1Module__loggerObj__C\n", this.cname);

        this.out.printf("#undef Module__LOGFXN4\n");
        this.out.printf("#define Module__LOGFXN4 %1Module__loggerFxn4__C\n", this.cname);

        this.out.printf("#undef Module__LOGFXN8\n");
        this.out.printf("#define Module__LOGFXN8 %1Module__loggerFxn8__C\n", this.cname);

        this.out.printf("#undef Module__G_OBJ\n");
        this.out.printf("#define Module__G_OBJ %1Module__gateObj__C\n", this.cname);

        this.out.printf("#undef Module__G_PRMS\n");
        this.out.printf("#define Module__G_PRMS %1Module__gatePrms__C\n", this.cname);

        for (String fxn : GATEFXNS) {
            this.out.printf("#undef Module__GP_%1\n", fxn);
            this.out.printf("#define Module__GP_%2 %1Module_GateProxy_%2\n", this.cname, fxn);
        }
    }

    // genSections
    void genSections( String... secL )
    {
        String pre = " *";
        String tab = "    ";

        this.out.printf("/*\n%1 ======== GENERATED SECTIONS ========\n", pre);
        for (String sec: secL) {
            this.out.printf("%1%2 %3\n", pre, tab, sec);
        }
        this.out.printf("%1/\n", pre);

    }

    // genRef
    void genRef( Ref r )
    {
        if (r.isGlobal()) {
            String s = r.getId();
            if (mode == SCHMODE) {
            	s = "xdc.module('xdc.rov.support.ScalarStructs').S_" + s;
            }
            else if (mode != SYNMODE && Character.isUpperCase(s.charAt(0))) {
                s = "xdc_" + s;
            }
            this.out.print(s);
            return;
        }

        if (r.getId().startsWith("XDC_")) {
            this.out.print(r.getId());
            return;
        }

        switch (mode) {
        case SCHMODE:
            if (this.sch2) {    /// FIX LATER
                this.out.printf("xdc.om['%1", r.getScope());
                String s = r.getId().equals("") || r.getId().equals("module") ? "" : ("." + r.getId());
                this.out.printf("%1']", s);
            }
            else if (r.getId().equals("")) {
                this.out.printf("$$Expr(\"xdc.om['%1']\")", r.getScope());
            }
            else {
                this.out.printf("xdc.om['%1.%2']", r.getScope(), r.getId());
            }
            break;
        case JCLMODE:
            this.out.printf("om.find(\"%1", r.getScope());
            String s = r.getId().equals("") || r.getId().equals("module") ? "" : ("." + r.getId());
            this.out.printf("%1\")", s);
            break;
        case CDLMODE:
        case CPFMODE:
            Decl.Proxy prx = r.getProxy();
            if (
                prx != null && this.curUnit.isMod() &&
                !this.curUnit.isProxy() && prx.getParent() == this.curUnit
            ) {
                /* reference is via a proxy defined in the current module */
                this.out.printf("%1%2_%3", this.cname, prx.getName(), r.getId());
            }
            else {
                this.out.printf("%1%2", mkCname(r.getScope()), r.getId());
            }
            break;
        case CPPMODE:
//            this.out.printf("%1::%2", nsName(r.getScope()), r.getId());
            this.out.printf("%1%2", mkCname(r.getScope()), r.getId());
            break;
        case SYNMODE:
            // TODO: combine with above
//          String cn = mkCname(r.getScope());
            String cn = mkCname(r.getNode().getParent().getName());
//            System.out.println("cn = " + cn);
            if (this.cname.equals(cn)) {
                cn = this.sname;
            }
            prx = r.getProxy();
            if (
                prx != null && this.curUnit.isMod() &&
                !this.curUnit.isProxy() && prx.getParent() == this.curUnit
            ) {
                /* reference is via a proxy defined in the current module */
            	this.out.printf("%1%2_%3", this.sname, prx.getName(), r.getId());
            }
            else {
              this.out.printf("%1%2", cn, r.getId());
//                this.out.printf("%1%2", this.sname, r.getId());
            }
            break;
        }
    }

    // genStateFlds
    void genStateFlds( Unit unit, boolean iflg ) { genStateFlds(unit, iflg, false, false); }

    void genStateFlds( Unit unit, boolean iflg, boolean isStruct, boolean limit )
    {
        Decl.Struct sobj = (Decl.Struct)unit.getSession().findDecl(unit, iflg ? "Instance_State" : "Module_State");
        String ucn = this.mkCname(unit.getQualName());

        if (iflg && unit.isHeir()) {
            this.out.printf("%tconst %1Fxns__* __fxns;\n", ucn);
        }
        if (sobj != null) {
            int fid = 0;
            for (Decl.Field fld : sobj.getFields()) {

                String fn = isStruct ? ("__f" + fid++) : fld.getName();
                Decl.Signature.ObjKind kind = Decl.objKind(fld);

                if (kind != Decl.Signature.ObjKind.NONE) {
                    if (limit) {
                        this.out.printf("%tchar __dummy;\n");
                        break;
                    }
                    if (!isStruct) {
                        fn = "Object_field_" + fn;
                    }
                    String cn = mkCname(fld.getType().tspec().getRef().getScope());
                    this.out.printf("%t%1%3 %2", cn, fn, isStruct ? "Struct" : "Object__");
                    if (kind == Decl.Signature.ObjKind.ARRAY) {
                        this.out.printf("[");
                        Type.Array tarr = (Type.Array)fld.getType();
                        this.genExpr(tarr.getDim());
                        this.out.printf("]");
                    }
                    this.out.printf(";\n");
                }
                else if (this.isArr(fld.getTypeCode())) {
                    this.out.printf("%t__TA_%1 %2;\n", ucn + sobj.getName() + "__" + fld.getName(), fn);
                }
                else {
                    this.out.tab();
                    this.genType(fld.getType(), Glob.Type$TYP, fn);
                    this.out.printf(";\n");
                }
            }
        }

        if (iflg && unit.delegatesTo() != null && unit.isSized()) {
            this.out.printf("%t%1Object __deleg;\n", this.mkCname(unit.delegatesTo().getQualName()));
        }
    }

    // genTitle
    void genTitle(String msg)
    {
        if (this.curSkip) {
            this.out.printf("\n");
        }
        this.out.printf("\n/*\n * ======== %1 ========\n */\n\n", msg);
        this.curSkip = false;
    }

    // genTitleD
    void genTitleD(String name)
    {

        if (this.curSkip) {
            this.out.printf("\n");
        }
        this.out.printf("/* %1 */\n", name);
        this.curSkip = true;
    }

    // genType
    void genType( Type t, int i, String s )
    {
        String nsav = this.qname;
        if (i == Type$SCO || i == Type$SCT) {
            this.qname = this.qname + s;
        }
        else if (i == Type$TYP || i == Type$INT) {
            this.qname = s;
        }

        if (mode == SYNMODE && t instanceof Type.Array) {
            this.genArrType(t, s);
        }
        else {
            genType(t, i);
        }

        this.qname = nsav;
    }

    // genType
    void genType( Type t, int i )
    {
        boolean ssav = this.spflg;
        int isav = this.idflg;
        this.spflg = (i != Type$ABS);
        this.idflg = i;

        if (t instanceof Type.Array && (((Type.Array)t).isVec())) {
            ;
        }
        else {
            this.genAny(t.tspec());
        }

        this.genAny(t);
        this.spflg = ssav;
        this.idflg = isav;
    }

    // genType$Array
    void genType$Array( Type.Array t )
    {
        if (t.isVec()) {
            this.out.print("struct { int length; ");
            this.genType(t.getBase(), Type$TYP, "*elem");
            this.out.print("; } ");
            Type.Declarator td = (Type.Declarator)t.term();
            this.out.print(this.cname + td.getId());
        }
        else {
            this.genAny(t.getBase());
            this.out.print('[');
            if (t.getDim() != null) {
                this.genExpr(t.getDim());
            }
            this.out.print(']');
        }
    }

    // genType$Declarator
    void genType$Declarator( Type.Declarator t )
    {
        if (t.getId() == null || this.idflg == Type$ABS) {
            return;
        }
        if (this.spflg) {
            this.out.print(' ');
            this.spflg = false;
        }
        switch (this.idflg) {
            case Type$LCL:
            case Type$LCT:
            {
                this.out.printf("%1%2", this.idflg == Type$LCT ? "const " : "", t.getId());
                return;
            }
            case Type$SCO:
            case Type$SCT:
            {
                this.out.printf("%1%2::%3",
                    this.idflg == Type$SCT ? "const " : "", this.qname, t.getId());
                return;
            }
            case Type$PTR:
            case Type$LPC:
            {
                this.out.printf("(*%1%2)", this.idflg == Type$LPC ? "const " : "", t.getId());
                return;
            }
            case Type$SPR:
            case Type$SPC:
            {
                this.out.printf("(*%1%2::%3)",
                this.idflg == Type$LPC ? "const " : "", this.qname, t.getId());
                return;
            }
            case Type$TYP:
            {
                this.out.printf("%1", this.qname);
                return;
            }
            case Type$CDL:
            {
                this.out.printf("%1%2%3", this.idflg == Type$LCT ? "const " : "", this.cname, t.getId());
                return;
            }
            case Type$INT:
            {
                this.out.printf("const &%1%2", this.qname, t.getId());
                return;
            }
        }
    }

    // genType$Fxn
    void genType$Fxn( Type.Fxn t )
    {
        this.genAny(t.getBase());
        this.out.print('(');
        this.genArgDecls(t.getArgs(), Type$ABS, "");
        if (t.isVarg()) {
            this.out.print(", ...");
        }
        this.out.print(')');
    }

    // genType$Paren
    void genType$Paren( Type.Paren t )
    {
        if (this.spflg) {
            this.out.print(' ');
            this.spflg = false;
        }
        this.out.print('(');
        this.genAny(t.getBase());
        this.out.print(')');
    }

    // genType$Ptr
    void genType$Ptr( Type.Ptr t )
    {
        this.out.print('*');
        this.genTypeMods(t.getMods());
        this.genAny(t.getBase());
    }

    // genType$Spec
    void genType$Spec( Type.Spec t )
    {
        this.genTypeMods(t.getMods());
        this.genRef(t.getRef());
    }

    // genTypeMods
    void genTypeMods( EnumSet<Type.Modifier> mods)
    {
        if (mods == null) {
            return;
        }

        if (mods.contains(Type.Modifier.CONST)) {
            this.out.print("const ");
        }
        if (mods.contains(Type.Modifier.RESTRICT)) {
            this.out.print("restrict ");
        }
        if (mods.contains(Type.Modifier.VOLATILE)) {
            this.out.print("volatile ");
        }
    }

    // genWarning
    void genWarning()
    {
        this.out.printf("/*\n *  Do not modify this file; it is automatically \n");
        this.out.printf(" *  generated and any modifications will be overwritten.\n");
        if (Glob.xdctree != null) {
            this.out.printf(" *\n");
            this.out.printf(" * %s\n", this.vers());
        }
        this.out.printf(" */\n");
    }

    // dims
    int dims( String code )
    {
        int d;

        for (d = 0; code.charAt(d) == 'A' || code.charAt(d) == 'M' || code.charAt(d) == 'V'; d++) ;
        return d;
    }

    // encode
    public String encode( String text )
    {
        String res = null;
        try {
            res = URLEncoder.encode(text, "UTF-8");
        }
        catch (Exception e) {
            Err.abort(e);
        }
        return res;
    }

    // encodedTypeRef
    public Ref encodedTypeRef( Type type )
    {
        Ref res = null;

        while (type instanceof Type.Declarator) {
            Node n = type.tspec().getRef().getNode();
            if (!(n instanceof Decl.Typedef)) {
                break;
            }
            Decl.Typedef td = (Decl.Typedef)n;
            if (td.attrBool(Attr.A_Encoded)) {
                return type.tspec().getRef();
            }
            type = td.getType();
        }

        return res;
    }

    // errArg
    public String errArg( Unit u )
    {
        return u.attrBool(Attr.A_InstanceInitError) ? (", " + ERRARG) : "";
    }

    public String errBlk( Unit u )
    {
        return u.attrBool(Attr.A_InstanceInitError) ? (", " + ERRBLK) : "";
    }

    // fmtSpec
    String fmtSpec( Decl.Signature sig )
    {
        String res = "";

        switch (sig.getTypeCode().charAt(0)) {
        case 'A':
        case 'P':
        case 'o':
            res = "%p";
            break;
        case 'b':
        case 'e':
            res = "%d";
            break;
        case 's':
            res = "\"%s\"";
            break;
        case 'n':
            res = sig.getType().tspec().isUns() ? "0x%x" : "%d";
            break;
        }

        return res;
    }

    // isAgg
    public boolean isAgg( Decl.Typedef td )
    {
        char c = td.getTypeCode().charAt(0);
        return c == 'A' || c == 'V';
    }

    // isArr
    public boolean isArr( String code )
    {
        return code.startsWith("A") || code.startsWith("M") || code.startsWith("V");
    }

    // isVoid
    boolean isVoid( Decl.Fxn fxn )
    {
        return (fxn.getType() instanceof Type.Declarator && fxn.getTypeCode().startsWith("v"));
    }

    // mkCname
    public String mkCname( String qn )
    {
        return qn.replace('.', '_') + '_';
    }

    // mkFname
    public String mkFname( String qn )
    {
        return (qn.replace('.', '/'));
    }

    // mkIname
    public String mkIname( String qn )
    {
        return (qn.replace('.', '_') + '_');
    }

    // mkQname
    public String mkQname( String qn )
    {
        int k = qn.lastIndexOf('.');
        if (k == -1) {
            return qn;
        }

        int len = (k == qn.length() - 1) ? k : qn.length();

        String res = "";

        for (int i = 0; i < len; i++) {
            char c = qn.charAt(i);
            if (c == '.') {
                res += "::";
            }
            else {
                res += c;
            }
        }

        return res;
    }

    // mkQname_old
    public String mkQname_old( String qn )
    {
        int k = qn.lastIndexOf('.');
        if (k == -1) {
            return qn;
        }
        String un = qn.substring(k + 1);
        qn = qn.substring(0, k);
        k = qn.lastIndexOf('.');

        if (k == -1) {
            return qn + '_' + un;
        }

        return (qn.substring(0, k).replace('.', '_') + "::"
                + qn.substring(k + 1) + '_' + un);
    }

    // mkSname
    public String mkSname( String qn )
    {
        String res = mkQname_old(qn);
        int k = res.indexOf(':');

        return ((k == -1) ? res
                : res.substring(0, k) + "$$" + res.substring(k + 2));
    }

    // mkUname
    public String mkUname( String qn )
    {
        int k = qn.lastIndexOf('.');
        return ((k != -1) ? qn.substring(k + 1) : qn);
    }

    // nsBeg
    String nsBeg( Unit unit )
    {
        return "namespace " + unit.getPkgName().replace('.', '_') + " { namespace " + unit.getName();
    }

    // nsEnd
    String nsEnd()
    {
        return "}}";
    }

    // nsName
    String nsName( Unit unit )
    {
        return unit.getPkgName().replace('.', '_') + "::" + unit.getName();
    }

    String nsName( String qn )
    {
        int k = qn.lastIndexOf('.');
        return qn.substring(0, k).replace('.', '_') + "::" + qn.substring(k + 1);
    }

    // offsetFlds
    public List<Decl.Field> offsetFlds( Unit unit )
    {
        List<Decl.Field> fL = new ArrayList();

        for (String sn : new String[] {"Module_State", "Instance_State"}) {
            Decl.Struct str = (Decl.Struct)unit.getSession().findDecl(unit, sn);
            boolean mflg = sn.charAt(0) == 'M';
            if (str != null) {
                for (Decl.Field fld : str.getFields()) {
                    Decl.Signature.ObjKind ok = Decl.objKind(fld);
                    if (ok == Decl.Signature.ObjKind.NONE) {
                        continue;
                    }
                    fL.add(fld);
                }
            }
        }

        return fL;
    }

    // rawType
    public Type rawType( Type t )
    {
    	return t != null ? t.raw() : null;
    }

    // setNames
    void setNames( Unit unit )
    {
        String qn = unit.getQualName();

        nsvec = unit.getPkgName().equals("") ? null : split(unit.getPkgName());

        cname = mkCname(qn);
        iname = mkIname(qn);
        uname = mkUname(qn);

        sname = uname + '_';

        qname = "";
        if (nsvec != null) {
            for (String s : nsvec) {
                qname += s + "::";
            }
        }
        qname += unit.getName();

        this.curUnit = unit;
    }

    // split
    Vector<String> split( String qn )
    {
        if (qn.equals("")) {
            return null;
        }

        Vector<String> v = new Vector();

        int k;

        for (int s = 0; (k = qn.indexOf('.', s)) != -1; s = k + 1) {
            v.add(qn.substring(s, k));
        }

        return v;
    }

    // vers
    public static String vers()
    {
        return "@(#) " + Glob.xdctree;
    }

    // xdctree
    public static String xdctree()
    {
        return Glob.xdctree;
    }

    // xmlAttr
    public void xmlAttr( String name, boolean bval )
    {
        xmlAttr(name, bval ? "1" : "0");
    }

    // xmlAttr
    public void xmlAttr( String name, int ival )
    {
        xmlAttr(name, Integer.toString(ival));
    }

    // xmlAttr
    public void xmlAttr( String name, String val )
    {
        this.out.printf(" %1='%2'", name, val);
    }

    // xmlContent
    public void xmlContent()
    {
        this.out.printf(">\n%+");
    }

    // xmlElem
    public void xmlElem( String tag )
    {
        this.out.printf("%t<%1", tag);
    }

    // xmlExpr
    String xmlExpr( Expr e )
    {
        return this.encode(Expr.toXml(e));
    }

    // xmlEnd
    public void xmlEnd()
    {
        this.out.printf("/>\n");
    }

    // xmlEnd
    public void xmlEnd( String tag )
    {
        this.out.printf("%-%t</%1>\n", tag);
    }

    // xmlType
    String xmlType( Type t )
    {
        return this.encode(t.xmlsig());
    }

}

