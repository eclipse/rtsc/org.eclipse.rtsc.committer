/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.gen;

import xdc.services.spec.*;
import java.util.*;

public class Cpp
{
    private String curpre;
    private Glob glob;
    private boolean enabled;
    private boolean isProxy = false;
    
    private ArrayList<String> defs = new ArrayList();
    private ArrayList<String> prxs = new ArrayList();

    Cpp( Glob glob )
    {
        this.glob = glob;
        this.enabled = "yes".equals(java.lang.System.getenv("XDCPLUSPLUS"));
    }
    
    // genClientAlias
    void genClientAlias( Unit unit )
    {
        if (!this.enabled) {
            return;
        }
    
        if (unit.isProxy()) {
            return;
        }
        
        glob.genTitleD("C++ wrapper");
//      glob.out.printf("#if %3(xdc__plusplus) && %3(%1_cplusplus) && !%3(%1%2)\n", glob.cname, Internal.IS, "defined");
        glob.out.printf("#if %3(xdc__plusplus) && !%3(%1%2)\n", glob.cname, Internal.IS, "defined");
        glob.out.printf("namespace %2 = %1;\n", glob.nsName(unit), unit.getName());
        glob.out.printf("#endif\n");
    }
    
    // genAuxDefs
    private void genAuxDefs( Unit unit )
    {
        for (Decl d : unit.getDecls()) {
            if (d.isMeta() || !(d instanceof Decl.AuxDef) || d instanceof Decl.EnumVal) {
                continue;
            }
            if (d instanceof Decl.Struct && (d.getName().equals("Module_State") || d.getName().equals("Instance_State"))) {
                continue;
            }
            addDef(d.getName());
            glob.genTitleD(curpre + d.getName());
            glob.genDecl(d);
        }
    }
    
    // genInstCfgs
    private void genInstCfgs( Unit unit )
    {
        boolean isIInst = unit.getQualName().equals("xdc.runtime.IInstance");

        addDef("Params");
        glob.genTitleD(curpre + "Params");
        glob.out.printf("struct Params {\n%+");
        
        if (unit.isMod()) {
            glob.out.printf("%tParams() { %1Params_init((%1Params*)this); }\n", glob.cname);
            glob.out.printf("%tParams( const Params& prms ) { %1Params_copy((%1Params*)this, (%1Params*)&prms); }\n", glob.cname);
            glob.out.printf("%tParams& operator=( const Params& prms )\n%t{\n%+");
            glob.out.printf("%t%1Params_copy((%1Params*)this, (%1Params*)&prms);\n", glob.cname);
            glob.out.printf("%treturn *this;\n%-%t}\n");
        }

        glob.out.printf("private:\n");
        glob.out.printf("%tsize_t __size;\n");
        if (!isIInst) {
            glob.out.printf("%tconst void* __self;\n");
            glob.out.printf("%tvoid* __fxns;\n");
        }
        glob.out.printf("public:\n");
        if (!isIInst) {
            glob.out.printf("%txdc_runtime_IInstance_Params* instance;\n");
        }
        for (Decl d : unit.getDecls()) {
            if (!(d instanceof Decl.Config) || d.isStatic() || d.isMeta() || d.overrides() != null) {
                continue;
            }
            Decl.Config cfg = (Decl.Config)d;
            if (cfg.isStatic() || cfg.isMeta()) {
                continue;
            }
            if (glob.isArr(cfg.getTypeCode())) {
                glob.out.printf("%t__TA_%1 %2;\n", glob.cname + cfg.getName(), cfg.getName());
            }
            else {
                glob.out.tab();
                glob.genType(cfg.getType(), Glob.Type$TYP, cfg.getName());
                glob.out.printf(";\n");
            }
        }
        if (unit.isMod() && !isIInst) {
            glob.out.printf("private:\n");
            glob.out.printf("%txdc_runtime_IInstance_Params __iprms;\n");
        }
        glob.out.printf("%-%t};\n");
    }
    
    // genInstFxns
    private void genInstFxns( Unit unit )
    {
        glob.genTitleD(curpre + "Instance");
        glob.out.printf("struct Instance {\n%+");
        glob.out.printf("private:\n");
        glob.out.printf("%tvoid* operator new( size_t ) { return (void*)-1; }\n");
        glob.out.printf("public:\n");
        glob.out.printf("%tvoid operator delete( void* obj ) { %1delete((%1Handle*)&obj); }\n", glob.cname);
        glob.out.printf("%-");
        
        String pre = curpre + "Instance::";

        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isStatic() || fxn.isMeta() || fxn.isSys() || fxn.isInternal()) {
                continue;
            }
            if (fxn.hasAttr(Attr.A_Macro)) {
                continue;
            }
            glob.genTitleD(pre + fxn.getName());
            glob.genType(fxn.getType(), Glob.Type$TYP, fxn.getName());
            glob.out.printf("( ");
            glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, "");
            glob.out.printf(" )\n{\n%+");
            glob.out.printf("%t%1%2%3(", !glob.isVoid(fxn) ? "return " : "", glob.cname, fxn.getName());
            glob.out.printf("(%1Handle)this", glob.cname);
            glob.genArgNames(fxn.getArgs(), ", ");
            glob.out.printf(");\n}\n%-");
        }
        
        glob.out.printf("\n};\n");

        if (unit.isMod()) {
            glob.genTitleD(curpre + "Struct");
            glob.out.printf("class Struct {\n%+");
            glob.out.printf("%tchar __dummy[sizeof(%1Struct)];\n", glob.cname);
            glob.out.printf("%tvoid* operator new( size_t ) { return (void*)-1; }\n");
            glob.out.printf("public:\n");
            glob.out.printf("%tInstance* operator->() { return (Instance*)this; }\n");
            glob.out.printf("%tvoid operator delete( void* obj ) { %1destruct((%1Struct*)obj); }\n", glob.cname);
            glob.out.printf("%-};\n");
        }
    }
    
    // genInternals
    void genInternals( Unit unit )
    {
        if (!this.enabled) {
            return;
        }
     
        int oldmode = glob.mode;
        glob.mode = Glob.CPPMODE;
        
//      glob.out.printf("#if %3(xdc__plusplus) && %3(%1_cplusplus) && %3(%1%2)\n", glob.cname, Internal.IS, "defined");
        glob.out.printf("#if %3(xdc__plusplus) && %3(%1%2)\n", glob.cname, Internal.IS, "defined");
        glob.out.printf("namespace %1 {\n", unit.getName());
        String ns = glob.nsName(unit);
        glob.out.indent();
        for (String ds : defs) {
            glob.out.printf("%tusing %1::%2;\n", ns, ds);
        }
        for (String ps : prxs) {
            glob.out.printf("%tnamespace %1 = %2::%1;\n", ps, ns);
        }
        glob.out.outdent();
        skip();
        
        String pre = unit.getName() + "::";
        String nl = "\\\n";
        String sp;

        if (unit.attrBool(Attr.A_ModuleStartup)) {
            glob.genTitleD(pre + "Module_startup");
            glob.out.printf("inline int Module_startup( int state );\n");
            glob.out.printf("#define DEFINE__Module_startup%1%+", nl);
            glob.out.printf("%tint %1Module_startup( int state )%2%t{%2%+", glob.cname, nl);
            glob.out.printf("%treturn %1Module_startup(state);%-%2%t}%2%-", pre, nl);
        }
        
        if (unit.isInst()) {

            glob.genTitleD(pre + "Instance_init");
            glob.out.printf("inline void Instance_init( Object* obj");
            glob.genCreArgDecls(unit, Glob.Type$LCL, ", ");
            glob.out.printf("const Params* prms );\n");
            glob.out.printf("#define DEFINE__Instance_init%1%+", nl);
            glob.out.printf("%tvoid %1Instance_init( %1Object* obj", glob.cname);
            glob.mode = Glob.CDLMODE;
            glob.genCreArgDecls(unit, Glob.Type$LCL, ", ");
            glob.mode = Glob.CPPMODE;
            glob.out.printf("const %1Params* prms )%2%t{%2%+", glob.cname, nl);
            glob.out.printf("%t%1Instance_init((%1Object*)obj", pre);
            glob.genCreArgNames(unit, ", ");
            glob.out.printf("(const %1Params*)prms);%-%2%t}%2%-", pre, nl);
            
            glob.genTitleD(pre + "Instance_finalize");
            glob.out.printf("inline void Instance_finalize( Object* obj );\n");
            glob.out.printf("#define DEFINE__Instance_finalize%1%+", nl);
            glob.out.printf("%tvoid %1Instance_finalize( %1Object* obj )%2%t{%2%+", glob.cname, nl);
            glob.out.printf("%t%1Instance_finalize((%1Object*)obj);%-%2%t}%2%-", pre, nl);
        }
        
        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isMeta() || fxn.isSys() || fxn.hasAttr(Attr.A_Macro)) {
                continue;
            }

            boolean varg = fxn.isVarg();
            String fn = fxn.getName() + (varg ? "_va" : "");
            
            glob.genTitleD(pre + fn);
            glob.out.printf("inline ");
            glob.genType(fxn.getType(), Glob.Type$TYP, fn);
            glob.out.printf("( ");
            if (fxn.isInst()) {
                glob.out.printf("Object* __obj");
                sp = ", ";
            }
            else {
                sp = "";
            }
            glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, sp);
            glob.out.printf("%1 );\n", varg ? ", VaList __va" : "");

            glob.mode = Glob.CDLMODE;
            glob.out.printf("#define DEFINE__%1%2%+", fn, nl);
            glob.out.printf("%t");
            glob.genType(fxn.getType(), Glob.Type$TYP, glob.cname + fn);
            glob.out.printf("( ");
            if (fxn.isInst()) {
                glob.out.printf("%1Object* __obj", glob.cname);
                sp = ", ";
            }
            else {
                sp = "";
            }
            glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, sp);
            glob.out.printf("%1 )%2%t{%2%+", varg ? ", VaList __va" : "", nl);
            glob.out.printf("%t%1%2%3(", !glob.isVoid(fxn) ? "return " : "", pre, fn);
            if (fxn.isInst()) {
                glob.out.printf("(%1Object*)__obj", pre);
                sp = ", ";
            }
            else {
                sp = "";
            }
            glob.genArgNames(fxn.getArgs(), sp);
            glob.out.printf("%1);%-%2%t}%2%-", varg ? ", __va" : "", nl);
            glob.mode = Glob.CPPMODE;
        }
        
        skip();
        glob.out.printf("}\n");
        glob.out.printf("#endif /* xdc__plusplus */\n");
        
        glob.mode = oldmode;
    }
    
    // genModCfgs
    private void genModCfgs( Unit unit )
    {
        for (Decl.Config cfg : unit.getConfigs()) {
            if (!cfg.isStatic() || cfg.isMeta() || cfg.isSys()) {
                continue;
            }
            addDef(cfg.getName());
            glob.genTitleD(curpre + cfg.getName());
            glob.out.printf("static const CT__%1%2& %2 = %1%2__C;\n", glob.cname, cfg.getName());
        }
    }
    
    // genModFxns
    private void genModFxns( Unit unit )
    {
        for (Decl.Fxn fxn : unit.getFxns()) {
            if (!fxn.isStatic() || fxn.isMeta() || fxn.isSys() || fxn.isInternal()) {
                continue;
            }
            if (fxn.hasAttr(Attr.A_Macro)) {
                continue;
            }
            
            boolean varg = fxn.isVarg();
            boolean ret = !(glob.isVoid(fxn));
            
            glob.genTitleD(curpre + fxn.getName());
            glob.out.printf("static %1 ", !varg ? "inline" : "");
            glob.genType(fxn.getType(), Glob.Type$TYP, fxn.getName());
            glob.out.printf("( ");
            glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, "");
            glob.out.printf("%1 )\n{\n%+", fxn.isVarg() ? " ..." : "");
            if (varg) {
                if (ret) {
                    glob.out.tab();
                    glob.genType(fxn.getType(), Glob.Type$TYP, "__ret;\n");
                }
                Decl.Arg lastArg = fxn.getArgs().get(fxn.getArgs().size() - 1);
                glob.out.printf("%tva_list __va; va_start(__va, %1);\n", lastArg.getName());
                glob.out.printf("%t%1%2%3_va__E(", ret ? "__ret = " : "", glob.cname, fxn.getName());
            }
            else {
                glob.out.printf("%t%1%2%3(", ret ? "return " : "", glob.cname, fxn.getName());
            }
            glob.genArgNames(fxn.getArgs(), "");
            if (varg) {
                glob.out.printf(", __va);\n%tva_end(__va);\n");
                if (ret) {
                    glob.out.printf("%treturn __ret;\n");
                }
                glob.out.printf("}\n%-");
            }
            else {
                glob.out.printf(");\n}\n%-");
            }
        }
    }
    
    // genModSingle
    private void genModSingle( Unit unit )
    {
        glob.genTitleD(curpre + "Module");
        glob.out.printf("class Module {\n%+");
        glob.out.printf("%t%1Module __mod;\n", glob.cname);
        glob.out.printf("public:\n");
        if (unit.isMod()) {
            glob.out.printf("%tModule() { __mod = &%1Module__FXNS__C; }\n", glob.cname);
        }
        else {
            glob.out.printf("%tModule() { __mod = NULL; }\n");
            glob.out.printf("%tModule( %1Module m ) { __mod = m; }\n", glob.cname);
            glob.out.printf("%tModule& operator =( %1Module m ) { __mod = m; return *this; }\n", glob.cname);
        }
        glob.out.printf("%toperator %1Module() { return __mod; }\n", glob.cname);
        for (Unit iu : unit.getInherits()) {
            if (iu.getQualName().equals("xdc.runtime.IModule")) {
                break;
            }
            String icn = glob.mkCname(iu.getQualName());
            glob.out.printf("%toperator %1Module() { return (%1Module)__mod; }\n", icn);
            glob.out.printf("%toperator %1::Module&() { return reinterpret_cast<%1::Module&>(*this); }\n", glob.nsName(iu));
        }
        if (unit.isInter()) {
            String pre = curpre + "Module::";

            if (unit.getCreator() != null) {
                glob.genTitleD(curpre + "create");
                glob.out.printf("%tstruct Params;\n");
                glob.out.printf("%tHandle create(");
                glob.genCreArgDecls(unit, Glob.Type$LCL, " ");
                glob.out.printf("const Params* __prms=NULL, %1=NULL )\n%t{\n%+", Glob.ERRARG);
                glob.out.printf("%treturn (Handle)%1create(__mod", glob.cname);
                glob.genCreArgNames(unit, ", ");
                glob.out.printf("(%1Params*)__prms, __eb);\n%-%t}\n", glob.cname);
            }

            for (Decl.Fxn fxn : unit.getFxns()) {
                if (fxn.isInst() || fxn.isMeta() || fxn.isSys() || fxn.isInternal()) {
                    continue;
                }
                if (fxn.hasAttr(Attr.A_Macro)) {
                    continue;
                }
                glob.genTitleD(pre + fxn.getName());
                glob.out.tab();
                glob.genType(fxn.getType(), Glob.Type$TYP, fxn.getName());
                glob.out.printf("( ");
                glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, "");
                glob.out.printf(" )\n%t{\n%+");
                glob.out.printf("%t%1%2%3(__mod", !glob.isVoid(fxn) ? "return " : "", glob.cname, fxn.getName());
                glob.genArgNames(fxn.getArgs(), ", ");
                glob.out.printf(");\n%-%t}\n");
            }
        }
        glob.out.printf("%-};\n");
    }
    
    // genProxies
    private void genProxies( Unit unit )
    {
        String oldpre = curpre;
        int k = unit.getName().length();
        isProxy = true;
        
        for (Unit pu : unit.getProxies()) {

            glob.setNames(pu);
            String pname = pu.getName().substring(k + 1);

            prxs.add(pname);
            glob.genTitleD(curpre + pname);
            glob.out.printf("namespace %1 {\n", pname);
            skip();

            curpre = oldpre + pname + "::";

            genAuxDefs(pu);
            genSysDefs(pu);
            genModFxns(pu);

            skip();
            glob.out.printf("}\n");
        }
        
        curpre = oldpre;
        isProxy = false;
        glob.setNames(unit);
    }
    
    // genPublics
    void genPublics( Unit unit )
    {
        if (!this.enabled) {
            return;
        }
     
        if (unit.isProxy()) {
            return;
        }
        
        int oldmode = glob.mode;
        glob.mode = Glob.CPPMODE;
        
        curpre = unit.getName() + "::";
        
//      glob.out.printf("#if defined(xdc__plusplus) && defined(%1_cplusplus)\n", glob.cname);
        glob.out.printf("#if defined(xdc__plusplus)\n");
        glob.out.printf("%1 {\n", glob.nsBeg(unit));
        skip();
        
        if (unit.isMod()) {
            genProxies(unit);
        }
        genAuxDefs(unit);
        genSysDefs(unit);
        if (unit.isMod()) {
            genModCfgs(unit);
            genModFxns(unit);
        }
        if (unit.isHeir() || unit.isInter()) {
            genModSingle(unit);
        }
        if (unit.isInst()) {
            genInstCfgs(unit);
            genInstFxns(unit);
        }
        genSysFxns(unit);
                
        skip();
        glob.out.printf("%1\n", glob.nsEnd());
        glob.out.printf("#endif /* xdc__plusplus */\n");
        
        glob.mode = oldmode;
    }
    
    // genState
    void genState( Unit unit )
    {
        if (!this.enabled) {
            return;
        }
     
        int oldmode = glob.mode;
        glob.mode = Glob.CPPMODE;
        
        glob.genTitleD("C++ wrapper");
//        glob.out.printf("#if defined(xdc__plusplus) && defined(%1_cplusplus)\n", glob.cname);
        glob.out.printf("#if defined(xdc__plusplus)\n");
        glob.out.printf("%1 {\n", glob.nsBeg(unit));
        skip();
        
        if (unit.getSession().findDecl(unit, "Module_State") != null) {
            addDef("Module_State");
            glob.genTitleD(curpre + "Module_State");
            glob.out.printf("struct Module_State {\n%+", glob.cname);
            glob.genStateFlds(unit, false, false, true);
            glob.out.printf("%-};\n");
            addDef("module");
            glob.genTitleD(curpre + "module");
            glob.out.printf("static Module_State* const module = (Module_State*)&%1Module__state__V;\n", glob.cname);
        }

        if (unit.isInst()) {
            addDef("Object");
            glob.genTitleD(curpre + "Object");
            glob.out.printf("struct Object {\n%+", glob.cname);
            glob.genStateFlds(unit, true, false, true);
            glob.out.printf("%-};\n");
        }
        
        skip();
        glob.out.printf("%1\n", glob.nsEnd());
        glob.out.printf("#endif /* xdc__plusplus */\n");

        glob.mode = oldmode;
    }

    // genSysDefs
    private void genSysDefs( Unit unit )
    {
        if (!unit.isInst()) {
            return;
        }
        
        if (unit.isProxy()) {
            glob.genTitleD(curpre + "Handle");
//          glob.out.printf("typedef %1::Handle Handle;\n", glob.nsName(unit.getSuper().getQualName()));
            glob.out.printf("typedef %1Handle Handle;\n", glob.mkCname(unit.getSuper().getQualName()));
            return;
        }
        
        glob.genTitleD(curpre + "Handle");
        glob.out.printf("struct Instance;\n");
        if (unit.isMod()) {
            glob.out.printf("struct Object;\n");
            glob.out.printf("struct Struct;\n");
        }
        glob.out.printf("class Handle {\n%+");
        glob.out.printf("%tInstance* inst;\n");
        glob.out.printf("%tvoid* operator new( size_t ) { return (void*)-1; }\n");
        glob.out.printf("public:\n");
        glob.out.printf("%tHandle() { this->inst = NULL; }\n");
        glob.out.printf("%tHandle( %1Handle const inst ) { this->inst = (Instance*)inst; }\n", glob.cname);
        glob.out.printf("%tHandle& operator=( %1Handle const inst ) { this->inst = (Instance*)inst; return *this; }\n", glob.cname);
        glob.out.printf("%tHandle( Instance* const inst ) { this->inst = inst; }\n");
        glob.out.printf("%tHandle& operator=( Instance* const inst ) { this->inst = inst; return *this; }\n");
        if (unit.isMod()) {
            glob.out.printf("%tHandle( Struct* const str ) { this->inst = (Instance*)str; }\n");
            glob.out.printf("%tHandle& operator=( Struct* const str ) { this->inst = (Instance*)str; return *this; }\n");
        }
        for (Unit iu : unit.getInherits()) {
            if (iu.getQualName().equals("xdc.runtime.IModule") || iu.isStatic()) {
                break;
            }
            glob.out.printf("%toperator %1::Handle&() { return reinterpret_cast<%1::Handle&>(*this); }\n", glob.nsName(iu));
        }
        glob.out.printf("%toperator %1Handle*() { return (%1Handle*)(this->inst); }\n", glob.cname);
        glob.out.printf("%toperator xdc_IArg() { return (xdc_IArg)(this->inst); }\n");
        glob.out.printf("%tInstance* operator->() { return inst; }\n");
        glob.out.printf("%tvoid operator delete( void* obj ) { %1delete((%1Handle*)obj); }\n", glob.cname);
        glob.out.printf("%-};\n");
    }
    
    // genSysFxns
    private void genSysFxns( Unit unit )
    {
        if (!unit.isInst()) {
            return;
        }
        
        if (unit.isMod()) {
            glob.genTitleD(curpre + "create");
            glob.out.printf("static inline Instance* create(");
            glob.genCreArgDecls(unit, Glob.Type$LCL, " ");
            glob.out.printf("const Params* __prms=NULL, %1=NULL )\n{\n%+", Glob.ERRARG);
            glob.out.printf("%treturn (Instance*)%1create(", glob.cname);
            glob.genCreArgNames(unit, "");
            glob.out.printf("(%1Params*)__prms, __eb);\n%-}\n", glob.cname);
        }
        
        if (unit.isMod()) {
            glob.genTitleD(curpre + "construct");
            glob.out.printf("static inline void construct( Struct* __obj");
            glob.genCreArgDecls(unit, Glob.Type$LCL, ", ");
            glob.out.printf("const Params* __prms=NULL )\n{\n%+");
            glob.out.printf("%t%1construct((%1Struct*)__obj", glob.cname);
            glob.genCreArgNames(unit, ", ");
            glob.out.printf("(%1Params*)__prms);\n%-}\n", glob.cname);
        }

        glob.genTitleD(curpre + "Instance_delete");
        glob.out.printf("static inline void Instance_delete( Handle* instp ) { %1delete((%1Handle*)instp); }\n", glob.cname);

        if (unit.isMod()) {
            glob.genTitleD(curpre + "destruct");
            glob.out.printf("static inline void destruct( Struct* strp ) { %1destruct((%1Struct*)strp); }\n", glob.cname);
        }

        if (unit.isInter()) {
            return;
        }
        
        addDef("Params_init");
        glob.genTitleD(curpre + "Params_init");
        glob.out.printf("static inline void Params_init( Params* prms ) { %1Params_init((%1Params*)prms); }\n", glob.cname);
        
        addDef("Object_count");
        glob.genTitleD(curpre + "Object_count");
        glob.out.printf("static inline int Object_count() { return %1Object_count(); }\n", glob.cname);

        addDef("Object_get");
        glob.genTitleD(curpre + "Object_get");
        glob.out.printf("static inline Object* Object_get( int i ) { return (Object*)%1Object_get(NULL, i); }\n", glob.cname);
        glob.out.printf("static inline Object* Object_get( Object oa[], int i )");
        glob.out.printf("{ return (Object*)%1Object_get((%1Instance_State*)oa, i); }\n", glob.cname);
    }
    
    // isEnabled
    boolean isEnabled()
    {
        return (this.enabled);
    }
     
    // addDef
    private void addDef( String def )
    {
        if (!isProxy) {
            defs.add(def);
        }
    }
    
    // skip
    private void skip()
    {
        glob.out.print("\n");
    }
}
