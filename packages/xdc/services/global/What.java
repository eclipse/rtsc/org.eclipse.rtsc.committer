/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== What.java ========
 *  This helper class extracts an SCCS "what" string from a file.
 */
package xdc.services.global;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

/*
 *  ======== What ========
 */
public class What {

    static private final int MAXNULL = 3;
    
    /*
     *  ======== getWhatString ========
     */
    public static String getWhatString(String fileName)
        throws java.io.IOException
    {
        return (getWhatString(new File(fileName)));
    }
    
    public static String getWhatString(File file)
        throws java.io.IOException
    {
        BufferedReader src = new BufferedReader(new FileReader(file));
        StringBuffer result = new StringBuffer(80);
        int nullCount = 0;
        int cur;
        char ch; 
        int state;
        
        /* infinite loop - nextCh() will exit() */
        for (cur = src.read(), state = 0; cur >= 0;) {
    
            ch = (char)cur;

            switch (state) {
    
                /* initial state */
                case 0 : {
                    state = (ch == '@') ? 1 : 0;
                    cur = src.read();

                    /* compute null padding (for non-byte addressable ISAs) */
                    nullCount = 0;
                    for (int i = 0; (char)cur == '\0' && i < MAXNULL; i++) {
                        nullCount++;
                        cur = src.read();
                    }

                    break;
                }
    
                /* previous character was a '@' */
                case 1 : {
                    if (ch == '(') {
                        state = 2;	    /* proceed to next state */
                        cur = nextChar(src, nullCount);
                    }
                    else {
                        state = 0;	    /* return to initial state */
                    }
                    break;
                }
    
                /* previous two characters we "@(" */
                case 2 : {
                    if (ch == '#') {
                        state = 3;	    /* proceed to next state */
                        cur = nextChar(src, nullCount);
                    }
                    else {
                        state = 0;	    /* return to initial state */
                    }
                    break;
                }
    
                /* previous three characters we "@(#" */
                case 3 : {
                    if (ch == ')') {
                        result.append('\t');
                        state = 4;	    /* proceed to next state */
                        cur = nextChar(src, nullCount);
                    }
                    else {
                        state = 0;	    /* return to initial state */
                    }
                    break;
                }
    
                /* previous characters were "@(#)", print version string! */
                case 4 : {
                    /* if we reach the end of the version string return
                     * to the initial state.
                     */
                    if (ch == '\000' || ch == '\n' || ch == '"') {
                        result.append('\n');
                        state = 0;	    /* return to initial state */
                        cur = src.read();
                        break;
                    }
    
                    /* Otherwise, output next character in version string
                     * and stay in this state.
                     */
                    result.append(ch);
                    cur = nextChar(src, nullCount);
                    break;
                }
    
                /* we should never get here! */
                default : {
                    cur = -1;   /* terminate the loop */
                    break;
                }
            }
        }

        return (result.toString());
    }
    
    private static int nextChar(BufferedReader src, int nbytes)
        throws java.io.IOException
    {
        for (int i = 0; i < nbytes; i++) {
            src.read();
        }

        return (src.read());
    }

}
