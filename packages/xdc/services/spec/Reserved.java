/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

import java.util.*;

public class Reserved {
    
    static private HashSet kwdset = null;

    // init
    static private void init()
    {
        kwdset = new HashSet();
        
        // current javascript keywords
        kwdset.add("break");
        kwdset.add("case");
        kwdset.add("catch");
        kwdset.add("continue");
        kwdset.add("default");
        kwdset.add("delete");
        kwdset.add("do");
        kwdset.add("else");
        kwdset.add("false");
        kwdset.add("finally");
        kwdset.add("for");
        kwdset.add("function");
        kwdset.add("if");
        kwdset.add("in");
        kwdset.add("instanceof");
        kwdset.add("new");
        kwdset.add("null");
        kwdset.add("return");
        kwdset.add("switch");
        kwdset.add("this");
        kwdset.add("throw");
        kwdset.add("true");
        kwdset.add("try");
        kwdset.add("typeof");
        kwdset.add("var");
        kwdset.add("void");
        kwdset.add("while");
        kwdset.add("with");
        
        // future javascript keywords
        kwdset.add("abstract");
        kwdset.add("boolean");
        kwdset.add("byte");
        kwdset.add("char");
        kwdset.add("class");
        kwdset.add("const");
        kwdset.add("debugger");
        kwdset.add("double");
        kwdset.add("enum");
        kwdset.add("export");
        kwdset.add("extends");
        kwdset.add("final");
        kwdset.add("float");
        kwdset.add("goto");
        kwdset.add("implements");
        kwdset.add("import");
        kwdset.add("int");
        kwdset.add("interface");
        kwdset.add("long");
        kwdset.add("native");
        kwdset.add("package");
        kwdset.add("private");
        kwdset.add("protected");
        kwdset.add("public");
        kwdset.add("short");
        kwdset.add("static");
        kwdset.add("super");
        kwdset.add("synchronized");
        kwdset.add("throws");
        kwdset.add("transient");
        kwdset.add("volatile");
    }
    
    // lookupIdent
    static boolean lookupIdent( String id )
    {
        if (kwdset == null) {
            init();
        }
        return kwdset.contains(id);
    }
    
    // lookupPath
    static String lookupPath( String path )
    {
        String[] ids = path.split("\\.");
        for (int i = 0; i < ids.length; i++) {
            if (lookupIdent(ids[i])) {
                return ids[i];
            }
        }
        
        return null;
    }
}
