/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== Body.java ========
 *
 *! Revision History
 *! ================
 *! 13-Feb-2008 sasha	changed the names of the members of Types_Label
 *! 04-Jan-2008 sasha	added __FAR__ and a module name to '__dummy__'
 */

package xdc.services.intern.gen;

import xdc.services.global.*;
import java.io.*;
import java.util.*;
import xdc.services.spec.*;

public class Body
{
    private Glob glob = new Glob();
    
    // gen
    public void gen( Pkg pkg, Out out )
    {
        glob.out = out;
        glob.genWarning();
        out.printf("\n");
        
        String pn = pkg.getName().replace('.', '_');

        out.printf("#include <xdc/std.h>\n");
        out.printf("\n");
        out.printf("__FAR__ char %1__dummy__;\n", pn);
        out.printf("\n");
        out.printf("#define __xdc_PKGVERS %1\n", pkg.getKey());
        out.printf("#define __xdc_PKGNAME %1\n", pkg.getName());
        out.printf("#define __xdc_PKGPREFIX %1_\n", pn);
        out.printf("\n");
        out.printf("#ifdef __xdc_bld_pkg_c__\n");
        out.printf("#define __stringify(a) #a\n");
        out.printf("#define __local_include(a) __stringify(a)\n");
        out.printf("#include __local_include(__xdc_bld_pkg_c__)\n");
        out.printf("#endif\n\n");
    }
}
