/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.tools;

public class ServerSocket
{
    Object inMsg = null;
    Object outMsg = null;
    
    boolean clientDone = false;
    boolean serverDone = false;
    
    // giveToClient
    synchronized public void giveToClient( Object msg )
    {
        this.outMsg = msg;
        this.serverDone = msg == null;
        this.notify();
        
        while (!this.clientDone && this.outMsg != null) {
            try {
                this.wait();
            }
            catch (Exception e) {}
        }
    }
    
    // takeFromClient
    synchronized public Object takeFromClient()
    {
        while (!this.clientDone && this.inMsg == null) {
            try {
                this.wait();
            }
            catch (Exception e) {}
        }
        
        Object res = this.inMsg;
        this.inMsg = null;
        this.notify();
        
        return res;
    }
    
    // giveToServer
    synchronized public void giveToServer( Object msg )
    {
        this.inMsg = msg;
        this.clientDone = msg == null;
        this.notify();

        while (!this.serverDone && this.inMsg != null) {
            try {
                this.wait();
            }
            catch (Exception e) {}
        }
    }
    
    // takeFromServer
    synchronized public Object takeFromServer()
    {
       while (!this.serverDone && this.outMsg == null) {
            try {
                this.wait();
            }
            catch (Exception e) {}
        }
        
        Object res = this.outMsg;
        this.outMsg = null;
        this.notify();
        
        return res;
    }
    
    // isClientDone
    public boolean isClientDone() {
        return (this.clientDone);
    }
    
    // isServerDone
    public boolean isServerDone() {
        return (this.serverDone || !Server.socketMap.containsValue(this));
    }

}
