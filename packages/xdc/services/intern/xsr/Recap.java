/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;
import xdc.services.global.*;
import java.io.*;
import java.util.*;

public class Recap
{
    static final String TAB = "    ";
    
    static Out out;
    static Hashtable<Object,Integer> otab = new Hashtable();
    static Vector<Scriptable> oarr = new Vector();
    static Scriptable xdcobj;
    static int cnt = 0;
    
    static public void gen( Scriptable obj, String fn, Scriptable xdcobj )
    {
        Recap.xdcobj = xdcobj;
        
        try {
            out = new Out(new BufferedOutputStream(new FileOutputStream(fn)));

            out.print("__o = null\n");
            out.print("__obj = [\n");
            scan(obj);
            out.print("]\n");

            for (int i = 0; i < oarr.size(); i++) {
                init(oarr.get(i), i);
            }

            out.print("\ndelete __o\n");
            out.print("delete __obj");
            
            out.close();
        }
        catch (Exception e) {
            xdc.services.intern.xsr.Err.exit(e);
        }
    }
    
    static void assign( String sel, Object val, boolean isMap )
    {
        String vs;

        if (val instanceof Function) {
            return;
        }

        if (val instanceof Value.Obj) {
            val = ((Value.Obj)val).getOrig();
        }

        if (val == null) {
            vs = "null";
        }
        else if (val instanceof Undefined) {
            vs = "undefined";
        }
        else if (val == Recap.xdcobj.get("global", null)) {
            vs = "xdc.global";
        }
        else if (val instanceof String) {
            String s = null;
            try {
                s = java.net.URLEncoder.encode((String)val, "UTF-8");
            }
            catch (Exception e) {
                Err.exit(e);
            }
            vs = "String(java.net.URLDecoder.decode('" + s + "', 'UTF-8'))";
        }
        else if (val instanceof Number) {
            Number num = (Number)val;
            long l = num.longValue();
            double d = num.doubleValue();
            vs = (l == d) ? Long.toString(l) : Double.toString(d);
        }
        else if (!(val instanceof Scriptable)) {
            vs = val.toString();
        }
        else if (val instanceof XScriptO && !(val instanceof Value)) {
            String s = null;
            try {
                s = java.net.URLEncoder.encode((String)((XScriptO)val).getDefaultValue(String.class), "UTF-8");
            }
            catch (Exception e) {
                Err.exit(e);
            }
            vs = "String(java.net.URLDecoder.decode('" + s + "', 'UTF-8'))";
        }
        else if (val instanceof Value.Obj && ((Value.Obj)val).has("delegate$", null)) {
            Value delmod = ((Value.Obj)val).getv("delegate$");
            if (delmod == null) {
                vs = "null";
            }
            else {
                Integer idx = otab.get(((Value.Obj)delmod).getOrig());
                String suf = idx == null ? "" : ".0";
                vs = "__obj[" + idx + suf + "]";
            }
        }
        else {
            Integer idx = otab.get(val);
            String suf = idx == null ? "" : ".0";
            vs = "__obj[" + idx + suf + "]";
        }
        
        String pre = isMap ? "__o.push(" : "";
        String suf = isMap ? "); __o.$keys.push('" + sel + "')" : "";

        out.printf("%1%2__o['%3'] = %4\n", TAB, pre, sel, vs + suf);
        
    }
    
    static Object getSel( Scriptable obj, String sel )
    {
        Object val = obj.get(sel, null);
        if (val == UniqueTag.NOT_FOUND) {
            try {
                val = obj.get(Integer.parseInt(sel), null);
            }
            catch (Exception e) {
            }
        }
        
        return val;
    }
    
    static void init( Scriptable obj, int idx )
    {
        if (obj instanceof Value.Obj) {
            obj = ((Value.Obj)obj).getOrig();
        }

        boolean isMap = obj instanceof Value.Map;
        int dim = -1;
        
        if (obj instanceof NativeArray) {
            dim = (int)((NativeArray)obj).getLength();
        }
        else if (obj instanceof Value.Arr) {
            dim = ((Value.Arr)obj).curlen;
        }

        String ns = obj.has("$name", null) ? "// " + (String)obj.get("$name", null) : "";
        out.printf("\n__o = __obj[%1]  %2\n", idx, ns);
        
        if (isMap) {
            out.printf("%s__o.$keys = []\n", TAB);
        }

        if (dim == -1) {
            for (String sel : selList(obj)) {
                assign(sel, getSel(obj, sel), isMap);
            }
        }
        else {
            if (obj instanceof Value.Arr) {
                Value.Arr varr = (Value.Arr)obj;
                assign("$category", "Vector", isMap);
                assign("$name", varr.getName(), isMap);
            }
            for (int i = 0; i < dim; i++) {
                assign(Integer.toString(i), obj.get(i, null), isMap);
            }
        }

        if (isMap) {
            XScriptO xobj = (XScriptO)obj;
            assign("$category", xobj.gets("$category"), false);
            assign("$name", xobj.getName(), false);
        }
    }
    
    static void scan( Scriptable obj )
    {
        if (obj instanceof Function) {
            return;
        }

        if (obj instanceof Value.Obj) {
            Value.Obj vobj = ((Value.Obj)obj).getOrig();
            if (vobj.has("PROXY$", null) && vobj.geti("PROXY$") == 1) {
                Value dobj = vobj.getv("delegate$");
                if (dobj != null) {
                    vobj = ((Value.Obj)dobj).getOrig();
                }
                else {
                    return;
                }
            }
            obj = vobj;
        }

        if (otab.get(obj) != null) {
            return;
        }

        if (obj == Recap.xdcobj.get("global", null)) {
            return;
        }

        String cons;
        int dim = -1;

        if (cnt == 0) {
            cons = "this,";
        }
        else if (obj instanceof NativeArray) {
            cons = "[],  ";
            dim = (int)((NativeArray)obj).getLength();
        }
        else if (!(obj instanceof XScriptO) || obj instanceof Value.Obj) {
            cons = "{},  ";
        }
        else if (obj instanceof Value.Arr) {
            cons = "[],  ";
            dim = ((Value.Arr)obj).curlen;
        }
        else if (obj instanceof Value.Map) {
            cons = "[],  ";
        }
        else {
            return;
        }
        
        String ns = obj.has("$name", null) ? (String)obj.get("$name", null) : "";
        out.printf("%1%2  // #%3 %4\n", TAB, cons, cnt, ns);
        
        otab.put(obj, cnt++);
        oarr.add(obj);

        if (dim == -1) {
            for (String sel : selList(obj)) {
                Object val = getSel(obj, sel);
                if (val == UniqueTag.NOT_FOUND) {
                    try {
                        val = obj.get(Integer.parseInt(sel), null);
                    }
                    catch (Exception e) {
                    }
                }
                if (val instanceof Scriptable) {
                    scan((Scriptable)val);
                }
            }
        }
        else {
            for (int i = 0; i < dim; i++) {
                Object val = obj.get(i, null);
                if (val instanceof Scriptable) {
                    scan((Scriptable)val);
                }
            }
        }
    }
    
    static String[] selList( Scriptable obj )
    {
        if (obj instanceof Value.Map) {
            String[] sA = {};
            sA = ((Value.Map)obj).keyvec.toArray(sA);
            return sA;
        }

        String AVOID = "$private|prototype|constructor";
        ArrayList<String> sL = new ArrayList();

        for (Object sel: obj.getIds()) {
            String ss = sel.toString();
            if (ss.matches(AVOID)) {
                continue;
            }
            sL.add(ss);
        }
        
        if (obj instanceof Value) {
            sL.add("$category");
            sL.add("$name");
        }
        
        if (obj instanceof Value.Obj) {
            if (obj == Recap.xdcobj.get("om", null)) {
                sL.add("$packages");
                sL.add("$modules");
            }
            String cat = ((Value.Obj)obj).gets("$category");
            if (cat.equals("Package")) {
                sL.add("$vers");
                sL.add("$modules");
            }
            else if (cat.equals("Module")) {
                sL.add("$instances");
            }
            else if (cat.equals("Instance")) {
                sL.add("$module");
            }
        }
        
        String[] sA = {};
        sA = sL.toArray(sA);
        Arrays.sort(sA);
        return sA;
        
    }
}
