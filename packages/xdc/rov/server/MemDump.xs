/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
//var ofd6x = "/db/toolsrc/library/tools/vendors/ti/c6x/6.0.7/Linux/bin/ofd6x"
//var executable = "/db/ztree/cmcc/avala-g06x/src/ti/sysbios/knl/tests/whole_program/TaskTest1.x64";

var ofd6x = "C:\\CCStudio_v3.3\\c6000\\cgtools\\bin\\ofd6x.exe";
//var executable = "Z:\\cmcc\\avala-g06x\\src\\ti\\sysbios\\knl\\tests\\whole_program\\TaskTest1.x64";

generateDumpFile(ofd6x, arguments[0]);

/*
 *  ======== generateDumpFile ========
 *
 */
function generateDumpFile(ofd, exec)
{

    /* Run ofd on the executable to get the section info. */
    var sections = getSections(ofd, exec);
    
    /* Print the section information */
    print("======== UDATA and DATA Sections ========");
    for (var i = 0; i < sections.length; i++) {
        for (p in sections[i]) {
            print(p + " = " + sections[i][p]);
        }
        print("");
    }
    
    // Wait here for user to ok continue...
    print("Please have CCS open with the program loaded and\n" + 
          "execution halted at the desired location for analysis.");
    print("\nPress any key to continue or Ctrl C to quit");
    
    java.lang.System["in"].read();
    
    print("Reading target memory...");
    
    /* Save off the memory sections into coff files */
    saveSections(sections);
    
    /* Run ofd to merge the coff files into one XML dump file */
    mergeCoffs(ofd, exec, sections);
    
    return(exec + "_dump.xml");   
}
    
/*
 *  ======= getSections ========
 *  This function runs ofd on the given executable and extracts the DATA and
 *  UDATA sections.
 */
function getSections(ofdPath, executable)
{
    var ofdFile = new xdc.jre.java.io.File(ofdPath);
    var execFile = new xdc.jre.java.io.File(executable);
    if (!ofdFile.exists()) { 
        print("OFD file does not exist!!!");
    }
    if (!execFile.exists()) {
        print("Exe file does not exist!!!");
    }
    
    
    var ofdproc = xdc.jre.java.lang.Runtime.getRuntime().exec(ofdPath + " -x " + executable);
    
    // Code from xdc.tci
    var br = new xdc.jre.java.io.BufferedReader(new xdc.jre.java.io.InputStreamReader(ofdproc.getInputStream()));

    var sb = new java.lang.StringBuilder();

    var line = "";
    while ((line = br.readLine()) != null) {
        sb.append(line);
    }

    /*  
     * Parse the ofd xml output.
     * 
     * A typical xml section element:
     * <section>
     *    <name>.far</name>
     *    <physical_addr>0x80001998</physical_addr>
     *    <virtual_addr>0x80001998</virtual_addr>
     *    <raw_data_size>0x1b30</raw_data_size>
     *    <reloc_count>0x0</reloc_count>
     *    <line_count>0x0</line_count>
     *    <addr_size>0x1</addr_size>
     *    <alignment>0x8</alignment>
     *    <bss>true</bss>
     *    <file_offsets>
     *       <raw_data_ptr>0x0</raw_data_ptr>
     *       <reloc_ptr>0x0</reloc_ptr>
     *        <line_ptr>0x0</line_ptr>
     *    </file_offsets>
     * </section>
     *
     */
       
    var XMLobject = new XML(sb.toString());
    var sectElems = XMLobject.object_file.ti_coff.section;

    /* 
     * Read all of the sections into javascript objects.
     */
    var sections = new Array();
    
    for (var i in sectElems) {
        var section = new Object();
        
        /* Store all of the udata and data sections with size > 0. */
        if (((sectElems[i].bss == "true") || (sectElems[i].data == "true"))
            && (Number(sectElems[i].raw_data_size) > 0)) {
            section.name = sectElems[i].name;
            section.physical_addr = sectElems[i].physical_addr;
            /* 
             * The raw data size shown by ofd is in bytes. GEL_MemorySave takes
             * sizes in words. Convert to int, divide by 4 (4 bytes per word), 
             * round up in case it's an odd number of bytes, and convert back
             * to hex string. 
             */
            //DEBUG - C28_large
            //var sizeMAUs = Math.ceil(Number(sectElems[i].raw_data_size));
            var sizeMAUs = Math.ceil(Number(sectElems[i].raw_data_size) / 4);
            section.raw_data_size = "0x" + sizeMAUs.toString(16);
            sections[sections.length] = section;
        }
    }
    
    return(sections);  
}


/*
 *  ======== saveSections ========
 *  
 */
function saveSections(sections) 
{
    /* Generate the CCScript which will make the calls to GEL_MemorySave. */
    generateMemSaveScript(sections);
    
    /* Run the MemorySave script using a ProcessBuilder */
    //var cmd = new xdc.jre.java.util.List;
    //cmd[0] = "cscript";
    //cmd[1] = "MemorySave.js";
    
    var procB = xdc.jre.java.lang.ProcessBuilder("cscript", "MemorySave.js");
    var memSaveProc = procB.start();
    
    /* Wait until the script has finished. */
    memSaveProc.waitFor();
    
}

/*
 *  ======== generateMemSaveScript ========
 *
 */
function generateMemSaveScript(sections)
{
    var outputFile = java.io.File("MemorySave.js");
    var fos = java.io.FileWriter(outputFile);
    
    fos.write("var ccs = WScript.CreateObject(\"CCS_Scripting_Com.CCS_Scripting\");\n");
    fos.write("ccs.CCSOpenNamed(\"*\", \"*\", 0);\n");

    for (var i = 0; i < sections.length; i++) {
        /*
        print('GEL_MemorySave(' + sections[i].physical_addr +
              ', 0, ' + sections[i].raw_data_size + ', "' + 
              sections[i].name + '.out", 5) ');
        */
        //DEBUG - C28_large
        //fos.write("ccs.TargetEvalExpression('GEL_MemorySave(" + 
              //sections[i].physical_addr + ", 1, " + 
              //sections[i].raw_data_size + ", \"" + 
              //sections[i].name + ".out\", 5) ');\n");

        fos.write("ccs.TargetEvalExpression('GEL_MemorySave(" + 
              sections[i].physical_addr + ", 0, " + 
              sections[i].raw_data_size + ", \"" + 
              sections[i].name + ".out\", 5) ');\n");
                  
    }
    
    fos.close();

}

/*
 *  ======== mergeCoffs ========
 *
 */
function mergeCoffs(ofdPath, executable, sections)
{
    /* Run ofd to combine the coff files into a single xml dump file. */
    var ofdRunCmd = new xdc.jre.java.util.ArrayList();
    ofdRunCmd.add(ofdPath);
    ofdRunCmd.add("-x");
    ofdRunCmd.add("-o");
    ofdRunCmd.add(executable + "_dump.xml");

    
    for (var i = 0; i < sections.length; i++) {
        ofdRunCmd.add(sections[i].name + ".out");
    }
    
    var procB = xdc.jre.java.lang.ProcessBuilder(ofdRunCmd);
    var ofdProc = procB.start();
    
    /* Wait for ofd to finish before deleting the coff files. */
    ofdProc.waitFor();
    
    /* Delete the coff files */
    for (var i = 0; i < sections.length; i++) {
        var file = java.io.File(sections[i].name + ".out");
        file["delete"]();
    }
}
