/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
#ifndef __STRINGUTILS_H_
#define __STRINGUTILS_H_

#include <afx.h>

namespace StringUtils
{

int split(const CString input, const CString delimiter, CStringArray& results);

void join(const CStringArray& input, const CString delimiter, CString& result);

};

#endif
