/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

import xdc.services.global.Err;

import java.io.Serializable;
import java.util.*;
import java.util.regex.*;

public class XDoc
{
    static private final int            M_TEXT    = 0;
    static private final int            M_CODE    = 1;
    static private final int            M_BLIST   = 2;
    static private final int            M_DLIST   = 3;
    static private final int            M_NLIST   = 4;
    static private final int            M_HTML    = 5;

    static private final int            T_MISC    = 0;
    static private final int            T_SUMMARY = 1;
    static private final int            T_DETAILS = 2;
    static private final int            T_HEADER  = 3;
    static private final int            T_TRAILER = 4;
    static private final int            T_CHILD   = 5;
    static private final int            T_NODOC   = 6;
    static private final int            T_SEE     = 7;

    static private Map<String, Integer> modeTab   = new HashMap();
    static private Map<String, Integer> tagTab    = new HashMap();

    {
        XDoc.modeTab.put("text", M_TEXT);
        XDoc.modeTab.put("code", M_CODE);
        XDoc.modeTab.put("blist", M_BLIST);
        XDoc.modeTab.put("dlist", M_DLIST);
        XDoc.modeTab.put("nlist", M_NLIST);
        XDoc.modeTab.put("html", M_HTML);

        XDoc.tagTab.put("_nodoc", T_NODOC);
        XDoc.tagTab.put("summary", T_SUMMARY);
        XDoc.tagTab.put("details", T_DETAILS);
        XDoc.tagTab.put("b", T_HEADER);
        XDoc.tagTab.put("a", T_TRAILER);
        XDoc.tagTab.put("param", T_CHILD);
        XDoc.tagTab.put("value", T_CHILD);
        XDoc.tagTab.put("field", T_CHILD);
        XDoc.tagTab.put("c", T_CHILD);
        XDoc.tagTab.put("see", T_SEE);
    }

    static private Map<String, String> oneParaTags  = new HashMap();

    {
        XDoc.oneParaTags.put("see", "true");
        XDoc.oneParaTags.put("summary", "true");
    }

    private Map<String, List<String>>   child     = new LinkedHashMap();
    private List<String>                details   = new ArrayList();
    private Map<String, List<String>>   headers   = new LinkedHashMap();
    private boolean                     nodoc     = false;
    private Map<String, List<String>>   others    = new LinkedHashMap();
    private String                      summary   = "";
    private Map<String, List<String>>   trailers  = new LinkedHashMap();

    XDoc( List<DocComment> dcL, XDoc xdi ) { this(dcL, xdi, "", false); }

    /**
     * Constructor with explicit defaults for summary and nodoc. Covers the
     * case where docs are available in the .xdc file but are not being
     * loaded by the BrowserSession. In this case the values will have
     * been persisted in a Node object during a previous session.
     */
    XDoc( List<DocComment> dcL, XDoc xdi, String summary, boolean nodoc )
    {
        setSummary(summary);
        this.nodoc = nodoc;

        if (xdi != null) {
            this.inherit(xdi);
        }

        if (dcL != null) {
            this.parse(dcL);
        }
    }

    private void parse( List<DocComment> dcL )
    {

        DocComment[] dcArr =  new DocComment[0];
        dcArr = dcL.toArray(dcArr);

        /* changed by @fn, @struct, @union, or @typedef */
        XDoc current = this;

        /* state to handle one-paragraph tags (like @see) */
        boolean oneParaTagIsActive = false;
        String previousTag = "details";
        String previousArg = "";
        String currentOneParaTag = "";
        String currentOneParaArg = "";

        int i = 0;
        while (i < dcArr.length) {
            DocComment dc = dcArr[i];
            int parId = dc.getParId();
            if (parId < 0) {
                ++i;
                continue;
            }
            /* collect the paragraph metainformation */
            String mode = dc.getMode();
            if (mode == null) {
                mode = "";
            }
            String tag = dc.getTag();
            if (tag == null) {
                tag = "";
            }
            String arg = dc.getArg();
            if (arg == null) {
                arg = "";
            }

            /* collect all the lines in the paragraph */
            String body = dc.getBody();
            if (body == null) {
                body = "";
            }

            String lines = body + "\n";
            while (++i < dcArr.length) {

                DocComment _dc = dcArr[i];
                int _parId = _dc.getParId();
                String _mode = _dc.getMode();
                if (_mode == null) {
                    _mode = "";
                }
                String _tag = _dc.getTag();
                if (_tag == null) {
                    _tag = "";
                }
                String _arg = _dc.getArg();
                if (_arg == null) {
                    _arg = "";
                }

                /* stop if we've crossed into the next paragraph */
                if (parId != _parId || mode.compareTo(_mode) != 0||
                    tag.compareTo(_tag) != 0 || arg.compareTo(_arg) != 0) {
                    break;
                }
                /* accumulate the line */
                String _body = _dc.getBody();
                if (_body == null) {
                    _body = "";
                }
                lines += _body + "\n";
            }

            /* handle tags that end automatically after one paragraph */
            if (oneParaTags.containsKey(tag)) {
                /* Work around a bug in which tags that end in one paragraph
                 * (like @see) instead remain in effect until the next tag.
                 * Here, the paragraph is known to have such a tag, but it is
                 * not yet known whether this is the first or a subsequent
                 * occurrence of the tag.
                 */
                if (!oneParaTagIsActive ||
                    tag.compareTo(currentOneParaTag) != 0 ||
                    arg.compareTo(currentOneParaArg) != 0) {

                    /* first occurrence of a one-paragraph tag.
                     * It may be first because the previous tag was not a one-
                     * paragraph tag, or because the previous tag was a different
                     * one-paragraph tag.
                     */
                    /* remember which tag we're on to detect a change, and
                     * let it pass through */
                    currentOneParaTag = tag;
                    currentOneParaArg = arg;
                }
                else {
                    /* the second or later occurrence of the same tag */
                    /* restore the tag and arg to before the one-paragraph tag */
                    tag = previousTag;
                    arg = previousArg;
                }

                /* remember that we're in one-paragraph tag mode */
                oneParaTagIsActive = true;
            }
            else {
                /* cancel one-paragraph tag mode */
                oneParaTagIsActive = false;

                /*
                 * remember this tag and arg to return to after the next
                 * one-paragraph tag
                 */
                previousTag = tag;
                previousArg = arg;
            }

            int tid = XDoc.tagTab.containsKey(tag) ? XDoc.tagTab.get(tag) : XDoc.T_MISC;
            int mid = XDoc.modeTab.containsKey(mode) ? XDoc.modeTab.get(mode) : XDoc.M_TEXT;

            switch (mid) {

            case M_CODE:
                lines = "<PRE class=\"Example\">" + XDoc.escape(lines) + "</PRE>";
                break;
            case M_BLIST:
                lines = XDoc.escape(lines);
                lines = XDoc.expand(lines);
                lines = XDoc.replaceMulti(lines, "^\\s*-", "</LI><LI>");
                lines = "<LI>" + lines + "</LI>";
                lines = lines.replaceFirst("^<LI></LI>", "");
                lines = "<UL>" + lines + "</UL>";
                break;
            case M_DLIST:
                lines = XDoc.escape(lines);
                lines = XDoc.expand(lines);
                lines = XDoc.replaceMulti(lines, "^\\s*-(.*)$", "</DD><DT>$1</DT><DD>");
                lines = "<DD>" + lines + "</DD>";
                lines = lines.replaceFirst("^<DD></DD>", "");
                lines = "<DL>" + lines + "</DL>";
                break;
            case M_NLIST:
                lines = XDoc.escape(lines);
                lines = XDoc.expand(lines);
                lines = XDoc.replaceMulti(lines, "^\\s*-", "</LI><LI>");
                lines = "<LI>" + lines + "</LI>";
                lines = lines.replaceFirst("^<LI></LI>", "");
                lines = "<OL>" + lines + "</OL>";
                break;
            case M_HTML:
                break;
            default:
                lines = XDoc.escape(lines);
                if (tag.compareTo("example") != 0) {
                    lines = XDoc.expand(lines);
                }
                break;
            }

            switch (tid) {

            case T_NODOC:
                current.nodoc = true;
                return;
            case T_SUMMARY:
                current.summary = stripSummary(lines);
                break;
            case T_DETAILS:
                current.details.add(lines);
                break;
            case T_HEADER:
                XDoc.addLines(current.headers, arg, lines);
                break;
            case T_TRAILER:
                XDoc.addLines(current.trailers, arg, lines);
                break;
            case T_MISC:
                XDoc.addLines(current.others, tag, lines);
                break;
            case T_CHILD:
                XDoc.addLines(current.child, arg, lines);
                break;
            case T_SEE:
                lines = seeSect(lines);
                XDoc.addSeeLines(current.trailers, tag, lines);
                break;
            default:
                Err.exit("unknown autodoc tag: " + tag);
            }
        }
    }

    // addBeforeSect
    public void addBeforeSect( String key, String lines )
    {
        addLines(this.headers, key, expand(lines));
    }

    // addChild
    public void addChild( String key, String lines )
    {
        addLines(this.child, key, expand(lines));
    }


    // addLines
    static private void addLines( Map<String, List<String>> sect, String key, String lines )
    {
        List<String> l = sect.get(key);
        if (l == null) {
            l = new ArrayList();
            sect.put(key, l);
        }
        l.add(lines.trim());
    }

    // addSect
    static private void addSect( Map<String, List<String>> toSect, Map<String, List<String>> fromSect )
    {
        for (Map.Entry<String,List<String>> me : fromSect.entrySet()) {
            for (String lines : me.getValue()) {
                addLines(toSect, me.getKey(), lines);
            }
        }
    }

    // addSeeLines
    static private void addSeeLines( Map<String, List<String>> sect,
                                     String key, String lines )
    {
        List<String> l = sect.get(key);
        if (l == null) {
            l = new ArrayList();
            sect.put(key, l);
            l.add(lines);
        }
        else {
            String seeLines = l.get(0);
            l.set(0, seeLines + ", " + lines);
        }
    }

    /**
     * Convert the text of a 'see' section into a normal text section that
     * happens to have inline {@link} tags. Once semantic info is available
     * from all packages, we will scrub all text to convert inline links,
     * including these, into HTML links.
     */
    static private String seeSect( String lines )
    {
        String[] lineIn = lines.split("\\n");
        String refOut = "";

        for (int i = 0; i < lineIn.length; i++) {
            String[] refs = lineIn[i].split(",");
            for (int j = 0; j < refs.length; j++) {
                if (refs[j].matches("^\\s*$")) {
                    /* entry is all white space, so skip it */
                    continue;
                }
                if (refOut.length() > 0) {
                    refOut += ", ";
                }

                /* allow HTML markup in @see */
                Pattern reg = Pattern.compile("(<[^>]+>)([^`]*)(<\\/[^>]+>)");

                Matcher matcher = reg.matcher(refs[j]);
                if (matcher.groupCount() == 4) {
                    refOut += matcher.group(1) + "{@link " +
                              matcher.group(2) + "}" + matcher.group(3);
                }
                else {
                    refOut += "{@link " + refs[j] + "}";
                }
            }
        }
        return (refOut);
    }

    // copy
    public XDoc copy()
    {
        return new XDoc(null, this, summary, nodoc);
    }

    // escape
    static private String escape( String lines )
    {
        return lines.replaceAll("\\&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }

    // expand
    static String expand( String lines )
    {
        return (lines.replaceAll("`([^`]+)`", "<tt>$1</tt>"));
    }

    // getDetails
    final public List<String> getDetails()
    {
        return Collections.unmodifiableList(this.details);
    }

    // getChild
    final public Map<String, List<String>> getChild()
    {
        return Collections.unmodifiableMap(this.child);
    }

    // getHeaders
    final public Map<String,List<String>> getHeaders()
    {
        return Collections.unmodifiableMap(this.headers);
    }

    // getOthers
    final public Map<String,List<String>> getOthers()
    {
        return Collections.unmodifiableMap(this.others);
    }

    // getSummary
    final public String getSummary()
    {
        return this.summary;
    }

    // getTrailers
    final public Map<String,List<String>> getTrailers()
    {
        return Collections.unmodifiableMap(this.trailers);
    }

    // inherit
    final public void inherit( XDoc xdi )
    {
        if (xdi.nodoc) {
            this.nodoc = true;
            return;
        }

        if (this.summary.equals("")) {
            this.summary = xdi.summary;
        }

        this.details.addAll(xdi.details);

        addSect(this.headers, xdi.headers);
        addSect(this.others, xdi.others);
        addSect(this.trailers, xdi.trailers);
        addSect(this.child, xdi.child);
    }

    // isNodoc
    final public boolean isNodoc()
    {
        return this.nodoc;
    }

    // pushChild
    public void pushChild( String key, String lines )
    {
        Map<String,List<String>> m = this.child;
        this.child = new LinkedHashMap();
        addLines(this.child, key, expand(lines));
        this.child.putAll(m);
    }

    // pushDetails
    public void pushDetails( String lines )
    {
        List<String> l = this.details;
        this.details = new ArrayList();
        this.details.add(lines);
        this.details.addAll(l);
    }

    // replaceMulti
    static private String replaceMulti( String s, String regex, String replace )
    {
        return Pattern.compile(regex, Pattern.MULTILINE).matcher(s).replaceAll(replace);
    }

    // setSummary
    final public void setSummary( String s )
    {
        this.summary = s == null? "" : s;
    }

    // stripSummary
    final private String stripSummary( String s )
    {
        s = s.trim();
        
        if (s.endsWith(".")) {
            s = s.substring(0, s.length() - 1);
        }
        
        return s;
    }
}
