include $(XDCROOT)/packages/xdc/bld/xdc_java.mak

CLASSES  = $(patsubst %.java,%,$(wildcard *.java))
JARFILE  = java/package.jar

.libraries: Dos.h
$(JARFILE): $(patsubst %,$(CLASSDIR)/%.class,$(CLASSES))

Dos.h: $(JARFILE) $(CLASSDIR)/Dos.class
	@rm -rf $@
	echo "javah -jni xdc.services.io.Dos ..."
	$(JDK)/bin/javah -jni -classpath java/classes -o $@ xdc.services.io.Dos


clean::
	$(RM) $(JARFILE) Dos.h
