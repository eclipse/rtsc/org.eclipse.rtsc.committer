/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.global;
import java.lang.*;
import java.util.*;

/*
 *  ======== XDCException ========
 */
public class XDCException extends Exception 
{
    String id;
    String msg;
    List<XDCExceptionAgent> agents = null;
    
    /*
     *  ======== XDCExceptionLocation ========
     */
    public class XDCExceptionLocation
    {
    	String fileName;
    	int lineNum;
    }
    
    /*
     *  ======== XDCExceptionAgent ========
     */
    public class XDCExceptionAgent
    {
    	String name;
    	XDCExceptionLocation loc;
    }
    
    /*
     *  ======== XDCException ========
     */
    public XDCException(String errId, String errMsg)
    {
        super(errId + ": " + errMsg);
        id = errId;
        msg = errMsg;
        agents = null;
    }

    public XDCException(String errId, String errMsg,
        List<XDCExceptionAgent> errAgents)
    {
        super(errId + ": " + errMsg);
        id = errId;
        msg = errMsg;
        agents = errAgents;
    }

    /*
     *  ======== getLocalizedMessage ========
     */
    public String getLocalizedMessage()
    {
        return new String(id + ": " + msg);
    }

    /*
     *  ======== main ========
     *  Simple unit test
     */
    public static void main(String[] args) {
        try {
            throw new XDCException("xdc.services", "XDC Error message");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
