/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== host.h ========
 *  Map XDC defined symbols to legacy symbols
 */
#if defined(xdc_target__os_Linux) | defined(gnu_targets_Linux86)
#define _LINUX_ 1
#elif  defined(xdc_target__os_Solaris) || defined(gnu_targets_Sparc)
#define _SUN_ 1
#elif  defined(xdc_target__os_Windows)
#define _WINDOWS_ 1
#endif

