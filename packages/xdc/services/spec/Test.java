/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

import java.util.*;

public class Test
{
    Vector<Integer> vec;
    
    enum Color {
        RED, BLUE, GREEN
    };

    static public void talk()
    {
        System.out.printf("hello world\n");
        Decl.Error err = new Decl.Error();
        System.out.println(err.getClass().getCanonicalName());
    }

    public Test()
    {
        this.vec = new Vector<Integer>();
        this.vec.addElement(100);
        this.vec.addElement(200);
        this.vec.addElement(300);
    }

    public void addNums(Integer... vals)
    {
        for (Integer i : vals) {
            this.vec.addElement(i);
        }
    }

    public Vector<Integer> getVec()
    {
        return this.vec;
    }

    public Color getColor()
    {
        return Color.BLUE;
    }
}
