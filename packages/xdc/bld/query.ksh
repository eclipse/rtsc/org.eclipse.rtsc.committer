#!/bin/sh
#

XDCROOT=/home/dr/xdc-linux
for d in `$XDCROOT/bin/xdcpkg $*`; do
    cd $d
    
    $XDCROOT/xdc -ra -q .interfaces > /dev/null 2>&1
    status=$?
    if [ $status -eq 2 ]; then
	echo "can't determine state of $d"
        continue;
    fi

    if [ $status -eq 1 ]; then
        msg="not up to date."
    else
        msg="up to date."
    fi
    echo "$d interfaces are $msg (status = $status)"
done

