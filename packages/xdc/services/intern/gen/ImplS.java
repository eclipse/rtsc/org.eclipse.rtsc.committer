package xdc.services.intern.gen;

import java.util.*;
import xdc.services.global.*;
import xdc.services.spec.*;

public class ImplS {

    private Glob glob = new Glob();

    // gen
    public void gen( Unit unit, Out out )
    {
        glob.setNames(unit);
        glob.mode = Glob.SCHMODE;
        glob.out = out;
        
        glob.genWarning();
        skip();

        genPrologue(unit);
        
        genMetaInit(unit);
       	genModUse(unit);
       	genModStaticInit(unit);
       	if (unit.isInst() && !unit.isMeta()) {
       		genInstStaticInit(unit);
       	}
        
        for (Impl.Body fb : unit.getFxnImpls()) {
        	if (fb.getFxn().isMeta()) {
            	genFxnBody(fb);
        	}
        }
    }

    // genCfgAssign
    private void genCfgAssign( Impl.Assign ca )
    {
        if (ca.isAccum()) {
    		genCfgAssignAccum(ca);
    		return;
    	}
    	
    	Expr.Path lval = ca.getLval();
    	
    	if (lval == null) {
            glob.out.printf("%t");
            genExpr(ca.getRval());
            glob.out.print(";\n");
            return;
        }

    	if (lval instanceof Expr.Select) {
    		Expr.Select es = (Expr.Select)lval;
        	Expr obj = es.getObj();
    		String fn = es.getSel().getText();
        	if (obj == null) {
        		glob.out.printf("%tif (!(%1.$written('%2'))) %1.%2 = ", glob.curUnit.getName(), fn);
        	}
        	else if (fn.equals("length")) {
        		genExpr((Expr)lval);
        		glob.out.printf(" = ");
        	}
        	else {
        		glob.out.printf("%tif (!(");
        		genExpr(obj);
        		glob.out.printf(".$written('%1'))) ", fn);
        		genExpr(obj);
        		glob.out.printf(".%1 = ", fn);
        	}
    	}
    	else if (lval instanceof Expr.Index) {
            glob.out.printf("%t");
    		genExpr((Expr.Index)lval);
    		glob.out.print(" = ");
    	}

    	genExpr(ca.getRval());
        glob.out.print(";\n");
    }
    
    // genCfgAssignAccum
    private void genCfgAssignAccum( Impl.Assign ca )
    {
    	Expr.Path lval = ca.getLval();
    	glob.out.printf("%t");
    	genExpr((Expr)lval);
    	if (lval.getCat() instanceof Cat.Arr) {
    		glob.out.printf(".$add(");
        	genExpr(ca.getRval());
        	glob.out.printf(");\n");
    	}
    	else {
    		glob.out.printf(" += ");
        	genExpr(ca.getRval());
    		glob.out.print(";\n");
    	}
    }
    
    // genExpr
    private void genExpr( Expr e )
    {
    	if (e == null) {
    		return;
    	}
    	glob.genAny(e, ImplS.class, this);
    }
    
    // genExpr$Addr
    void genExpr$Addr( Expr.Addr e )
    {
    	List<Expr.Path> chain = new ArrayList<Expr.Path>();
    	((Expr.Path)e.getBody()).buildChain(chain);

		int k = chain.size();
		if (k > 1) {
			genExpr((Expr)chain.get(k - 2));
		}
		Expr.Path last = chain.get(k - 1);
		glob.out.print(".$addrof(");
		if (last instanceof Expr.Select) {
			glob.out.printf("'%1'", ((Expr.Select)last).getSel().getText());
		}
		else if (last instanceof Expr.Index ){
			genExpr(((Expr.Index)last).getIdx());
		}
		glob.out.print(")");
    }
    
    // genExpr$Array
    void genExpr$Array( Expr.Array e )
    {
        String sep = "";
        glob.out.print("[");
        for (Expr e2 : e.getElems()) {
            glob.out.print(sep);
            genExpr(e2);
            sep = ", ";
        }
        glob.out.print("]");
    }
    
    // genExpr$Call
    void genExpr$Call( Expr.Call e )
    {
    	genExpr(e.getFxn());
    	glob.out.print("(");
    	String sep = "";
    	for (Expr a : e.getArgs()) {
    		glob.out.print(sep);
    		sep = ", ";
    		genExpr(a);
    	}
    	glob.out.print(")");
    }
    
    // genExpr$Cond
    void genExpr$Cond( Expr.Cond e )
    {
    	genExpr(e.getCond());
    	glob.out.print(" ? ");
    	genExpr(e.getLeft());
    	glob.out.print(" : ");
    	genExpr(e.getRight());
    }
    
    // genExpr$Creat
    void genExpr$Creat( Expr.Creat e )
    {
    	genExpr(e.getMod());
    	glob.out.print("(");
    	String sep = "";
    	for (Expr a : e.getArgs()) {
    		glob.out.print(sep);
    		sep = ", ";
    		genExpr(a);
    	}
    	glob.out.print(")");
    }
    
    // genExpr$Binary
    void genExpr$Binary( Expr.Binary e )
    {
    	String op = e.getOp();
    	
    	genExpr(e.getLeft());
    	if (op.charAt(0) == ',') {
    		glob.out.print(", ");
    	}
    	else {
        	glob.out.printf(" %1 ", e.getOp());
    	}
        genExpr(e.getRight());
    }
    
    // genExpr$Cast
    void genExpr$Cast( Expr.Cast e )
    {
    	genExpr(e.getBody());
    }
    
    // genExpr$Const
    void genExpr$Const( Expr.Const e )
    {
        String cs = e.getVal();
        if (cs.length() > 3 && cs.startsWith("'\"")) {
            cs = cs.substring(1, cs.length() - 1);
        }
        glob.out.print(cs);
    }
    
    // genExpr$Frag
    void genExpr$Frag( Expr.Frag e )
    {
    	glob.out.print(e.getText());
    }
    
    // genExpr$Hash
    void genExpr$Hash( Expr.Hash e )
    {
        String sep = "";
        glob.out.print("{");
        for (int i = 0; i < e.getIds().size(); i++) {
            glob.out.printf("%1%2", sep, e.getIds().get(i).getText());
            glob.out.print(": ");
            genExpr(e.getVals().get(i));
            sep = ", ";
        }
        glob.out.print("}");
    }
    
    // genExpr$Index
    void genExpr$Index( Expr.Index e )
    {
    	genExpr(e.getArr());
    	glob.out.print("[");
    	genExpr(e.getIdx());
    	glob.out.print("]");
    }
    
    // genExpr$Paren
    void genExpr$Paren( Expr.Paren e )
    {
    	glob.out.print("(");
    	genExpr(e.getBody());
    	glob.out.print(")");
    }
    
    // genExpr$Select
    void genExpr$Select( Expr.Select e )
    {
    	Expr obj = e.getObj();
    	Atom id = e.getSel();
    	
    	if (obj != null) {
    		genExpr(obj);
    		glob.out.print(".");
    	}
    	else if (e.getUnit() != null) {
    		glob.out.printf("%1.", glob.curUnit.getName());
    	}
    	
		glob.out.print(id.getText());
		Cat cat = e.getCat();
		if (cat instanceof Cat.Uni && ((Cat.Uni)cat).isProxy()) {
			glob.out.print(".delegate$.$orig");
		}
    }
    
    // genExpr$Size
    void genExpr$Size( Expr.Size e )
    {
    	Ref r = e.getTspec().getRef();
    	glob.genRef(r);
       	glob.out.printf(".$%1(", e.getKeyword().getText());
       	String fn = e.getFieldName();
       	if (fn != null) {
       		glob.out.printf("'%1'", fn);
       	}
       	glob.out.print(")");
    }

    // genExpr$Term
    void genExpr$Term( Expr.Term e )
    {
    	glob.out.printf(e.getRef().getId());
    }
    
    // genExpr$Unary
    void genExpr$Unary( Expr.Unary e )
    {
    	if (e.isPost()) {
            genExpr(e.getRight());
        	glob.out.print(e.getOp());
    	}
    	else {
        	glob.out.print(e.getOp());
            genExpr(e.getRight());
    	}
    }
    
    // genExpr$Var
    void genExpr$Var( Expr.Var e )
    {
    	Decl.Arg d = e.getDecl();
    	
    	glob.out.printf("%1 %2", e.getKeyword(), d.getName());
    	if (d.getInit() == null) {
    		return;
    	}
		glob.out.print(" = ");
		genExpr(d.getInit());
    }
    
    // genFxnDecl
    private void genFxnDecl( Decl.Fxn fxn, List<Decl.Arg> args )
    {
        glob.out.printf("function %1(", fxn.getName().replace(".", "__"));

        String sep = " ";
        boolean sp = false;
        for (Decl.Arg a : args) {
        	if (!(a.getName().equals("this"))) {
            	glob.out.printf("%1%2", sep, a.getName());
            	sep = ", ";
            	sp = true;
        	}
        }

        glob.out.printf("%1)\n", sp ? " " : "");
    }
    
    // genFxnBody
    private void genFxnBody( Impl.Body fb )
    {
    	Decl.Fxn fxn = fb.getFxn();
    	
    	glob.genTitle(fxn.getName());
    	genFxnDecl(fxn, fb.getArgs());
    	glob.out.printf("{%+\n");
    	for (Stmt st : fb.getBody().getElems()) {
    		genStmt(st);
    	}
    	glob.out.printf("%-%t}\n");
    }
    
    // genInstStaticInit
    private void genInstStaticInit( Unit unit )
    {
    	glob.genTitle("instance$static$init");
    	glob.out.printf("function instance$static$init()\n{\n%+");
    	glob.out.printf("}\n%-");
    }
    
    // genMetaInit
    private void genMetaInit( Unit unit )
    {
    	glob.genTitle("module$meta$init");
    	glob.out.printf("function module$meta$init()\n{\n%+");

    	glob.out.printf("%t%1 = this;\n", unit.getName());
    	skip();
    	
    	glob.out.printf("%t/* -------- config bindings -------- */\n\n");
    	for (Impl.Assign ca : unit.getCfgAssigns()) {
    		if (ca.isFinal()) {
    			genCfgAssign(ca);
    		}
    	}
    	
    	skip();
    	glob.out.printf("%tthis.$super.module$meta$init();\n");
    	glob.out.printf("}\n%-");
    }
    
    // genModStaticInit
    private void genModStaticInit( Unit unit )
    {
    	if (unit.getSession().findDecl(unit, "Module_State.init") == null) {
    		return;
    	}
    	
    	glob.genTitle("module$static$init");
    	glob.out.printf("function module$static$init( obj )\n{\n%+");
    	glob.out.printf("%tModule_State__init.apply(obj);\n");
    	glob.out.printf("}\n%-");
    }
    
    // genModUse
    private void genModUse( Unit unit )
    {
    	glob.genTitle("module$use");
    	glob.out.printf("function module$use()\n{\n%+");
    	
    	glob.out.printf("%t/* -------- config bindings -------- */\n\n");
    	for (Impl.Assign ca : unit.getCfgAssigns()) {
    		if (!ca.isFinal()) {
    			genCfgAssign(ca);
    		}
    	}

    	skip();
    	genUsed(unit, false);
    	
    	skip();
    	genUsed(unit, true);
    	
    	skip();
    	glob.out.printf("%tthis.$super.module$use();\n");
    	glob.out.printf("}\n%-");
    }
    
    // genPrologue
    private void genPrologue( Unit unit )
    {
    	for (Unit u = unit; u != null; u = u.getSuper()) {
        	for (Import im : u.getImports()) {
        		if (u != unit && !im.isExport()) {
        			continue;
        		}
        		Unit iu = im.getUnit();
       	    	glob.out.printf("var %1 = xdc.module('%2');\n", im.getAlias(), iu.getQualName());
        	}
    	}
    	glob.out.printf("var %1;\n", unit.getName());
    }
    
    // genStmt
    void genStmt( Stmt st )
    {
    	if (st == null) {
    		return;
    	}
    	glob.genAny(st, ImplS.class, this);
    }
    
    // genStmt$Basic
    void genStmt$Basic( Stmt.Basic st )
    {
    	Expr e = st.getExpr();
    	if (e instanceof Expr.Var && ((Expr.Var)e).getDecl().getInit() == null) {
    		return;
    	}
    	glob.out.printf("%t");
    	genExpr(e);
    	glob.out.printf(";\n");
    }
    
    // genStmt$Block
    void genStmt$Block( Stmt.Block st )
    {
    	glob.out.printf("{%+\n");
    	for (Stmt se : st.getElems()) {
    		genStmt(se);
    	}
    	glob.out.printf("%-%t}\n");
    }
    
    // genStmt$Break
    void genStmt$Break( Stmt.Break st )
    {
    	glob.out.printf("%t%1;\n", st.getKeyword());
    }
    
    // genStmt$For
    void genStmt$For( Stmt.For st )
    {
    	glob.out.printf("%tfor (");
    	genExpr(st.getInit());
    	glob.out.printf("; ");
    	genExpr(st.getCond());
    	glob.out.printf("; ");
    	genExpr(st.getIncr());
    	glob.out.printf(") ");
    	genStmt(st.getBody());
    }
    
    // genStmt$If
    void genStmt$If( Stmt.If st )
    {
    	glob.out.printf("%tif (");
    	genExpr(st.getCond());
    	glob.out.printf(") ");
    	genStmt(st.getBodyIf());
    	Stmt es = st.getBodyElse();
    	if (es != null) {
        	glob.out.printf("%telse ");
        	genStmt(es);
    	}
    }
    
    // genStmt$Native
    void genStmt$Native( Stmt.Native st )
    {
    	glob.out.printf("%t");
    	glob.out.print(st.getText());
    }
    
    // genStmt$Return
    void genStmt$Return( Stmt.Return st )
    {
    	glob.out.printf("%treturn");
    	if (st.getExpr() != null) {
    		glob.out.print(" ");
        	genExpr(st.getExpr());
    	}
    	glob.out.printf(";\n");
    }
    
    // genStmt$Switc
    void genStmt$Switch( Stmt.Switch st )
    {
    	List<Stmt.Case> csL = st.getCaseList();
    	Stmt.Case ds = st.getDefault();
    	
    	glob.out.printf("%tswitch (");
    	genExpr(st.getCond());
    	glob.out.printf(") {\n");
    	for (Stmt.Case cs : csL) {
    		glob.out.printf("%tcase ");
    		genExpr(cs.getCond());
    		glob.out.printf(":\n%+");
    		if (cs.getElems() != null) {
        		for (Stmt s : cs.getElems()) {
        			glob.out.printf("%t");
        			genStmt(s);
        		}
    		}
    		glob.out.printf("%-");
    	}
    	if (ds != null) {
    		glob.out.printf("%tdefault:\n%+");
    		if (ds.getElems() != null) {
        		for (Stmt s : ds.getElems()) {
        			glob.out.printf("%t");
        			genStmt(s);
        		}
    		}
    		glob.out.printf("%-");
    	}
    	glob.out.printf("%t}\n");
    }
    
    // genStmt$While
    void genStmt$While( Stmt.While st )
    {
    	if (st.getKeyword().equals("while")) {
        	glob.out.printf("%twhile (");
        	genExpr(st.getCond());
        	glob.out.printf(") ");
        	genStmt(st.getBody());
    	}
    	else {
        	glob.out.printf("%tdo ");
        	genStmt(st.getBody());
        	glob.out.printf("%twhile (");
        	genExpr(st.getCond());
        	glob.out.printf(");\n");
    	}
    }
    
    // genUsed
    private void genUsed( Unit unit, boolean isMeta )
    {
    	glob.out.printf("%t/* -------- used %1 modules -------- */\n\n", isMeta ? "meta" : "target");
    	for (Import im : unit.getImports()) {
    		Unit iu = im.getUnit();
    		if (iu.isMod() && iu.isMeta() == isMeta) {
    			if (!im.isExport()) {
            		glob.out.printf("%tif (%1.$used || !(%1.$written('$used'))) xdc.useModule('%2');\n",
            				im.getAlias(), iu.getQualName());
    			}
    			else {
            		glob.out.printf("%tif (%1.$used) xdc.useModule('%2');\n",
            				im.getAlias(), iu.getQualName());
    			}
    		}
    	}
    }
    
    // skip
    private void skip()
    {
        glob.out.print("\n");
    }
}
