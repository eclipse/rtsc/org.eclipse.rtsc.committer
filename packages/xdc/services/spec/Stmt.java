package xdc.services.spec;

import java.util.*;

public class Stmt {

	void resolve()
	{
	}
	
	// Stmt.Basic
	static public class Basic extends Stmt
	{
		private Expr expr;
		
		Basic( Expr expr )
		{
			this.expr = expr;
		}
		
		public final Expr getExpr() { return this.expr; }
		
		void resolve()
		{
			if (this.expr != null) {
				Impl.resolveExpr(this.expr);
			}
		}
	}

	// Stmt.Block
	static public class Block extends Stmt
	{
		private List<Stmt> elems;
		
		Block( List<Stmt> elems )
		{
			this.elems = elems;
		}
		
		public List<Stmt> getElems() { return this.elems; }

		void resolve()
		{
			for (Stmt s : this.elems) {
				s.resolve();
			}
		}
	}

	// Stmt.Break
	static public class Break extends Stmt
	{
		private String keyword;
		
		Break( String keyword )
		{
			this.keyword = keyword;
		}
		
		public String getKeyword() { return this.keyword; }
	}
	
	// Stmt.Case
	static public class Case extends Stmt
	{
		private Expr cond;
		private List<Stmt> elems;
		
		Case( Expr cond, List<Stmt> elems )
		{
			this.cond = cond;
			this.elems = elems;
		}
		
		public Expr getCond() { return this.cond; }
		public List<Stmt> getElems() { return this.elems; }
		
		void resolve()
		{
			if (this.cond != null) {
				Impl.resolveExpr(this.cond);
			}
			if (this.elems != null) {
				for (Stmt s : this.elems) {
					s.resolve();
				}
			}
		}
	}
	
	// Stmt.For
	static public class For extends Stmt
	{
		private Expr init;
		private Expr cond;
		private Expr incr;
		private Stmt body;
		
		For( Expr init, Expr cond, Expr incr, Stmt body )
		{
			this.init = init;
			this.cond = cond;
			this.incr = incr;
			this.body = body;
		}
		
		public final Expr getInit() { return this.init; }
		public final Expr getCond() { return this.cond; }
		public final Expr getIncr() { return this.incr; }
		public final Stmt getBody() { return this.body; }

		void resolve()
		{
			if (this.init != null) {
				Impl.resolveExpr(this.init);
			}
			if (this.cond != null) {
				Impl.resolveExpr(this.cond);
			}
			if (this.incr != null) {
				Impl.resolveExpr(this.incr);
			}

			this.body.resolve();
		}
	}

	// Stmt.If
	static public class If extends Stmt
	{
		private Expr cond;
		private Stmt bodyIf;
		private Stmt bodyElse;
		
		If( Expr cond, Stmt bodyIf, Stmt bodyElse )
		{
			this.cond = cond;
			this.bodyIf = bodyIf;
			this.bodyElse = bodyElse;
		}
		
		public Expr getCond() { return this.cond; }
		public Stmt getBodyIf() { return this.bodyIf; }
		public Stmt getBodyElse() { return bodyElse; }

		void resolve()
		{
			Impl.resolveExpr(this.cond);
			this.bodyIf.resolve();
			if (this.bodyElse != null) {
				this.bodyElse.resolve();
			}
		}
	}

	// Stmt.Native
	static public class Native extends Stmt
	{
		private String text;
		
		Native( String text )
		{
			this.text = text;
		}
		
		public String getText() { return this.text; }
	}

	// Stmt.Return
	static public class Return extends Stmt
	{
		private Expr expr;
		
		Return( Expr expr)
		{
			this.expr = expr;
		}
		
		public Expr getExpr() { return this.expr; }
		
		void resolve()
		{
			if (this.expr != null) {
				Impl.resolveExpr(this.expr);
			}
		}
	}

	// Stmt.Switch
	static public class Switch extends Stmt
	{
		private Expr cond;
		private List<Stmt.Case> caseList;
		private Stmt.Case defaultStmt;
		
		Switch( Expr cond, List<Stmt.Case> caseList, Stmt.Case defaultStmt )
		{
			this.cond = cond;
			this.caseList = caseList;
			this.defaultStmt = defaultStmt;
		}
		
		public Expr getCond() { return this.cond; }
		public List<Stmt.Case> getCaseList() { return this.caseList; }
		public Stmt.Case getDefault() { return this.defaultStmt; }
		
		
		void resolve()
		{
			Impl.resolveExpr(this.cond);
			for (Stmt s : this.caseList) {
				s.resolve();
			}
			if (this.defaultStmt != null) {
				this.defaultStmt.resolve();
			}
		}
	}

	// Stmt.While
	static public class While extends Stmt
	{
		private String keyword;
		private Expr cond;
		private Stmt body;

		While( String keyword, Expr cond, Stmt body )
		{
			this.keyword = keyword;
			this.cond = cond;
			this.body = body;
		}
		
		public final String getKeyword() { return this.keyword; }
		public final Expr getCond() { return this.cond; }
		public final Stmt getBody() { return this.body; }
		
		void resolve()
		{
			Impl.resolveExpr(this.cond);
			this.body.resolve();
		}
	}
}
