include $(XDCROOT)/packages/xdc/bld/xdc_java.mak

CLASSES  = $(patsubst %.java,%,$(wildcard *.java))
JARFILE  = java/package.jar

ANTLR = $(PKGROOT)/xdc/shelf/java/antlr.jar
RHINO = $(PKGROOT)/xdc/shelf/java/js.jar
ECLIPSE = $(PKGROOT)/xdc/shelf/java/ecj.jar

JCPATH:=$(PKGROOT)$(PATHCHAR)$(ANTLR)$(PATHCHAR)$(RHINO)$(PATHCHAR)$(ECLIPSE)

.libraries: $(JARFILE)
$(JARFILE): $(patsubst %,$(CLASSDIR)/%.class,$(CLASSES))

clean::
	$(RM) $(JARFILE)

