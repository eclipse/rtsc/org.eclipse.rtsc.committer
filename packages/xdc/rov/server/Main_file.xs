/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== run ========
 *  coreView creates a CoreDumpImage instance and a SymbolsViaOfd instance, and
 *  passes them to Model.start. Then it uses Program.scanModule to gather all
 *  of the module, instance, and embedded object views, which it outputs in
 *  an XML format. 
 */
function run( cmdr, args )
{
    var inst = this.$private;
    inst.self = this;

    for (p in inst.self) inst[p] = inst.self[p];

    inst.cmdr = cmdr;
    inst.args = args;
    
    print("---- ROVServer: Parsing the XML file -----");
    var xmlObj = readRovFile("C:\\ROVServer\\ROVExample.xml");
    print("---- ROVServer: Done parsing -----");
        
    while (true) {
        //var input = stdin.readLine();
        /* If it's a Java string, convert it to JavaScript String. */
        var input = new String(inst.cmdr.read());
        
        
        /* Clear Cache */
        if (input == "cc") {
        
        }
        /* List modules */
        else if (input == "lm") {
            print("---- ROVServer: Fetching the module list-----");
            inst.cmdr.write(getModuleList(xmlObj));
        }
        /* Retrieve Module 'm' == 109? */
        else if (input.charAt(0) == 'm') {
            var tokens = input.split(/\s+/);
            inst.cmdr.write(getModule(xmlObj, tokens[1], tokens[2]));
        }
        /* Unrecognized command */
        else if (input == "q") {
            break;
        }
        else {
            print("Unrecognized command");
        }
        
    }
}

function readRovFile(filename)
{
    /* Create the File */    
    var file = xdc.jre.java.io.File(filename);

    /* Create the file reader */
    var fis = new xdc.jre.java.io.FileInputStream(file);
    var isr = new xdc.jre.java.io.InputStreamReader(fis);
    var input = new xdc.jre.java.io.BufferedReader(isr);

    /* Read through the file */

    var line;    
    var sb = new java.lang.StringBuilder();
    while((line = input.readLine()) != null) {
        sb.append(line + "\n");
    }

    return(new XML(sb.toString()));
}

/*
 * The module list returned is of the form  
 * <ModuleList>
 *   <ti>
 *   <xdc>
 *
 * This is because the Java XML parser must have
 * a root node in order to parse the XML.
 */
function getModuleList(XMLObject) {
    var pkgRoots = XMLObject.ModuleList;
    
    return(pkgRoots.toString());
}

/*
 * The module list returned is of the form  
 * <Module>
 *   <Instance_View>
 *   <Instance_View>
 *
 * This is because the Java XML parser must have
 * a root node in order to parse the XML.
 */
function getModule(XMLObject, tabName, modName) {
	var tabs = XMLObject.Views.*;
	
    var modViews = null;
	/* Go through the list of tabs to find the one we want */
	for (var i in tabs) {
        if (tabs[i].name() == tabName) {
			modViews = tabs[i].*;
			break;
		}
	}
	
	/* Go through the list of modules to find the one we want */
	for (var i in modViews) {
		if (modViews[i].name() == modName) {
			return(modViews[i].toString());
		}
	}
	
	// TODO: Should this return an empty list instead? Some way to indicate not found?
	return("");
}
