package xdc.rta;

public interface IOFReader {

    public String findString(long addr);
    
    public String parse(String fileName) throws java.io.IOException;
    
    public void close() throws java.io.IOException;

}
