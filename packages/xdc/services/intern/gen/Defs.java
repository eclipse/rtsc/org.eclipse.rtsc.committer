/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.gen;

import xdc.services.global.*;
import java.io.*;
import java.util.*;
import xdc.services.spec.*;

public class Defs
{
    private Glob glob = new Glob();
    
    // gen
    public void gen( Unit unit, Out out )
    {
        glob.setNames(unit);
        glob.mode = Glob.CDLMODE;
        glob.out = out;
        
        glob.genTitle((unit.isMod() ? "module" : "interface") + ' ' + unit.getQualName());

        for (Decl d : unit.getDecls()) {

        	if (!d.isMeta() && d instanceof Decl.Struct && d.getParent() == unit) {
                if (!d.getName().equals("Instance_State") && d.overrides() == null) {
                    Decl.Struct str = (Decl.Struct)d;
                    String skw = str.isUnion() ? "union" : "struct";
                    glob.out.printf("typedef %2 %1 %1;\n", glob.cname + d.getName(), skw);
                }
            }
         }

        if (unit.isInter() || unit.isHeir()) {
            glob.out.printf("typedef struct %1Fxns__ %1Fxns__;\n", glob.cname);
            glob.out.printf("typedef const %1Fxns__* %1Module;\n", glob.cname);
        }
        
        if (unit.isStatic()) {
            return;
        }
        
        glob.out.printf("typedef struct %1Params %1Params;\n", glob.cname);
        
        if (unit.isProxy()) {
            glob.out.printf("typedef struct %1__Object *%2Handle;\n",
                    glob.mkCname(unit.getSuper().getQualName()), glob.cname);
        }
        else if (unit.isMod()) {
            glob.out.printf("typedef struct %1Object %1Object;\n", glob.cname);
            glob.out.printf("typedef struct %1Struct %1Struct;\n", glob.cname);
            glob.out.printf("typedef %1Object* %1Handle;\n", glob.cname);
            glob.out.printf("typedef struct %1Object__ %1Instance_State;\n", glob.cname);
        }
        else {
            // TODO -- re-work __label support
            glob.out.printf("typedef struct %1__Object { %1Fxns__* __fxns; xdc_Bits32 __label; } *%1Handle;\n",
                    glob.cname);
        }
     }
     
    // gen
    public void gen( Pkg pkg, boolean top, Out out )
    {
        String pn = glob.mkIname(pkg.getName());

        if (top) {
            glob.out = out;
            glob.genWarning();
            glob.out.printf("\n");
        }

        if (top) {
            glob.out.printf("#ifndef %1_\n#define %1_\n\n", pn);
        }
        else {
            glob.out.printf("\n\n#endif /* %1_ */ \n", pn);
        }
    }
}
