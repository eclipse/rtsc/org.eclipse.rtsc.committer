/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
//
// ======== xdc.services.spec.Grammar.g ========
//

grammar Grammar;

options {
    language=Java;
    k = 3;
}

@parser::header {

    package xdc.services.spec;
    
    import java.util.EnumSet;
    import java.util.HashSet;
	import java.util.Set;
}

@parser::members {
    String file;
    GrammarLexer lexer;
    ParserSession ses;
    Atom curId;
    Atom curVar = null;
    Type.Spec curTS;
    EnumSet<Qual> curQuals;
    ArrayList<Decl> curDL;
    boolean curMeta = false;
    boolean inFxnDef = false;
    boolean inFxnHdr = false;
    int curStrId = 1;

    static final Decl.Error ERRDECL = new Decl.Error();
    static final Expr.Error ERREXPR = new Expr.Error();
    static final Type.Error ERRTYPE = new Type.Error();

    GrammarParser(GrammarLexer lexer, String file, ParserSession ses)
    {
        this(new CommonTokenStream(lexer));
        this.file = file;
        this.lexer = lexer;
        this.ses = ses;
    }

    public void reportError( RecognitionException e )
    {
        reportError(this.getErrorMessage(e, tokenNames), e.line);
    }

    public void reportError( String message )
    {
        reportError(message, (lexer != null)? lexer.getLine() : -1);
    }

    /**
     * Report an error at the given line.
     */
    public void reportError( String message, int line )
    {
        ses.msg.error("\""+this.file+"\", line "+line+":  "+message);
    }
}

@lexer::header {
    package xdc.services.spec;
}

@lexer::members {
    boolean sep = false;

    void nl() { sep = false; }

    void nl( String s )
    {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '\n') {
                this.nl();
            }
        }
    }
}

// PARSER

absDeclarator returns [Type ret]
@init {
    ret = ERRTYPE;
}
    :
        IDENT?
        {
            ret = new Type.Declarator(curTS, curVar);
        }
    |
        STAR ('const')? t=absDeclarator
        {
            ret = new Type.Ptr(t);
        }
    |
        LPAR r=absDeclarator RPAR
            { ret = r instanceof Type.Ptr ? new Type.Paren(r) : r; }
        (r=absDeclarator_tail[ret])+ {ret = r;}
    |
        (
            { e = null; }
            LBRK (e=expr)? RBRK
            {
                Type.Declarator td = new Type.Declarator(curTS, curVar);
                ret = new Type.Array(td, e, Type.Array.Kind.CARR); /// update for maps
            }
        )+
    ;

absDeclarator_tail [Type base] returns [Type ret]
@init {
    boolean va = false;

    ret = ERRTYPE;
}
    :
        LBRK (e=expr)? RBRK
        {
            ret = new Type.Array(base, e, Type.Array.Kind.CARR);
        }
    |
        LPAR aL=absArgDeclListOpt ( COMMA ELIPSIS { va = true; } )? RPAR
        {
            ret = new Type.Fxn(base, aL, va);
        }
    ;

absArgDeclListOpt returns [ArrayList<Decl.Arg> ret]
@init {
    ret = new ArrayList();
}
    :
        // empty
    |
        d=absArgDecl { ret.add((Decl.Arg)d); } (COMMA d=absArgDecl { ret.add((Decl.Arg)d); })*
    ;

absArgDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        typeSpecC t=absDeclarator
        {
            ret = new Decl.Arg(null, curQuals, t);
        }
    ;

annotation returns [ArrayList<Attr> ret]
@init {
    ret = null;
}
    :
        (r=annotAttr[ret])+ {ret = r;}
    ;

annotAttr [ArrayList<Attr> aL] returns [ArrayList<Attr> ret]
@init {
    ret = (aL == null) ? new ArrayList() : aL;
}
    :
        i=ATTR (LPAR e=expr RPAR)?
        {
            Attr a = new Attr(new Atom(i, file), e == null ? new Expr.Const(new Atom("true")) : e);
            ret.add(a);
        }
    ;

argDeclListOpt returns [ArrayList<Decl.Arg> ret]
@init {
    ret = new ArrayList();
}
    :
        // empty
    |
        d=argDecl { ret.add((Decl.Arg)d); } (COMMA d=argDecl { ret.add((Decl.Arg)d); })*
    ;

argDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        typeSpecC t=declarator (EQ e=expr)?
        {
            ret = new Decl.Arg(curId, curQuals, t, e);
        }
    ;

basicDeclarator returns [Type ret]
@init {
    ret = ERRTYPE;
}
    :
        {!inFxnHdr}?=> i=IDENT
        {
            curId = new Atom(i, file);
            ret = new Type.Declarator(curTS, curId);
        }
    |
        {inFxnHdr}?=> q=qualName
        {
            curId = q;
            ret = new Type.Declarator(curTS, curId);
        }
    |
        STAR mods=typeMods t=basicDeclarator
        {
            ret = new Type.Ptr(t, mods);
        }
    |
        LPAR t=declarator RPAR
        {
            ret = new Type.Paren(t);
        }
    ;

cfgAssign returns [Impl.Assign ret]
@init {
    boolean isFinal = false;
    boolean isAccum = false;
    ret = null;
}
    :
        ('final' { isFinal = true; })? lval=exprPath (COLONEQ | (PLUSEQ { isAccum = true; })) rval=expr (SEMI)?
        {
            ret = new Impl.Assign((Expr.Path)lval, rval, isFinal, isAccum);
        }
    |
        ('final' { isFinal = true; })? rval=exprFrag (SEMI)?
        {
            ret = new Impl.Assign(null, rval, isFinal, false);
        }       
    ;
	
configDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        ('readonly' { curQuals.add(Qual.READONLY); })?
        'config' typeSpec t=declarator (EQ e=expr)? SEMI
        {
            ret = new Decl.Config(curId, curQuals, t, e);
        }
    ;

constDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        'const' typeSpec t=declarator (EQ e=expr)? SEMI
        {
            ret = new Decl.Const(curId, curQuals, t, e);
        }
    ;

declQuals
    :
        ('final' { curQuals.add(Qual.FINAL); })?
        ('override' { curQuals.add(Qual.OVERRIDE); })?
        ('metaonly' { curQuals.add(Qual.METAONLY); })?   
    ;

declarator returns [Type ret]
@init {
    ret = ERRTYPE;
}
    :
        r1=declarator_term r2=declarator_tail[r1] {ret = r2;}
    ;


declarator_term returns [Type ret]
@init {
    ret = ERRTYPE;
}
    :
        STAR mods=typeMods t=declarator_term
        {
            ret = new Type.Ptr(t, mods);
        }
    |
        i=IDENT
        {
            curId = new Atom(i, file);
            ret = new Type.Declarator(curTS, curId);
        }
    |
        LPAR t=declarator RPAR
        {
            ret = t instanceof Type.Ptr ? new Type.Paren(t) : t;
        }
    ;

declarator_tail [Type base] returns [Type ret]
@init {
    Type.Array.Kind kind = Type.Array.Kind.CARR;
    boolean va = false;

    ret = ERRTYPE;
}
    :
        /* empty */
        {
            ret = base;
        }
    |   
        LBRK (('string' { kind = Type.Array.Kind.MAP; })
        	| ('length' { kind = Type.Array.Kind.VEC; })
        	| (e=expr))?
        RBRK b=declarator_tail[base]
        {
            ret = new Type.Array(b, e, kind);
        }
    |
        LPAR aL=absArgDeclListOpt ( COMMA ELIPSIS { va = true; } )? RPAR b=declarator_tail[base]
        {
            ret = new Type.Fxn(b, aL, va);
        }
    ;

doc_block [ArrayList<DocComment> dL] returns [ArrayList<DocComment> ret]
@init {
    ret = dL;
}
    :
        (r=one_doc_block[ret] { ret = r; })*
    ;

one_doc_block [ArrayList<DocComment> dL] returns [ArrayList<DocComment> ret]
@init {
    ret = dL;
}
    :
        a=DOC_BLK {
            if (ret == null) {
                ret = new ArrayList();
            }
            ret.add(new DocComment(new Atom(a, file)));
        }
    ;

doc_inline [ArrayList<DocComment> dL] returns [ArrayList<DocComment> ret]
@init {
    ret = dL;
}
   :
        (a=DOC_INL {
            if (ret == null) {
                ret = new ArrayList();
            }
            ret.add(new DocComment(new Atom(a, file)));
        })*
    ;

enumDecl returns [Decl ret]
@init {
    ArrayList<Decl.EnumVal> evL = new ArrayList();
    ret = ERRDECL;
}
    :
        'enum' i=IDENT (rep=enumRep)? LCUR enumValList[evL] RCUR (SEMI)?
        {
            ret = new Decl.Enum(new Atom(i, file), curQuals, evL, rep);
        }
    ;

enumRep returns [Type.Spec ret]
@init {
    ret = null;
}
    :
		COLON typeSpec
		{
			ret = curTS;
		}
	;

enumValList [ArrayList<Decl.EnumVal> evL]
    :
        d=enumVal
        { evL.add((Decl.EnumVal)d); } 
        (
            COMMA
            docL=doc_inline[null] { d.bindDocs(docL); }
            d=enumVal
            { evL.add((Decl.EnumVal)d); }
        )*
        docL=doc_block[null] { d.bindDocs(docL); }
    ;
        
enumVal returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
         i=IDENT (EQ e=expr)?
        {
            ret = new Decl.EnumVal(new Atom(i, file), curQuals, e);
        }
    ;

expr returns [Expr ret]
@init {
    ArrayList eL = new ArrayList();
    ArrayList iL = new ArrayList();

    ret = ERREXPR;
}
    :
        r=exprCnd
        {
            ret = r;
        }
    |
        LCUR (exprProp[iL,eL])? (COMMA exprProp[iL,eL])* (COMMA)? RCUR
        {
            ret = new Expr.Hash(iL, eL);
        }
    |
        LBRK (e=expr { eL.add(e); })? (COMMA e=expr { eL.add(e); })* (COMMA)? RBRK
        {
            ret = new Expr.Array(eL);
        }
    ;

exprProp [ List<Atom> iL, List<Expr> eL ]
    :
        i=IDENT { iL.add(new Atom(i, file)); } COLON e=expr { eL.add(e); }
    ;

exprCnd returns [Expr ret]
@init {
    ret = ERREXPR;
}
    :
        r=exprBin { ret = r; } (QUEST e1=exprCnd COLON e2=exprCnd { ret = new Expr.Cond(r, e1, e2); })?
    ;

exprBin returns [Expr ret]
@init {
    ret = ERREXPR;
}
    :
        r=exprUry { ret = r; } (eBOP)=> (a=eBOP e=exprBin { ret = new Expr.Binary(a, r, e); })?
    ;
    
exprUry returns [Expr ret]
@init {
    ret = ERREXPR;
}
    :
        a1=eUOP r=exprUry
    	{
    		ret = new Expr.Unary(a1, r);
    	}
    |
        r=exprTrm (a2=ePOP)?
        {
        	ret = (a2 == null) ? r : new Expr.Unary(a2, r, true);
        }
    ;
    
exprSize returns [Expr ret]
@init {
    Token tok = input.LT(1);
    ret = ERREXPR;
}
	:
    	('sizeof' | 'alignof' | 'offsetof') LT typeSpecG[null] GT
		{
			ret = new Expr.Size(new Atom(tok, file), curTS);
		}
    ;
		
exprTrm returns [Expr ret]
@init {
    ret = ERREXPR;
}
    :
    	{inFxnDef}?=> e=exprPath { ret = e; }
    |
        {!inFxnDef}?=> q=qualName (eL=exprList)?
        {
            Expr.Term t = new Expr.Term(new Ref(null, new Atom(q, file)));
            ret = (eL == null) ? t : new Expr.Creat(t, eL);
        }
    |
    	{inFxnDef}?=> e=exprAddr { ret = e; }
    |
    	{inFxnDef}?=> e=exprCast { ret = e; }
    |
    	{inFxnDef}?=> e=exprEscape { ret = e; }
    |
    	{inFxnDef}?=> e=exprFrag { ret = e; }
    |
    	{inFxnDef}?=> e=exprSize { ret = e; }
    |
        a1='true'
        {
            ret = new Expr.Const(new Atom(a1, file));
        }
    |
        a2='false'
        {
            ret = new Expr.Const(new Atom(a2, file));
        }
    |
        a3='null'
        {
            ret = new Expr.Const(new Atom(a3, file));
        }
    |
        a4='undefined'
        {
            ret = new Expr.Const(new Atom(a4, file));
        }
    |
        aNUM=NUM
        {
            ret = new Expr.Const(new Atom(aNUM, file));
        }
    |
        aCHAR=CHAR
        {
            ret = new Expr.Const(new Atom(aCHAR, file));
        }
    |
        aSTRING=STRING
        {
            ret = new Expr.Const(new Atom(aSTRING, file));
        }
    |
        LPAR (e=expr)? RPAR
        {
            ret = new Expr.Paren(e);
        }
    ;
    
exprAddr returns [Expr ret]
@init {
    Token tok = input.LT(1);
    ret = ERREXPR;
}
	:
		AMP e=exprPath
		{
			ret = new Expr.Addr(e, new Atom(tok, file));
		}
	;

exprCast returns [Expr ret]
@init {
	ret = ERREXPR;
}
	:
		LT d=absArgDecl GT LPAR (e=exprCnd) RPAR
		{
			ret = new Expr.Cast(((Decl.Arg)d).getType(), e);
		}
	;
	
exprEscape returns [Expr ret]
@init {
	ret = ERREXPR;
}
	:
		CAR i=IDENT (eL=exprList)?
    	{
    		ret = new Expr.Frag(i.getText(), eL);
    	}
    ;


exprFrag returns [Expr ret]
@init {
	ret = ERREXPR;
}
	:
		c=CODE_FRAG
		{
			ret = new Expr.Frag(c.getText());
		}
	; 

exprPath returns [Expr ret]
@init {
	ret = ERREXPR;
}
	:
		e=exprPathIds[null] { ret = e; } (e=exprPathItem[ret] { ret = e; })*
	;
	
exprPathIds [Expr e] returns [Expr ret]
@init {
    ret = e;
}
	:
		q=qualName
		{
			String[] idA = q.getText().split("\\.");
			for (String id : idA) {
				ret = new Expr.Select(ret, new Atom(q, file).copy(id));
			}
		}
	;

exprPathItem [Expr e] returns [Expr ret]
@init {
    Token tok = input.LT(1);
    ret = ERREXPR;
}
	:
		eL=exprList 
		{
			ret = new Expr.Call(e, eL, new Atom(tok, file));
		}
	|
		t=LBRK e2=expr RBRK
		{
			ret = new Expr.Index(e, e2, new Atom(tok, file));
		}
	|
		DOT e3=exprPathIds[e]
		{
			ret = e3;
		}
	;

exprList returns [List<Expr> ret]
@init {
    ret = new ArrayList();
}
    :
        LPAR (e=expr { ret.add(e); } (COMMA e=expr { ret.add(e); })*)? RPAR
    ;
    
exprVar returns [Expr ret]
@init {
	e = null;
	ret = null;
}	
	:
		kw=exprVarKw typeSpecG[null] t=declarator (EQ)=> (EQ e=exprCnd)?
		{
			ret = new Expr.Var(kw, new Decl.Arg(curId, null, t, e));
		}
	|
		kw=exprVarKw i=IDENT { curVar = new Atom(i, file); } COLON typeSpecC t=absDeclarator (EQ)=> (EQ e=exprCnd)?
		{
			ret = new Expr.Var(kw, new Decl.Arg(curVar, null, t, e));
			curVar = null;
		}
	|
		e=exprCnd
		{
			ret = e;
		}
	;

exprVarKw returns [String ret]
@init {
	ret = null;
}
	:
		'const' { ret = "const"; }
	|
		'restrict' { ret = "restrict"; }
	|
		'var' { ret = "var"; }
	|
		'volatile' { ret = "volatile"; }
	;

eBOP returns [Atom ret]
@init {
    Token tok = input.LT(1);
    ret = new Atom(tok, file);
}
    :
        aAMP=AMP
    |
        aAMPAMP=AMPAMP
    |
        aBANGEQ=BANGEQ
    |
        aBAR=BAR
    |
        aBARBAR=BARBAR
    |
        aCAR=CAR
    |
        aDIV=DIV
    |
        aEQEQ=EQEQ
    |
        aGT=GT
    |
        aGTEQ=GTEQ
    |
        aGTGT=GTGT
    |
        aLT=LT
    |
        aLTEQ=LTEQ
    |
        aLTLT=LTLT
    |
        aMIN=MIN
    |
        aPCT=PCT
    |
        aPLUS=PLUS
    |
        aSTAR=STAR
    |
        {inFxnDef}?=> aEQ=EQ
    |
        {inFxnDef}?=> aAMPEQ=AMPEQ
    |
        {inFxnDef}?=> aBAREQ=BAREQ
    |
        {inFxnDef}?=> aCAREQ=CAREQ
    |
        {inFxnDef}?=> aDIVEQ=DIVEQ
    |
        {inFxnDef}?=> aGTGTEQ=GTGTEQ
    |
        {inFxnDef}?=> aLTLTEQ=LTLTEQ
    |
        {inFxnDef}?=> aMINEQ=MINEQ
    |
        {inFxnDef}?=> aPCTEQ=PCTEQ
    |
        {inFxnDef}?=> aPLUSEQ=PLUSEQ
    |
        {inFxnDef}?=> aSTAREQ=STAREQ
    ;
    
eUOP returns [Atom ret]
@init {
    Token tok = input.LT(1);
    ret = new Atom(tok, file);
}
    :
        aBANG=BANG
    |
        aMIN=MIN
    |
        aPLUS=PLUS
    |
        aSTAR=STAR
    |
        aTILDE=TILDE
    |
        {inFxnDef}?=> aPLUSPLUS=PLUSPLUS
    |
        {inFxnDef}?=> aMINMIN=MINMIN
    ;   

ePOP returns [Atom ret]
@init {
    Token tok = input.LT(1);
    ret = new Atom(tok, file);
}
    :
        {inFxnDef}?=> aPLUSPLUS=PLUSPLUS
    |
        {inFxnDef}?=> aMINMIN=MINMIN
    ;   


exportDecl returns [Decl ret]
@init {
	iSup = null;
    ret = ERRDECL;
}
    :
        'export' i=IDENT ('inherits' iSup=superInt)? SEMI
        {
            ret = new Decl.Imp(new Atom(i, file), iSup);
        }
    ;

externDecl returns [Decl ret]
@init {
    Atom a = null;
    ret = ERRDECL;
}
    :
        'extern' typeSpecC t=declarator (EQ i=IDENT { a = new Atom(i, file); })? SEMI
        {
            ret = new Decl.Extern(curId, curQuals, t, a);
        }
    ;

fieldDeclList [List<Decl.Field> fL]
    :
        ( d=fieldDecl docL=doc_inline[null] { fL.add((Decl.Field)d); d.bindDocs(docL); } )+
    ;

fieldDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        typeSpecF t=declarator SEMI
        {
            ret = new Decl.Field(curId, curQuals, t);
        }
    ;

fxnDecl returns [Decl ret]
@init {
    Atom a = null;
    boolean vf = false;
    
    inFxnHdr = true;

    ret = ERRDECL;
}
    :
        t=fxnHdr { inFxnHdr = false; a = curId; }
        LPAR aL=argDeclListOpt ( COMMA ELIPSIS { vf = true; } )? RPAR SEMI
        {
            ret = new Decl.Fxn(a, curQuals, t, aL, vf);
        }
    ;

fxnHdr returns [Type ret]
@init {
    ret = ERRTYPE;
}
    :
        typeSpec r=basicDeclarator { ret = r; }
    |
        { Token tok = input.LT(1); curId = new Atom(tok, file); }
        'create'
        { ret = new Type.Creator(); }
    ;
        
fxnImpl returns [Impl.Body ret]
@init {
	ret = null;
}
    :
    	'function' a=qualName LPAR aL=metArgDeclListOpt RPAR s=stmtBlock
    	{
    		ret = new Impl.Body(a, aL, s);
    	}
    ;

instDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        r=configDecl { ret = r; }
    |
    	r=fxnDecl { ret = r; }
    |
    	r=methodDecl { ret = r; }
    ;

instDeclSpec returns [Decl ret]
@init {
	curQuals = curMeta ? EnumSet.of(Qual.METAONLY) : EnumSet.noneOf(Qual.class);
	curQuals.add(Qual.INSTANCE);

    ret = ERRDECL;
}
    :
        docL=doc_block[null] (attL=annotation)? declQuals r=instDecl docL=doc_inline[docL]
	        { ret = r; ret.bindDocs(docL); ret.bindAttrs(attL); }
    ;

internDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        r=configDecl { ret = r; }
    |
        r=constDecl { ret = r; }
    |
        r=enumDecl { ret = r; }
    |
        r=externDecl { ret = r; }
    |
        r=fxnDecl { ret = r; }
    |
        r=methodDecl { ret = r; }
    |
        r=proxyDecl { ret = r; }
    |
        r=structDecl { ret = r; }
    |
        r=typeDecl { ret = r; }
    ;

internDeclSpec returns [Decl ret]
@init {
	curQuals = curMeta ? EnumSet.of(Qual.METAONLY) : EnumSet.noneOf(Qual.class);
	curQuals.add(Qual.INTERNAL);

    ret = ERRDECL;
}
    :
        docL=doc_block[null] (attL=annotation)? declQuals r=internDecl docL=doc_inline[docL]
	        { ret = r; ret.bindDocs(docL); ret.bindAttrs(attL); }
    ;

methodDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        'function' i=IDENT (LPAR aL=metArgDeclListOpt RPAR)? SEMI
        {
			curQuals.add(Qual.METAONLY);
            ret = new Decl.Fxn(new Atom(i, file), curQuals, null, aL, false);
        }
    ;
    
metArgDeclListOpt returns [ArrayList<Decl.Arg> ret]
@init {
    ret = new ArrayList();
}
    :
        // empty
    |
        a=metArg { ret.add(a); } (COMMA a=metArg { ret.add(a); })*
    ;

metArg returns [Decl.Arg ret]
@init {
	ret = null;
}
	:
		i=IDENT { ret = new Decl.Arg(new Atom(i, file), curQuals, null); }
	;

modDecl returns [Decl ret]
@init {

    ret = ERRDECL;
}
    :
        r=configDecl {ret = r;}
    |
        r=constDecl {ret = r;}
    |
        r=enumDecl {ret = r;}
    |
        r=exportDecl {ret = r;}
    |
        r=externDecl {ret = r;}
    |
        r=fxnDecl {ret = r;}
    |
        r=methodDecl {ret = r;}
    |
        r=proxyDecl {ret = r;}
    |
        r=structDecl {ret = r;}
    |
        r=typeDecl {ret = r;}
    ;

modDeclSpec returns [Decl ret]
@init {
	curQuals = curMeta ? EnumSet.of(Qual.METAONLY) : EnumSet.noneOf(Qual.class);
    ret = ERRDECL;
}
    :
        docL=doc_block[null] (attL=annotation)? declQuals r=modDecl docL=doc_inline[docL]
	        { ret = r; ret.bindDocs(docL); ret.bindAttrs(attL); }
    ;

pkgSpec [String pkgFile] returns [Pkg ret]
@init {
	ArrayList<Atom> mL = new ArrayList();
	ArrayList<Atom> iL = new ArrayList();
	ArrayList<Atom> rL = new ArrayList();
	HashSet<Atom> metaS = new HashSet<Atom>();
	HashMap<Atom,List<String>> genM = new HashMap<Atom,List<String>>();
	
	ret = null;
}
	:
        (
            pkgReqList[rL] {
                if (docL != null) {
                    reportError("requires statement must come before XDOC comment");
                }
                if (attL != null) {
                    reportError("requires statement must come before attributes");
                }
            } |
            docL=one_doc_block[docL] {
                if (attL != null) {
                    reportError("XDOC comment must come before attributes");
                }
            } |
            attL=annotAttr[attL]
        )*
         {
    		Token tok = input.LT(1);
    		a = new Atom(tok, file, ses.getEnv().getUnnamedPkgName(pkgFile));
         }
        'package' (a=qualName)? (vs=pkgVers)?
        LCUR
            (pkgUnit[mL,iL,metaS,genM])*
        RCUR (SEMI)?
        docL=doc_block[docL]
        {
            ret = new Pkg(a, vs, mL, iL, rL);
            ret.bindAttrs(attL);
            ret.bindDocs(docL);
            ret.bindMetaSet(metaS);
            ret.bindGenMap(genM);
        }
	;		

pkgReqList [List<Atom> rL]
@init
 {
    boolean iFlg = false;
}
    :
        'requires' ('internal' { iFlg = true; })? pkgReq[rL, iFlg] (COMMA pkgReq[rL, iFlg])* SEMI
	;

pkgReq [List<Atom> rlist, boolean iFlg]
@init {
    vs = "";
}
    :
        a=qualName (vs=pkgVers)?
        {
			if (iFlg) {
			    a.text = "*" + a.text;
			}
            a.text += '{' + vs;
            rlist.add(a);
        }
    ;

pkgUnit [List<Atom> mlist, List<Atom> ilist, Set<Atom> metaS, Map<Atom,List<String>> genM]
@init {
    List<Atom> l = null;
    Set<Atom> mS = null;
}
    :
        ('metaonly' { mS = metaS; })? ( 'module' { l = mlist; } | 'interface' { l = ilist; } )
        a=pkgUnitName[l, mS] (((LARR)=> (LARR pkgUnitGen[a, genM]?) | (COMMA pkgUnitName[l, mS])*)) SEMI 
    ;
    
pkgUnitGen [Atom a, Map<Atom,List<String>> genM]
@init {
    e = null;
}
	:
		p=PATH e=pkgUnitGenArgs?
		{
			ArrayList<String> sL = new ArrayList<String>();
			sL.add(p.getText());
			if (e != null) {
				sL.add(Expr.toText(e));
			}
		    genM.put(a, sL);
		}
	;
	
pkgUnitGenArgs returns [Expr ret]
@init {
    ArrayList eL = new ArrayList();
    ArrayList iL = new ArrayList();

    ret = ERREXPR;
}
	:
        LCUR (exprProp[iL,eL])? (COMMA exprProp[iL,eL])* (COMMA)? RCUR
        {
            ret = new Expr.Hash(iL, eL);
        }
	; 

pkgUnitName [List<Atom> l, Set<Atom> mS] returns [Atom ret]
    :
        i=IDENT
        {
        	ret = new Atom(i, file);
        	l.add(ret);
        	if (mS != null) {
        		mS.add(ret);
        	}
        }  
    ;

pkgVers returns [String ret]
@init {
    ret = "";
}
    :
        LBRK 
            n1=NUM { ret += $n1.text; } 
            (COMMA n2=NUM { ret += ", " + $n2.text; })*
        RBRK
    ;

proxyDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        'proxy' i=IDENT 'inherits' s=superInt SEMI
            { ret = new Decl.Proxy(new Atom(i, file), curQuals, s); }
    ;

qualName returns [Atom ret]
@init {
    ret = null;
}
    :
        ( i=IDENT { ret = new Atom(i, file); } | p=PATH { ret = new Atom(p, file); } )
    ;

qualNameStar returns [Atom ret]
@init {
    ret = null;
}
    :
        ( i=IDENT { ret = new Atom(i, file); } | p=PATH { ret = new Atom(p, file); } | s=PATHSTAR { ret = new Atom(s, file); })
    ;

stmt returns [Stmt ret]
@init {
	ret = null;
}
	:
		sba=stmtBasic { ret = sba; }
	|
		sbl=stmtBlock { ret = sbl; }
	|
		sbr=stmtBreak { ret = sbr; }
	|
		sdo=stmtDo { ret = sdo; }
	|
		sfo=stmtFor { ret = sfo; }
	|
		sif=stmtIf { ret = sif; }
	|
		sna=stmtNative { ret = sna; }
	|
		sre=stmtReturn { ret = sre; }
	|
		ssw=stmtSwitch { ret = ssw; }
	|
		swh=stmtWhile { ret = swh; }
	;

stmtBasic returns [Stmt.Basic ret]
@init {
    e = null;
    ret = null;
}
    :
    	(e=exprVar)? SEMI
    	{
    		ret = new Stmt.Basic(e);
    	}
    ;
    
stmtBlock returns [Stmt.Block ret]
@init {
	List<Stmt> sL = new ArrayList();
	ret = null;
}
	:
		LCUR (s=stmt { sL.add(s); })* RCUR
		{
			ret = new Stmt.Block(sL);
		}
	;

stmtBreak returns [Stmt.Break ret]
@init {
	String kw = null;
    ret = null;
}
	:
		(('break' {kw = "break"; }) | ('continue' {kw = "continue"; })) SEMI
		{
			ret = new Stmt.Break(kw);
		}
	;

stmtCase returns [Stmt.Case ret]
@init {
	sL = null;
    ret = null;
}
	:
		'case' e=exprTrm COLON (sL=stmtList)?
		{
			ret = new Stmt.Case(e, sL);
		}
	;

stmtDefault returns [Stmt.Case ret]
@init {
    sL = null;
    ret = null;
}
	:
		'default' COLON (sL=stmtList)?
		{
			ret = new Stmt.Case(null, sL);
		}
	;

stmtDo returns [Stmt.While ret]
@init {
    ret = null;
}
	:
		'do' s=stmt 'while' LPAR e=exprCnd RPAR SEMI
		{
			ret = new Stmt.While("do", e, s);
		}
	;

stmtElse returns [Stmt ret]
@init {
    ret = null;
}
	:
		('else')=> 'else' s=stmt { ret = s; }
	|
		/* empty */
	;

stmtFor returns [Stmt.For ret]
@init {
	e1 = null;
	e2 = null;
	e3 = null;
	ret = null;
}
	:
		'for' LPAR (e1=exprVar)? SEMI (e2=exprCnd)? SEMI (e3=exprCnd)? RPAR s=stmt
		{
			ret = new Stmt.For(e1, e2, e3, s);
		}
	;

stmtList returns [List<Stmt> ret]
@init {
	ret = new ArrayList();
}
	:
		s=stmt { ret.add(s); } (s=stmt { ret.add(s); })*
	;

stmtIf returns [Stmt.If ret]
@init {
    ret = null;
}
	:
		'if' LPAR e=exprCnd RPAR s1=stmt s2=stmtElse
		{
			ret = new Stmt.If(e, s1, s2);
		}
	;

stmtNative returns [Stmt.Native ret]
@init {
    ret = null;
}
	:
		c=NATIVE_CODE
		{
			String s = c.getText();
			s = s.replaceFirst("native", "/* native */");
			ret = new Stmt.Native(s);
		}
	;

stmtReturn returns [Stmt.Return ret]
@init {
	e = null;
    ret = null;
}
	:
		'return' (e=expr)? SEMI
		{
			ret = new Stmt.Return(e);
		}
	;

stmtSwitch returns [Stmt.Switch ret]
@init {
	List<Stmt.Case> csL = new ArrayList();
	cs = null;
	ret = null;
}
	:
		'switch' LPAR e=exprCnd RPAR LCUR (cs=stmtCase { csL.add(cs); })* (ds=stmtDefault)? RCUR
		{
			ret = new Stmt.Switch(e, csL, ds);
		}
	;

stmtWhile returns [Stmt.While ret]
@init {
    ret = null;
}
	:
		'while' LPAR e=exprCnd RPAR s=stmt
		{
			ret = new Stmt.While("while", e, s);
		}
	;

structDecl returns [Decl ret]
@init {
    ArrayList<Decl.Field> fL = null;
    boolean uflag = false;
    ret = ERRDECL;
}
    :
        ('struct' | 'union' {uflag = true;} ) i=IDENT 
        ((LCUR { fL = new ArrayList(); } (fieldDeclList[fL])? RCUR (SEMI)?)? | SEMI)
            { ret = new Decl.Struct(new Atom(i, file), curQuals, fL, uflag); }
    ;

structFieldDecl returns [Decl ret]
@init {
    ArrayList<Decl.Field> fL = null;
    boolean uflag = false;
    ret = ERRDECL;
}
    :
        ('struct' | 'union' {uflag = true;} ) LCUR { fL = new ArrayList(); } (fieldDeclList[fL])? RCUR
            { ret = new Decl.Struct(new Atom("__struct__" + curStrId++), curQuals, fL, uflag); }
    ;

superInt returns [Sup ret]
@init {
    ret = null;
}
    :
        ai=qualName
        {
            ret = new Sup(ai);
        }
    ;

typeDecl returns [Decl ret]
@init {
    ret = ERRDECL;
}
    :
        'typedef' typeSpecC t=declarator SEMI
        {
            ret = new Decl.Typedef(curId, curQuals, t);
        }
    ;

typeMods returns [EnumSet<Type.Modifier> ret]
@init {
	ret = EnumSet.noneOf(Type.Modifier.class);
}
	:
		(
			'const' { ret.add(Type.Modifier.CONST); }
		|
			'restrict' { ret.add(Type.Modifier.RESTRICT); }
		|
			'volatile' { ret.add(Type.Modifier.VOLATILE); }
		)*
	;

typeSpec : typeSpecG[null];

typeSpecC
    :
        mods=typeMods typeSpecG[mods]
    ;

typeSpecF
	:
		typeSpecC
	|
		str=structFieldDecl
		{
			curTS = new Type.Spec(new Ref(null, new Atom(str.getName())), null, false, null);
			curDL.add(str);
		}
	;

typeSpecG [EnumSet<Type.Modifier> mods]
@init {
    Atom a = null;
    String s = null;
    boolean u = false;
    Type.Spec res = null;
}
    :
    	{ Token tok = input.LT(1); a = new Atom(tok, file); }
    (
        q=qualName  
        {
            res = new Type.Spec(new Ref(null, q), null, false, mods);
        }


    // standard C keywords

    |
        'void'                              { a.text = "Void"; }
    |
        'char'                              { a.text = "Char"; s = Type.CHAR; }
    |
        'unsigned' 'char'                   { a.text = "UChar"; s = Type.CHAR; u = true; }
    |
        'short'                             { a.text = "Short"; s = Type.SHORT; }
    |
        'unsigned' 'short'                  { a.text = "UShort"; s = Type.SHORT; u = true; }
    |
        'int'                               { a.text = "Int"; s = Type.INT; }
    |
        'unsigned'                          { a.text = "UInt"; s = Type.INT; u = true; }
    |
        'unsigned' 'int'                    { a.text = "UInt"; s = Type.INT; u = true; }
    |
        'long'                              { a.text = "Long"; s = Type.LONG; }
    |
        'unsigned' 'long'                   { a.text = "ULong"; s = Type.LONG; u = true; }
    |
        'long' 'long'                       { a.text = "LLong"; s = Type.LONGLONG; }
    |
        'unsigned' 'long' 'long'            { a.text = "ULongLong"; s = Type.LONGLONG; u = true; }
    |
        'float'                             { a.text = "Float"; s = Type.FLOAT; }
    |
        'double'                            { a.text = "Double"; s = Type.DOUBLE; }
    |
        'long' 'double'                     { a.text = "LDouble"; s = Type.LONGDOUBLE; }
    |
        'size_t'                            { a.text = "SizeT"; s = Type.SIZE; u = true; }
    |
        'va_list'                           { a.text = "VaList"; s = Type.PTR; u = true; }


    // TitleCase C keywords

    |
        'Void'
    |
        'Char'                              { s = Type.CHAR; }
    |
        'UChar'                             { s = Type.CHAR; u = true; }
    |
        'Short'                             { s = Type.SHORT; }
    |
        'UShort'                            { s = Type.SHORT; u = true; }
    |
        'Int'                               { s = Type.INT; }
    |
        'UInt'                              { s = Type.INT; u = true; }
    |
        'Long'                              { s = Type.LONG; }
    |
        'ULong'                             { s = Type.LONG; u = true; }
    |
        'LLong'                             { s = Type.LONGLONG; }
    |
        'ULLong'                            { s = Type.LONGLONG; u = true; }
    |
        'Float'                             { s = Type.FLOAT; }
    |
        'Double'                            { s = Type.DOUBLE; }
    |
        'LDouble'                           { s = Type.LONGDOUBLE; }
    |
        'SizeT'                             { s = Type.SIZE; u = true; }
    |
        'VaList'                            { s = Type.PTR; u = true; }


    // extended types

    |
        'Any'
    |
        'IArg'                              { s = Type.ARG;}
    |
        'UArg'                              { s = Type.ARG; u = true; }
    |
        'Bits8'                             { s = Type.INT8; u = true; }
    |
        'Bits16'                            { s = Type.INT16; u = true; }
    |
        'Bits32'                            { s = Type.INT32; u = true; }
    |
        'Bits64'                            { s = Type.INT64; u = true; }
    |
        'Bool'                              { s = Type.BOOL; u = true; }
    |
        'Fxn'                               { s = Type.FXN; u = true; }
    |
        'Int8'                              { s = Type.INT8; }
    |
        'Int16'                             { s = Type.INT16; }
    |
        'Int32'                             { s = Type.INT32; }
    |
        'Int64'                             { s = Type.INT64; }
    |
        'Ptr'                               { s = Type.PTR; u = true; }
    |
        'String'                            { s = Type.PTR; u = true; }
    |
        'StringC'                           { s = Type.PTR; u = true; }
    |
        'UInt8'                             { s = Type.INT8; u = true; }
    |
        'UInt16'                            { s = Type.INT16; u = true; }
    |
        'UInt32'                            { s = Type.INT32; u = true; }
    |
        'UInt64'                            { s = Type.INT64; u = true; }
        
    // deprecated lower-case types

    |
        'any'                               { a.text = "Any"; }
    |
        'bool'                              { a.text = "Bool"; s = Type.SHORT; u = true; }
    |
        'string'                            { a.text = "String"; s = Type.PTR; u = true; }
    )
        {
            if (res == null) {
                res = new Type.Spec(new Ref(Ref.GBL, a), s, u, mods);
            }
            curTS = res;
        }
    ;

unitBody [Unit unit, boolean local]
@init {
	ArrayList<Impl.Assign> aL = new ArrayList();
	ArrayList<Impl.Body> bL = new ArrayList();
}
	:
		// TODO conditional parse on local
		
       	{ inFxnDef = true; }

       	(a=cfgAssign { aL.add(a); })*
       	{ if (local) unit.bindCfgAssigns(aL); }

		(c=NATIVE_CODE { unit.bindCodeFrag(c.getText()); } )?

       	(f=fxnImpl { bL.add(f); })*
       	{ if (local) unit.bindFxnBodies(bL); }
	;	

unitImp [List<Import> iL]
@init {
    String alias = null;
}
    :
        'import' a=qualNameStar ('as' i=IDENT { alias = $i.text; })? SEMI
        {
            iL.add(new Import(a, alias));
        }
    ;

unitSpec [String pname, boolean local] returns [Unit ret]
@init {
	ArrayList<Decl> dL = new ArrayList();
	ArrayList<Impl> fL = new ArrayList();
	ArrayList<Import> iL = new ArrayList();
	EnumSet<Qual> qS = EnumSet.noneOf(Qual.class);

	curDL = dL;
	ret = null;
}
	:
        ('package' a=qualName (SEMI)?
            {
                if (!a.getText().equals(pname)) {
                    ses.msg.error(a, "incorrect package name");
                }
            }
        )?

		(unitImp[iL])*

        (
            docL=one_doc_block[docL] {
                if (attL != null) {
                    reportError("XDOC comment must come before attributes");
                }
            } |
            attL=annotAttr[attL]
        )*
        ('metaonly' { qS.add(Qual.METAONLY); curMeta = true; })?
        ('module' { qS.add(Qual.MODULE); } | 'interface' { qS.add(Qual.INTERFACE); })
        i=IDENT 
            ('inherits' iSup=superInt)? ('delegates' dSup=superInt)?
        LCUR 
        	(d=modDeclSpec { dL.add(d); })*
        ('instance' COLON
        	{ qS.add(Qual.INSTANCE); } 
        	(d=instDeclSpec { dL.add(d); })*
        )?
        ('internal' COLON
        	{ if (qS.contains(Qual.INTERFACE)) ses.msg.error("internal section not allowed"); }
        	{ if (qS.contains(Qual.METAONLY)) ses.msg.error("internal section not allowed"); }
        	(d=internDeclSpec { dL.add(d); })*
        )?    
        RCUR (SEMI)?
        docL=doc_block[docL]
        {
            ret = new Unit(new Atom(i, file), pname, qS, dL, iL, iSup, dSup);
            ret.bindAttrs(attL);
            ret.bindDocs(docL);
        }
        ('implementation' LCUR unitBody[ret, local] RCUR)?
        EOF
	;

// LEXER

WS:
        (' ' | '\t' | '\f' | '\r' | ('\n' { this.nl(); }))
            { $channel=HIDDEN; }
    ;

SL_COMMENT:
        '//' '\n'
            { $channel=HIDDEN; this.nl(); }
    |
        '//' ~('!' | '\n') (~'\n')* '\n'
            { $channel=HIDDEN; this.nl(); }
    |
        '//!' (~'\n')* '\n'
        {
            String s = $text;
            int k = s.endsWith("\r\n") ? 2 : 1;
            this.emit(new Atom(sep ? DOC_INL : DOC_BLK, s.substring(0, s.length() - k), this.getSourceName(), $line));
            this.nl();
        }
    ;

ML_COMMENT:
        '/*' ~('!' | '*') (~'*' | '*' ~'/')* '*/'
            { $channel=HIDDEN; this.nl($text); }
    |
        '/**' ('/' | (~'/' (~'*' | '*' ~'/')* '*/'))
            { $channel=HIDDEN; this.nl($text); }
    |
        '/*!' (~'*' | '*' ~'/')* '*/'
        {
            String s = $text;
            this.emit(new Atom(sep ? DOC_INL : DOC_BLK, s.substring(0, s.length() - 2), this.getSourceName(), $line));
            this.nl($text);
        }
    ;

AMP         : '&' ;
AMPAMP      : '&&' ;
AMPEQ       : '&=' ;
BANG        : '!' ;
BANGBANG    : '!!' ;
BANGEQ      : '!=' ;
BAR         : '|' ;
BARBAR      : '||' ;
BAREQ       : '|=' ;
CAR         : '^' ;
CAREQ       : '^=' ;
COLON       : ':' ;
COLONCOLON  : '::' ;
COLONEQ     : ':=' ;
COMMA       : ','       { sep = true; } ;
DIV         : '/' ;
DIVEQ       : '/=' ;
EQ          : '=' ;
EQEQ        : '==' ;
GT          : '>' ;
GTEQ        : '>=' ;
GTGT        : '>>' ;
GTGTEQ      : '>>=' ;
LARR        : '<-' ;
LBRK        : '[' ;
LCUR        : '{' ;
LPAR        : '(' ;
LT          : '<' ;
LTEQ        : '<=' ;
LTLT        : '<<' ;
LTLTEQ      : '<<=' ;
MIN         : '-' ;
MINEQ       : '-=' ;
MINMIN      : '--' ;
PCT         : '%' ;
PCTEQ       : '%=' ;
PLUS        : '+' ;
PLUSEQ      : '+=' ;
PLUSPLUS    : '++' ;
QUEST       : '?' ;
RBRK        : ']' ;
RCUR        : '}' ;
RPAR        : ')'       { sep = true; } ;
SEMI        : ';'       { sep = true; } ;
STAR        : '*' ;
STAREQ      : '*=' ;
TILDE       : '~' ;

ATTR :
        '@' ID { $type = ATTR; }
    ;
    
IDENT
    :
        ID { $type = IDENT; } ('.' ID {$type = PATH; })* ('.*' {$type = PATHSTAR; })?
    ;

CHAR :
        '\'' (('\\' ~'\n') | ~('\\' | '\'' | '\n'))+ '\''
    ;

STRING:
        '"' (('\\' ~'\n') | ~('\\' | '"' | '\n'))* '"' 
    ;

DOT:
        '...' 						{ $type = ELIPSIS; }
    |
        '.' 						{ $type = DOT; }
    |
        '.' (D)+ (E)? ('l' | 'L')?	{ $type = NUM; }
    ;

NUM:
    (
        ('0x' | '0X') (H)+ (LU)?
    |
        (D)+ (
                (LU)?
            |
                E ('l' | 'L')?
            |
                '.' (D)* (E)? ('l' | 'L')?
        )
    )
    ;

fragment ID:       ('a'..'z'|'A'..'Z'|'_'|'$') ('a'..'z'|'A'..'Z'|'_'|'$'|'0'..'9')*;

fragment D:        '0'..'9' ;
fragment R:        (D)+ ('.' (D)+)? ;
fragment H:        'a'..'f' | 'A'..'F' | '0'..'9' ;
fragment E:        ('E' | 'e') ('+' | '-')? (D)+ ;
fragment LU:       'LU' | 'Lu' | 'lU' | 'lu' | 'UL' | 'uL' | 'Ul' | 'ul' | 'l' | 'u' | 'L' | 'U';


NATIVE_CODE: 
		'native' WS* CODE_BLOB WS*
	;

fragment CODE_BLOB: '{' ( CODE_BLOB | ~('{' |'}' ) )* '}' ;


CODE:
    	'^^' ( ~'^' | '^' ~'^')* '^^'
        {
            String s = $text;
            this.emit(new Atom(CODE_FRAG, s.substring(2, s.length() - 2), this.getSourceName(), $line));
        	$type = CODE_FRAG;
        }
    ;


fragment CODE_FRAG: ;

fragment DOC_BLK:	;
fragment DOC_INL:	;
fragment DOC_PRE:	;
fragment DOC_SUF:	;
fragment ELIPSIS:	;
fragment OPER:		;
fragment PATH:		;
fragment PATHSTAR:	;


