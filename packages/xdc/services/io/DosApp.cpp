/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== DosApp ========
 */
#include "DosDll.h"
#include <stdio.h>
#include <string.h>

const char usage[] = "\
usage: path2dos [-v] \"path[;path;...]\"\n\
\n\
Converts a semicolon-separated list of one or more Windows\n\
directories into their short (8.3) pathname equivalents.\n\
Remove spaces from the pathnames. Consecutive semicolons are\n\
removed from the list.\n\
";

/*
 *  ======== main ========
 */
int main(int argc, char* argv[])
{
    /* decode verbose flag */
    if (argc > 1 && strcmp(argv[1], "-v") == 0) {

        /* if verbose, echo the arguments to stederr */
        for (int i = 0; i < argc; i++) {
            char separator = (i==argc-1)? '\n' : ' ';
            fprintf(stderr, "\"%s\"%c", argv[i], separator);
        }

        /* skip the -v argument */
        argc--;
        argv++;
    }

    /* check usage */
    if (argc != 2) {
        fprintf(stderr, usage);
        return 1;
    }

    /* convert the one argument */
    LPCTSTR noSpacePath = (LPCTSTR)xdc_services_io_Dos_getDOSPath(argv[1]);
    printf("%s\n", noSpacePath);

    /* successfully removed all spaces? */
    int noSpaces = strchr(noSpacePath, ' ') == NULL;

    return (noSpaces? 0: 1);
}

