/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;
import java.util.*;
import java.lang.reflect.*;

public class OpTab extends Hashtable
{
    OpTab( Class cls, String[] ops )
    {
        Method[] mets;
        
        for (int i = 0; i < ops.length; i++) {
            String[] opdesc = ops[i].split(":");
            mets = Global.findMethods(cls, opdesc[0]);
            if (Global.getTopScope() != null) {
                this.put(opdesc[1],
                    new FunctionObject(opdesc[1], mets[0],
                        Global.getTopScope()));
            }
            else {
                System.err.println(
                    "Warning: OpTab creted before Global.setTopScope called");
            }
        }
    }
}
